-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 07, 2019 at 06:47 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.3.6-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insta`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL,
  `job_hashkey` varchar(20) NOT NULL,
  `is_orginal` int(11) NOT NULL,
  `job_cat` varchar(255) NOT NULL,
  `job_lang_id` varchar(20) NOT NULL,
  `job_title` varchar(90) NOT NULL,
  `job_start` date DEFAULT NULL,
  `job_end` date DEFAULT NULL,
  `job_company` varchar(255) NOT NULL,
  `job_y_exp` varchar(20) NOT NULL,
  `job_desc` text NOT NULL,
  `job_sallary` varchar(20) NOT NULL,
  `job_email_cv` varchar(90) NOT NULL,
  `job_links` text NOT NULL,
  `job_type` varchar(255) NOT NULL,
  `job_location` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `job_hashkey`, `is_orginal`, `job_cat`, `job_lang_id`, `job_title`, `job_start`, `job_end`, `job_company`, `job_y_exp`, `job_desc`, `job_sallary`, `job_email_cv`, `job_links`, `job_type`, `job_location`) VALUES
(5, '', 0, '', '', 'PHP developer', '2019-12-07', '0000-00-00', 'sadeem', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dol', '100', '', '', 'Full Time', 'cairo'),
(6, '', 0, '', '', 'PHP developer', '2019-12-07', '0000-00-00', 'sadeem', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dol', '100', '', '', 'Full Time', 'cairo'),
(7, '', 0, '', '', 'PHP developer', '2019-12-07', '0000-00-00', 'sadeem', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dol', '100', '', '', 'Full Time', 'cairo'),
(8, '', 0, '', '', 'PHP developer', '2019-12-07', '0000-00-00', 'sadeem', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dol', '100', '', '', 'Full Time', 'cairo'),
(9, '', 0, '', '', 'PHP developer', '2019-12-07', '0000-00-00', 'sadeem', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dol', '100', '', '', 'Full Time', 'cairo'),
(10, '', 0, '', '', 'PHP developer', '2019-12-07', '0000-00-00', 'sadeem', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dol', '100', '', '', 'Full Time', 'cairo'),
(11, '', 0, '', '', 'PHP developer', '2019-12-07', '2020-01-07', 'sadeem', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dol', '100', '', '', 'Full Time', 'cairo'),
(12, '', 0, '', '', 'PHP developer2', '2019-12-07', '2020-01-07', 'sadeem', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dol', '100', '', '', 'Part Time', 'cairo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
