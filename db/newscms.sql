-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2020 at 09:58 PM
-- Server version: 5.6.21
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `newscms`
--

-- --------------------------------------------------------

--
-- Table structure for table `aac_login`
--

CREATE TABLE IF NOT EXISTS `aac_login` (
`userkey` bigint(255) NOT NULL,
  `username` varchar(200) NOT NULL,
  `activationcode` varchar(255) NOT NULL,
  `userpassword` varchar(200) NOT NULL,
  `stage` int(11) NOT NULL,
  `packedg_id` varchar(200) DEFAULT NULL,
  `fullName` text,
  `ComanyName` varchar(200) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `userpath` varchar(255) DEFAULT NULL,
  `companylogo` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `databasename` varchar(200) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `lastip` varchar(100) NOT NULL,
  `accesstime` bigint(20) DEFAULT NULL,
  `activetime` bigint(20) DEFAULT NULL,
  `isonline` int(2) DEFAULT NULL,
  `safeIp` varchar(200) DEFAULT NULL,
  `absTime` bigint(250) DEFAULT NULL,
  `validUntile` bigint(250) DEFAULT NULL,
  `createTime` varchar(90) DEFAULT NULL,
  `domain` varchar(200) DEFAULT NULL,
  `MasterSerial` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aac_login`
--

INSERT INTO `aac_login` (`userkey`, `username`, `activationcode`, `userpassword`, `stage`, `packedg_id`, `fullName`, `ComanyName`, `phone`, `userpath`, `companylogo`, `mobile`, `email`, `databasename`, `token`, `lastip`, `accesstime`, `activetime`, `isonline`, `safeIp`, `absTime`, `validUntile`, `createTime`, `domain`, `MasterSerial`) VALUES
(10, 'qsystic', '36bcc282ddf68594e7bb451a2680daaf', 'c4ca4238a0b923820dcc509a6f75849b', 1, '5', NULL, NULL, NULL, NULL, NULL, NULL, 'm.elqrwash@gmail.com', 'bse_1_qsystic', '90a53d8467a7e7141c33e8b1aa076202f', '::1', NULL, NULL, 1, NULL, NULL, 1436738400, NULL, NULL, NULL),
(15, 'm.elqrwash@gmail.com', '', '1c21d9b19298645fa2534d9828ee0778', 0, NULL, 'Mohammed alaaeldin', NULL, '01021318353', NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE IF NOT EXISTS `about` (
`about_id` int(11) NOT NULL,
  `lang_id` varchar(60) NOT NULL,
  `about_hashkey` varchar(90) NOT NULL,
  `about_is_orginal` int(11) NOT NULL,
  `about_title` varchar(90) NOT NULL,
  `about_pragraph` text NOT NULL,
  `about_imge_path` text,
  `about_order` int(100) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`about_id`, `lang_id`, `about_hashkey`, `about_is_orginal`, `about_title`, `about_pragraph`, `about_imge_path`, `about_order`) VALUES
(1, '2', '48_584f1e00e1ed1', 1, 'من نحن', 'نحن شركه مهم جدا في عالم التدريب', '9d0e9eb552bf7508eecb3885885b873b.jpg', 1),
(6, '1', '87_5bcb591ab6b7c', 1, 'Ok', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s. when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', '9cb8d9a968dcb390807fd667b673a84c.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `apply`
--

CREATE TABLE IF NOT EXISTS `apply` (
`d` bigint(255) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `gender` varchar(200) NOT NULL,
  `age` varchar(200) NOT NULL,
  `marital` varchar(200) NOT NULL,
  `college` varchar(200) NOT NULL,
  `degree` varchar(200) NOT NULL,
  `job` varchar(200) NOT NULL,
  `salary` varchar(200) NOT NULL,
  `skills` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `apply`
--

INSERT INTO `apply` (`d`, `fullname`, `phone`, `address`, `gender`, `age`, `marital`, `college`, `degree`, `job`, `salary`, `skills`) VALUES
(1, 'Mohamed alaa ', '8126378687612876', 'address', 'male', '35', 'single', 'none', 'gsayfut ', 'gtu', 'ytuygu', 'guyg'),
(2, 'Mohamed alaa ', '8126378687612876', 'address', 'male', '35', 'single', 'none', 'gsayfut ', 'gtu', 'ytuygu', 'guyg'),
(3, 'glighg', 'jlhgljh', 'gjhg', 'male', '18', 'single', 'h', 'kjhg', 'hjk', 'gjh', 'g'),
(4, 'jhkhkjhkjh', 'kjhkj', 'hkh', 'male', '18', 'single', 'hkj', 'hkjh', 'khk', 'hkh', 'kk'),
(5, 'jhkhkjhkjh', 'kjhkj', 'hkh', 'male', '18', 'single', 'hkj', 'hkjh', 'khk', 'hkh', 'kk'),
(6, 'jhkhkjhkjh', 'kjhkj', 'hkh', 'male', '18', 'single', 'hkj', 'hkjh', 'khk', 'hkh', 'kk'),
(7, 'asdjkhkjsahdkh', 'hkj', 'hkjh', 'male', '18', 'single', 'kjhk', 'jhkjh', 'kjhk', 'jhkjh', 'kjhk');

-- --------------------------------------------------------

--
-- Table structure for table `articls`
--

CREATE TABLE IF NOT EXISTS `articls` (
`articls_id` bigint(255) NOT NULL,
  `articls_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_orginal` int(11) DEFAULT NULL,
  `articls_authr` varchar(90) DEFAULT NULL,
  `articls_lang_id` varchar(25) DEFAULT NULL,
  `articls_hashkey` varchar(25) DEFAULT NULL,
  `articls_title` varchar(120) DEFAULT NULL,
  `articls_description` text,
  `articls_text` longtext,
  `articls_image` text,
  `articls_media_type` varchar(233) DEFAULT NULL,
  `articls_cat_id` bigint(255) NOT NULL,
  `articls_class` varchar(255) NOT NULL,
  `articls_attachment` longblob NOT NULL,
  `articls_link` text
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articls`
--

INSERT INTO `articls` (`articls_id`, `articls_date`, `is_orginal`, `articls_authr`, `articls_lang_id`, `articls_hashkey`, `articls_title`, `articls_description`, `articls_text`, `articls_image`, `articls_media_type`, `articls_cat_id`, `articls_class`, `articls_attachment`, `articls_link`) VALUES
(1, '2020-07-12 20:14:55', 1, 'Mohamed ALaa', '1', '1_5f0b6e7355095', 'Coronavirus: Florida sets new state daily case record of 15,299', '<p><b>Florida has registered a state record of 15,299 new coronavirus cases in 24 hours - around a quarter of all of the United States'' daily infections.</b><br></p>', '<p></p><p>The state, with just 7% of the US population, surpassed the previous daily record held by California. </p><p>Florida, which began lifting coronavirus restrictions in May, has proved vulnerable due to tourism and an elderly population. </p><p>Its figures eclipse the worst daily rates seen in New York in April. </p><p>Florida also registered an additional 45 deaths. </p><p>The state would rank fourth in the world for new cases if it were a country, according to a Reuters analysis. More than 40 hospitals in Florida say their intensive care facilities are at full capacity. </p><img width="976" alt="Hospital in Florida" src="https://ichef.bbci.co.uk/news/624/cpsprodpb/5DA6/production/_113347932_mediaitem113347931.jpg" height="549">Image copyrightEPAImage captionIntensive care units at many Florida hospitals are reaching capacity<ul><li><a target="_blank" rel="nofollow" href="https://www.bbc.com/news/world-us-canada-53252483">Living in Florida and Texas as virus cases surge</a></li><li><a target="_blank" rel="nofollow" href="https://www.bbc.com/news/health-51665497">How close to developing a vaccine are we?</a></li><li><a target="_blank" rel="nofollow" href="https://www.bbc.com/future/article/20200504-coronavirus-what-is-the-best-kind-of-face-mask">Why we should all be wearing face masks</a></li><li><a target="_blank" rel="nofollow" href="https://www.bbc.com/news/world-51235105">Where are the world''s coronavirus hotspots?</a></li></ul><p>The latest figures were released a day after Walt Disney World in Orlando, Florida reopened, but with safety measures including mask-wearing and widespread use of sanitiser.</p><p>The caseload in Florida has continued to rise despite Republican Governor Ron DeSantis ordering some bars to close again last month. </p><p>The top adviser on the White House coronavirus taskforce, Dr Anthony Fauci, had criticised lockdown easing in the state, saying the data on infections did not support the move. Mr DeSantis has also declined to make mask-wearing obligatory. </p><img width="2666" alt="Chart showing daily cases and deaths in the US 12 July" src="https://ichef.bbci.co.uk/news/624/cpsprodpb/15594/production/_113344478_us_cases_deaths12jul-nc.png" height="1875"><p>The issue of masks has become highly politicised in the United States, with opponents saying having to wear them encroaches on personal freedom. There have been demonstrations against masks and other coronavirus measures in several states.</p><p>But on Saturday, President Donald Trump appeared wearing a mask in the public for the first time after previously casting doubt on their usefulness. He was visiting the Walter Reed military hospital outside Washington, where he met wounded soldiers and health care workers.</p><p>"I''ve never been against masks but I do believe they have a time and a place," he said as he left the White House.</p><p>The United States overall has been exceeding new daily totals of 60,000 cases for the past few days. Other states including Arizona, California and Texas continue to see a rising cases. </p><p>Since the pandemic hit the US, more than 134,000 people there have died with Covid-19.</p><br><p></p>', '5e2a987a0da41ca402803401ec4afae6.jpg', 'text', 1, 'NORMAL', 0x3c703e266c743b696672616d652077696474683d2235363022206865696768743d2233313522207372633d223c61207461726765743d225f626c616e6b222072656c3d226e6f666f6c6c6f772220687265663d2268747470733a2f2f7777772e796f75747562652e636f6d2f656d6265642f6b70617257454875784a552671756f743b223e68747470733a2f2f7777772e796f75747562652e636f6d2f656d6265642f6b70617257454875784a55223c2f613e3b206672616d65626f726465723d22302220616c6c6f773d22616363656c65726f6d657465723b206175746f706c61793b20656e637279707465642d6d656469613b206779726f73636f70653b20706963747572652d696e2d706963747572652220616c6c6f7766756c6c73637265656e2667743b266c743b2f696672616d652667743b3c62723e3c2f703e, NULL),
(2, '2020-07-12 20:20:30', 1, 'Mohamed Salah', '1', '37_5f0b6ff829e23', 'Mali opposition rejects President Keïta', '<p><b>Opposition leaders in Mali have called for President Ibrahim Boubacar Keïta to resign, after they rejected concessions intended to quell growing unrest.</b><br></p>', '<p></p><p>At least four people were killed during street protests on Friday, and there were further clashes on Saturday.</p><p>It prompted the president to dissolve a top court which has been at the centre of the controversy after it overturned provisional election results in March.</p><p>But a coalition of opposition leaders rejected his proposal shortly after.</p><p>Nouhoum Togo, a spokesman for the M5-RFP group of religious and political leaders which organised recent protests, said "we are not going to accept this nonsense".</p><p>"We demand his resignation, plain and simple," he told Reuters news agency on Sunday.</p><br><p></p>', 'a467f6bbeb9e808e1b38b8d7d5379276.jpg', 'video', 1, 'NORMAL', 0x3c703e68747470733a2f2f7777772e796f75747562652e636f6d2f656d6265642f686367546b38524461656f22266c743b2f612667743b3b206672616d65626f726465723d22302220616c6c6f773d22616363656c65726f6d657465723b206175746f706c61793b20656e637279707465642d6d656469613b206779726f73636f70653b20706963747572652d696e2d706963747572652220616c6c6f7766756c6c73637265656e2667743b3c62723e3c2f703e, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`category_id` bigint(255) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_disc` longtext,
  `category_img` varchar(255) DEFAULT NULL,
  `category_lang_id` int(11) DEFAULT '1',
  `category_score` bigint(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `category_disc`, `category_img`, `category_lang_id`, `category_score`) VALUES
(1, 'News', '<p>new section</p>', 'ecc85b6ffb389bdc16c87bc35294983f.jpg', 1, 0),
(2, 'Sport', '<p>Sport section<br></p>', '105d56c9979792c7ec1c59b42800d61b.jpg', 1, 0),
(3, 'Lifestyle', '<p>Lifestyle section<br></p>', '45113875aca6ecb43d5b0178ae283595.jpg', 1, 0),
(4, 'Fashion', '<p><a target="_blank" rel="nofollow"></a>Fashion section<br></p>', '72d155bb47c5028d5f5c43666533bebd.jpg', 1, 0),
(5, 'Music', '<p>Music section<br></p>', '68918c87285e9345b82c918615a4368c.jpg', 1, 0),
(6, 'Business', '<p>Business section<br></p>', 'a4fb758097521ae399c9808c35672a99.jpg', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `citys`
--

CREATE TABLE IF NOT EXISTS `citys` (
`citys_id` int(11) NOT NULL,
  `citys_county` varchar(200) DEFAULT NULL,
  `citys_name` varchar(200) NOT NULL,
  `citys_name_ar` varchar(255) DEFAULT NULL,
  `citys_lang` double DEFAULT NULL,
  `citys_lat` double DEFAULT NULL,
  `img_path` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `citys`
--

INSERT INTO `citys` (`citys_id`, `citys_county`, `citys_name`, `citys_name_ar`, `citys_lang`, `citys_lat`, `img_path`) VALUES
(1, NULL, 'Istanbul', 'اتطانبول', 28.964722, 41.018611, '9d0e9eb552bf7508eecb3885885b873b.jpg'),
(2, NULL, 'Casablanca', 'الدار البيضاء', -7.619157, 33.592779, '21c23f9dabe26554e7683777e5b2aa5e.jpg'),
(3, NULL, 'Bratislava', 'براتيسلافا', 17.1166667, 48.15, 'b623341152b2409dcd47d817f7b431dc.jpg'),
(4, NULL, 'Prague', 'براغ', 14.466667, 50.083333, '41b3f4be9e130d7aca67ddceffd4c31e.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
`contacts_id` int(11) NOT NULL,
  `contacts_hashkey` varchar(25) NOT NULL,
  `is_orginal` int(11) NOT NULL,
  `contacts_lang_id` varchar(25) NOT NULL,
  `contacts_title` varchar(120) NOT NULL,
  `contacts_pragraph` text NOT NULL,
  `contacts_phone` varchar(100) NOT NULL,
  `contacts_email` varchar(90) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`contacts_id`, `contacts_hashkey`, `is_orginal`, `contacts_lang_id`, `contacts_title`, `contacts_pragraph`, `contacts_phone`, `contacts_email`) VALUES
(1, '20_584f1f32b2f7b', 1, '1', 'Head office', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s. when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. but also the leap into electronic typesetting. remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1122121221', 'mcom@kkks.com'),
(2, '76_587e52a988c5a', 1, '1', 'branch 1', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s. when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. but also the leap into electronic typesetting. remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2193787391287389', 'mail.com'),
(3, '50_587e52b8129ab', 1, '1', 'branch 2', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s. when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries. but also the leap into electronic typesetting. remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '27364621467120376', 'mail2.com');

-- --------------------------------------------------------

--
-- Table structure for table `decoments`
--

CREATE TABLE IF NOT EXISTS `decoments` (
`decoments_id` bigint(255) NOT NULL,
  `decoments_name` varchar(255) NOT NULL,
  `decoments_lang` int(2) NOT NULL,
  `decoments_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `details_groub`
--

CREATE TABLE IF NOT EXISTS `details_groub` (
`details_groub_id` int(11) NOT NULL,
  `details_groub_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `featuresmanager`
--

CREATE TABLE IF NOT EXISTS `featuresmanager` (
`features_id` bigint(255) NOT NULL,
  `features_item_id` bigint(255) NOT NULL,
  `features_name` varchar(100) NOT NULL,
  `features_value` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `featuresmanager`
--

INSERT INTO `featuresmanager` (`features_id`, `features_item_id`, `features_name`, `features_value`) VALUES
(8, 3, 'Dog 3 F1', '<p>Dog 3 F1<br></p>'),
(9, 2, 'Dog 2 Ferurr1', '<p>Fetur 1</p>'),
(13, 6, 'Size', '<p>Medium, with males weighing 10 to 14 pounds and females weighing 6 to 10 pounds<br></p>'),
(11, 6, 'Coat', '<p></p><div>Short to medium</div><p></p>'),
(12, 6, 'Color', '<p>Silver, bronze and smoke<br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
`pic_id` int(11) NOT NULL,
  `pic_server_abslout` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`pic_id`, `pic_server_abslout`) VALUES
(4, 'bef959fa9607b1bc1db84fa00f2217dc.jpg'),
(6, '4367caed71f6050d07700b1a195f9c80.jpg'),
(12, 'b7179a4773305a17d63f3b23759faffd.jpg'),
(15, 'f397b745ae7eddedfb5a71efc91d6bc8.jpg'),
(16, '21c23f9dabe26554e7683777e5b2aa5e.jpg'),
(44, '2306761f7cbdf8342adaa36394414f3c.jpg'),
(45, '745d70c0ec61e6e8a2f344603083f23a.jpg'),
(46, 'e772d80754870773638d4cbfab36e004.jpg'),
(47, '9cb8d9a968dcb390807fd667b673a84c.jpg'),
(51, '32b0614fa50761ed26838b5d1b517cbe.jpg'),
(60, 'b18bf934aee42a75859676180f00c192.jpg'),
(72, 'training_center_1400696644.jpg'),
(75, 'b623341152b2409dcd47d817f7b431dc.jpg'),
(76, '47321edd2071a3f615ef321410547d11.jpg'),
(77, '00e72b03ac784d52494e5546f08c067c.jpg'),
(79, 'e14399902af864d9d3ae6a8b3d800f56.jpg'),
(80, '41b3f4be9e130d7aca67ddceffd4c31e.jpg'),
(81, 'a894c2c5501113edb3c85fea3550b1e0.jpg'),
(82, '9d0e9eb552bf7508eecb3885885b873b.jpg'),
(83, '2cd94243559d7a6be360b0e987c51870.jpg'),
(84, 'ecc85b6ffb389bdc16c87bc35294983f.jpg'),
(87, '45113875aca6ecb43d5b0178ae283595.jpg'),
(88, '72d155bb47c5028d5f5c43666533bebd.jpg'),
(89, '68918c87285e9345b82c918615a4368c.jpg'),
(90, '105d56c9979792c7ec1c59b42800d61b.jpg'),
(91, 'a4fb758097521ae399c9808c35672a99.jpg'),
(92, '3b8938a2c18811ca9d322abc0e53bd2d.jpg'),
(93, 'd630f3b5931bb2b2c139941261f07615.jpg'),
(94, '5e2a987a0da41ca402803401ec4afae6.jpg'),
(95, 'a467f6bbeb9e808e1b38b8d7d5379276.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `footerlinks`
--

CREATE TABLE IF NOT EXISTS `footerlinks` (
`footerlinks_id` int(255) NOT NULL,
  `footerlinks_sec_id` int(255) NOT NULL,
  `footerlinks_text` varchar(255) NOT NULL,
  `footerlinks_url` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `footerlinks`
--

INSERT INTO `footerlinks` (`footerlinks_id`, `footerlinks_sec_id`, `footerlinks_text`, `footerlinks_url`) VALUES
(1, 1, 'Link 1', 'http://link.com'),
(3, 1, 'All about me', 'http://link.com'),
(4, 6, 'Link 2', 'http://link.com'),
(5, 6, 'Link 3', 'http://link.com'),
(6, 6, 'Link 4', 'http://link.com'),
(7, 7, 'Link 3 test', 'http//.dhsgsyu');

-- --------------------------------------------------------

--
-- Table structure for table `footersection`
--

CREATE TABLE IF NOT EXISTS `footersection` (
`footersection_id` int(255) NOT NULL,
  `footersection_langid` int(11) NOT NULL,
  `footersection_text` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `footersection`
--

INSERT INTO `footersection` (`footersection_id`, `footersection_langid`, `footersection_text`) VALUES
(1, 1, 'About Us'),
(2, 2, 'حول الشركه'),
(6, 1, 'About Us 2'),
(7, 1, 'Link 3');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
`items_id` bigint(255) NOT NULL,
  `items_name` varchar(100) NOT NULL,
  `items_cat_id` bigint(255) NOT NULL,
  `items_description` longblob NOT NULL,
  `items_date_add` date DEFAULT NULL,
  `items_add_by` bigint(255) NOT NULL,
  `items_img` text NOT NULL,
  `items_lang` varchar(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`items_id`, `items_name`, `items_cat_id`, `items_description`, `items_date_add`, `items_add_by`, `items_img`, `items_lang`) VALUES
(6, 'Egyptian Mau Cat', 6, 0x3c703e3c2f703e3c703e54686520456779707469616e204d6175206973206669657263656c79206465766f74656420746f206865722068756d616e7320616e6420766f63616c6c792073686f7773207369676e73206f662068617070696e65737320616e6420616666656374696f6e206279206d656f77696e6720696e206120706c656173616e7420766f6963652e20536865e280996c6c20616c736f20736c6f776c7920737769736820686572207461696c20616e64206b6e6561642077697468206865722066726f6e7420706177732e20536865206c6f76657320746f20646973706c6179206865722068756e74696e6720736b696c6c732062792063686173696e6720616e642072657472696576696e67206120746f792e2041732061206d6f6465726174652d20746f20686967686c79206163746976652062726565642c20796f75206d61792066696e6420686572206f6e20746f70206f6620796f757220726566726967657261746f72206f7220626f6f6b7368656c7665732e3c2f703e3c703e456779707469616e204d61757320616c736f206c6f766520706c6179696e67207769746820776174657220616e642061726520736d61727420656e6f75676820746f206c6561726e20686f7720746f207475726e206f6e20746865206661756365742e204465737069746520686572206869676820656e65726779206c6576656c732c207368652061646f726573206375726c696e6720757020696e20796f7572206c617020666f72206120736e7567676c652073657373696f6e2e20536865e2809973206772656174207769746820706c617966756c206368696c6472656e20616e64206f74686572206361742d667269656e646c7920706574732077686f2063616e206b6565702075702077697468206865722061637469766520616e6420656e65726765746963206c6966657374796c652c2074686f75676820726573657276656420616e6420776172792061726f756e6420756e66616d696c696172206775657374732e3c2f703e3c62723e3c703e3c2f703e, '2019-12-07', 0, '3b8938a2c18811ca9d322abc0e53bd2d.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
`job_id` int(11) NOT NULL,
  `job_hashkey` varchar(20) NOT NULL,
  `is_orginal` int(11) NOT NULL,
  `job_lang_id` varchar(20) NOT NULL,
  `job_title` varchar(90) NOT NULL,
  `job_y_exp` varchar(20) NOT NULL,
  `job_desc` text NOT NULL,
  `job_sallary` varchar(20) NOT NULL,
  `job_email_cv` varchar(90) NOT NULL,
  `job_links` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `key_setting`
--

CREATE TABLE IF NOT EXISTS `key_setting` (
`id` int(255) NOT NULL,
  `key` text NOT NULL,
  `val` text NOT NULL,
  `lang_id` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `key_setting`
--

INSERT INTO `key_setting` (`id`, `key`, `val`, `lang_id`) VALUES
(1, 'WELCCOM', 'Trianig and Development', 1),
(2, 'ABOUTUS', 'About us', 1),
(3, 'UpcomingTrainingCourses', 'Upcoming Training Courses', 1),
(4, 'WhoweAre', 'Who we Are', 1),
(5, 'Cities', 'Cities', 1),
(6, 'TrainingCourses', 'Training Courses', 1),
(7, 'Category', 'Category', 1),
(8, 'TrainingCategories', 'Training Categories', 1),
(9, 'CityLocation', 'City & Location', 1),
(10, 'contactinfo', 'contact info', 1),
(12, 'Contactus', 'Contact us', 1),
(13, 'RightsReserved', 'Investcenter. All Rights Reserved.', 1),
(14, 'PHONE', '+1 8736 7763', 1),
(15, 'Email', 'info@test.com', 1),
(16, 'ADDRESS', '5 streen london , 89122 CI', 1),
(17, 'Category', 'Category', 1),
(18, 'City', 'City', 1),
(19, 'StartDate', 'Start Date', 1),
(20, 'articles', 'Articles', 1),
(21, 'HOME', 'Home', 1),
(22, 'SEARCH', 'Search', 1),
(23, 'ABOUTUSWORD', 'We are a leading Agancy in Training for Personal and Bussness needs We are a leading Agancy in Training for Personal and Bussness needs We are a leading Agancy in Training for Personal and Bussness needs', 1),
(24, 'WELCCOM', 'تعليم تدريب تطوير', 2),
(25, 'ABOUTUS', 'من نحن', 2),
(26, 'UpcomingTrainingCourses', 'الكورسات القادمه', 2),
(27, 'WhoweAre', 'أي نحن وماذا مثل', 2),
(28, 'Cities', 'المدن', 2),
(29, 'TrainingCourses', 'الكورسات ، والتدريب', 2),
(30, 'Category', 'التخصصات', 2),
(31, 'TrainingCategories', 'قطاعات التدريب', 2),
(32, 'CityLocation', 'المدن - الاماكن', 2),
(33, 'contactinfo', 'معلومات الاتصال', 2),
(34, 'Contactus', 'إتصل بنا', 2),
(35, 'RightsReserved', 'جميع الحقوق محفوظه ل انفست تريننج سنتر', 2),
(36, 'PHONE', '٠١٨٨٢٨٨٣٧٧١', 2),
(37, 'Email', 'info@test.com', 2),
(38, 'ADDRESS', 'العنوان الخاص بالمركز الرئيس ـ في المدينه - في الدوله رقم بريدي ٩٩٨٣٧', 2),
(39, 'Category', 'تخصصات', 2),
(40, 'City', 'مدينه', 2),
(41, 'StartDate', 'تاريخ البد', 2),
(42, 'articles', 'مقالات', 2),
(43, 'HOME', 'الرئيسيه', 2),
(44, 'SEARCH', 'بحث', 2),
(45, 'ABOUTUSWORD', 'نحن شركه عريقه في مجال التدريب دربنا الاف حول العالم - قدمنا احدث برامج التدريب الموافقه مع متطلبات القياده الحديثه', 2),
(46, 'Headwelcome', 'Head''s welcome', 1),
(47, 'Headwelcome', 'المدير التنفيذي', 2),
(48, 'Headwelcomebody', 'Every day I am impressed by the variety of efforts to stimulate the heart, body and soul, to make you a better person in general', 1),
(49, 'Headwelcomebody', 'كلمه المدير التنفيذي - مرحبا بكم', 2),
(50, 'EVENT', 'Events', 1),
(51, 'EVENT', 'برامج', 2),
(52, 'about', 'About', 1),
(53, 'about', 'حول', 2),
(54, 'outlines', 'Outlines', 1),
(55, 'outlines', 'العناوين', 2),
(56, 'available_date', 'Available Date', 1),
(57, 'available_date', 'التواريخ والاماكن المتاحه', 2),
(58, 'start', 'Start', 1),
(59, 'end', 'End', 1),
(60, 'city', 'City', 1),
(61, 'start', 'بداء', 2),
(62, 'end', 'انتهاء', 2),
(63, 'city', 'المدينه', 2),
(64, 'send', 'Send', 1),
(65, 'send', 'ارسل', 2),
(66, 'full_name', 'Full name', 1),
(67, 'full_name', 'الاسم كامل', 2),
(68, 'email', 'Email', 1),
(69, 'email', 'بريد اليكتروني', 2),
(70, 'phone', 'Phone', 1),
(71, 'phone', 'رقم الجوال', 2),
(72, 'note', 'Note''s', 1),
(73, 'note', 'ملاحظات', 2),
(74, 'available_dates', 'Available Dates', 1),
(75, 'available_dates', 'التواريخ والاماكن المتاحه', 2),
(76, 'Pages', 'Pages', 1),
(77, 'Pages', 'صفحات', 2),
(78, 'Documents', 'Documents', 1),
(79, 'Documents', 'وثائق', 2);

-- --------------------------------------------------------

--
-- Table structure for table `landpage`
--

CREATE TABLE IF NOT EXISTS `landpage` (
`lp_id` int(200) NOT NULL,
  `lp_t1` text CHARACTER SET utf8 NOT NULL,
  `lp_t2` text CHARACTER SET utf8 NOT NULL,
  `lp_st1` text CHARACTER SET utf8 NOT NULL,
  `lp_p1` text CHARACTER SET utf8 NOT NULL,
  `lp_p2` text CHARACTER SET utf8 NOT NULL,
  `lp_p3` text CHARACTER SET utf8 NOT NULL,
  `lp_p4` text CHARACTER SET utf8 NOT NULL,
  `lp_p5` text CHARACTER SET utf8 NOT NULL,
  `lp_im1` text CHARACTER SET utf8 NOT NULL,
  `lp_st2` text CHARACTER SET utf8 NOT NULL,
  `lp_st3` text CHARACTER SET utf8 NOT NULL,
  `lp_st4` text CHARACTER SET utf8 NOT NULL,
  `lp_st5` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `landpage`
--

INSERT INTO `landpage` (`lp_id`, `lp_t1`, `lp_t2`, `lp_st1`, `lp_p1`, `lp_p2`, `lp_p3`, `lp_p4`, `lp_p5`, `lp_im1`, `lp_st2`, `lp_st3`, `lp_st4`, `lp_st5`) VALUES
(1, 'THINGS YOU SHOULD KNOW               \n            \n                    \n            \n                    \n            \n        ', 'You Should Know', 'Training Methods                         \n                         \n            \n                    \n                         \n                         \n                         \n                         \n             ', 'Invest Center Group relies on a variety of training and facilitation methods and techniques. Used whenever applicable, these methods are aimed at enhancing individual and group interaction while maximizing learning.\n\nSome of these methods are:', '+ Brief presentations by the consultant\n+ Group debriefs\n+ Individual and team exercises (indoors and outdoors)\n+ Behavior modeling and role-playing\n+ One-to-one and group discussions\n+ Case studies, simulations and small projects\n+ Video films, videotaping and playback\n+ Self-analysis questionnaires and learning instruments\n+ Individual action plans (to follow up and evaluate training results)', 'We are delivering our solutions and services in more than 25 cities and metropolitan around the world', 'More than 2000 training programs, workshops, and seminars in different specialization for different industries', 'More than 200 experts, consultants from different areas are ready to design and tailor the solution and service to you', 'http://139.162.173.163/uploads/b7179a4773305a17d63f3b23759faffd.jpg', 'Where', 'Find', 'Experts ', ''),
(2, 'الأساليب التدريبية', 'الأساليب التدريبية', 'الأساليب التدريبية', 'نعتمد في تقديم خدمات تنمية وتطوير الموارد البشرية على باقة متنوعة من الأساليب والتقنيات والأدوات التدريبة وتُستخدم بما يتلاءم مع الموضوع التدريبي ومع مستوى المُتلقي، ويعزز تنوع  استخدامات هذه الباقة التفاعل الفردي والجماعي كما يضمن تحقيق أقصى قدر من توصيل أو تصحيح الأفكار والمفاهيم وتنمية أو إكساب المهارات والقدرات. ومن بعض هذه الأساليب:', 'محاضرات قصيرة مُعدّة من قِبَل متخصصينالمناقشات الجماعية المُنظمةالتمارين الفردية والجماعية (داخلية وخارجية)  نمذجة سلوك وتمثيل الأدوارالمحادثات والأسئلة الفردية والجماعيةدراسة حالات عملية والمحاكاةأفلام الفيديو القصيرةاستبيان التحليل الذاتي وتقييم القدراتخطة العمل والأنشطة الفردية (لمتابعة وتقييم نتائج التدريب)', '                                           exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum Ut enim ad minim veniam, quis nostrud ', ' exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum Ut enim ad minim veniam, quis nostrud ', ' exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum Ut enim ad minim veniam, quis nostrud ', 'http://139.162.173.163/uploads/41abdfbfc3650e6b78c312edc6c95695.png', '???', '???', '?????', '');

-- --------------------------------------------------------

--
-- Table structure for table `languge`
--

CREATE TABLE IF NOT EXISTS `languge` (
`lang_id` int(11) NOT NULL,
  `lang_name` varchar(20) NOT NULL,
  `lang_orintation` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languge`
--

INSERT INTO `languge` (`lang_id`, `lang_name`, `lang_orintation`) VALUES
(1, 'En', 'ltr');

-- --------------------------------------------------------

--
-- Table structure for table `layout`
--

CREATE TABLE IF NOT EXISTS `layout` (
`layout_id` int(11) NOT NULL,
  `layout_css_path` text NOT NULL,
  `layout_name` varchar(25) NOT NULL,
  `layout_oriant` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
`media_id` int(11) NOT NULL,
  `media_url` int(11) NOT NULL,
  `media_lang_id` int(11) NOT NULL,
  `media_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mlist`
--

CREATE TABLE IF NOT EXISTS `mlist` (
`mlist_id` int(11) NOT NULL,
  `mlist_lang_id` varchar(80) NOT NULL,
  `is_orginal` int(2) NOT NULL,
  `mlist_hashkey` varchar(80) NOT NULL,
  `mlist_text` varchar(50) NOT NULL,
  `mlist_link` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `onlineorders`
--

CREATE TABLE IF NOT EXISTS `onlineorders` (
`onlineorders_id` int(11) NOT NULL,
  `onlineorders_name` varchar(255) NOT NULL,
  `onlineorders_email` varchar(255) NOT NULL,
  `onlineorders_phone` varchar(200) NOT NULL,
  `onlineorders_product_name` varchar(255) NOT NULL,
  `onlineorders_msg` text NOT NULL,
  `onlineorders_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `onlineorders_eid` bigint(255) DEFAULT NULL,
  `onlineorders_estart` varchar(255) DEFAULT NULL,
  `onlineorders_eend` varchar(255) DEFAULT NULL,
  `onlineorders_elocation` varchar(255) DEFAULT NULL,
  `onlineorders_jtitle` varchar(255) DEFAULT NULL,
  `onlineorders_cphone` varchar(255) DEFAULT NULL,
  `onlineorders_cfax` varchar(255) DEFAULT NULL,
  `onlineorders_compnayname` varchar(255) DEFAULT NULL,
  `order_type` enum('R','I') DEFAULT NULL,
  `responsible_name` varchar(255) DEFAULT NULL,
  `responsible_phone` varchar(255) DEFAULT NULL,
  `hr_name` varchar(255) DEFAULT NULL,
  `hr_phone` varchar(255) DEFAULT NULL,
  `readed` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `onlineorders`
--

INSERT INTO `onlineorders` (`onlineorders_id`, `onlineorders_name`, `onlineorders_email`, `onlineorders_phone`, `onlineorders_product_name`, `onlineorders_msg`, `onlineorders_time`, `onlineorders_eid`, `onlineorders_estart`, `onlineorders_eend`, `onlineorders_elocation`, `onlineorders_jtitle`, `onlineorders_cphone`, `onlineorders_cfax`, `onlineorders_compnayname`, `order_type`, `responsible_name`, `responsible_phone`, `hr_name`, `hr_phone`, `readed`) VALUES
(1, 'MOHAMMED ALAAELDIN MOHAMED', 'm.elqrwash@gmail.com', '1091204608', 'بناء وقيادة وتطوير فرق العمل وتحقيق الإبداع في العمل', 'dasasd', '2019-07-24 19:22:46', 359032, '2019-07-28', '2019-08-01', 'London', 'dasd', 'asdasd', 'dasd', 'asdasd', 'R', 'asdasd', 'sdadasd', 'asdasd', 'dasdasd', 0),
(2, 'Xman of wn readed Human Kind', 'm.elqrwash@gmail.com', '1091204608', 'Contracts and Contract Claims Management', 'TEXTA', '2019-07-25 14:53:08', 412324, '2019-08-04', '2019-08-08', 'Amman', 'CEO', '92478799', '82953378678345', 'Volat', 'I', NULL, NULL, NULL, NULL, 0),
(3, 'Xman of wn readed Human Kind', 'm.elqrwash@gmail.com', '1091204608', 'Effective Purchasing, Tendering, Negotiating and Supplier Selection', 'TEXTA', '2019-07-25 14:54:57', 423262, '2019-09-15', '2019-09-19', 'Dubai', 'CEO', '92478799', '82953378678345', 'Volat', 'I', NULL, NULL, NULL, NULL, 0),
(4, 'MOHAMMED ALAAELDIN MOHAMED', 'm.elqrwash@gmail.com', '1091204608', 'Contract Administration for Non-Contract Professionals', 'notila', '2019-07-25 14:59:21', 414133, '2019-08-11', '2019-08-15', 'Kuwait', 'IT manager', '0482554455', '468465464684', 'Volar', 'R', 'Hrmiz', '464646464', 'Mozas', '4684646546', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
`pages_id` bigint(255) NOT NULL,
  `pages_title` varchar(255) NOT NULL,
  `pages_meta_k` text NOT NULL,
  `pages_meta_d` text NOT NULL,
  `pages_lang` int(22) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pages_id`, `pages_title`, `pages_meta_k`, `pages_meta_d`, `pages_lang`) VALUES
(1, 'About Our Programs', 'new prpgrams and about us', 'new prpgrams and about us', 1);

-- --------------------------------------------------------

--
-- Table structure for table `paragraphs`
--

CREATE TABLE IF NOT EXISTS `paragraphs` (
`paragraphs_id` bigint(255) NOT NULL,
  `paragraphs_page_id` bigint(255) NOT NULL,
  `paragraphs_title` varchar(255) DEFAULT NULL,
  `paragraphs_class` varchar(255) NOT NULL,
  `paragraphs_text` longtext NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paragraphs`
--

INSERT INTO `paragraphs` (`paragraphs_id`, `paragraphs_page_id`, `paragraphs_title`, `paragraphs_class`, `paragraphs_text`) VALUES
(1, 1, NULL, '1', '<p><b>dsfsdafsdafsafd</b></p>');

-- --------------------------------------------------------

--
-- Table structure for table `searchbox`
--

CREATE TABLE IF NOT EXISTS `searchbox` (
`searchbox_id` int(100) NOT NULL,
  `searchbox_lang` varchar(100) NOT NULL,
  `searchbox_title` varchar(255) NOT NULL,
  `searchbox_bg` varchar(255) NOT NULL,
  `searchbox_p1_i` varchar(255) NOT NULL,
  `searchbox_p1_t` varchar(255) NOT NULL,
  `searchbox_p2_i` varchar(255) NOT NULL,
  `searchbox_p2_t` varchar(255) NOT NULL,
  `searchbox_p3_i` varchar(255) NOT NULL,
  `searchbox_p3_t` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `searchbox`
--

INSERT INTO `searchbox` (`searchbox_id`, `searchbox_lang`, `searchbox_title`, `searchbox_bg`, `searchbox_p1_i`, `searchbox_p1_t`, `searchbox_p2_i`, `searchbox_p2_t`, `searchbox_p3_i`, `searchbox_p3_t`) VALUES
(1, '1', 'Welcome to InstaBloom', 'd630f3b5931bb2b2c139941261f07615.jpg', 'c.png', 'you next Pit in One Click', 'w.png', 'his Specialization provides a case-based introduction to the 2', 'c.png', 'his Specialization provides a case-based introduction to the 3');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
`settinges_id` int(11) NOT NULL,
  `def_lang` varchar(20) NOT NULL,
  `def_phone` varchar(25) NOT NULL,
  `def_fax` varchar(25) NOT NULL,
  `def_email` varchar(25) NOT NULL,
  `def_tweeter` varchar(25) NOT NULL,
  `def_skype` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
`slider_id` int(11) NOT NULL,
  `slider_image_path` text NOT NULL,
  `slider_title` varchar(120) DEFAULT NULL,
  `slider_textbody` text,
  `slider_link` text,
  `slider_lang_id` varchar(20) DEFAULT NULL,
  `css_rolls` text,
  `css_class` varchar(200) DEFAULT NULL,
  `slider_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `smallfooter`
--

CREATE TABLE IF NOT EXISTS `smallfooter` (
`smallfooter_id` int(255) NOT NULL,
  `smallfooter_langid` int(22) NOT NULL,
  `smallfooter_text` text NOT NULL,
  `smallfooter_url` varchar(255) NOT NULL,
  `smallfooter_icon` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smallfooter`
--

INSERT INTO `smallfooter` (`smallfooter_id`, `smallfooter_langid`, `smallfooter_text`, `smallfooter_url`, `smallfooter_icon`) VALUES
(1, 1, 'Contact us', 'http://contcatus.com', '9cb8d9a968dcb390807fd667b673a84c.jpg'),
(2, 1, 'Email', 'asd@asdasf.com', 'b18bf934aee42a75859676180f00c192.jpg'),
(3, 1, 'abs', 'asd@asdasf.com', 'f397b745ae7eddedfb5a71efc91d6bc8.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE IF NOT EXISTS `social_links` (
`social_id` int(11) NOT NULL,
  `social_imgepath` text NOT NULL,
  `social_link` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `social_links`
--

INSERT INTO `social_links` (`social_id`, `social_imgepath`, `social_link`) VALUES
(1, 'uk-icon-linkedin', 'https://www.linkedin.com/'),
(2, 'uk-icon-vk', 'https://vs.com'),
(3, 'uk-icon-behance', 'https://behance.com');

-- --------------------------------------------------------

--
-- Table structure for table `spcifaction`
--

CREATE TABLE IF NOT EXISTS `spcifaction` (
`spcifaction` int(11) NOT NULL,
  `spcifaction_details_groub_id` varchar(80) NOT NULL,
  `spcifaction_name` varchar(120) NOT NULL,
  `spcifaction_test` text NOT NULL,
  `isTage` set('true','false') NOT NULL DEFAULT 'false',
  `banarpath` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `statistic`
--

CREATE TABLE IF NOT EXISTS `statistic` (
`statistic_id` bigint(120) NOT NULL,
  `statistic_url` text,
  `statistic_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `statistic_unixtime` int(255) DEFAULT NULL,
  `statistic_source` text,
  `statistic_product_id` varchar(255) DEFAULT NULL,
  `statistic_event_id` varchar(255) DEFAULT NULL,
  `statistic_ip` varchar(255) DEFAULT NULL,
  `statistic_time_in` varchar(255) DEFAULT NULL,
  `statistic_country` varchar(255) DEFAULT NULL,
  `statistic_city` varchar(255) NOT NULL,
  `statistic_browser` varchar(255) DEFAULT NULL,
  `statistic_atype` varchar(100) NOT NULL,
  `statistic_os` varchar(255) DEFAULT NULL,
  `statistic_isproduct` varchar(255) DEFAULT NULL,
  `statistic_pagename` varchar(255) DEFAULT NULL,
  `statistic_lang` varchar(255) NOT NULL,
  `statistic_agenttxt` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statistic`
--

INSERT INTO `statistic` (`statistic_id`, `statistic_url`, `statistic_time`, `statistic_unixtime`, `statistic_source`, `statistic_product_id`, `statistic_event_id`, `statistic_ip`, `statistic_time_in`, `statistic_country`, `statistic_city`, `statistic_browser`, `statistic_atype`, `statistic_os`, `statistic_isproduct`, `statistic_pagename`, `statistic_lang`, `statistic_agenttxt`) VALUES
(1, 'http://localhost/instaanimal/', '2019-12-21 18:52:50', 1576957970, '', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Safari/605.1.15'),
(2, 'http://localhost/instaanimal/', '2019-12-21 19:10:51', 1576959051, 'http://localhost/instaanimal/', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Safari/605.1.15'),
(3, 'http://localhost/instaanimal/', '2020-07-12 15:41:37', 1594568497, '', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(4, 'http://localhost/instaanimal/', '2020-07-12 15:41:53', 1594568513, '', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(5, 'http://localhost/instaanimal/', '2020-07-12 15:41:59', 1594568519, '', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(6, 'http://localhost/instaanimal/', '2020-07-12 16:09:00', 1594570140, '', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(7, 'http://localhost/instaanimal/', '2020-07-12 16:13:22', 1594570402, '', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(8, 'http://localhost/instaanimal/Userlogin', '2020-07-12 16:14:46', 1594570486, 'http://localhost/instaanimal/', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/Userlogin', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(9, 'http://localhost/instaanimal/home', '2020-07-12 16:14:53', 1594570493, 'http://localhost/instaanimal/Userlogin', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/home', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(10, 'http://localhost/instaanimal/home', '2020-07-12 16:26:02', 1594571162, 'http://localhost/instaanimal/Userlogin', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/home', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(11, 'http://localhost/instaanimal/Userlogin', '2020-07-12 16:27:45', 1594571265, 'http://localhost/instaanimal/home', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/Userlogin', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(12, 'http://localhost/instaanimal/Userlogin', '2020-07-12 16:28:10', 1594571290, 'http://localhost/instaanimal/home', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/Userlogin', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(13, 'http://localhost/instaanimal/', '2020-07-12 16:30:21', 1594571421, 'http://localhost/instaanimal/home', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(14, 'http://localhost/instaanimal//explore?ca=6', '2020-07-12 16:31:21', 1594571481, 'http://localhost/instaanimal/', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal//explore?ca=6', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(15, 'http://localhost/instaanimal/explore/v/Egyptian Mau Cat?uuid=6', '2020-07-12 16:31:27', 1594571487, 'http://localhost/instaanimal//explore?ca=6', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/explore/v/Egyptian Mau Cat?uuid=6', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(16, 'http://localhost/instaanimal/', '2020-07-12 16:33:21', 1594571601, 'http://localhost/instaanimal/home', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(17, 'http://localhost/instaanimal/', '2020-07-12 16:33:29', 1594571609, 'http://localhost/instaanimal/home', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(18, 'http://localhost/instaanimal/index.php', '2020-07-12 16:37:28', 1594571848, '', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/index.php', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(19, 'http://localhost/instaanimal/', '2020-07-12 17:09:49', 1594573789, 'http://localhost/instaanimal/home', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(20, 'http://localhost/instaanimal/', '2020-07-12 17:10:08', 1594573808, 'http://localhost/instaanimal/home', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15'),
(21, 'http://localhost/instaanimal/', '2020-07-12 17:11:11', 1594573871, 'http://localhost/instaanimal/home', NULL, NULL, '127.0.0.1', NULL, '-', '-', 'Safari', 'Safari 605.1.15', 'Mac OS X', NULL, 'http://localhost/instaanimal/', '0', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15');

-- --------------------------------------------------------

--
-- Table structure for table `supscripers`
--

CREATE TABLE IF NOT EXISTS `supscripers` (
`supscripers_id` int(11) NOT NULL,
  `supscripers_name` varchar(50) NOT NULL,
  `supscripers_email` varchar(80) NOT NULL,
  `supscripers_phone` varchar(80) NOT NULL,
  `supscripers_regtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `userkey` varchar(80) NOT NULL,
  `username` varchar(80) NOT NULL,
  `userpassword` varchar(64) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `userlevel` varchar(50) NOT NULL,
  `useremail` varchar(80) NOT NULL,
  `lastip` varchar(80) NOT NULL,
  `token` varchar(80) NOT NULL,
  `isonline` int(11) NOT NULL DEFAULT '0',
  `accesstime` datetime NOT NULL,
  `safeip` varchar(80) NOT NULL,
  `activesys` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`userkey`, `username`, `userpassword`, `full_name`, `userlevel`, `useremail`, `lastip`, `token`, `isonline`, `accesstime`, `safeip`, `activesys`) VALUES
('456789876543456', 'max', '2ffe4e77325d9a7152f7086ea7aa5114', 'mohamed', '777', 'carlos@gmail.com', '127.0.0.1', 'c09a3b1c370582d0fb1752a4b60cc181', 1, '2020-07-12 18:09:12', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `website`
--

CREATE TABLE IF NOT EXISTS `website` (
`website_id` int(255) NOT NULL,
  `website_logo` varchar(255) NOT NULL,
  `website_keywords` longtext NOT NULL,
  `website_description` longtext NOT NULL,
  `website_defulatimg` varchar(255) NOT NULL,
  `website_icon` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `website`
--

INSERT INTO `website` (`website_id`, `website_logo`, `website_keywords`, `website_description`, `website_defulatimg`, `website_icon`) VALUES
(2, 'b7179a4773305a17d63f3b23759faffd.jpg', '', '', '00e72b03ac784d52494e5546f08c067c.jpg', 'f397b745ae7eddedfb5a71efc91d6bc8.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `welcomword`
--

CREATE TABLE IF NOT EXISTS `welcomword` (
`ww_id` int(255) NOT NULL,
  `ww_lang_id` int(25) NOT NULL,
  `ww_img` varchar(255) NOT NULL,
  `ww_title` varchar(255) NOT NULL,
  `ww_text` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `welcomword`
--

INSERT INTO `welcomword` (`ww_id`, `ww_lang_id`, `ww_img`, `ww_title`, `ww_text`) VALUES
(1, 1, '9cb8d9a968dcb390807fd667b673a84c.jpg', 'Welocing', '<p>ghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hs</p><p><br></p><p><br></p><p>dihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fhghg ashl kghuhg luhausdgh ds;hu dsufhjasdhhashf hsdihf dshi fh</p>');

-- --------------------------------------------------------

--
-- Table structure for table `widget`
--

CREATE TABLE IF NOT EXISTS `widget` (
`widget_id` bigint(255) NOT NULL,
  `widget_pages_id` bigint(255) NOT NULL,
  `widget_order` int(255) NOT NULL,
  `widget_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wordlist`
--

CREATE TABLE IF NOT EXISTS `wordlist` (
`wordlist_id` bigint(255) NOT NULL,
  `wordlist_hash` varchar(255) NOT NULL,
  `wordlist_w` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aac_login`
--
ALTER TABLE `aac_login`
 ADD PRIMARY KEY (`userkey`), ADD UNIQUE KEY `username` (`username`), ADD KEY `databasename` (`databasename`);

--
-- Indexes for table `about`
--
ALTER TABLE `about`
 ADD PRIMARY KEY (`about_id`);

--
-- Indexes for table `apply`
--
ALTER TABLE `apply`
 ADD PRIMARY KEY (`d`), ADD KEY `fullname` (`fullname`);

--
-- Indexes for table `articls`
--
ALTER TABLE `articls`
 ADD PRIMARY KEY (`articls_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`category_id`), ADD FULLTEXT KEY `category_name` (`category_name`);

--
-- Indexes for table `citys`
--
ALTER TABLE `citys`
 ADD PRIMARY KEY (`citys_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
 ADD PRIMARY KEY (`contacts_id`);

--
-- Indexes for table `decoments`
--
ALTER TABLE `decoments`
 ADD PRIMARY KEY (`decoments_id`);

--
-- Indexes for table `details_groub`
--
ALTER TABLE `details_groub`
 ADD PRIMARY KEY (`details_groub_id`);

--
-- Indexes for table `featuresmanager`
--
ALTER TABLE `featuresmanager`
 ADD PRIMARY KEY (`features_id`), ADD KEY `features_item_id` (`features_item_id`,`features_name`), ADD FULLTEXT KEY `features_value` (`features_value`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
 ADD PRIMARY KEY (`pic_id`);

--
-- Indexes for table `footerlinks`
--
ALTER TABLE `footerlinks`
 ADD PRIMARY KEY (`footerlinks_id`);

--
-- Indexes for table `footersection`
--
ALTER TABLE `footersection`
 ADD PRIMARY KEY (`footersection_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
 ADD PRIMARY KEY (`items_id`), ADD KEY `items_name` (`items_name`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
 ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `key_setting`
--
ALTER TABLE `key_setting`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landpage`
--
ALTER TABLE `landpage`
 ADD PRIMARY KEY (`lp_id`);

--
-- Indexes for table `languge`
--
ALTER TABLE `languge`
 ADD PRIMARY KEY (`lang_id`), ADD KEY `lang_name` (`lang_name`);

--
-- Indexes for table `layout`
--
ALTER TABLE `layout`
 ADD PRIMARY KEY (`layout_id`), ADD UNIQUE KEY `layout_name` (`layout_name`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
 ADD PRIMARY KEY (`media_id`);

--
-- Indexes for table `mlist`
--
ALTER TABLE `mlist`
 ADD PRIMARY KEY (`mlist_id`), ADD KEY `mlist_link` (`mlist_link`);

--
-- Indexes for table `onlineorders`
--
ALTER TABLE `onlineorders`
 ADD PRIMARY KEY (`onlineorders_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
 ADD PRIMARY KEY (`pages_id`), ADD UNIQUE KEY `pages_title` (`pages_title`);

--
-- Indexes for table `paragraphs`
--
ALTER TABLE `paragraphs`
 ADD PRIMARY KEY (`paragraphs_id`);

--
-- Indexes for table `searchbox`
--
ALTER TABLE `searchbox`
 ADD PRIMARY KEY (`searchbox_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`settinges_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
 ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `smallfooter`
--
ALTER TABLE `smallfooter`
 ADD PRIMARY KEY (`smallfooter_id`);

--
-- Indexes for table `social_links`
--
ALTER TABLE `social_links`
 ADD PRIMARY KEY (`social_id`);

--
-- Indexes for table `spcifaction`
--
ALTER TABLE `spcifaction`
 ADD PRIMARY KEY (`spcifaction`);

--
-- Indexes for table `statistic`
--
ALTER TABLE `statistic`
 ADD PRIMARY KEY (`statistic_id`);

--
-- Indexes for table `supscripers`
--
ALTER TABLE `supscripers`
 ADD PRIMARY KEY (`supscripers_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
 ADD PRIMARY KEY (`userkey`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `website`
--
ALTER TABLE `website`
 ADD PRIMARY KEY (`website_id`);

--
-- Indexes for table `welcomword`
--
ALTER TABLE `welcomword`
 ADD PRIMARY KEY (`ww_id`);

--
-- Indexes for table `widget`
--
ALTER TABLE `widget`
 ADD PRIMARY KEY (`widget_id`), ADD KEY `widget_pages_id` (`widget_pages_id`);

--
-- Indexes for table `wordlist`
--
ALTER TABLE `wordlist`
 ADD PRIMARY KEY (`wordlist_id`), ADD UNIQUE KEY `wordlist_hash` (`wordlist_hash`), ADD KEY `wordlist_w` (`wordlist_w`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aac_login`
--
ALTER TABLE `aac_login`
MODIFY `userkey` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
MODIFY `about_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `apply`
--
ALTER TABLE `apply`
MODIFY `d` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `articls`
--
ALTER TABLE `articls`
MODIFY `articls_id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `category_id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `citys`
--
ALTER TABLE `citys`
MODIFY `citys_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
MODIFY `contacts_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `decoments`
--
ALTER TABLE `decoments`
MODIFY `decoments_id` bigint(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `details_groub`
--
ALTER TABLE `details_groub`
MODIFY `details_groub_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `featuresmanager`
--
ALTER TABLE `featuresmanager`
MODIFY `features_id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
MODIFY `pic_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `footerlinks`
--
ALTER TABLE `footerlinks`
MODIFY `footerlinks_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `footersection`
--
ALTER TABLE `footersection`
MODIFY `footersection_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
MODIFY `items_id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `key_setting`
--
ALTER TABLE `key_setting`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `landpage`
--
ALTER TABLE `landpage`
MODIFY `lp_id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `languge`
--
ALTER TABLE `languge`
MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `layout`
--
ALTER TABLE `layout`
MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
MODIFY `media_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mlist`
--
ALTER TABLE `mlist`
MODIFY `mlist_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `onlineorders`
--
ALTER TABLE `onlineorders`
MODIFY `onlineorders_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
MODIFY `pages_id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `paragraphs`
--
ALTER TABLE `paragraphs`
MODIFY `paragraphs_id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `searchbox`
--
ALTER TABLE `searchbox`
MODIFY `searchbox_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
MODIFY `settinges_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smallfooter`
--
ALTER TABLE `smallfooter`
MODIFY `smallfooter_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `social_links`
--
ALTER TABLE `social_links`
MODIFY `social_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `spcifaction`
--
ALTER TABLE `spcifaction`
MODIFY `spcifaction` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `statistic`
--
ALTER TABLE `statistic`
MODIFY `statistic_id` bigint(120) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `supscripers`
--
ALTER TABLE `supscripers`
MODIFY `supscripers_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `website`
--
ALTER TABLE `website`
MODIFY `website_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `welcomword`
--
ALTER TABLE `welcomword`
MODIFY `ww_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `widget`
--
ALTER TABLE `widget`
MODIFY `widget_id` bigint(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wordlist`
--
ALTER TABLE `wordlist`
MODIFY `wordlist_id` bigint(255) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
