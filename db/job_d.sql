-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 04, 2019 at 06:20 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.3.6-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insta`
--

-- --------------------------------------------------------

--
-- Table structure for table `job_d`
--

CREATE TABLE `job_d` (
  `jobd_id` bigint(255) NOT NULL,
  `event_id` bigint(255) NOT NULL,
  `experience_needed:` int(11) NOT NULL,
  `career_level` varchar(222) CHARACTER SET latin1 NOT NULL,
  `job_type` varchar(222) CHARACTER SET latin1 NOT NULL,
  `salary` varchar(222) CHARACTER SET latin1 NOT NULL,
  `languages` varchar(222) CHARACTER SET latin1 NOT NULL,
  `vacancies` int(11) NOT NULL,
  `aboutjob` longblob NOT NULL,
  `requirements` longblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `job_d`
--
ALTER TABLE `job_d`
  ADD PRIMARY KEY (`jobd_id`),
  ADD KEY `event_id` (`event_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `job_d`
--
ALTER TABLE `job_d`
  MODIFY `jobd_id` bigint(255) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
