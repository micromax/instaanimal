-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 29, 2019 at 10:38 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.3.6-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invest`
--

-- --------------------------------------------------------

--
-- Table structure for table `pcategory`
--

CREATE TABLE `pcategory` (
  `pcategory_id` bigint(255) NOT NULL,
  `pcategory_crm_id` bigint(255) DEFAULT NULL,
  `pcategory_name` varchar(255) NOT NULL,
  `pcategory_name_ar` varchar(255) DEFAULT NULL,
  `pcategory_img` varchar(255) DEFAULT NULL,
  `pcategory_lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pcategory`
--

INSERT INTO `pcategory` (`pcategory_id`, `pcategory_crm_id`, `pcategory_name`, `pcategory_name_ar`, `pcategory_img`, `pcategory_lang_id`) VALUES
(1, NULL, 'Mobile & Tablets', NULL, '4367caed71f6050d07700b1a195f9c80.jpg', 1),
(2, NULL, 'Home Appliances', NULL, '21c23f9dabe26554e7683777e5b2aa5e.jpg', 1),
(3, NULL, 'Computers And Networking', NULL, '00e72b03ac784d52494e5546f08c067c.jpg', 1),
(4, NULL, 'Electronics', NULL, 'b7179a4773305a17d63f3b23759faffd.jpg', 1),
(5, NULL, 'Home , Kitchen and Tools', NULL, '4367caed71f6050d07700b1a195f9c80.jpg', 1),
(6, NULL, 'Watches & jewelery', NULL, '4367caed71f6050d07700b1a195f9c80.jpg', 1),
(7, NULL, 'Fashion', NULL, '4367caed71f6050d07700b1a195f9c80.jpg', 1),
(8, NULL, 'Market', NULL, '', 1),
(9, NULL, 'Health &Beauty', NULL, NULL, 1),
(10, NULL, 'Baby Accessories', NULL, NULL, 1),
(11, NULL, 'Sport And Fitness', NULL, NULL, 1),
(12, NULL, 'Books , Games & Toys', NULL, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pcategory`
--
ALTER TABLE `pcategory`
  ADD PRIMARY KEY (`pcategory_id`),
  ADD KEY `category_crm_id` (`pcategory_crm_id`);
ALTER TABLE `pcategory` ADD FULLTEXT KEY `category_name` (`pcategory_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pcategory`
--
ALTER TABLE `pcategory`
  MODIFY `pcategory_id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
