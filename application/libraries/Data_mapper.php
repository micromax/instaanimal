<?php


class data_mapper {

      public $CI;
      
      public $tables;

      public $schema;

      public function __construct() {
           
          $this->CI =& get_instance();
          //$this->CI->load->library('database');
          $this->CI->load->helper('file');
          
          
          
      }
      
      public function getTables()
      {
          $this->tables = $this->CI->db->list_tables();
          //var_dump($this->tables);
      }
      
      public function tableTree($tablename)
      {
       
          return $fields = $this->CI->db->list_fields("`".$tablename."`");   
          
      }
      
      public function schemaBuiler()
      {
          foreach($this->tables as  $table)
              {
                
              //$this->schema[(string)$table] = null;  
              foreach ($this->tableTree($table) as $tableCols)
               {
                   $this->schema[$table][] = $tableCols;
               }
              
              
              }
              
              return $this->schema;
      }
      
      
      
      public function wFILE($path , $data)
      {
          
          $out = '<?php'." \n";
          $out .= '$dbMAP = array( ';
          
          foreach ($data as $KLV1 => $VLV1)
          {
          $out .= "'".$KLV1."'=> ";
            if(is_array($VLV1))
            {
                $out .= 'array( '." \n";
                foreach ($VLV1 as $KLV2 => $VLV2)
                {
                    $out .= "'".$KLV2."'=>'". $VLV2."', "." \n";
                }
                $out .= '),'." \n";
            }
            else {
                $out .= " '".$VLV1."', "." \n";
            }
          }
          $out .= ");"." \n";
        write_file($path , $out);
        
      
        
      }
    
    
}

