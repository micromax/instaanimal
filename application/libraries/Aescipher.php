<?php
class aescipher {
    
     const OPENSSL_CIPHER_NAME = "aes-128-cbc";
  
     const CIPHER_KEY_LEN = 16; //128 bits
    public static function fixKey($key) {
        
        if (strlen($key) < Aescipher::CIPHER_KEY_LEN) {
            //0 pad to len 16
            return str_pad("$key", Aescipher::CIPHER_KEY_LEN, "0"); 
        }
        
        if (strlen($key) > Aescipher::CIPHER_KEY_LEN) {
            //truncate to 16 bytes
            return substr($key, 0, Aescipher::CIPHER_KEY_LEN); 
        }
        return $key;
    }
    /**
    * Encrypt data using AES Cipher (CBC) with 128 bit key
    * 
    * @param type $key - key to use should be 16 bytes long (128 bits)
    * @param type $iv - initialization vector
    * @param type $data - data to encrypt
    * @return encrypted data in base64 encoding with iv attached at end after a :
    */
    public static function encrypt($key, $iv, $data) {
        $encodedEncryptedData = base64_encode(openssl_encrypt($data, Aescipher::OPENSSL_CIPHER_NAME, Aescipher::fixKey($key), OPENSSL_RAW_DATA, $iv));
        $encodedIV = base64_encode($iv);
        $encryptedPayload = $encodedEncryptedData.":".$encodedIV;
        return $encryptedPayload;
    }
    /**
    * Decrypt data using AES Cipher (CBC) with 128 bit key
    * 
    * @param type $key - key to use should be 16 bytes long (128 bits)
    * @param type $data - data to be decrypted in base64 encoding with iv attached at the end after a :
    * @return decrypted data
    */
    public static function decrypt($key, $data) {
        $parts = explode(':', $data); //Separate Encrypted data from iv.
        $encrypted = $parts[0];
        $iv = $parts[1];
        $decryptedData = openssl_decrypt(base64_decode($encrypted), Aescipher::OPENSSL_CIPHER_NAME, Aescipher::fixKey($key), OPENSSL_RAW_DATA, base64_decode($iv));
        return $decryptedData;
    }
}
//$decrypted = AesCipher::decrypt('0123456789abcdef', 'G0a6cK+sxBkSwyCjcG4efA==:YWJjZGVmOTg3NjU0MzIxMA==');///
//var_dump($decrypted);
