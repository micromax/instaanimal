<?php
class uxdata {

    public $tableName;
    public $tableHANDELER;
    public $dataHD;

    public $DATA;

    public $primKEY;

    public $tabeCOLS;

    public $output;

    public $deletHANDELER;

    public $editHANDELER;
    
    public $editUrl; 
    
    public $renewHands;
    
    public $xselect = null;
    
    
    public $imgfilds = null;
    public $fullview = false;
    public $fullview_url = null;

    



    public $extraHands  = null;
    public $extraHands2  = null;
    
    public function  __construct($config)
    {
        $this->tableHANDELER    = $config["tableHANDELER"];
        $this->dataHD           = $config["dataHD"];
        $this->data             = $config["data"];
        $this->primKEY          = $config["primKEY"];
        $this->tabeCOLS         = $config["tabeCOLS"];
        $this->tableName        = $config["tableName"];
        $this->deletHANDELER    = $config["deletHANDELER"];
        $this->editHANDELER     = $config["editHANDELER"];
        $this->editUrl          = $config["editUrl"];
        $this->imgfilds         = (isset($config["imgfilds"]) == TRUE )? $config["imgfilds"] : NULL ;
        
        if(isset($config["fullview"]) &&  $config["fullview"] == TRUE)
        {
            $this->fullview         = $config["fullview"];
            $this->fullview_url         = $config["fullview_url"];
      
        }
        
        if(isset($config["xselect"])){
        $this->xselect          = $config["xselect"];   
       }
           
        
        if(isset($config["extraHands"]))
        {
            $this->extraHands  = $config["extraHands"];
        }
        if(isset($config["extraHands2"]))
        {
            $this->extraHands2  = $config["extraHands2"];
        }
        if(isset($config["renew"]))
        {
            $this->renewHands  = $config["renew"];
        }
        
        
    }

    public function render()
    {   $this->output = "";
        $this->output .= "<table  id=\"datatable\" class=\"table table-bordered table-striped dataTable\"  role='grid' >";
        //satar render table head
        $this->output .="<thead> <tr>";
        foreach($this->dataHD as $hd){
        $this->output .="<td>";
        $this->output .=$hd;
        $this->output .="</td>";
        }
        $this->output .="<td>";
        $this->output .="Delete ";
        $this->output .="</td>";
        
        $this->output .="<td>";
        $this->output .="Action Edit";
        $this->output .="</td>";
        if($this->fullview == TRUE){
        $this->output .="<td>";
        $this->output .="Action View";
        $this->output .="</td>";
         }
        $this->output .="</tr></thead>";
        //end reander table head
        $this->output .= "<tbody>";
        //satart table
       
            
            $primKEY = $this->primKEY;
            if($this->data != null)
            {
            foreach ($this->data as $row)
            {
                $this->output .="<tr>";
                foreach ($this->tabeCOLS as $col){
               
                if($this->imgfilds[$col] == TRUE)
                {
                    $this->output .="<td>";
                $this->output .= "<img src='".base_url("uploads/".$row->$col)."' width='150px' />";
                $this->output .="</td>";
                    
                } else {
                
                    $this->output .="<td>";
                $this->output .= $row->$col;
                $this->output .="</td>";
                }
                    
                
                }
                
                $this->output .="<td>";
                $this->output .="<a class='del' rel='del' into='".$this->tableName."' col='".$this->primKEY."' item='".$row->$primKEY."' >$this->deletHANDELER</a>";
                $this->output .="</td>";
                if($this->editUrl != NULL)
                {
                    $this->output .="<td>";
                $this->output .="<a  href='". base_url($this->editUrl."?action=edit&item=").$row->$primKEY."'>$this->editHANDELER</a>";
                $this->output .="</td>";
                } else {
                
                    $this->output .="<td>";
                $this->output .="<a  class='edit' rel='edit' into='".$this->tableName."' col=".$this->primKEY." item='".$row->$primKEY."'>$this->editHANDELER</a>";
                $this->output .="</td>";
                }
                
                if($this->extraHands != null)
                {
                 $this->output .="<td>";
                 $this->output .="<a  class='extra' rel='extra' into='".$this->tableName."' col=".$this->primKEY." item='".$row->$primKEY."'>$this->extraHands</a>";
                 $this->output .="</td>";
                }
                
                if($this->extraHands2 != null)
                {
                 $this->output .="<td>";
                 $this->output .="<a  class='extra2' rel='extra2' into='".$this->tableName."' col=".$this->primKEY." item='".$row->$primKEY."'>$this->extraHands2</a>";
                 $this->output .="</td>";
                }
                if($this->fullview == TRUE)
                {
                 $this->output .="<td>";
                 $this->output .="<a  href='". base_url($this->fullview_url."?action=v&item=").$row->$primKEY."'>View</a>";
                 $this->output .="</td>";
                }
                
                if($this->xselect != null)
                {
                 $this->output .="<td>";
                 $this->output .= "<select name='xselect' item=".$row->$primKEY." >";
                 foreach ($this->xselect as $key=>$value) 
                  {
                     $this->output .="<option value='$key' > $value </option>";
                  }
                 $this->output .="</select>";
                 
                 $this->output .="</td>";
                }
                
                
                if($this->renewHands != null)
                {
                 $this->output .="<td>";
                 $this->output .="<a  class='renew' rel='renew' into='".$this->tableName."' col=".$this->primKEY." item='".$row->$primKEY."'>$this->renewHands</a>";
                 $this->output .="</td>";
                }

               $this->output .="</tr>";
            }
            }
            
            $this->output.="</tbody>";
             $this->output.="</table>";
             
        
        //end table
        return $this->output;
        
    }

    

}
