<?php
        $site["list"]["defaults"] = "home" ;
        $site["list"]["about"] = "about us" ;
        $site["list"]["contact"] = "contact us" ;
        $site["list"]["products"] = "products" ;
        $site["list"]["jobs"] = "jobs" ;
        $site["list"]["brands"] = "brands" ;

        $site["static"]["search"]['1'] = "search" ;
        $site["static"]["search"]['2'] = "بحث" ;

        $site["static"]["JoinourNewsletter"]['1'] = "Join our Newsletter!" ;
        $site["static"]["JoinourNewsletter"]['2'] = "اشترك في القائمه البريدية" ;

        $site["static"]["firstname"]['1'] = "First name" ;
        $site["static"]["firstname"]['2'] = "الاسم الاول" ;

        $site["static"]["lastname"]['1'] = "last name" ;
        $site["static"]["lastname"]['2'] = "الاسم الاخير" ;

        $site["static"]["email"]['1'] = "Email" ;
        $site["static"]["email"]['2'] = "بريدك الاليكتروني" ;

        $site["static"]["subscribe"]['1'] = "subscribe" ;
        $site["static"]["subscribe"]['2'] = "اشترك معنا" ;

        $site["static"]["TWEETER"]['1'] = "TWEETER" ;
        $site["static"]["TWEETER"]['2'] = "تويتر" ;

        $site["static"]["News"]['1'] = "News" ;
        $site["static"]["News"]['2'] = "الاخبار" ;

        $site["static"]["contact"]['1'] = "Contact us" ;
        $site["static"]["contact"]['2'] = "الاتصال بنا" ;

        $site["static"]["message"]['1'] = "message" ;
        $site["static"]["message"]['2'] = "الرساله" ;

        $site["static"]["sendmessage"]['1'] = "send message" ;
        $site["static"]["sendmessage"]['2'] = "ارسل رساله" ;

        $site["static"]["readmore"]['1'] = "read more" ;
        $site["static"]["readmore"]['2'] = "اقراء المزيد" ;


function l($w , $lid = '1')
    {
        $def = '1';
       if(isset($site[$w][$lid]))
         {
           return $site[$w][$lid];
         }else {
             return $site[$w][$def];
         }
    }
    