<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of My_Controller
 *
 * @author sadeem-pc
 */
class MY_Controller extends CI_Controller{
    //put your code here
    
    protected static $airBook_MAP;
    public $dbMAP;
    public $userPER;
    public $user;
    public $userName;
    public $ResultPerPage;
    public $userpassword;
    public $userscount;
    public $resellercount;
    public $androidchancount;
    public $enigmachancount;
    
    
    public function __construct()
	{
		parent::__construct();
                
                
        $config['TABLE'] = 'user_login';
        $config['USERCOL'] = 'username';
        $config['USERIDCOL'] = 'userkey';
        $config['PASSCOL'] = 'userpassword';
        $config['TOKCOL'] = 'token';
        $config['USERSTATUS'] = 'isonline';
        $config['USERIP'] = 'lastip';
        $this->load->library("authrized", $config);

        $this->load->model('check_user_model');
        $this->load->model('data_model');
        //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
        if ($this->check_user_model->chekUserViaSession() == true) {
            
        } elseif ($this->check_user_model->chekUserViaCookie() == true) {
            
        } else {
            redirect("admin/login");
        }


        $this->userPER = $this->session->Get_mySession("userLevel");
        $userinf = $this->data_model->get_row("user_login", "userkey", $this->session->Get_mySession("userKey"));
        foreach ($userinf as $info) {
            $this->user = $info->userkey;
            $this->userName = $info->username;
            $this->userpassword = $info->userpassword;
        }

        if (!is_file(APPPATH . 'config/dbMAP.php')) {
            $this->load->library("data_mapper");
            $this->data_mapper->getTables();
            $map = $this->data_mapper->schemaBuiler();
            $this->data_mapper->wFILE(APPPATH . 'config/dbMAP.php', $map);
            @include_once APPPATH . 'config/dbMAP.php';
        } else {
            @include_once APPPATH . 'config/dbMAP.php';
        }
            $this->dbMAP = $dbMAP;
	}

    
    
}
