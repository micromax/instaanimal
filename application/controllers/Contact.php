<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of contact
 *
 * @author iptv
 */
class Contact extends CI_Controller {

    public $langList;
   public $currunt_lang ;
   public $stsicwords;
   public $catlist;
   public $barndlist;
   public $wordofabout40;
   public $slinks;

    public function  __construct() {
        parent::__construct();
         $this->load->model("site_model");
        $this->langList = $this->site_model->getLangList();
        $this->currunt_lang = $this->site_model->GetLangRUN();
        $this->stsicwords = $this->site_model->getStaticWords($this->currunt_lang);
        $this->catlist = $this->site_model->genreateCatList($this->currunt_lang);
        
         $this->slinks = $this->site_model->get("social_links" , false , false , false) ;
        $this->wordofabout40 = $this->site_model->get("about" , $limit = 1 , false , array("lang_id" , $this->currunt_lang));

    }

    public function index()
    {
        //AIzaSyBVEmBdH7El9NOtXtuQv0fF6KpXbS7vh8g
        
        $data['citys'] = $this->site_model->get("citys" , 1000);
       
        $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       
       $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
       
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
       if($this->wordofabout40 != false){
       foreach($this->wordofabout40 as $word){}

       $data["aboutword"] = word_limiter($word->about_pragraph , 45) ;
       }else
           {
           $data["aboutword"] = "no data";
       }
       $data["name"] = "contact us";
       $data["bodyview"] = "master/contact";
       $this->load->view("master" , $data);

    }
}