<?php

class Defaults extends CI_Controller {



   public $langList;
   public $currunt_lang ;
   public $stsicwords;
   public $catlist;
   public $barndlist;
   public $slinks;
   public $KV;
   
   

   public function  __construct()
    {
        parent::__construct();

        $this->load->model("master_model");
        $this->load->model("site_model");

        $this->load->model("data_model");

        $this->load->model("Citys_model");

        $this->langList = $this->site_model->getLangList();
        $this->currunt_lang = $this->site_model->GetLangRUN();
        $this->stsicwords = $this->site_model->getStaticWords($this->currunt_lang);
        $this->catlist = $this->site_model->genreateCatList($this->currunt_lang);
        $this->barndlist = $this->site_model->genreateBrandlist($this->currunt_lang);
        $this->slinks = $this->site_model->get("social_links" , false , false , false) ;
        $this->KV =  $this->site_model->get("key_setting" , false , false , array("lang_id" , $this->currunt_lang) , FALSE , FALSE, FALSE,FALSE , array("key" , "val")) ;
        
        //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
    }

   public function index()
    {
       
       foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }
       
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data['citys'] = $this->site_model->geycity();
       $joine = array("category" , "category_pcategory_id = pcategory_id" , "left");
       $dax = $this->site_model->get("pcategory" ,  FALSE , array("category_score", "DESC") , FALSE ,FALSE , FALSE ,  $joine) ;
       
       $joins = array("programes" , " events.events_program_id = programes.programes_crm_id  " , "LEFT");
       $gb = array("events.events_program_id" ,"events.events_start" );

       $data["docs"] = $this->site_model->get("decoments" , null , NULL , array("decoments.decoments_lang" , $this->currunt_lang));
       
       $data["expage"] = $this->site_model->get("pages" , null , NULL , array("pages.pages_lang" , $this->currunt_lang));
       
       $data["category"] = $this->site_model->get("category");
       $sel = NULL;
       $data["last5"] = $this->site_model->get("events" ,   12 , array("events_start" , "DESC" ) , array("programes.programes_lang_id" , $this->currunt_lang) , false ,  FALSE , $joins   , $gb , $sel);
       $data["sbx"] = $this->site_model->GetSearchBox($this->currunt_lang);
       $dao = array();
       
       $data["ww"] = $this->site_model->get("welcomword" , 1 , false , array("ww_lang_id" , $this->currunt_lang));
       $data["citys_c"] = $this->site_model->getCount("citys");
       
       $data["programes_c"] = $this->site_model->getCount("events");
       
       $data["category_c"] = $this->site_model->getCount("category");
       
       $data["pdata"] = $this->data_model->getl("landpage", array("lp_id" , $this->currunt_lang) );
       foreach ($dax as $k=>$v){
           
           $dao[$v->pcategory_id]["id"] = $v->pcategory_id  ;
           $dao[$v->pcategory_id]["name"] = $v->pcategory_name  ;
           $dao[$v->pcategory_id]["img"] = $v->pcategory_img  ;
           if(isset($dao[$v->pcategory_id]["supcat"])){
               
           if(sizeof($dao[$v->pcategory_id]["supcat"])  <= 8){
               if($this->currunt_lang != 1){
                   $dao[$v->pcategory_id]["supcat"][$v->category_id] = $v->category_name_ar  ;
               }  else {
                   $dao[$v->pcategory_id]["supcat"][$v->category_id] = $v->category_name  ;
               }
           
           }else{
               continue;
           } }  else {
                        if($this->currunt_lang != 1){
                            $dao[$v->pcategory_id]["supcat"][$v->category_id] = $v->category_name_ar  ;
                        }  else {
                            $dao[$v->pcategory_id]["supcat"][$v->category_id] = $v->category_name  ;
                        }
                }
           
       }
       //var_dump($dao);
       $data["first3"] = $dao;
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       $data["brandist"] = $this->barndlist;
       
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
       //$data["city_ps"] = $this->Citys_model->getCtiysWithProg();
       $data["city_ps"] = $this->Citys_model->getdefultcitys();
       
       $data["langs"] = $this->master_model->get_langs();
       $data["extraview"] =  "master/ext";
       $data["bodyview"] = "master/slider";
       
       
       
       
       $data["siteconfig"] = $this->site_model->Getwebsite();
       $data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
       $data["smallfooter"] = $this->site_model->Getsmallfooter($this->currunt_lang);
       
   
       
       
       
       //$this->load->view("master" , $data);
      // $this->load->view("ecomtheme/landingpage/master" , $data);
       
        $this->load->view("Masterpage" , $data);
      
    }

    
}