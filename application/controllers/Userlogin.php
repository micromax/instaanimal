<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author sadeem-pc
 */
class Userlogin extends MY_Central  {
       public function  __construct()
    {
        parent::__construct();

        $this->load->model("master_model");
        $this->load->model("site_model");

        $this->load->model("data_model");

        $this->load->model("Citys_model");
        define('THEME','jobportal');
         //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
    }
    
    
    public function index()
     {
           $this->data["islogin"] = $this->islogin;
               $this->data["userkey"] = $this->userkey;
               $this->data["username"] = $this->username;
               $this->data["userFullname"] = $this->userFullname;
               
              foreach($this->KV as $value){
           $this->data["$value->key"] = $value->val ;
       }
          $this->data["sbx"] = $this->site_model->GetSearchBox($this->currunt_lang);
       $this->data["lang"] = $this->langList;
       
       $this->data["lid"] = $this->currunt_lang;
       
       $this->data["catlist"] = $this->catlist;
       
       $this->data["slinks"] = $this->slinks;
       
       $this->data["sw"] = $this->stsicwords;
       
       $this->data["brandist"] = $this->barndlist;
         $this->data["category"] = $this->site_model->get("category");
       $this->data["mlist"] = $this->site_model->getList($this->currunt_lang);

       $this->data["langs"] = $this->master_model->get_langs();
  

       $this->data["siteconfig"] = $this->site_model->Getwebsite();
       
       $this->data["footerlinks"] = $this->site_model->GetFotterinJson($this->currunt_lang); 
            $this->data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
        //$this->Templets
        					
		$this->template
                        ->set_partial("header" , "partials/header" , $this->data)
                         ->set_partial("menu" , "partials/homemenu" , $this->data)    
                        ->set_partial("welcome" , "partials/loginform" , $this->data)
                       
                        ->set_partial("footer" , "partials/footer" , $this->data)
			->set_layout('layouts/main')
			->enable_parser(FALSE)
			->title("CodeIgniter Mutiple Themes")			
			->build('homepage/login', $this->data);
     }

        public function getaccess()
    {

        $this->load->model('check_vistor_model');

        if($this->check_vistor_model->chekData())
        {
           foreach($this->check_vistor_model->chekData() as $row)
                {
                    $data["userName_v"]=  $row->username;
                    $data["userKey_v"]=   $row->userkey;
                    $data["userToken_v"] =  md5(rand(10000, 9999999)) ;
                    $data["userLevel_v"] = $row->userlevel;
                    $data["userpass_v"] = $row->userpassword;

                     $updatauser = array(
                                "token"  => $data["userToken_v"] ,
                                "lastip"  => $_SERVER['REMOTE_ADDR'],
                                "isonline"  =>  '1',
                                "accesstime" => date("Y-m-d H:i:s")
                            );
             $this->authrized->UPDATA_USER_RECORED($data["userName_v"] ,$data["userKey_v"] , $updatauser);
             $this->session->Create_mySession($data);
             $chied = $this->encrypt->decode($this->input->post("save"));
        if($chied == 1)
            {

               $cookieuser = array( 'name'   => "username_v" ,
                'value'  => $this->encrypt->encode($row->username),
		'expire' => '86500',
		'path'   => '/',
		);
		$cookieuserk = array( 'name'   => "userkey_v" ,
                'value'  => $this->encrypt->encode($row->userkey),
		'expire' => '86500',
		'path'   => '/',
		);

                $cookieuserkpass = array( 'name'   => "useros_v" ,
                'value'  => $this->encrypt->encode($row->userpassword),
		'expire' => '86500',
		'path'   => '/',
		);

                     set_cookie($cookieuser);
                     set_cookie($cookieuserk);
                     set_cookie($cookieuserkpass);

                    }
                }
                redirect("user");

        }
        else{
            delete_cookie("username_v");
            delete_cookie("userkey_v");
            delete_cookie("useros_v");
            session_destroy();
             redirect("userlogin?msg=".urlencode("wronge user name or password")); 



        }



    
    

}



    public function newuser()
        {
            
            $username  = $this->input->post("email");
            $password = $this->input->post("password");
            $fullname = $this->input->post("fullname");
        
            
            
        $this->db->where('username'  , $username);
        $this->db->limit(1);
        $query = $this->db->get("aac_login");
        
      //  $result = $query->result();
        
        
        if($query->num_rows() == 0)
        {
         
            $data["username"] = $username;
            $data["userpassword"]  = md5($password);
            $data["fullname"]  = $fullname;
            $data["token"] =  md5(rand(10000, 9999999)) ;
            $data["lastip"]  = $_SERVER['REMOTE_ADDR'] ; 
            $data["isonline"]  =  '1' ;
            $data["accesstime"] = date("Y-m-d H:i:s") ;
             //$this->db->trans_start();
            $this->db->insert("aac_login" , $data);
             //$this->db->trans_complete();
            $key =  $this->db->insert_id();
                $sess["userName_v"]=  $data["username"];
                $sess["userKey_v"]=   $key;
                $sess["userToken_v"] =  $data["token"]  ;
                
                $sess["userpass_v"] =  $data["userpassword"];
                $this->session->Create_mySession($sess);
                redirect("user"); 
        }else {
                  redirect("Userlogin?msg=".urlencode("this user is exite try another one")); 
        }
        
            
        }
     
     
     
}
