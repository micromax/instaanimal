<?php

class Qcms extends CI_Controller {

    protected static $airBook_MAP;
    public $dbMAP;
    public $userPER;
    public $user;
    public $userName;
    public $ResultPerPage;
    public $userpassword;
    public $userscount;
    public $resellercount;
    public $androidchancount;
    public $enigmachancount;

    public function __construct() {

        parent::__construct();
        $config['TABLE'] = 'user_login';
        $config['USERCOL'] = 'username';
        $config['USERIDCOL'] = 'userkey';
        $config['PASSCOL'] = 'userpassword';
        $config['TOKCOL'] = 'token';
        $config['USERSTATUS'] = 'isonline';
        $config['USERIP'] = 'lastip';
        $this->load->library("authrized", $config);

        $this->load->model('check_user_model');
        $this->load->model('data_model');
        //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
        if ($this->check_user_model->chekUserViaSession() == true) {
            
        } elseif ($this->check_user_model->chekUserViaCookie() == true) {
            
        } else {
            redirect("login");
        }


        $this->userPER = $this->session->Get_mySession("userLevel");
        $userinf = $this->data_model->get_row("user_login", "userkey", $this->session->Get_mySession("userKey"));
        foreach ($userinf as $info) {
            $this->user = $info->userkey;
            $this->userName = $info->username;
            $this->userpassword = $info->userpassword;
        }

        if (!is_file(APPPATH . 'config/dbMAP.php')) {
            $this->load->library("data_mapper");
            $this->data_mapper->getTables();
            $map = $this->data_mapper->schemaBuiler();
            $this->data_mapper->wFILE(APPPATH . 'config/dbMAP.php', $map);
            @include_once APPPATH . 'config/dbMAP.php';
        } else {
            @include_once APPPATH . 'config/dbMAP.php';
        }
        $this->dbMAP = $dbMAP;
    }

    public function index() {

        $data["username"] = $this->userName;



        $data["static"] = $this->data_model->getstat();

        $data["graph"] = $this->data_model->getgraph();
        $data["agent"] = $this->data_model->getgraphagent();
        $data["urlss"] = $this->data_model->getgraphpage();
        $data["country"] = $this->data_model->getgraphcou();

        $data["twiter"] = "";

        $this->load->view("dbthm", $data);
    }

    public function settings() {

        $data['method'] = "qcms/settings";
        $data["winhand"] = "settings";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "settings";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["def_lang"]["label"] = "default Language";
        $input['ur']["def_lang"]["type"] = "select";
        $input['ur']["def_lang"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");
        ;
        $input['ur']["def_lang"]["atrr"] = "class='form-control'";

        $input['ur']["def_phone"]["label"] = "default phone";
        $input['ur']["def_phone"]["type"] = "text";
        $input['ur']["def_phone"]["model"] = "false";
        $input['ur']["def_phone"]["atrr"] = "class='form-control'";

        $input['ur']["def_fax"]["label"] = "default fax";
        $input['ur']["def_fax"]["type"] = "text";
        $input['ur']["def_fax"]["model"] = "false";
        $input['ur']["def_fax"]["atrr"] = "class='form-control'";


        $input['ur']["def_email"]["label"] = "default email";
        $input['ur']["def_email"]["type"] = "text";
        $input['ur']["def_email"]["model"] = "false";
        $input['ur']["def_email"]["atrr"] = "class='form-control'";


        $input['ur']["def_tweeter"]["label"] = "default Twitter";
        $input['ur']["def_tweeter"]["type"] = "text";
        $input['ur']["def_tweeter"]["model"] = "false";
        $input['ur']["def_tweeter"]["atrr"] = "class='form-control'";


        $input['ur']["def_skype"]["label"] = "default Skype";
        $input['ur']["def_skype"]["type"] = "text";
        $input['ur']["def_skype"]["model"] = "false";
        $input['ur']["def_skype"]["atrr"] = "class='form-control'";






        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));




        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "default  Phone", "default Fax", "default Email", "default Twitter", "default skype");
        $conx["data"] = $this->data_model->get("settings");
        $conx["primKEY"] = "settinges_id";
        $conx["tabeCOLS"] = array("settinges_id", "def_phone", "def_fax", "def_email", "def_tweeter", "def_skype");
        $conx["tableName"] = "settings";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function articls() {
        $data["ux"] = "company";
        $data['method'] = "qcms/articls";
        $data["winhand"] = "articls";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "articls";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;

        $input['ur']["articls_lang_id"]["label"] = "Language of item ";
        $input['ur']["articls_lang_id"]["type"] = "select";
        $input['ur']["articls_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        $input['ur']["articls_authr"]["label"] = "article author name";
        $input['ur']["articls_authr"]["type"] = "text";
        $input['ur']["articls_authr"]["model"] = "false";

        $rand = rand(1, 99) . "_";
        $input['ur']["articls_hashkey"]["type"] = "hidden";
        $input['ur']["articls_hashkey"]["default"] = uniqid($rand);

        $input['ur']["articls_title"]["label"] = "article title";
        $input['ur']["articls_title"]["type"] = "text";
        $input['ur']["articls_title"]["model"] = "false";


        $input['ur']["articls_description"]["label"] = "article short text";
        $input['ur']["articls_description"]["type"] = "textarea";
        $input['ur']["articls_description"]["model"] = "false";



        $input['ur']["articls_text"]["label"] = "article body text";
        $input['ur']["articls_text"]["type"] = "textarea";
        $input['ur']["articls_text"]["model"] = "false";


        $input['ur']["articls_link"]["label"] = "article refrance link";
        $input['ur']["articls_link"]["type"] = "text";
        $input['ur']["articls_link"]["model"] = "false";



        $input['ur']["articls_image"]["label"] = "article image";
        $input['ur']["articls_image"]["type"] = "text";
        $input['ur']["articls_image"]["model"] = "false";
        $input['ur']["articls_image"]["atrr"] = "class ='img' id ='img'";



        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "article title", "article short");
        $condation = array("is_orginal", "", 1);
        $conx["data"] = $this->data_model->GetCondiction("articls");
        $conx["primKEY"] = "articls_id";
        $conx["tabeCOLS"] = array("articls_id", "articls_title", "articls_description");
        $conx["tableName"] = "articls";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    
    
    public function kv(){
        $data["ux"] = "company";
        $data['method'] = "qcms/kv";
        $data["winhand"] = "kv";
        
        
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "key_setting";

        $input['ur']["key"]["label"] = "Key String";
        $input['ur']["key"]["type"] = "text";
        $input['ur']["key"]["model"] = "false";

        $input['ur']["val"]["label"] = "Value String";
        $input['ur']["val"]["type"] = "text";
        $input['ur']["val"]["model"] = "false";


        $input['ur']["lang_id"]["label"] = "Language of item ";
        $input['ur']["lang_id"]["type"] = "select";
        $input['ur']["lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");
        
        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));


        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "key", "val", "lang id");
        $condation = array("is_orginal", "", 1);
        $conx["data"] = $this->data_model->GetCondiction("key_setting");
        $conx["primKEY"] = "id";
        $conx["tabeCOLS"] = array("id", "key", "val", "lang_id");
        $conx["tableName"] = "key_setting";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);

    }

    public function statics()
    {
        
        $data["pdata"] = $this->data_model->getl("landpage", array("lp_id" , 1) );
        $this->load->view("panal/paagraphs", $data);
    }

    
    
    public function staticsar()
    {
        
        $data["pdata"] = $this->data_model->getl("landpage", array("lp_id" , 2) );
        $this->load->view("panal/paagraphsar", $data);
    } 
    
    public function landpageedit(){
        
        
        $income = $this->input->post("tochange");
        $value = $this->input->post("greay");
        
        $dataMap = array(
                        "title" => "lp_t1",
                        "title2" => "lp_t2",
                        "uk-sub-title-small" => "lp_st1" ,
                        "st2" => "lp_st2" ,
                        "st3" => "lp_st3" ,
                        "st4" => "lp_st4" ,
                        "st5" => "lp_st5" ,
                        "p1" => "lp_p1" ,
                        "p2" => "lp_p2" ,
                        "p5" => "lp_p3" ,
                        "p6" => "lp_p4" ,
                        "p7" => "lp_p5" ,
                        "p3" => "lp_im1" 
                );
        
        
        
        
        $ref = 1;//$this->input->post("ref");
        $data[$dataMap[$income]] = $value;
        
       if($this->data_model->edit($data, "landpage", "lp_id", $ref))
                {
                    echo "Done";
                }else{
                    echo "Edit Error";
                }
        
        
        
    }

    
     public function landpageeditar(){
        
        
        $income = $this->input->post("tochange");
        $value = $this->input->post("greay");
        
        $dataMap = array(
                        "title" => "lp_t1",
                        "title2" => "lp_t2",
                        "uk-sub-title-small" => "lp_st1" ,
                        "st2" => "lp_st2" ,
                        "st3" => "lp_st3" ,
                        "st4" => "lp_st4" ,
                        "st5" => "lp_st5" ,
                        "p1" => "lp_p1" ,
                        "p2" => "lp_p2" ,
                        "p5" => "lp_p3" ,
                        "p6" => "lp_p4" ,
                        "p7" => "lp_p5" ,
                        "p3" => "lp_im1" 
                );
        
        
        
        
        $ref = 2;//$this->input->post("ref");
        $data[$dataMap[$income]] = $value;
        
       if($this->data_model->edit($data, "landpage", "lp_id", $ref))
                {
                    echo "Done";
                }else{
                    echo "Edit Error";
                }
        
        
        
    }
    public function contact() {
        $data["ux"] = "company";
        $data['method'] = "qcms/contact";
        $data["winhand"] = "contact";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "contacts";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;


        $input['ur']["contacts_lang_id"]["label"] = "Language of item ";
        $input['ur']["contacts_lang_id"]["type"] = "select";
        $input['ur']["contacts_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        $rand = rand(1, 99) . "_";
        $input['ur']["contacts_hashkey"]["type"] = "hidden";
        $input['ur']["contacts_hashkey"]["default"] = uniqid($rand);

        $input['ur']["contacts_title"]["label"] = "contact title";
        $input['ur']["contacts_title"]["type"] = "text";
        $input['ur']["contacts_title"]["model"] = "false";


        $input['ur']["contacts_pragraph"]["label"] = "contact pragraph";
        $input['ur']["contacts_pragraph"]["type"] = "textarea";
        $input['ur']["contacts_pragraph"]["model"] = "false";


        $input['ur']["contacts_phone"]["label"] = "contact phone";
        $input['ur']["contacts_phone"]["type"] = "text";
        $input['ur']["contacts_phone"]["model"] = "false";

        $input['ur']["contacts_email"]["label"] = "contact Email";
        $input['ur']["contacts_email"]["type"] = "text";
        $input['ur']["contacts_email"]["model"] = "false";






        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));


        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "title", "phone", "email");
        $condation = array("is_orginal", "", 1);
        $conx["data"] = $this->data_model->GetCondiction("contacts");
        $conx["primKEY"] = "contacts_id";
        $conx["tabeCOLS"] = array("contacts_id", "contacts_title", "contacts_phone", "contacts_email");
        $conx["tableName"] = "contacts";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function product() {
        $data["ux"] = "company";
        $data['method'] = "qcms/product";
        $data["winhand"] = "product";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "programes";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;




        $input['ur']["programes_lang_id"]["label"] = "Language of item ";
        $input['ur']["programes_lang_id"]["type"] = "select";
        $input['ur']["programes_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");



        $input['ur']["programes_cat_id"]["label"] = "category";
        $input['ur']["programes_cat_id"]["type"] = "select";
        $input['ur']["programes_cat_id"]["model"] = $this->data_model->Get_ForSelect("category", "category_id", "category_name", FALSE);


        $input['ur']["programes_crm_id"]["label"] = "CRM ID#";
        $input['ur']["programes_crm_id"]["type"] = "text";
        $input['ur']["programes_crm_id"]["model"] = "false";
        
        $input['ur']["programes_name"]["label"] = "programes name";
        $input['ur']["programes_name"]["type"] = "text";
        $input['ur']["programes_name"]["model"] = "false";

        $input['ur']["programes_desc"]["label"] = "programes discription";
        $input['ur']["programes_desc"]["type"] = "textarea";
        $input['ur']["programes_desc"]["model"] = "false";

      

        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));


        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "CRM ID" , "programes name");

        $conx["data"] = $this->data_model->GetCondiction("programes");
        $conx["primKEY"] = "programes_id";
        $conx["tabeCOLS"] = array("programes_id","programes_crm_id" ,"programes_name");
        $conx["tableName"] = "programes";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function cat() {
        $data["ux"] = "company";
        $data['method'] = "qcms/cat";
        $data["winhand"] = "cat";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "category";



        $input['ur']["category_lang_id"]["label"] = "Language of item ";
        $input['ur']["category_lang_id"]["type"] = "select";
        $input['ur']["category_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");



        $input['ur']["category_pcategory_id"]["label"] = "Main category";
        $input['ur']["category_pcategory_id"]["type"] = "select";
        $input['ur']["category_pcategory_id"]["model"] = $this->data_model->Get_ForSelect("pcategory", "pcategory_id", "pcategory_name");


        $input['ur']["category_crm_id"]["label"] = "CRM ID";
        $input['ur']["category_crm_id"]["type"] = "text";
        $input['ur']["category_crm_id"]["model"] = "false";
        
        $input['ur']["category_name"]["label"] = "Category Name";
        $input['ur']["category_name"]["type"] = "text";
        $input['ur']["category_name"]["model"] = "false";


        $input['ur']["category_disc"]["label"] = "category description";
        $input['ur']["category_disc"]["type"] = "textarea";
        $input['ur']["category_disc"]["model"] = "false";

        $input['ur']["category_img"]["label"] = "Image";
        $input['ur']["category_img"]["type"] = "text";
        $input['ur']["category_img"]["model"] = "false";
        $input['ur']["category_img"]["atrr"] = "class ='img' id ='img'";


        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "name", "description");

        $conx["data"] = $this->data_model->GetCondiction("category");
        $conx["primKEY"] = "category_id";
        $conx["tabeCOLS"] = array("category_id", "category_name", "category_disc");
        $conx["tableName"] = "category";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    
    
    
    
    public function city() {
        $data["ux"] = "company";
        $data['method'] = "qcms/city";
        $data["winhand"] = "city";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "citys";


        $input['ur']["citys_name"]["label"] = "City name";
        $input['ur']["citys_name"]["type"] = "text";
        $input['ur']["citys_name"]["model"] = "false";
        
        $input['ur']["citys_name_ar"]["label"] = "City arabic Name";
        $input['ur']["citys_name_ar"]["type"] = "text";
        $input['ur']["citys_name_ar"]["model"] = "false";


      

        $input['ur']["img_path"]["label"] = "Image for city";
        $input['ur']["img_path"]["type"] = "text";
        $input['ur']["img_path"]["model"] = "false";
        $input['ur']["img_path"]["atrr"] = "class ='img' id ='img'";


        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Name", "Arabic name");

        $conx["data"] = $this->data_model->GetCondiction("citys");
        $conx["primKEY"] = "citys_id";
        $conx["tabeCOLS"] = array("citys_id", "citys_name", "citys_name_ar" );
        $conx["tableName"] = "citys";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }
    
    
    
    public function pcat() {
        $data["ux"] = "company";
        $data['method'] = "qcms/pcat";
        $data["winhand"] = "pcat";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "pcategory";



        $input['ur']["pcategory_lang_id"]["label"] = "Language of item ";
        $input['ur']["pcategory_lang_id"]["type"] = "select";
        $input['ur']["pcategory_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");


        $input['ur']["pcategory_name"]["label"] = "Main Category Name";
        $input['ur']["pcategory_name"]["type"] = "text";
        $input['ur']["pcategory_name"]["model"] = "false";




        $input['ur']["pcategory_img"]["label"] = "Image";
        $input['ur']["pcategory_img"]["type"] = "text";
        $input['ur']["pcategory_img"]["model"] = "false";
        $input['ur']["pcategory_img"]["atrr"] = "class ='img' id ='img'";


        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "name");

        $conx["data"] = $this->data_model->GetCondiction("pcategory");
        $conx["primKEY"] = "pcategory_id";
        $conx["tabeCOLS"] = array("pcategory_id", "pcategory_name");
        $conx["tableName"] = "pcategory";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function supcat() {
        $data["ux"] = "company";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $data['method'] = "qcms/supcat";
        $data["winhand"] = "supcat";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "subcats";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;


        $input['ur']["subcats_lang_id"]["label"] = "Language of item ";
        $input['ur']["subcats_lang_id"]["type"] = "select";
        $input['ur']["subcats_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        $expecteds = array('is_orginal', 1);
        $input['ur']["subcats_cat_id"]["label"] = "Category name";
        $input['ur']["subcats_cat_id"]["type"] = "select";
        $input['ur']["subcats_cat_id"]["model"] = $this->data_model->Get_ForSelect("cat", "cat_id", "cat_name", $expecteds);

        $input['ur']["subcats_name"]["label"] = " subcategory name";
        $input['ur']["subcats_name"]["type"] = "text";
        $input['ur']["subcats_name"]["model"] = "false";

        $rand = rand(1, 99) . "_";
        $input['ur']["subcats_hashkey"]["type"] = "hidden";
        $input['ur']["subcats_hashkey"]["default"] = uniqid($rand);

        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));


        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "sub name");
        $condation = array("is_orginal", "", 1);
        $conx["data"] = $this->data_model->GetCondiction("subcats");
        $conx["primKEY"] = "subcats_id";
        $conx["tabeCOLS"] = array("subcats_id", "subcats_name");
        $conx["tableName"] = "subcats";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function supcattr() {
        $data["ux"] = "company";
        $data['method'] = "qcms/supcattr";
        $data["winhand"] = "supcattr";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "subcats";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 0;


        $input['ur']["subcats_lang_id"]["label"] = "Language of item ";
        $input['ur']["subcats_lang_id"]["type"] = "select";
        $input['ur']["subcats_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        $expected = array('is_orginal', 0);
        $input['ur']["subcats_cat_id"]["label"] = "Category name";
        $input['ur']["subcats_cat_id"]["type"] = "select";
        $input['ur']["subcats_cat_id"]["model"] = $this->data_model->Get_ForSelect("cat", "cat_id", "cat_name", $expected);

        $input['ur']["subcats_name"]["label"] = " subcategory name";
        $input['ur']["subcats_name"]["type"] = "text";
        $input['ur']["subcats_name"]["model"] = "false";
        $expecteds = array('is_orginal', 1);
        $input['ur']["subcats_hashkey"]["label"] = "transleat for subcategory";
        $input['ur']["subcats_hashkey"]["type"] = "select";
        $input['ur']["subcats_hashkey"]["model"] = $this->data_model->Get_ForSelect("subcats", "subcats_hashkey", "subcats_name", $expecteds);


        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));


        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "sub name");
        $condation = array("is_orginal", "", 0);
        $conx["data"] = $this->data_model->GetCondiction("subcats");
        $conx["primKEY"] = "subcats_id";
        $conx["tabeCOLS"] = array("subcats_id", "subcats_name");
        $conx["tableName"] = "subcats";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function supscription() {

        $data['method'] = "qcms/supscription";
        $data["winhand"] = "supscription";



        $data["ux"] = "company";
        //$config[1] = "UX";
        // $this->load->library("ux", $config);
        $data["form"] = '';
        $data["csv"] = $this->data_model->getcol("supscripers", "supscripers_email");




        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "supscription name", "supscription email", "supscription Phone", "supscription time");
        $conx["data"] = $this->data_model->get("supscripers");
        $conx["primKEY"] = "supscripers_id";
        $conx["tabeCOLS"] = array("supscripers_id", "supscripers_name", "supscripers_email", "supscripers_phone", "supscripers_regtime");
        $conx["tableName"] = "supscripers";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = FALSE;


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function onlineorders() {

        $data['method'] = "qcms/onlineorders";
        $data["winhand"] = "onlineorders";



        $data["ux"] = "company";
        //$config[1] = "UX";
        // $this->load->library("ux", $config);
        $data["form"] = '';
        $data["csv"] = $this->data_model->getcol("onlineorders", "onlineorders_email");




        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "client name", "client  email", "client Phone", "product name", "client message", "request time");
        $conx["data"] = $this->data_model->get("onlineorders");
        $conx["primKEY"] = "onlineorders_id";
        $conx["tabeCOLS"] = array("onlineorders_id", "onlineorders_name", "onlineorders_email", "onlineorders_phone", "onlineorders_product_name", "onlineorders_msg", "onlineorders_time");
        $conx["tableName"] = "onlineorders";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = FALSE;


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function sociallinks() {
        
        
        $x = array(
            "uk-icon-behance"=> "behance" ,
            "uk-icon-vk"=> "vk" ,
            "uk-icon-youtube"=> "youtube" ,
            "uk-icon-facebook"=> "facebook" ,
            "uk-icon-linkedin"=> "linkedin" ,
            "uk-icon-twitter"=> "twitter" ,
            "uk-icon-instagram"=> "instagram" ,
            
            
        );
        $data['method'] = "qcms/sociallinks";
        $data["winhand"] = "sociallinks";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "social_links";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["social_imgepath"]["label"] = "Icon ";
        $input['ur']["social_imgepath"]["type"] = "select";
        $input['ur']["social_imgepath"]["model"] = $x;
        
        $input['ur']["social_link"]["label"] = " LINK ";
        $input['ur']["social_link"]["type"] = "text";
        $input['ur']["social_link"]["model"] = "false";

        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));




        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "LINK");
        $conx["data"] = $this->data_model->get("social_links");
        $conx["primKEY"] = "social_id";
        $conx["tabeCOLS"] = array("social_id", "social_link");
        $conx["tableName"] = "social_links";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function qcmsslide() {


        $data['method'] = "qcms/qcmsslide";
        $data["winhand"] = "qcmsslide";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "searchbox";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        
        $input['ur']["searchbox_title"]["label"] = " Title text ";
        $input['ur']["searchbox_title"]["type"] = "text";
        $input['ur']["searchbox_title"]["model"] = "false";

        $input['ur']["searchbox_bg"]["label"] = "image of bg";
        $input['ur']["searchbox_bg"]["type"] = "text";
        $input['ur']["searchbox_title"]["model"] = "false";
        $input['ur']["searchbox_bg"]["atrr"] = "class ='img' id ='img_1'";

      //  $input['ur']["searchbox_p1_i"]["label"] = " Pragaph 1 imge ";
       // $input['ur']["searchbox_p1_i"]["type"] = "text";
        //$input['ur']["searchbox_p1_i"]["model"] = "false";
        //$input['ur']["searchbox_p1_i"]["atrr"] = "class ='img' id ='img_2'";

        
        $input['ur']["searchbox_p1_t"]["label"] = " Tect body ";
        $input['ur']["searchbox_p1_t"]["type"] = "textarea";
        $input['ur']["searchbox_p1_t"]["model"] = "false";
        

        
        
        
        
//         $input['ur']["searchbox_p2_i"]["label"] = " Pragaph 2 imge ";
//        $input['ur']["searchbox_p2_i"]["type"] = "text";
//        $input['ur']["searchbox_p2_i"]["model"] = "false";
//        $input['ur']["searchbox_p2_i"]["atrr"] = "class ='img' id ='img_3'";
//
//        
//        $input['ur']["searchbox_p2_t"]["label"] = " Pragaph 2 TEXT ";
//        $input['ur']["searchbox_p2_t"]["type"] = "text";
//        $input['ur']["searchbox_p2_t"]["model"] = "false";
//        
//
//        
//        
//         $input['ur']["searchbox_p3_i"]["label"] = " Pragaph 3 imge ";
//        $input['ur']["searchbox_p3_i"]["type"] = "text";
//        $input['ur']["searchbox_p3_i"]["model"] = "false";
//        $input['ur']["searchbox_p3_i"]["atrr"] = "class ='img' id ='img_4'";
//
//        
//        $input['ur']["searchbox_p3_t"]["label"] = " Pragaph 3 TEXT ";
//        $input['ur']["searchbox_p3_t"]["type"] = "text";
//        $input['ur']["searchbox_p3_t"]["model"] = "false";
//        

        
        
        
        

       

        $input['ur']["searchbox_lang"]["label"] = " Language Of slide ";
        $input['ur']["searchbox_lang"]["type"] = "select";
        $input['ur']["searchbox_lang"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");
        //$this->data_model->Get_ForSelect("categories", "id", "name");


       


        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));




        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Title");
        $conx["data"] = $this->data_model->get("searchbox");
        $conx["primKEY"] = "searchbox_id";
        $conx["tabeCOLS"] = array("searchbox_id", "searchbox_title");
        $conx["tableName"] = "searchbox";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function aboutusen() {
        $data['method'] = "qcms/aboutusen";
        $data["winhand"] = "aboutusen";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "about";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";



        $input['ur']["lang_id"]["label"] = "Language of item ";
        $input['ur']["lang_id"]["type"] = "select";
        $input['ur']["lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        //$input['ur']["about_hashkey"]["label"] = " Name ";
        $rand = rand(1, 99) . "_";
        $input['ur']["about_hashkey"]["type"] = "hidden";
        $input['ur']["about_hashkey"]["default"] = uniqid($rand);

        $input['ur']["about_is_orginal"]["type"] = "hidden";
        $input['ur']["about_is_orginal"]["default"] = 1;

        $input['ur']["about_title"]["label"] = "Pragraph Title ";
        $input['ur']["about_title"]["type"] = "text";
        $input['ur']["about_title"]["model"] = "false";


        $input['ur']["about_pragraph"]["label"] = " text body ";
        $input['ur']["about_pragraph"]["type"] = "textarea";
        $input['ur']["about_pragraph"]["model"] = "false";
        
        $input['ur']["about_order"]["label"] = " Order like 1 , 2 , 3 .... ";
        $input['ur']["about_order"]["type"] = "textarea";
        $input['ur']["about_order"]["model"] = "false";
        


        $input['ur']["about_imge_path"]["label"] = "image of Pragraph";
        $input['ur']["about_imge_path"]["type"] = "text";
        $input['ur']["about_imge_path"]["model"] = "false";
        $input['ur']["about_imge_path"]["atrr"] = "class ='img' id ='img'";

      



        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));




        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Title");

        $conx["data"] = $this->data_model->GetCondiction("about");
        $conx["primKEY"] = "about_id";
        $conx["tabeCOLS"] = array("about_id", "about_title");
        $conx["tableName"] = "about";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function mlist() {


        $data['method'] = "qcms/mlist";
        $data["winhand"] = "mlist";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "mlist";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";



        $input['ur']["mlist_lang_id"]["label"] = "Language of item ";
        $input['ur']["mlist_lang_id"]["type"] = "select";
        $input['ur']["mlist_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        //$input['ur']["about_hashkey"]["label"] = " Name ";
        $rand = rand(1, 99) . "_";
        $input['ur']["mlist_hashkey"]["type"] = "hidden";
        $input['ur']["mlist_hashkey"]["default"] = uniqid($rand);

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;

        $input['ur']["mlist_text"]["label"] = " display name on site list";
        $input['ur']["mlist_text"]["type"] = "text";
        $input['ur']["mlist_text"]["model"] = "false";


        $input['ur']["mlist_link"]["label"] = "linked to";
        $input['ur']["mlist_link"]["type"] = "select";
        $input['ur']["mlist_link"]["model"] = array(
            "defaults" => "qcms page",
            "about" => "About page",
            "articles" => "articles page",
            "contact" => "Contact page ",
            "products" => "products page",
            "brands" => "brands page",
            "jobs" => "jobs page",
            "category" => "category"
        );






        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));




        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "page name", "linke");
        $condation = FALSE;
        $conx["data"] = $this->data_model->GetCondiction("mlist");
        $conx["primKEY"] = "mlist_id";
        $conx["tabeCOLS"] = array("mlist_id", "mlist_text", "mlist_link");
        $conx["tableName"] = "mlist";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function language() {
        $data['method'] = "qcms/language";
        $data["winhand"] = "language";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "languge";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";



        $input['ur']["lang_name"]["label"] = " Name ";
        $input['ur']["lang_name"]["type"] = "text";
        $input['ur']["lang_name"]["model"] = "false";

        $input['ur']["lang_orintation"]["label"] = " Orientation ";
        $input['ur']["lang_orintation"]["type"] = "select";
        $input['ur']["lang_orintation"]["model"] = array("ltr" => "left to right", "rtl" => "right to left");







        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));




        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "name");
        $conx["data"] = $this->data_model->get("languge");
        $conx["primKEY"] = "lang_id";
        $conx["tabeCOLS"] = array("lang_id", "lang_name");
        $conx["tableName"] = "languge";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";


        $this->load->library("uxdata", $conx);
        $data["gread"] = $this->uxdata->render();



        $this->load->view("ui", $data);

        unset($data);
    }

    public function galaryview() {
        $data["imgs"] = $this->data_model->get("files");
        $data["ux"] = "gview";
        $this->load->view("ui", $data);
    }

    
     public function galaryview2() {
        $data["imgs"] = $this->data_model->get("files");
        $data["ux"] = "gview2";
        $this->load->view("ui", $data);
    }
    public function uploader() {
        $data['method'] = "qcms/uploader";
        $data["winhand"] = "uploader";
        $f = "<form   action=\"qcms/uploadfile\"  method=\"post\" enctype=\"multipart/form-data\">
                <input  type=\"text\" name=\"junk\" id=\"junk\" value=\"\" placeholder='resize width' />
                <input type=\"file\" name=\"userfile\" id=\"userfile\" size=\"100\" />
                <button class='btn btn-primary' type=\"submit\" id=\"buttonUpload\" > Upload</button>
                </form>";




           
       
      
        $data["form"] = $f;





        $data["gread"] = "";


        $data["imges"] = $this->data_model->getallimges();
        
         $data["ux"] = "fupload";
        $this->load->view("ui", $data);

        
    }
    
     public function deleteimg(){
        $pid = $this->input->post("iid");
        $dbin = $this->input->post("dbin");
        $path = FCPATH."uploads/";
        $spath = FCPATH."uploads/small/";
        
        @unlink($path.$dbin); 
        @unlink($spath.$dbin); 
        
        $this->data_model->delete("files" , "pic_id" , $pid);
        
    }

    public function uploadfile() {
        $data["user"] = $this->user;
        $data["username"] = $this->userName;
        $dir = base_url() . 'uploads';
        $this->load->model("uploads");
        echo $this->uploads->upfile();
    }

    public function ToEdite() {

        $into = $this->input->post("UX_into");
        $col = $this->input->post("col");
        $item = $this->input->post("item");
        $datas = $this->data_model->GetThis($into, $col, $item);

        $out = null;
        foreach ($datas as $d) {
            foreach ($d as $col => $val) {
                //$v = htmlentities($val);
                $v = str_replace(",", ".", $val);
                $out .= "$col ::-> $v , ";
            }
        }
        echo $out;
    }

    public function exe() {
        $this->dbMAP;
        $into = $this->input->post("UX_into");
        $todo = $this->input->post("UX_todo");
        $brefix = "UX_";
        $skw = "";
        switch ($todo) {
            case "add":
                foreach ($this->dbMAP[$into] as $coulmns) {

                    //__prossestext
                    $input = trim($this->input->post($brefix . $coulmns));
                    if ($into == "programes") {
                        $skw .= $this->__prossestext($input);
                    }
                    if ($input != null) {
                        if ($coulmns == 'userpassword') {
                            $data[$coulmns] = md5($input);
                        } else {

                            $data[$coulmns] = $input;
                        }
                    }
                }
                if ($into == "programes") {
                    // $skw = $this->__prossestext($skw); 
                    $data["programes_kwords"] = $skw;
                    $xarray = explode(' , ', $skw);
                }

                if (!isset($data) || $data == null || $data == false) {
                    echo "Error no data";
                } else {
                    $this->data_model->add($into, $data);
                    if ($into == "programes") {
                        //$xarray
                        $this->data_model->addw($xarray);
                    }
                    echo "add date +";
                }




                break;
            case "edit":

                $col = $this->input->post("col");
                $item = $this->input->post("item");
                $skw = "";
                foreach ($this->dbMAP[$into] as $coulmns) {

                    $input = trim($this->input->post($brefix . $coulmns));
                    if ($into == "programes") {
                        $skw .= $this->__prossestext($input);
                    }
                    if ($input != null) {
                        if ($coulmns == 'userpassword') {
                            $data[$coulmns] = md5($input);
                        } else {
                            $data[$coulmns] = $input;
                        }

                        if ($into == "programes") {
                            // $skw = $this->__prossestext($skw); 
                            $data["programes_kwords"] = $skw;
                            $xarray = explode(',', $skw);
                            $this->data_model->addw($xarray);
                        }
                    }
                }
                if ($this->data_model->edit($data, $into, $col, $item)) {

                    echo "updated";
                } else {
                    echo "update error";
                }

                break;
            case "delete":

                $col = $this->input->post("col");
                $item = $this->input->post("item");
                if ($into == "sales_quotation_items") {
                    $IDx = array("id", $item);
                    $curntadd = $this->data_model->GetNodeValueForID("sales_quotation_items", $IDx, "Quantity");

                    $itemid = $this->data_model->GetNodeValueForID("sales_quotation_items", $IDx, "Item_ID");

                    $IDx2 = array("Item_ID", $itemid);

                    $curntstoreg = $this->data_model->GetNodeValueForID("item_adjustments_note", $IDx2, "Quantity");

                    $new_storg = $curntstoreg + $curntadd;
                    $x["Quantity"] = $new_storg;
                    $this->data_model->edit($x, "item_adjustments_note", "Item_ID", $itemid);
                }
                if ($into == "purchase_order_entry_item") {
                    $IDx = array("POEID", $item);
                    $curntadd = $this->data_model->GetNodeValueForID("purchase_order_entry_item", $IDx, "Quantity");

                    $itemid = $this->data_model->GetNodeValueForID("purchase_order_entry_item", $IDx, "Item_ID");

                    $IDx2 = array("Item_ID", $itemid);

                    $curntstoreg = $this->data_model->GetNodeValueForID("item_adjustments_note", $IDx2, "Quantity");

                    $new_storg = $curntstoreg - $curntadd;
                    $y["Quantity"] = $new_storg;
                    $this->data_model->edit($y, "item_adjustments_note", "Item_ID", $itemid);
                }

                $this->data_model->delete($into, $col, $item);
                echo "تم المسح";
                break;

            default :
                echo 'ERROR';
                break;
        }
    }

    private function __prossestext($txt) {


        $array1 = explode(" ", $txt);
        $array2 = array();
        foreach ($array1 as $v) {
            if (isset($array2[$v])) {
                
            } else {
                $r = trim($v);
                if ($r != "") {
                    $array2[$r] = $r;
                }
            }
        }
        $longstring = "";
        foreach ($array2 as $k => $v2) {
            $longstring .=" $k ,";
        }


        return $longstring;
    }

    public function logout() {
        $datauser = array("token" => 0, "isonline" => 0);

        $this->authrized->UPDATA_USER_RECORED($this->session->Get_mySession("userName")
                , $this->session->Get_mySession("userKey")
                , $datauser);
        $data = array("username", "userkey", "useros", "bdayah");
        $this->session->Destroy_mySession($data);
        unset($data);
        redirect("login");
    }

}


