<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Logout
 *
 * @author sadeem-pc
 */
class Logout extends MY_Central {
   
    
    public function __construct() {
        parent::__construct();
        
        
    }
    
    public function index(){
            delete_cookie("username_v");
            delete_cookie("userkey_v");
            delete_cookie("useros_v");
            session_destroy();
            redirect('home');
    }
}
