<?php

class login_user  extends CI_Controller {

    public function  __construct() {
        parent::__construct();
         $config['TABLE']      = 'acc_login';
         $config['USERCOL']    = 'username';
         $config['USERIDCOL']  = 'userkey';
         $config['PASSCOL']    = 'userpassword';
         $config['TOKCOL']     = 'token';
         $config['USERSTATUS'] = 'isonline';
         $config['USERIP']     = 'lastip';
         $this->load->library("authrized" , $config );
         //$this->load->model('logg_model');
        //$this->logg_model->inilmap();


    }


    public function index()
    {
        //load login form

        $this->load->view("user_login_view");

    }


    public function getaccess()
    {

        $this->load->model('check_vistor_model');

        if($this->check_vistor_model->chekData())
        {
           foreach($this->check_vistor_model->chekData() as $row)
                {
                    $data["userName_v"]=  $row->username;
                    $data["userKey_v"]=   $row->userkey;
                    $data["userToken_v"] =  md5(rand(10000, 9999999)) ;
                    $data["userLevel_v"] = $row->userlevel;
                    $data["userpass_v"] = $row->userpassword;

                     $updatauser = array(
                                "token"  => $data["userToken_v"] ,
                                "lastip"  => $_SERVER['REMOTE_ADDR'],
                                "isonline"  =>  '1',
                                "accesstime" => date("Y-m-d H:i:s")
                            );
             $this->authrized->UPDATA_USER_RECORED($data["userName_v"] ,$data["userKey_v"] , $updatauser);
             $this->session->Create_mySession($data);
             $chied = $this->encrypt->decode($this->input->post("save"));
        if($chied == 1)
            {

               $cookieuser = array( 'name'   => "username_v" ,
                'value'  => $this->encrypt->encode($row->username),
		'expire' => '86500',
		'path'   => '/',
		);
		$cookieuserk = array( 'name'   => "userkey_v" ,
                'value'  => $this->encrypt->encode($row->userkey),
		'expire' => '86500',
		'path'   => '/',
		);

                $cookieuserkpass = array( 'name'   => "useros_v" ,
                'value'  => $this->encrypt->encode($row->userpassword),
		'expire' => '86500',
		'path'   => '/',
		);

                     set_cookie($cookieuser);
                     set_cookie($cookieuserk);
                     set_cookie($cookieuserkpass);

                    }
                }
                redirect("user");

        }
        else{
            delete_cookie("username_v");
            delete_cookie("userkey_v");
            delete_cookie("useros_v");
            session_destroy();
            $this->load->view("user_login_view");



        }



    
    

}

}
