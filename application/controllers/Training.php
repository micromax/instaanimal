<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Training
 *
 * @author mac 6-5-2019 10:07 PM
 */
class Training extends CI_Controller{
    
    public $langList;
    public $currunt_lang ;
    public $stsicwords;
    public $catlist;
    public $barndlist;
    public $wordofabout40;
    public $slinks;
    public $KV;
    
    public function __construct() {
        parent::__construct();
         $this->load->model("site_model");
        $this->langList = $this->site_model->getLangList();
        $this->currunt_lang = $this->site_model->GetLangRUN();
        $this->stsicwords = $this->site_model->getStaticWords($this->currunt_lang);
        $this->catlist = $this->site_model->genreateCatList($this->currunt_lang);
        $this->barndlist = $this->site_model->genreateBrandlist($this->currunt_lang);
         $this->slinks = $this->site_model->get("social_links" , false , false ,
                 false) ;
        $this->wordofabout40 = $this->site_model->get("about" , $limit = 1 ,
                false , array("lang_id" , $this->currunt_lang));
        //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
        $this->KV =  $this->site_model->get("key_setting" , false , false 
                , array("lang_id" , $this->currunt_lang) , FALSE , FALSE
                , FALSE,FALSE , array("key" , "val")) ;
    }
    
    
    
    
    public function index()
    {
    
        
        
        
         $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data['citys'] = $this->site_model->geycity();
       $joine = array("category" , "category_pcategory_id = pcategory_id" , "left");
       $dax = $this->site_model->get("pcategory" ,  FALSE , array("category_score", "DESC") , FALSE ,FALSE , FALSE ,  $joine) ;
       
       $joins = array("programes" , " events.events_program_id = programes.programes_crm_id  " , "LEFT");
       $gb = array("events.events_program_id" ,"events.events_start" );
       $data["category"] = $this->site_model->get("category");
    //   $data["citys"] = $this->site_model->get("citys");
       
       //$data["last5"] = $this->site_model->get("events" ,   4 , array("events_start" , "DESC" ) , array("programes.programes_lang_id" , $this->currunt_lang) , false ,  FALSE , $joins   , $gb , FALSE);
       $data["sbx"] = $this->site_model->GetSearchBox($this->currunt_lang);
    
        
          foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }

        $x = 0;
        if($this->input->get("page")){
            $x = $this->input->get("page");
        }
        
        $page  = $x * 25;
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       $data["brandist"] = $this->barndlist;
       $data["category"] = $this->site_model->get("category");
      // $data["citys"] = $this->site_model->get("citys");
       
       if($this->input->get("sewo")){
           $data["programes"]  = $this->site_model->search($this->input->get("sewo") , $this->currunt_lang  , $page);
            $data["tt"]= $this->site_model->searchCount($this->input->get("sewo") , $this->currunt_lang);
       }
       
       
       else {

           $data["programes"]  = $this->site_model->search(null , $this->currunt_lang  , $page);
           $data["tt"]= $this->site_model->searchCount(NULL , $this->currunt_lang);
       }
       
       $data["tt"] =  ceil($data["tt"] / 24);
       $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
       $data["selectedcat"] = $this->input->get("sewo");
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
     
        
        
        
      
        
      //$data["bodyview"] = "master/training";
      $this->load->view("Mastertraining" , $data);
    }
    
    
    
}
