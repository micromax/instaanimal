<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of products
 *
 * @author iptv
 */
class products extends CI_Controller {

    public $langList;
   public $currunt_lang ;
   public $stsicwords;
   public $catlist;
   public $barndlist;
   public $wordofabout40;
   public $slinks;

    public function  __construct() {
        parent::__construct();
         $this->load->model("site_model");
         $this->load->model("check_user_model");
         
        $this->langList = $this->site_model->getLangList();
        $this->currunt_lang = $this->site_model->GetLangRUN();
        $this->stsicwords = $this->site_model->getStaticWords($this->currunt_lang);
        $this->catlist = $this->site_model->genreateCatList($this->currunt_lang);
        $this->barndlist = $this->site_model->genreateBrandlist($this->currunt_lang);
         $this->slinks = $this->site_model->get("social_links" , false , false , false) ;
        $this->wordofabout40 = $this->site_model->get("about" , $limit = 1 , false , array("lang_id" , $this->currunt_lang));
        //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
    }

    public function index()
    {
    /*
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       $data["brandist"] = $this->barndlist;
       $data["arts"] = $this->site_model->get("articls" , false , false , array("articls_lang_id" , $this->currunt_lang));
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
       if($this->wordofabout40 != false){
       foreach($this->wordofabout40 as $word){}

       $data["aboutword"] = word_limiter($word->about_pragraph , 45) ;
       }else
           {
           $data["aboutword"] = "no data";
       }
       $data["bodyview"] = "master/artmain";
       $this->load->view("master" , $data);
     
     */
        redirect('defaults');

    }

    public function viewproduct()
    {
       $item = $this->uri->segment(3);
       $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       $data["brandist"] = $this->barndlist;
       $data["products"] = $this->site_model->get("products" , false , false , array("products_lang_key" , $this->currunt_lang) , array( "products_name", urldecode($item)));
       $data["spacification"] = $this->site_model->getsp($item , $this->currunt_lang);
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
       foreach ($data["products"] as $k=>$v){}
        $data["kw"]  =   wordMR($v->products_desc);
        $data["dic"] = $v->products_desc;
        $data["name"] = $v->products_name;
       if($this->wordofabout40 != false){
       foreach($this->wordofabout40 as $word){}

       $data["aboutword"] = word_limiter($word->about_pragraph , 45) ;
       }else
           {
           $data["aboutword"] = "no data";
       }
       $data["bodyview"] = "master/productview";
       $this->load->view("master" , $data);
    }
    
    
    public function viewplans()
    {
       $item = $this->uri->segment(3);
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       $data["brandist"] = $this->barndlist;
       $data["plans"] = $this->site_model->getplans($item , $this->currunt_lang);
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
      
        if($this->wordofabout40 != false){
       foreach($this->wordofabout40 as $word){}

       $data["aboutword"] = word_limiter($word->about_pragraph , 45) ;
       }else
           {
           $data["aboutword"] = "no data";
       }
       
         $data["bodyview"] = "master/planview";
       $this->load->view("master" , $data);
       
        
    }
    
    
    public function order(){
       $isuser = $this->check_user_model->checkQuserSession();
        $planid = $this->uri->segment(3);
        $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
        if($isuser != TRUE){
      
       
       $param =  array("planid"=> $planid);
       
       $this->session->Create_mySession($param);
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       $data["brandist"] = $this->barndlist;
        $data["mlist"] = $this->site_model->getList($this->currunt_lang);
      
        if($this->wordofabout40 != false){
       foreach($this->wordofabout40 as $word){}

       $data["aboutword"] = word_limiter($word->about_pragraph , 45) ;
       }else
           {
           $data["aboutword"] = "no data";
       }
       
       $data["bodyview"] = "master/orderview";
       
       $this->load->view("master2" , $data);
        }  else {
           redirect("user/dashboard?newpk=".$planid);  
        }
        
    }

}