<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of home
 *
 * @author sadeem-pc
 */

class Home extends MY_Central{
    public function __construct() {
        parent::__construct();
    }
    
    
    public function index(){
       
      
        define('THEME','mosh');
        
        //$this->Templets
        	$this->data = '';				
		$this->template
                        ->set_partial("header" , "partials/header" , $this->data)
                        ->set_partial("welcome" , "partials/welcome" , $this->data)
                        ->set_partial("menu" , "partials/menu" , $this->data)
                        ->set_partial("footer" , "partials/footer" , $this->data)
			->set_layout('layouts/main')
			->enable_parser(FALSE)
			->title("CodeIgniter Mutiple Themes")			
			->build('homepage/index', $this->data);
              //  var_dump($this->template);
    }
}
