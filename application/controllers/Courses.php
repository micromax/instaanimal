<?php


class Courses extends CI_Controller {
   
       public $langList;
   public $currunt_lang ;
   public $stsicwords;
   public $catlist;
   public $barndlist;
   public $wordofabout40;
   public $slinks;
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model("site_model");
         //$this->load->model("Seo");
        $this->langList = $this->site_model->getLangList();
        $this->currunt_lang = $this->site_model->GetLangRUN();
        $this->stsicwords = $this->site_model->getStaticWords($this->currunt_lang);
        $this->catlist = $this->site_model->genreateCatList($this->currunt_lang);
        $this->barndlist = $this->site_model->genreateBrandlist($this->currunt_lang);
        $this->slinks = $this->site_model->get("social_links" , false , false , false) ;
        $this->wordofabout40 = $this->site_model->get("about" , $limit = 1 , false , array("lang_id" , $this->currunt_lang));
        
        $this->KV =  $this->site_model->get("key_setting" , false , false , array("lang_id" , $this->currunt_lang) , FALSE , FALSE, FALSE,FALSE , array("key" , "val")) ;

    }
    
    
    
    public function index()
    {
        $data["vtype"] = "g";
        if($this->input->get("v"))
        {
            $data["vtype"]  = $this->input->get("v");
        }
       
        
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data['citys'] = $this->site_model->geycity();
       $joine = array("category" , "category_pcategory_id = pcategory_id" , "left");
       $dax = $this->site_model->get("pcategory" ,  FALSE , array("category_score", "DESC") , FALSE ,FALSE , FALSE ,  $joine) ;
       
       $joins = array("programes" , " events.events_program_id = programes.programes_crm_id  " , "LEFT");
       $gb = array("events.events_program_id" ,"events.events_start" );
       $data["category"] = $this->site_model->get("category");
    //   $data["citys"] = $this->site_model->get("citys");
       
       //$data["last5"] = $this->site_model->get("events" ,   4 , array("events_start" , "DESC" ) , array("programes.programes_lang_id" , $this->currunt_lang) , false ,  FALSE , $joins   , $gb , FALSE);
       $data["sbx"] = $this->site_model->GetSearchBox($this->currunt_lang);
     $data["expage"] = $this->site_model->get("pages" , null , NULL , array("pages.pages_lang" , $this->currunt_lang));
        
          foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }

        $x = 0;
        if($this->input->get("page")){
            $x = $this->input->get("page");
        }
        
        $page  = $x * 25;
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       $data["brandist"] = $this->barndlist;
       $data["category"] = $this->site_model->get("category");
      // $data["citys"] = $this->site_model->get("citys");
       
       if($this->input->get("sewo")){
           $data["programes"]  = $this->site_model->SerachAlgorism($this->input->get("sewo") , $this->currunt_lang  , $page);
            $data["tt"]= $this->site_model->SerachAlgorismCounter($this->input->get("sewo") , $this->currunt_lang);
       }
       
       
       else {

           $data["programes"]  = $this->site_model->SerachAlgorism(null , $this->currunt_lang  , $page);
           $data["tt"]= $this->site_model->SerachAlgorismCounter(NULL , $this->currunt_lang);
       }
       
       $data["tt"] =  ceil($data["tt"] / 25);
       $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
       $data["selectedcat"] = $this->input->get("sewo");
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
       
       
       //$data["bodyview"] = "master/courses";
       
       $data["siteconfig"] = $this->site_model->Getwebsite();
       $data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
       $data["smallfooter"] = $this->site_model->Getsmallfooter($this->currunt_lang);
    
        $this->load->view("Masteritemnew" , $data);

        
    }
    
    
    

    
    
    public function v()
    {
     

        foreach($this->KV as $value)
            {
                $data["$value->key"] = $value->val ;
            }



                $item = $this->uri->segment(3);
                $uuid = $this->input->get("uuid");
                $j = array("events" , "events.events_program_id=programes.programes_id" ,"left");
                //array("programes_lang_id" , $this->currunt_lang)
                $data["programes"] = $this->site_model->get("programes" , 1 , FALSE , FALSE , array("programes_id" ,$uuid) , FALSE );

                $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
                $catname = "";
                $crm_id = "";

                //$catx = $this->site_model->get("category" , false , false , array("category_lang_id" , $this->currunt_lang) , array("category_id" , $catid));
                 $logTx = "";   
                foreach ( $data["programes"] as  $va) 
                    {
                        $data["name"] = $va->programes_name;
                        $crm_id = $va->programes_crm_id;
                        $logTx .=" ".$va->programes_name." ".$va->programes_name_ar." ".$va->programes_desc." ".$va->programes_desc_ar." ". $va->programes_aginda ." " .$va->programes_extra;
                    }

               $data["evntlist"] = $this->site_model->get("events" , false , array("events_start" , "ASC") , array("events_program_id" , $crm_id) ,
                       array("events_start >=" , date("Y-m-d"))
                       ); 
 $data['citys'] = $this->site_model->geycity();
               $data["catdisc"]  = $catname;
               $data["lang"] = $this->langList;
               $data["lid"] = $this->currunt_lang;
               $data["catlist"] = $this->catlist;
               //get static
               $data["slinks"] = $this->slinks;
               $data["sw"] = $this->stsicwords;
               $data["selectedcat"] = $item; //??
                 $data["category"] = $this->site_model->get("category");

              $data["seo"] = prossestext($logTx);

               $data["mlist"] = $this->site_model->getList($this->currunt_lang);

               $data["expage"] = $this->site_model->get("pages" , null , NULL , array("pages.pages_lang" , $this->currunt_lang)); 
                $data["expage"] = $this->site_model->get("pages" , null , NULL , array("pages.pages_lang" , $this->currunt_lang));
               $data["siteconfig"] = $this->site_model->Getwebsite();
                $data["FooterSections"] = $this->site_model->Getfootersection($this->currunt_lang);
                $data["smallfooter"] = $this->site_model->Getsmallfooter($this->currunt_lang);
             $data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
             if($this->currunt_lang == 1)
             {
                 $data["disks"] = word_limiter(strip_tags($va->programes_desc) , 60);
             }  else {
                 $data["disks"] =  word_limiter(strip_tags($va->programes_desc_ar) , 60);
             }
             
             
               $data["bodyview"] = "master/evview";
               //$this->load->view("master" , $data);
               $this->load->view("Masterproduct" , $data);
               

    }

    

    public function more(){
        
        
        
   
       
       
       
    }
    
    
    
    
}
