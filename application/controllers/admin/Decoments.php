<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Aboutusen
 *
 * @author sadeem-pc
 */
class Decoments  extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    public function index(){
                $data["username"] = $this->userName;

        
        $data['method'] = "qcms/decoments";
        $data["winhand"] = "decoments";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "decoments";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/decoments/manage";

        
        

        $input['ur']["decoments_lang"]["label"] = "Language of item ";
        $input['ur']["decoments_lang"]["type"] = "select";
        $input['ur']["decoments_lang"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

 

 
        $input['ur']["decoments_name"]["label"] = "Decoments name ";
        $input['ur']["decoments_name"]["type"] = "text";
        $input['ur']["decoments_name"]["model"] = "false";


        $input['ur']["decoments_url"]["label"] = " URL  ";
        $input['ur']["decoments_url"]["type"] = "text";
        $input['ur']["decoments_url"]["model"] = "false";
        //$input['ur']["about_pragraph"]["atrr"] = "class='textarea'";
        
      

        
        
        if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
           // $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "decoments_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("decoments", "decoments_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }


        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));



        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
        //$this->load->view("admin", $data);
    }
    
    
    
    public function manage(){
                $data["username"] = $this->userName;
    $data['method'] = "qcms/decoments";
        $data["winhand"] = "decoments";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "name" , "url");

        $conx["data"] = $this->data_model->GetCondiction("decoments");
        $conx["primKEY"] = "decoments_id";
        $conx["tabeCOLS"] = array("decoments_id", "decoments_name" , "decoments_url");
        $conx["tableName"] = "decoments";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/decoments";

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);


    }
    //put your code here
}
