<?php


class Events extends MY_Controller{
 
    
    
    public function __construct() {
        parent::__construct();
    }

    

    public function index() {
        
        
          $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "admin/Events";
        $data["winhand"] = "events";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "events";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;


        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Events/manage";


        $input['ur']["events_program_id"]["label"] = " the Program";
        $input['ur']["events_program_id"]["type"] = "select";
        $input['ur']["events_program_id"]["model"] = $this->data_model->Get_ForSelect("programes", "programes_crm_id", "programes_name");



        $input['ur']["events_start"]["label"] = "Date Start";
        $input['ur']["events_start"]["type"] = "text";
        $input['ur']["events_start"]["model"] = "false";
        
        $input['ur']["events_end"]["label"] = "Date End";
        $input['ur']["events_end"]["type"] = "text";
        $input['ur']["events_end"]["model"] = "false";

        $input['ur']["events_locaion"]["label"] = "Location or City";
        $input['ur']["events_locaion"]["type"] = "text";
        $input['ur']["events_locaion"]["model"] = "false";

      
                 if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
         //   $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "events_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("events", "events_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }

        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
        
        
    }
    
    
    
    public function manage(){
            $data["username"] = $this->userName;
       $data['method'] = "admin/Events";
        $data["winhand"] = "events";
         $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "CRM ID" , "start Date" , "End Date" ,  " Location");

        $condetion =  array ("events_end" , ">"  ,  date("Y-m-d")  );
        $conx["data"] = $this->data_model->GetCondiction("events" , $condetion);
        $conx["primKEY"] = "events_id";
        $conx["tabeCOLS"] = array("events_id","events_program_id" ,"events_start" , "events_end" , "events_locaion");
        
        $conx["tableName"] = "events";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Events";

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
}
