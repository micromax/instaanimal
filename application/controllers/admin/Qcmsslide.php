<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Qcmsslide
 *
 * @author sadeem-pc
 */
class Qcmsslide extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    //put your code here
    
     public function index() {

        $data["username"] = $this->userName;

        $data['method'] = "qcms/qcmsslide";
        $data["winhand"] = "qcmsslide";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "searchbox";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        
        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Qcmsslide/manage";

        
        $input['ur']["searchbox_title"]["label"] = " Title text ";
        $input['ur']["searchbox_title"]["type"] = "text";
        $input['ur']["searchbox_title"]["model"] = "false";

        $input['ur']["searchbox_bg"]["label"] = "image of bg";
        $input['ur']["searchbox_bg"]["type"] = "text";
        $input['ur']["searchbox_title"]["model"] = "false";
        $input['ur']["searchbox_bg"]["atrr"] = "class ='img' id ='img_1'";

      //  $input['ur']["searchbox_p1_i"]["label"] = " Pragaph 1 imge ";
       // $input['ur']["searchbox_p1_i"]["type"] = "text";
        //$input['ur']["searchbox_p1_i"]["model"] = "false";
        //$input['ur']["searchbox_p1_i"]["atrr"] = "class ='img' id ='img_2'";

        
        $input['ur']["searchbox_p1_t"]["label"] = " Tect body ";
        $input['ur']["searchbox_p1_t"]["type"] = "textarea";
        $input['ur']["searchbox_p1_t"]["model"] = "false";
        

        
        
        
        
//         $input['ur']["searchbox_p2_i"]["label"] = " Pragaph 2 imge ";
//        $input['ur']["searchbox_p2_i"]["type"] = "text";
//        $input['ur']["searchbox_p2_i"]["model"] = "false";
//        $input['ur']["searchbox_p2_i"]["atrr"] = "class ='img' id ='img_3'";
//
//        
//        $input['ur']["searchbox_p2_t"]["label"] = " Pragaph 2 TEXT ";
//        $input['ur']["searchbox_p2_t"]["type"] = "text";
//        $input['ur']["searchbox_p2_t"]["model"] = "false";
//        
//
//        
//        
//         $input['ur']["searchbox_p3_i"]["label"] = " Pragaph 3 imge ";
//        $input['ur']["searchbox_p3_i"]["type"] = "text";
//        $input['ur']["searchbox_p3_i"]["model"] = "false";
//        $input['ur']["searchbox_p3_i"]["atrr"] = "class ='img' id ='img_4'";
//
//        
//        $input['ur']["searchbox_p3_t"]["label"] = " Pragaph 3 TEXT ";
//        $input['ur']["searchbox_p3_t"]["type"] = "text";
//        $input['ur']["searchbox_p3_t"]["model"] = "false";
//        

        
        
        
        

       

        $input['ur']["searchbox_lang"]["label"] = " Language Of slide ";
        $input['ur']["searchbox_lang"]["type"] = "select";
        $input['ur']["searchbox_lang"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");
        //$this->data_model->Get_ForSelect("categories", "id", "name");


        
        
    if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
         //   $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "searchbox_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("searchbox", "searchbox_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }
       


        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));



        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
       
    }
    
    
    
    public function manage(){
        
                $data["username"] = $this->userName;

               $data['method'] = "qcms/qcmsslide";
        $data["winhand"] = "qcmsslide";

        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Title");
        $conx["data"] = $this->data_model->get("searchbox");
        $conx["primKEY"] = "searchbox_id";
        $conx["tabeCOLS"] = array("searchbox_id", "searchbox_title");
        $conx["tableName"] = "searchbox";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
         $conx["editUrl"] = "admin/Qcmsslide";

       
        
        
        
            $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
}
