<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Articls
 *
 * @author sadeem-pc
 */
class Articls extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
        public function index() {
                $data["username"] = $this->userName;
    
            
        $data["ux"] = "company";
        $data['method'] = "admin/Articls";
        $data["winhand"] = "Articls";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Articls/manage";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "articls";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;

        $input['ur']["articls_lang_id"]["label"] = "Language of item ";
        $input['ur']["articls_lang_id"]["type"] = "select";
        $input['ur']["articls_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        
        $input['ur']["articls_cat_id"]["label"] = "category of post ";
        $input['ur']["articls_cat_id"]["type"] = "select";
        $input['ur']["articls_cat_id"]["model"] = $this->data_model->Get_ForSelect("category", "category_id", "category_name");

        
        $input['ur']["articls_media_type"]["label"] = "media type ";
        $input['ur']["articls_media_type"]["type"] = "select";
        $input['ur']["articls_media_type"]["model"] = array("text" => "text" ,"video" => "video" , "img" => "img");

        
        
        $input['ur']["articls_class"]["label"] = "articls class ";
        $input['ur']["articls_class"]["type"] = "select";
        $input['ur']["articls_class"]["model"] = array("NORMAL" => "NORMAL", "POPULAR" => "POPULAR" ,"TRENDING" => "TRENDING" , "MOST" => "MOST");

        
        
        $input['ur']["articls_authr"]["label"] = "article author name";
        $input['ur']["articls_authr"]["type"] = "text";
        $input['ur']["articls_authr"]["model"] = "false";

        $rand = rand(1, 99) . "_";
        $input['ur']["articls_hashkey"]["type"] = "hidden";
        $input['ur']["articls_hashkey"]["default"] = uniqid($rand);

        $input['ur']["articls_title"]["label"] = "article title";
        $input['ur']["articls_title"]["type"] = "text";
        $input['ur']["articls_title"]["model"] = "false";


        $input['ur']["articls_description"]["label"] = "article short text";
        $input['ur']["articls_description"]["type"] = "textarea";
        $input['ur']["articls_description"]["model"] = "false";



        $input['ur']["articls_text"]["label"] = "article body text";
        $input['ur']["articls_text"]["type"] = "textarea";
        $input['ur']["articls_text"]["model"] = "false";


        $input['ur']["articls_link"]["label"] = "article refrance link";
        $input['ur']["articls_link"]["type"] = "text";
        $input['ur']["articls_link"]["model"] = "false";



        $input['ur']["articls_image"]["label"] = "article image";
        $input['ur']["articls_image"]["type"] = "text";
        $input['ur']["articls_image"]["model"] = "false";
        $input['ur']["articls_image"]["atrr"] = "class ='img' id ='img'";
       
        $input['ur']["articls_attachment"]["label"] = "article attachment like embedded video or any embedd code";
        $input['ur']["articls_attachment"]["type"] = "textarea";
        $input['ur']["articls_attachment"]["model"] = "false";
    

        
        
        if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
            //$input['ur']["articls_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "articls_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("articls", "articls_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                    if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                     
                }
            }
          
            

            
        }


        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));
        
        
          $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);

      
    }

    
    
    public function manage(){
        
                $data["username"] = $this->userName;
     $data['method'] = "admin/Articls";
        $data["winhand"] = "Articls";
          $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "article title", "article short");
        $condation = array("is_orginal", "", 1);
        $conx["data"] = $this->data_model->GetCondiction("articls");
        $conx["primKEY"] = "articls_id";
        $conx["tabeCOLS"] = array("articls_id", "articls_title", "articls_description");
        $conx["tableName"] = "articls";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Articls";


         $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
}
