<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cat
 *
 * @author sadeem-pc
 */
class Cat extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    
     public function index() {
                 $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "admin/Cat";
        $data["winhand"] = "cat";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "category";

        
        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Cat/manage";


        $input['ur']["category_lang_id"]["label"] = "Language of item ";
        $input['ur']["category_lang_id"]["type"] = "select";
        $input['ur']["category_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");




        
        $input['ur']["category_name"]["label"] = "Category Name";
        $input['ur']["category_name"]["type"] = "text";
        $input['ur']["category_name"]["model"] = "false";


        $input['ur']["category_disc"]["label"] = "category description";
        $input['ur']["category_disc"]["type"] = "textarea";
        $input['ur']["category_disc"]["model"] = "false";

        $input['ur']["category_img"]["label"] = "Image";
        $input['ur']["category_img"]["type"] = "text";
        $input['ur']["category_img"]["model"] = "false";
        $input['ur']["category_img"]["atrr"] = "class ='img' id ='img'";

        
        
        if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
           // $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "category_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("category", "category_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }

        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));
        
        
        
           $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);

        
    }
    
    
    public function manage() {
        
            $data['method'] = "admin/Cat";
             $data["winhand"] = "cat";
                $data["username"] = $this->userName;

        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "name", "description");

        $conx["data"] = $this->data_model->GetCondiction("category");
        $conx["primKEY"] = "category_id";
        $conx["tabeCOLS"] = array("category_id", "category_name", "category_disc");
        $conx["tableName"] = "category";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Cat";

         $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }

    
    
}
