<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pragraph
 *
 * @author sadeem1
 */
class Pragraph extends MY_Controller 
{
    
    
    
       
    public function __construct() {

        parent::__construct();
    }


    public function index(){
    
    
        
        $cpage = $this->uri->segment(4);
        
        
        
        $data["username"] = $this->userName;

        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "text");

        $conx["data"] = $this->data_model->get("paragraphs" , NULL, array("paragraphs_page_id" , $cpage ));
        
        $conx["primKEY"] = "paragraphs_id";
        $conx["tabeCOLS"] = array("paragraphs_id", "paragraphs_text");
        $conx["tableName"] = "paragraphs";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Pragraph/edit/".$cpage;

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        
        
        
        
        $data["cont"] = "ui/paragraph";

        $this->load->view("admin/adminmaster", $data);
    
    
    }
    
    public function add()
    {
        $data["paragraphs_page_id"] = $this->input->post("page");
        $data["paragraphs_class"] = $this->input->post("wid");
        $data["paragraphs_text"] = $this->input->post("body");
        
        
        echo $this->data_model->add("paragraphs", $data);
    }

    

    public function edit()
    {
        $it = $this->input->get("item");
         $cpage = $this->uri->segment(4);
                $data["username"] = $this->userName;

        
        $data['method'] = "qcms/Pragraph";
        $data["winhand"] = "Pragraph";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "paragraphs";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "edit";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Pragraph/index/".$cpage;

        
        

        $input['ur']["paragraphs_page_id"]["label"] = "Language of item ";
        $input['ur']["paragraphs_page_id"]["type"] = "select";
        $input['ur']["paragraphs_page_id"]["model"] = $this->data_model->Get_ForSelect("pages", "pages_id", "pages_title");

       

           $input['ur']["paragraphs_class"]["label"] = "Language of item ";
        $input['ur']["paragraphs_class"]["type"] = "select";
        $input['ur']["paragraphs_class"]["model"] = array(
            1=> "col width 1",
            2=> "col width 2",
            3=> "col width 3",
            4=> "col width 4",
            5=> "col width 5",
            6=> "col width 6",
            7=> "col width 7",
            8=> "col width 8",
            9=> "col width 9",
            10=> "col width 10",
            11=> "col width 11",
            12=> "col width 12"
        );


        $input['ur']["paragraphs_text"]["label"] = " text body ";
        $input['ur']["paragraphs_text"]["type"] = "textarea";
        $input['ur']["paragraphs_text"]["model"] = "false";
        //$input['ur']["about_pragraph"]["atrr"] = "class='textarea'";
        
        
  
        


   

      

        
        
        if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
           // $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "paragraphs_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("paragraphs", "paragraphs_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }


        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));



        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
        
    }
    
}
