<?php


class Website extends MY_Controller {
    
    
    public function __construct() {
        parent::__construct();
    }
    
    
    public function index()
    {
        
        
        
          $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "admin/Website";
        $data["winhand"] = "Website";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "website";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;


        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Website/manage";


        $input['ur']["website_keywords"]["label"] = " SEO METADATA Keywords";
        $input['ur']["website_keywords"]["type"] = "text";
        $input['ur']["website_keywords"]["model"] ="false"  ;//$this->data_model->Get_ForSelect("programes", "programes_crm_id", "programes_name");

        $input['ur']["website_description"]["label"] = " SEO METADATA Description";
        $input['ur']["website_description"]["type"] = "text";
        $input['ur']["website_description"]["model"] ="false"  ;//$this->data_model->Get_ForSelect("programes", "programes_crm_id", "programes_name");

        $input['ur']["website_defulatimg"]["label"] = " Default image";
        $input['ur']["website_defulatimg"]["type"] = "text";
        $input['ur']["website_defulatimg"]["model"] ="false"  ;//$this->data_model->Get_ForSelect("programes", "programes_crm_id", "programes_name");
        $input['ur']["website_defulatimg"]["atrr"] = "class ='img' id ='img'";
        
        $input['ur']["website_logo"]["label"] = "Site LOGO";
        $input['ur']["website_logo"]["type"] = "text";
        $input['ur']["website_logo"]["model"] ="false"  ;//$this->data_model->Get_ForSelect("programes", "programes_crm_id", "programes_name");
        $input['ur']["website_logo"]["atrr"] = "class ='img' id ='img'";
        
        $input['ur']["website_icon"]["label"] = "Site Icon";
        $input['ur']["website_icon"]["type"] = "text";
        $input['ur']["website_icon"]["model"] ="false"  ;//$this->data_model->Get_ForSelect("programes", "programes_crm_id", "programes_name");
        $input['ur']["website_icon"]["atrr"] = "class ='img' id ='img'";
        


        
                 if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
         //   $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "website_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("website", "website_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }

        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
        
    }
    
    public function manage()
    {
           $data["username"] = $this->userName;

        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Logo" , "Keywords" , "Metadata" , "Defult img" , "icon");
      $data['method'] = "admin/Website";
        $data["winhand"] = "Website";
        
        $conx["data"] = $this->data_model->GetCondiction("website");
        $conx["primKEY"] = "website_id";
        $conx["tabeCOLS"] = array("website_id", "website_logo" , "website_keywords" , "website_description" , "website_defulatimg" , "website_icon");
        $conx["tableName"] = "website";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Website";
        $conx["imgfilds"]["website_logo"] = TRUE;
        $conx["imgfilds"]["website_defulatimg"] = TRUE;
        $conx["imgfilds"]["website_icon"] = TRUE;
        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
        
    }
}
