<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Kv
 *
 * @author sadeem-pc
 */
class Kv extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    public function index(){
                $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "qcms/kv";
        $data["winhand"] = "kv";
        
        
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "key_setting";

        
        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Kv/manage";

        
        $input['ur']["key"]["label"] = "Key String";
        $input['ur']["key"]["type"] = "text";
        $input['ur']["key"]["model"] = "false";

        $input['ur']["val"]["label"] = "Value String";
        $input['ur']["val"]["type"] = "text";
        $input['ur']["val"]["model"] = "false";


        $input['ur']["lang_id"]["label"] = "Language of item ";
        $input['ur']["lang_id"]["type"] = "select";
        $input['ur']["lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");
        
        
                 if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
            //$input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("key_setting", "id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }
        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
       

    }
    
    
    public function manage() 
    {
                $data["username"] = $this->userName;
        $data['method'] = "qcms/kv";
        $data["winhand"] = "kv";
         $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "key", "val", "lang id");
        $condation = array("is_orginal", "", 1);
        $conx["data"] = $this->data_model->GetCondiction("key_setting");
        $conx["primKEY"] = "id";
        $conx["tabeCOLS"] = array("id", "key", "val", "lang_id");
        $conx["tableName"] = "key_setting";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
         $conx["editUrl"] = "admin/Kv";

            $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
        
    }
}
