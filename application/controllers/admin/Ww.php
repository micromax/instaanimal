<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Articls
 *
 * @author sadeem-pc
 */
class Ww extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
        public function index() {
                $data["username"] = $this->userName;
    
            
        $data["ux"] = "company";
        $data['method'] = "admin/Ww";
        $data["winhand"] = "Ww";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Ww/manage";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "welcomword";

  

        $input['ur']["ww_lang_id"]["label"] = "Language of item ";
        $input['ur']["ww_lang_id"]["type"] = "select";
        $input['ur']["ww_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

   
   

        $input['ur']["ww_title"]["label"] = "article title";
        $input['ur']["ww_title"]["type"] = "text";
        $input['ur']["ww_title"]["model"] = "false";


        $input['ur']["ww_text"]["label"] = "article short text";
        $input['ur']["ww_text"]["type"] = "textarea";
        $input['ur']["ww_text"]["model"] = "false";






        $input['ur']["ww_img"]["label"] = "article image";
        $input['ur']["ww_img"]["type"] = "text";
        $input['ur']["ww_img"]["model"] = "false";
        $input['ur']["ww_img"]["atrr"] = "class ='img' id ='img'";

        
        
        if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
            //$input['ur']["articls_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "ww_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("welcomword", "ww_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                    if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                     
                }
            }
          
            

            
        }


        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));
        
        
          $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);

      
    }

    
    
    public function manage(){
        
                $data["username"] = $this->userName;
    $data['method'] = "admin/Ww";
        $data["winhand"] = "Ww";
          $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "title", "Text" , "lang id");
       
        $conx["data"] = $this->data_model->GetCondiction("welcomword");
        $conx["primKEY"] = "ww_id";
        $conx["tabeCOLS"] = array("ww_id","ww_title", "ww_text", "ww_lang_id");
        $conx["tableName"] = "welcomword";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Ww";


         $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
}
