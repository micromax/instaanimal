<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Supscription
 *
 * @author sadeem-pc
 */
class Supscription extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
      public function index() {
        $data["username"] = $this->userName;

        $data['method'] = "qcms/supscription";
        $data["winhand"] = "supscription";




        $data["csv"] = $this->data_model->getcol("supscripers", "supscripers_email");




        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "supscription name", "supscription email", "supscription Phone", "supscription time");
        $conx["data"] = $this->data_model->get("supscripers");
        $conx["primKEY"] = "supscripers_id";
        $conx["tabeCOLS"] = array("supscripers_id", "supscripers_name", "supscripers_email", "supscripers_phone", "supscripers_regtime");
        $conx["tableName"] = "supscripers";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = FALSE;


        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
}
