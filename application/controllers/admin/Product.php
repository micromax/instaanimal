<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product
 *
 * @author sadeem-pc
 */
class Product extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
        public function index() {
                    $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "qcms/product";
        $data["winhand"] = "product";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "items";

        $input['ur']["items_add_by"]["type"] = "hidden";
        $input['ur']["items_add_by"]["default"] = $this->userName;

        $input['ur']["items_date_add"]["type"] = "hidden";
        $input['ur']["items_date_add"]["default"] = date("Y-m-d");


        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Product/manage";


        $input['ur']["items_lang"]["label"] = "Language of item ";
        $input['ur']["items_lang"]["type"] = "select";
        $input['ur']["items_lang"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");



        $input['ur']["items_cat_id"]["label"] = "category";
        $input['ur']["items_cat_id"]["type"] = "select";
        $input['ur']["items_cat_id"]["model"] = $this->data_model->Get_ForSelect("category", "category_id", "category_name", FALSE);



        $input['ur']["items_name"]["label"] = "item name";
        $input['ur']["items_name"]["type"] = "text";
        $input['ur']["items_name"]["model"] = "false";

        $input['ur']["items_description"]["label"] = "item Short discription";
        $input['ur']["items_description"]["type"] = "textarea";
        $input['ur']["items_description"]["model"] = "false";

      
      

    
        $input['ur']["items_img"]["label"] = "item img";
        $input['ur']["items_img"]["type"] = "text";
        $input['ur']["items_img"]["model"] = "false";
        $input['ur']["items_img"]["atrr"] = "class ='img' id ='img'";

        
    if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
         //   $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "items_id" ,
                "item" => $item
                
            );    
                 
            
            $data["tap_option"] = array(
                    "url"=>"admin/featuresmanager?item=".$item,
                    "name" =>"ADD new Features  "
                
                
            );
            
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("items", "items_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }

        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
       
    }
    
    
    
    public function manage()
    {
        $data["username"] = $this->userName;
        $data['method'] = "qcms/product";
        $data["winhand"] = "product";
         $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "name" , "date add");

        $conx["data"] = $this->data_model->GetCondiction("items");
        $conx["primKEY"] = "items_id";
        $conx["tabeCOLS"] = array("items_id","items_name" ,"items_date_add");
        $conx["tableName"] = "items";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Product";

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
    
}
