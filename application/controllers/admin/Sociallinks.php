<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sociallinks
 *
 * @author sadeem-pc
 */
class Sociallinks extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    
    public function index() {
        
                $data["username"] = $this->userName;

        $x = array(
            "icon-behance"=> "behance" ,
            "icon-vk"=> "vk" ,
            "icon-youtube"=> "youtube" ,
            "icon-facebook"=> "facebook" ,
            "icon-linkedin"=> "linkedin" ,
            "icon-twitter"=> "twitter" ,
            "icon-instagram"=> "instagram" ,
            
            
        );
        $data['method'] = "qcms/sociallinks";
        $data["winhand"] = "sociallinks";
        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/sociallinks/manage";
        
        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "social_links";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["social_imgepath"]["label"] = "Icon ";
        $input['ur']["social_imgepath"]["type"] = "select";
        $input['ur']["social_imgepath"]["model"] = $x;
        
        $input['ur']["social_link"]["label"] = " LINK ";
        $input['ur']["social_link"]["type"] = "text";
        $input['ur']["social_link"]["model"] = "false";

        
        
    if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
       //     $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "social_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("social_links", "social_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                    if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }
        
        
        
        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));



        
         $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);

        


        
    }
    
    
    public function manage(){
                $data["username"] = $this->userName;
        $data['method'] = "qcms/sociallinks";
        $data["winhand"] = "sociallinks";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "LINK");
        $conx["data"] = $this->data_model->get("social_links");
        $conx["primKEY"] = "social_id";
        $conx["tabeCOLS"] = array("social_id", "social_link");
        $conx["tableName"] = "social_links";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
         $conx["editUrl"] = "admin/Sociallinks";

       $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);

    }
    //put your code here
}
