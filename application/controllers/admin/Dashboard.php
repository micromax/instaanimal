<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard
 *
 * @author sadeem
 */
class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();

        // Force SSL
        //$this->force_ssl();
        // Form and URL helpers always loaded (just for convenience)
        $this->load->helper('url');
        $this->load->helper('form');
    }

    public function index() {
      
        $data["cont"] = null;
        $data["username"] = $this->userName;
        $data["static"] = $this->data_model->getstat();
        $data["graph"] = $this->data_model->getgraph();
        $data["agent"] = $this->data_model->getgraphagent();
        $data["urlss"] = $this->data_model->getgraphpage();
        $data["country"] = $this->data_model->getgraphcou();

        $data["twiter"] = "";
        
        $tmp = array();
        
        //  { d: '<?= $vv->years;   $vv->month;   $vv->day; ', item1: <?= $vv->vists;   },
      
        foreach ($data["graph"] as $key => $value) {
            if(isset($tmp[$value->years."-".$value->months]))
            {
           $tmp[$value->years."-".$value->months] += $value->vists;
            }  else {
                $tmp[$value->years."-".$value->months] = $value->vists;
            }
        }
        $data["graph"] = $tmp;
             
        $this->load->view("admin/adminmaster", $data);
        
    }

    public function login() {
     
    }

   
    public function logout() {

    }

    
    
    
    
}
