<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Aboutusen
 *
 * @author sadeem-pc
 */
class Aboutusen  extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    public function index(){
        $data["username"] = $this->userName;

        
        $data['method'] = "qcms/aboutusen";
        $data["winhand"] = "aboutusen";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "about";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Aboutusen/manage";

        
        

        $input['ur']["lang_id"]["label"] = "Language of item ";
        $input['ur']["lang_id"]["type"] = "select";
        $input['ur']["lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        //$input['ur']["about_hashkey"]["label"] = " Name ";
        $rand = rand(1, 99) . "_";
        $input['ur']["about_hashkey"]["type"] = "hidden";
        $input['ur']["about_hashkey"]["default"] = uniqid($rand);

        $input['ur']["about_is_orginal"]["type"] = "hidden";
        $input['ur']["about_is_orginal"]["default"] = 1;

        $input['ur']["about_title"]["label"] = "Pragraph Title ";
        $input['ur']["about_title"]["type"] = "text";
        $input['ur']["about_title"]["model"] = "false";


        $input['ur']["about_pragraph"]["label"] = " text body ";
        $input['ur']["about_pragraph"]["type"] = "textarea";
        $input['ur']["about_pragraph"]["model"] = "false";
        //$input['ur']["about_pragraph"]["atrr"] = "class='textarea'";
        
        
        $input['ur']["about_order"]["label"] = " Order like 1 , 2 , 3 .... ";
        $input['ur']["about_order"]["type"] = "text";
        $input['ur']["about_order"]["model"] = "false";
        


        $input['ur']["about_imge_path"]["label"] = "image of Pragraph";
        $input['ur']["about_imge_path"]["type"] = "text";
        $input['ur']["about_imge_path"]["model"] = "false";
        $input['ur']["about_imge_path"]["atrr"] = "class ='img' id ='img'";

      

        
        
        if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
           // $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "about_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("about", "about_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }


        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));



        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
        //$this->load->view("admin", $data);
    }
    
    
    
    public function manage(){
                $data["username"] = $this->userName;
   $data["winhand"] = "aboutusen";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Title");

        $conx["data"] = $this->data_model->GetCondiction("about");
        $conx["primKEY"] = "about_id";
        $conx["tabeCOLS"] = array("about_id", "about_title");
        $conx["tableName"] = "about";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Aboutusen";

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);


    }
    //put your code here
}
