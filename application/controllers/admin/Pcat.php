<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pcat
 *
 * @author sadeem-pc
 */
class Pcat extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
     public function index() {
                 $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "qcms/pcat";
        $data["winhand"] = "pcat";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "pcategory";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Pcat/manage";


        $input['ur']["pcategory_lang_id"]["label"] = "Language of item ";
        $input['ur']["pcategory_lang_id"]["type"] = "select";
        $input['ur']["pcategory_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");


        $input['ur']["pcategory_name"]["label"] = "Main Category Name";
        $input['ur']["pcategory_name"]["type"] = "text";
        $input['ur']["pcategory_name"]["model"] = "false";




        $input['ur']["pcategory_img"]["label"] = "Image";
        $input['ur']["pcategory_img"]["type"] = "text";
        $input['ur']["pcategory_img"]["model"] = "false";
        $input['ur']["pcategory_img"]["atrr"] = "class ='img' id ='img'";


        
        
         if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
          //  $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "pcategory_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("pcategory", "pcategory_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }
        
        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
       
    }
    
    
    public function manage(){
                $data["username"] = $this->userName;
     $data['method'] = "qcms/pcat";
        $data["winhand"] = "pcat";
        
         $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "name");

        $conx["data"] = $this->data_model->GetCondiction("pcategory");
        $conx["primKEY"] = "pcategory_id";
        $conx["tabeCOLS"] = array("pcategory_id", "pcategory_name");
        $conx["tableName"] = "pcategory";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Pcat";

           $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
    //put your code here
}
