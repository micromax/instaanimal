<?php


class Pages extends MY_Controller{
 
    
    public function __construct() {

        parent::__construct();
    }


    public function index(){
    
    
     $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "qcms/pages";
        $data["winhand"] = "pages";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "pages";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Pages/manage";


        $input['ur']["pages_lang"]["label"] = "Language of Page ";
        $input['ur']["pages_lang"]["type"] = "select";
        $input['ur']["pages_lang"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");


        $input['ur']["pages_title"]["label"] = "Page Title";
        $input['ur']["pages_title"]["type"] = "text";
        $input['ur']["pages_title"]["model"] = "false";


        $input['ur']["pages_meta_k"]["label"] = "Page meta keywords";
        $input['ur']["pages_meta_k"]["type"] = "text";
        $input['ur']["pages_meta_k"]["model"] = "false";


        $input['ur']["pages_meta_d"]["label"] = "Page meta description";
        $input['ur']["pages_meta_d"]["type"] = "text";
        $input['ur']["pages_meta_d"]["model"] = "false";




       
        
        
         if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
          //  $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "pages_id" ,
                "item" => $item
                
            );    
                 
            $data["pragraps"] = "admin/Pragraph/index/".$item;    
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("pages", "pages_id", $item);
        
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }
        
        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $data["cont"] = "ui/form_forpages";

        $this->load->view("admin/adminmaster", $data);
    
    
    }
    
    
    
    
    public function manage()
    {
                $data['method'] = "qcms/pages";
        $data["winhand"] = "pages";
           $data["username"] = $this->userName;

         $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Page name" , "Page Keywords ");

        $conx["data"] = $this->data_model->GetCondiction("pages");
        $conx["primKEY"] = "pages_id";
        $conx["tabeCOLS"] = array("pages_id","pages_title" ,"pages_meta_k");
        $conx["tableName"] = "pages";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Pages";

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
        
    }



}