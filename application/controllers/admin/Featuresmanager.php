<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Featuresmanager
 *
 * @author mac
 */
class Featuresmanager extends MY_Controller{
    //put your code here
     public function __construct() {
        parent::__construct();
    }
    
    public function index(){
         $data["username"] = $this->userName;
          $data["ux"] = "company";
        $data['method'] = "qcms/featuresmanager";
        $data["winhand"] = "featuresmanager";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";
        
        
        $input['ur']["features_item_id"]["type"] = "hidden";
        $input['ur']["features_item_id"]["default"] = $this->input->get("item");
        
        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "featuresmanager";
        
        $input['ur']["features_name"]["label"] = "features name";
        $input['ur']["features_name"]["type"] = "text";
        $input['ur']["features_name"]["model"] = "false";
        
        $input['ur']["features_value"]["label"] = "features value";
        $input['ur']["features_value"]["type"] = "textarea";
        $input['ur']["features_value"]["model"] = "false";
            
        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/featuresmanager?item=".$this->input->get("item");
          $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $data["cont"] = "ui/form";

        
        
        
         $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "name" , "value");
$condetion =  array ("features_item_id" , "="  , $this->input->get("item") );
        $conx["data"] = $this->data_model->GetCondiction("featuresmanager" , $condetion);
        $conx["primKEY"] = "features_id";
        $conx["tabeCOLS"] = array("features_id","features_name" ,"features_value");
        $conx["tableName"] = "featuresmanager";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/featuresmanager?item=".$this->input->get("item");

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        
        
        $this->load->view("admin/adminmaster", $data);
        
    }
}
