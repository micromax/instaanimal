<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Footerlinks
 *
 * @author sadeem1
 */
class Footerlinks extends MY_Controller {
    
    
    public function __construct() 
    {
        parent::__construct();
    }
    
    
    public function index(){
        
         $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "admin/footerlinks";
        $data["winhand"] = "footerlinks";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "footerlinks";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;


        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Footerlinks/manage";


        $input['ur']["footerlinks_sec_id"]["label"] = "Lang of Item";
        $input['ur']["footerlinks_sec_id"]["type"] = "select";
        $input['ur']["footerlinks_sec_id"]["model"] = $this->data_model->Get_ForSelect("footersection", "footersection_id", "footersection_text");
        
        $input['ur']["footerlinks_text"]["label"] = "Link Title";
        $input['ur']["footerlinks_text"]["type"] = "text";
        $input['ur']["footerlinks_text"]["model"] ="false"  ;//$this->data_model->Get_ForSelect("programes", "programes_crm_id", "programes_name");

        
        $input['ur']["footerlinks_url"]["label"] = "Link Url";
        $input['ur']["footerlinks_url"]["type"] = "text";
        $input['ur']["footerlinks_url"]["model"] ="false"  ;//$this->data_model->Get_ForSelect("programes", "programes_crm_id", "programes_name");

        

        
    if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
         //   $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "footerlinks_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("footerlinks", "footerlinks_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }

        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
        
    }
    
    public function manage()
    {
           $data["username"] = $this->userName;
        $data['method'] = "admin/footerlinks";
        $data["winhand"] = "footerlinks";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Text" , "URL");

        $conx["data"] = $this->data_model->GetCondiction("footerlinks");
        $conx["primKEY"] = "footerlinks_id";
        $conx["tabeCOLS"] = array("footerlinks_id", "footerlinks_text" , "footerlinks_url");
        $conx["tableName"] = "footerlinks";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Footerlinks";
        
        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
        
    }
    
}
