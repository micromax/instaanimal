<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Language
 *
 * @author sadeem-pc
 */
class Language extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    //put your code here
    
    
    public function index() {
                $data["username"] = $this->userName;

        $data['method'] = "qcms/language";
        $data["winhand"] = "language";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "languge";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        
        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Language/manage";

        $input['ur']["lang_name"]["label"] = " Name ";
        $input['ur']["lang_name"]["type"] = "text";
        $input['ur']["lang_name"]["model"] = "false";

        $input['ur']["lang_orintation"]["label"] = " Orientation ";
        $input['ur']["lang_orintation"]["type"] = "select";
        $input['ur']["lang_orintation"]["model"] = array("ltr" => "left to right", "rtl" => "right to left");




        if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
            //$input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "lang_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("languge", "lang_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                    if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }



        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));


        
        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);

 
    }
    
    
    public function manage() {
                $data["username"] = $this->userName;

                $data['method'] = "qcms/language";
        $data["winhand"] = "language";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "name");
        $conx["data"] = $this->data_model->get("languge");
        $conx["primKEY"] = "lang_id";
        $conx["tabeCOLS"] = array("lang_id", "lang_name");
        $conx["tableName"] = "languge";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
         $conx["editUrl"] = "admin/Language";

            $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }

}
