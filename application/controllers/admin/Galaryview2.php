<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Galaryview2
 *
 * @author sadeem-pc
 */
class Galaryview2 extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    //put your code here
    
    
    public function index(){
        
        $data["imgs"] = $this->data_model->get("files");
        $this->load->view("admin/helpers/image_gal_help", $data);
    }
}
