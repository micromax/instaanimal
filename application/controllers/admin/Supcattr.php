<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Supcattr
 *
 * @author sadeem-pc
 */
class Supcattr extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    //put your code here
    
     public function index() {
                 $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "qcms/supcattr";
        $data["winhand"] = "supcattr";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "subcats";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 0;


        
        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Supcattr/manage";
        
        
        $input['ur']["subcats_lang_id"]["label"] = "Language of item ";
        $input['ur']["subcats_lang_id"]["type"] = "select";
        $input['ur']["subcats_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        $expected = array('is_orginal', 0);
        $input['ur']["subcats_cat_id"]["label"] = "Category name";
        $input['ur']["subcats_cat_id"]["type"] = "select";
        $input['ur']["subcats_cat_id"]["model"] = $this->data_model->Get_ForSelect("cat", "cat_id", "cat_name", $expected);

        $input['ur']["subcats_name"]["label"] = " subcategory name";
        $input['ur']["subcats_name"]["type"] = "text";
        $input['ur']["subcats_name"]["model"] = "false";
        $expecteds = array('is_orginal', 1);
        $input['ur']["subcats_hashkey"]["label"] = "transleat for subcategory";
        $input['ur']["subcats_hashkey"]["type"] = "select";
        $input['ur']["subcats_hashkey"]["model"] = $this->data_model->Get_ForSelect("subcats", "subcats_hashkey", "subcats_name", $expecteds);


        
                 if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
          //  $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "subcats_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("subcats", "subcats_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }
        
        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));


          $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
        
    }
    
    
    public function manage(){
        
                $data["username"] = $this->userName;
       $data['method'] = "qcms/supcattr";
        $data["winhand"] = "supcattr";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "sub name");
        $condation = array("is_orginal", "", 0);
        $conx["data"] = $this->data_model->GetCondiction("subcats");
        $conx["primKEY"] = "subcats_id";
        $conx["tabeCOLS"] = array("subcats_id", "subcats_name");
        $conx["tableName"] = "subcats";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
         $conx["editUrl"] = "admin/Supcattr";

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
}
