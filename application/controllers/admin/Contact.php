<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Contact
 *
 * @author sadeem-pc
 */
class Contact extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    public function index() {
                $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "qcms/contact";
        $data["winhand"] = "contact";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "contacts";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Contact/manage";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;


        $input['ur']["contacts_lang_id"]["label"] = "Language of item ";
        $input['ur']["contacts_lang_id"]["type"] = "select";
        $input['ur']["contacts_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        $rand = rand(1, 99) . "_";
        $input['ur']["contacts_hashkey"]["type"] = "hidden";
        $input['ur']["contacts_hashkey"]["default"] = uniqid($rand);

        $input['ur']["contacts_title"]["label"] = "contact title";
        $input['ur']["contacts_title"]["type"] = "text";
        $input['ur']["contacts_title"]["model"] = "false";


        $input['ur']["contacts_pragraph"]["label"] = "contact pragraph";
        $input['ur']["contacts_pragraph"]["type"] = "textarea";
        $input['ur']["contacts_pragraph"]["model"] = "false";


        $input['ur']["contacts_phone"]["label"] = "contact phone";
        $input['ur']["contacts_phone"]["type"] = "text";
        $input['ur']["contacts_phone"]["model"] = "false";

        $input['ur']["contacts_email"]["label"] = "contact Email";
        $input['ur']["contacts_email"]["type"] = "text";
        $input['ur']["contacts_email"]["model"] = "false";



         if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
           /// $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "contacts_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("contacts", "contacts_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                    if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }



        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));





      $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);

       
    }
    
    
    public function manage(){
        
                $data["username"] = $this->userName;
     $data['method'] = "qcms/contact";
        $data["winhand"] = "contact";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "title", "phone", "email");
        $condation = array("is_orginal", "", 1);
        $conx["data"] = $this->data_model->GetCondiction("contacts");
        $conx["primKEY"] = "contacts_id";
        $conx["tabeCOLS"] = array("contacts_id", "contacts_title", "contacts_phone", "contacts_email");
        $conx["tableName"] = "contacts";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Contact";

            $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
}
