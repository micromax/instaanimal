<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of City
 *
 * @author sadeem-pc
 */
class City extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    
    
    
      public function index() {
                  $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "qcms/city";
        $data["winhand"] = "city";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "citys";

        
        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/City/manage";


        $input['ur']["citys_name"]["label"] = "City name";
        $input['ur']["citys_name"]["type"] = "text";
        $input['ur']["citys_name"]["model"] = "false";
        
        $input['ur']["citys_name_ar"]["label"] = "City arabic Name";
        $input['ur']["citys_name_ar"]["type"] = "text";
        $input['ur']["citys_name_ar"]["model"] = "false";


      

        $input['ur']["img_path"]["label"] = "Image for city";
        $input['ur']["img_path"]["type"] = "text";
        $input['ur']["img_path"]["model"] = "false";
        $input['ur']["img_path"]["atrr"] = "class ='img' id ='img'";

        
         if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
           // $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "citys_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("citys", "citys_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }

        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        
           $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);

    }
    
    
    
    public function manage() {
                $data["username"] = $this->userName;
 $data['method'] = "qcms/city";
        $data["winhand"] = "city";
        
                $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Name", "Arabic name");

        $conx["data"] = $this->data_model->GetCondiction("citys");
        $conx["primKEY"] = "citys_id";
        $conx["tabeCOLS"] = array("citys_id", "citys_name", "citys_name_ar" );
        $conx["tableName"] = "citys";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/City";

           $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
        
        
    }
    
    
}
