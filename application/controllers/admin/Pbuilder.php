<?php
use voku\CssToInlineStyles\CssToInlineStyles;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pbuilder
 *
 * @author sadeem-pc
 */
class Pbuilder  extends MY_Controller{
    //put your code here
       public function __construct() {
        parent::__construct();
        $this->load->model("Design");
    }
    
    public function index() {
         $data["username"] = $this->userName;   
        
         $data["cont"] = "ui/pagebuilder";

        $this->load->view("admin/adminmaster", $data);
        
    }
    
    
    
    public function add() {
           $docname = $this->input->post("docname");
        
            $this->db->trans_start();
            
            $post_data["design_name"]= $docname;
            $id =  $this->Design-> __add_design($post_data);
            
            $this->db->trans_complete();
            
            echo $id;
    }
    
    public function save()
    {
              $id = $this->input->post("docid");
              $docname = $this->input->post("docname");
              
              $html = $this->input->post("html");
              
              $css = $this->input->post("css");
              
              
              $cssToInlineStyles= new CssToInlineStyles();
              $cssToInlineStyles->setHTML($html);
              $cssToInlineStyles->setCSS($css);
            $htmlcomipled = $cssToInlineStyles->convert();
           $post_data["design_name"]= $docname;
           $post_data["design_html"]= $html;
           $post_data["design_css"]= $css;
           $post_data["design_compiled"]= $htmlcomipled;
           $this->db->trans_start();
           $this->Design->__edit_design($post_data , $id);
           $this->db->trans_complete();
           
    }
    
    
    public function getall()
    {
        echo $this->Design->__getalls();
    }
    
    
    public function getthis()
    {
         $id = $this->input->post("pid");
         
         echo $this->Design->__getbyid($id);
         
    }
  
    
    
}
