<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Deleteimg
 *
 * @author sadeem-pc
 */
class Deleteimg extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    public function index() {
        
        
          $pid = $this->input->post("item");
        $dbin = $this->input->post("dbin");
        $path = FCPATH."uploads/";
        $spath = FCPATH."uploads/small/";
        
        @unlink($path.$dbin); 
        @unlink($spath.$dbin); 
        
        $this->data_model->delete("files" , "pic_id" , $pid);
    }
}
