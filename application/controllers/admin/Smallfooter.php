<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SmallFooter
 *
 * @author sadeem1
 */
class Smallfooter extends MY_Controller {
    
    
    public function __construct() {
        parent::__construct();
    }
    
    
    public function index(){
        
                $data["username"] = $this->userName;

        $data['method'] = "admin/Smallfooter";
        $data["winhand"] = "smallfooter";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "smallfooter";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Smallfooter/manage";

        
        

        $input['ur']["smallfooter_langid"]["label"] = "Language of item ";
        $input['ur']["smallfooter_langid"]["type"] = "select";
        $input['ur']["smallfooter_langid"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        //$input['ur']["about_hashkey"]["label"] = " Name ";
        
        $input['ur']["smallfooter_text"]["label"] = "Link Text ";
        $input['ur']["smallfooter_text"]["type"] = "text";
        $input['ur']["smallfooter_text"]["model"] = "false";


        $input['ur']["smallfooter_url"]["label"] = " URL ";
        $input['ur']["smallfooter_url"]["type"] = "text";
        $input['ur']["smallfooter_url"]["model"] = "false";
       
        


        $input['ur']["smallfooter_icon"]["label"] = "image of Pragraph";
        $input['ur']["smallfooter_icon"]["type"] = "text";
        $input['ur']["smallfooter_icon"]["model"] = "false";
        $input['ur']["smallfooter_icon"]["atrr"] = "class ='img' id ='img'";

      

        
        
        if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
           // $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "smallfooter_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("smallfooter", "smallfooter_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }


        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));



        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);

        
    }
    
    
    public function manage()
    {
        
        $data["username"] = $this->userName;
        $data['method'] = "admin/Smallfooter";
        $data["winhand"] = "smallfooter";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "text" , "URL" , "Img");

        $conx["data"] = $this->data_model->GetCondiction("smallfooter");
        $conx["primKEY"] = "about_id";
        $conx["tabeCOLS"] = array("smallfooter_id", "smallfooter_text" , "smallfooter_url" , "smallfooter_icon");
        $conx["tableName"] = "smallfooter";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/smallfooter";
        $conx["imgfilds"]["smallfooter_icon"] = TRUE;
        
        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
        
    }
}
