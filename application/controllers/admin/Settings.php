<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Settings
 *
 * @author sadeem-pc
 */
class Settings extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    //put your code here
    
    
    public function index() {
        $data["username"] = $this->userName;

        $data['method'] = "qcms/settings";
        $data["winhand"] = "settings";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "settings";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";
        
        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Settings/manage";


        $input['ur']["def_lang"]["label"] = "default Language";
        $input['ur']["def_lang"]["type"] = "select";
        $input['ur']["def_lang"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");
        ;
        $input['ur']["def_lang"]["atrr"] = "class='form-control'";

        $input['ur']["def_phone"]["label"] = "default phone";
        $input['ur']["def_phone"]["type"] = "text";
        $input['ur']["def_phone"]["model"] = "false";
        $input['ur']["def_phone"]["atrr"] = "class='form-control'";

        $input['ur']["def_fax"]["label"] = "default fax";
        $input['ur']["def_fax"]["type"] = "text";
        $input['ur']["def_fax"]["model"] = "false";
        $input['ur']["def_fax"]["atrr"] = "class='form-control'";


        $input['ur']["def_email"]["label"] = "default email";
        $input['ur']["def_email"]["type"] = "text";
        $input['ur']["def_email"]["model"] = "false";
        $input['ur']["def_email"]["atrr"] = "class='form-control'";


        $input['ur']["def_tweeter"]["label"] = "default Twitter";
        $input['ur']["def_tweeter"]["type"] = "text";
        $input['ur']["def_tweeter"]["model"] = "false";
        $input['ur']["def_tweeter"]["atrr"] = "class='form-control'";


        $input['ur']["def_skype"]["label"] = "default Skype";
        $input['ur']["def_skype"]["type"] = "text";
        $input['ur']["def_skype"]["model"] = "false";
        $input['ur']["def_skype"]["atrr"] = "class='form-control'";


        
        
    if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
        //    $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "settinges_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("settings", "settinges_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }




        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));


        
        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);


    }
    
    public function manage(){
                $data["username"] = $this->userName;
       $data['method'] = "qcms/settings";
        $data["winhand"] = "settings";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "default  Phone", "default Fax", "default Email", "default Twitter", "default skype");
        $conx["data"] = $this->data_model->get("settings");
        $conx["primKEY"] = "settinges_id";
        $conx["tabeCOLS"] = array("settinges_id", "def_phone", "def_fax", "def_email", "def_tweeter", "def_skype");
        $conx["tableName"] = "settings";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
         $conx["editUrl"] = "admin/Settings";

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);



      
    }

}
