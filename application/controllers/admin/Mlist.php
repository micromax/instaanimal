<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mlist
 *
 * @author sadeem-pc
 */
class Mlist extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    
    
    public function index() {

        $data["username"] = $this->userName;

        $data['method'] = "qcms/mlist";
        $data["winhand"] = "mlist";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "mlist";

        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Mlist/manage";


        $input['ur']["mlist_lang_id"]["label"] = "Language of item ";
        $input['ur']["mlist_lang_id"]["type"] = "select";
        $input['ur']["mlist_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        //$input['ur']["about_hashkey"]["label"] = " Name ";
        $rand = rand(1, 99) . "_";
        $input['ur']["mlist_hashkey"]["type"] = "hidden";
        $input['ur']["mlist_hashkey"]["default"] = uniqid($rand);

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;

        $input['ur']["mlist_text"]["label"] = " display name on site list";
        $input['ur']["mlist_text"]["type"] = "text";
        $input['ur']["mlist_text"]["model"] = "false";


        $input['ur']["mlist_link"]["label"] = "linked to";
        $input['ur']["mlist_link"]["type"] = "select";
        $input['ur']["mlist_link"]["model"] = array(
            "defaults" => "qcms page",
            "about" => "About page",
            "articles" => "articles page",
            "contact" => "Contact page ",
            "products" => "products page",
            "brands" => "brands page",
            "jobs" => "jobs page",
            "category" => "category"
        );




        
        if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
          //  $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "mlist_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("mlist", "mlist_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }


        $data["ux"] = "company";
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));


        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);

    }
    
    
    public function manage($param) {
              $data["username"] = $this->userName;
  $data['method'] = "qcms/mlist";
        $data["winhand"] = "mlist";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "page name", "linke");
        $condation = FALSE;
        $conx["data"] = $this->data_model->GetCondiction("mlist");
        $conx["primKEY"] = "mlist_id";
        $conx["tabeCOLS"] = array("mlist_id", "mlist_text", "mlist_link");
        $conx["tableName"] = "mlist";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Mlist";

           $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
    //put your code here
}
