<?php


class Footersection  extends MY_Controller {
    
    
    public function __construct() {
        parent::__construct();
    }
    
    
    
    public function index(){
        
         $data["username"] = $this->userName;

        $data["ux"] = "company";
        $data['method'] = "admin/footersection";
        $data["winhand"] = "footersection";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";


        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "footersection";

        $input['ur']["is_orginal"]["type"] = "hidden";
        $input['ur']["is_orginal"]["default"] = 1;


        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Footersection/manage";


        $input['ur']["footersection_langid"]["label"] = "Lang of Item";
        $input['ur']["footersection_langid"]["type"] = "select";
        $input['ur']["footersection_langid"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");
        
        $input['ur']["footersection_text"]["label"] = " Section Title";
        $input['ur']["footersection_text"]["type"] = "text";
        $input['ur']["footersection_text"]["model"] ="false"  ;//$this->data_model->Get_ForSelect("programes", "programes_crm_id", "programes_name");

        

        
                 if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
         //   $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "footersection_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("footersection", "footersection_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }

        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        $data["cont"] = "ui/form";

        $this->load->view("admin/adminmaster", $data);
        
    }
    
    public function manage()
    {
           $data["username"] = $this->userName;
   $data['method'] = "admin/footersection";
        $data["winhand"] = "footersection";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "Text" );

        $conx["data"] = $this->data_model->GetCondiction("footersection");
        $conx["primKEY"] = "footersection_id";
        $conx["tabeCOLS"] = array("footersection_id", "footersection_text" );
        $conx["tableName"] = "footersection";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
        $conx["editUrl"] = "admin/Footersection";
        
        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
        
    }
    
}
