<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Uploadfile
 *
 * @author sadeem-pc
 */
class Uploadfile extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    //put your code here
    
    public function index() {
        
        $data["user"] = $this->user;
        $data["username"] = $this->userName;
        $dir = base_url() . 'uploads';
        $this->load->model("uploads");
        echo $this->uploads->upfile();
        redirect("admin/Uploader/manage");
    }
}
