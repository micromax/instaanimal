<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Onlineorders
 *
 * @author sadeem-pc
 */
class Onlineorders extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    
    
    public function index() {

        $data['method'] = "qcms/onlineorders";
        $data["winhand"] = "onlineorders";



        $data["ux"] = "company";
        //$config[1] = "UX";
        // $this->load->library("ux", $config);
        $data["form"] = '';
        $data["csv"] = $this->data_model->getcol("onlineorders", "onlineorders_email");

        $data["username"] = $this->userName;



        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "client name", "client  email", "client message", "request time" , "event location " , "event start" , "event end" , "requist type (I or R)");
        $conx["data"] = $this->data_model->get("onlineorders");
        $conx["primKEY"] = "onlineorders_id";
        $conx["tabeCOLS"] = array("onlineorders_id", "onlineorders_name",  "onlineorders_product_name", "onlineorders_msg", "onlineorders_time" , "onlineorders_elocation" , "onlineorders_estart" , "onlineorders_eend" , "order_type");
        $conx["tableName"] = "onlineorders";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = FALSE;
        $conx["fullview"] = TRUE;
        $conx["fullview_url"] = "admin/Onlineorders/v";

           $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
    
    
    public function v(){
        
        
        if($this->input->get("item") != null)
        {
            
            $item = $this->input->get("item");
             $this->load->model("data_model");
            $data["doc"] = $this->data_model->GetThis("onlineorders", "onlineorders_id", $item);
            
            
               $data["cont"] = "ui/document";

        $this->load->view("admin/adminmaster", $data);
            
            
        }
    }
    //put your code here
}
