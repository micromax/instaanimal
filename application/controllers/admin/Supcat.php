<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Supcat
 *
 * @author sadeem-pc
 */
class Supcat extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
        public function index() {
                    $data["username"] = $this->userName;

        $data["ux"] = "company";
        $input['ur']["todo"]["type"] = "hidden";
        $input['ur']["todo"]["default"] = "add";

        $data['method'] = "qcms/supcat";
        $data["winhand"] = "supcat";

        $input['ur']["into"]["type"] = "hidden";
        $input['ur']["into"]["default"] = "subcats";

  


        $input['ur']["goto"]["type"] = "hidden";
        $input['ur']["goto"]["default"] = "admin/Supcat/manage";
        
        
        $input['ur']["subcats_lang_id"]["label"] = "Language of item ";
        $input['ur']["subcats_lang_id"]["type"] = "select";
        $input['ur']["subcats_lang_id"]["model"] = $this->data_model->Get_ForSelect("languge", "lang_id", "lang_name");

        $expecteds = array('is_orginal', 1);
        $input['ur']["subcats_cat_id"]["label"] = "Category name";
        $input['ur']["subcats_cat_id"]["type"] = "select";
        $input['ur']["subcats_cat_id"]["model"] = $this->data_model->Get_ForSelect("cat", "cat_id", "cat_name", $expecteds);

        $input['ur']["subcats_name"]["label"] = " subcategory name";
        $input['ur']["subcats_name"]["type"] = "text";
        $input['ur']["subcats_name"]["model"] = "false";

        $rand = rand(1, 99) . "_";
        $input['ur']["subcats_hashkey"]["type"] = "hidden";
        $input['ur']["subcats_hashkey"]["default"] = uniqid($rand);

        
            if($this->input->get("action") =="edit" && $this->input->get("item") != null )
        {
         
            $input['ur']["todo"]["default"] = "edit";
            
         //   $input['ur']["about_id"]["type"] = "hidden";
            
            $item = $this->input->get("item");
            
            
            $data["editmark"] = array(
                "col" => "subcats_id" ,
                "item" => $item
                
            );    
                 
                
                
            $this->load->model("data_model");
            $datas = $this->data_model->GetThis("subcats", "subcats_id", $item);
            foreach ($datas as $d) {
                foreach ($d as $col => $val) {
                     if(isset($input['ur']["$col"]))
                    {
                        $input['ur']["$col"]["default"] = $val;   
                    }
                }
            }
          
            

            
        }
        
        
        
        $config[0] = $input;
        $config[1] = "UX";
        $this->load->library("ux", $config);
        $data["form"] = $this->ux->rendeForm($spot = array("ur"));

        
        
        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
        

        
    }
    
    
    public function manage()
    {
            $data["username"] = $this->userName;

                $data['method'] = "qcms/supcat";
        $data["winhand"] = "supcat";
        $conx["tableHANDELER"] = "class='graidtable'";
        $conx["dataHD"] = array("#", "sub name");
        $condation = array("is_orginal", "", 1);
        $conx["data"] = $this->data_model->GetCondiction("subcats");
        $conx["primKEY"] = "subcats_id";
        $conx["tabeCOLS"] = array("subcats_id", "subcats_name");
        $conx["tableName"] = "subcats";
        $conx["deletHANDELER"] = "Delete";
        $conx["editHANDELER"] = "Edit";
         $conx["editUrl"] = "admin/Supcat";

        $this->load->library("uxdata", $conx);
        $data["grid"] = $this->uxdata->render();
        
        $data["cont"] = "ui/grid";

        $this->load->view("admin/adminmaster", $data);
    }
    //put your code here
}
