<?php


class Category extends CI_Controller {
    
    public $langList;
    public $currunt_lang ;
   public $stsicwords;
   public $catlist;
   
   
    public $slinks;
    public $KV;
   
    public function __construct() {
        parent::__construct();
        
               $this->load->model("site_model");
               $this->load->model("Seo");
               
        $this->langList = $this->site_model->getLangList();
        $this->currunt_lang = $this->site_model->GetLangRUN();
        $this->stsicwords = $this->site_model->getStaticWords($this->currunt_lang);
        $this->catlist = $this->site_model->genreateCatList($this->currunt_lang);
       
         $this->slinks = $this->site_model->get("social_links" , false , false , false) ;
       $this->KV =  $this->site_model->get("key_setting" , false , false , array("lang_id" , $this->currunt_lang) , FALSE , FALSE, FALSE,FALSE , array("key" , "val")) ;
        //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
    }
    
    public function index() {
         foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }
         $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       //$data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
       //$data["category"] = $this->site_model->get("category" , false , false , array("category_lang_id" , $this->currunt_lang));
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
      
       
       $joine = array("category" , "category_pcategory_id = pcategory_id" , "left");
       $dax = $this->site_model->get("pcategory" ,  FALSE , array("category_score", "DESC") , FALSE ,FALSE , FALSE ,  $joine) ;
       $dao = array();
       
       foreach ($dax as $k=>$v){
           
           $dao[$v->pcategory_id]["id"] = $v->pcategory_id  ;
           $dao[$v->pcategory_id]["name"] = $v->pcategory_name  ;
           $dao[$v->pcategory_id]["img"] = $v->pcategory_img  ;
           if(isset($dao[$v->pcategory_id]["supcat"])){
           
               
            if($this->currunt_lang != 1){
                   $dao[$v->pcategory_id]["supcat"][$v->category_id] = $v->category_name_ar  ;
               }  else {
                   $dao[$v->pcategory_id]["supcat"][$v->category_id] = $v->category_name  ;
               }
           
           }else
            {
                if($this->currunt_lang != 1){
                   $dao[$v->pcategory_id]["supcat"][$v->category_id] = $v->category_name_ar  ;
               }  else {
                   $dao[$v->pcategory_id]["supcat"][$v->category_id] = $v->category_name  ;
               }
                }
           
       }
       $data["siteconfig"] = $this->site_model->Getwebsite();
       $data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
       
       $data["first3"] = $dao;
       $data["name"] = "category";
       $data["bodyview"] = "master/cat";
       $this->load->view("master" , $data);
    }
    
    
    
    public function v() {
        
         foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }
        $item = $this->uri->segment(3);
        
        $page = 0;
        if($this->input->get("page")){
            $page = $this->input->get("page");
        }
        
        if($this->currunt_lang == 1){
            $catx = $this->site_model->get("category" , false , false , FALSE , array("category_name" , urldecode($item)));
        }  else {
                $catx = $this->site_model->get("category" , false , false , FALSE , array("category_name_ar" , urldecode($item)));
        }
        
        $catid = 0;
        $catdisc = "";
        $img = FALSE;
        if($catx != FALSE)
        {
        foreach ($catx as  $value) 
            {
                $catid = $value->category_crm_id;
                $catdisc = $value->category_disc;
                $img = $value->category_img;
            }
            $this->db->query("UPDATE category SET category_score = category_score+1 WHERE category_id =".$catid);
        }
        
        
        $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
       $data["catdisc"]  = $catdisc;
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       $data["selectedcat"] = $item;
       $data["category"] = $this->site_model->get("category" , false , false , FALSE);
       $data["imgcat"] = $img;
       $joins = array("products_titles_cat" , "products_titles_cat.ptc_title_id = programes.programes_id" , "left");
       $t = ceil($this->site_model->getCountForCat($catid) /25);
       $data['total'] = $t ;
       $links = array();
       for($i = 0 ; $i < $t ; $i++)
       { 
           
       $links[] = base_url()."category/v/". $item."?page=".$i;
       }
       $data["links"] = $links;
       $data["programes"] =$this->site_model->getproductbycat($catid ,25 , $page*25);  //$this->site_model->get("programes" , false , array("programes_start" , "DESC") , array("programes_lang_id" , $this->currunt_lang) , array("products_titles_cat.ptc_category_id" ,$catid),   false , $joins  ,  FALSE , FALSE);
       
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
      
       
       $data["siteconfig"] = $this->site_model->Getwebsite();
       $data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
       $data["smallfooter"] = $this->site_model->Getsmallfooter($this->currunt_lang);
       
       $data["name"] = "category";
       $data["bodyview"] = "master/catview";
       $this->load->view("master" , $data);
    }
    
    public function m() {
        
         foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }
        $item = $this->uri->segment(3);
        $uuid = $this->input->get("uuid");
        $j = array("events" , "events.events_program_id=programes.programes_id" ,"left");
        //array("programes_lang_id" , $this->currunt_lang)
        $data["programes"] = $this->site_model->get("programes" , 1 , FALSE , FALSE , array("programes_id" ,$uuid) , FALSE );
    
        $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
        $catname = "";
        $crm_id = "";
        
        //$catx = $this->site_model->get("category" , false , false , array("category_lang_id" , $this->currunt_lang) , array("category_id" , $catid));
      
        foreach ( $data["programes"] as  $va) 
            {
                $crm_id = $va->programes_crm_id;
                
            }
        
       $data["evntlist"] = $this->site_model->get("events" , false , array("events_start" , "DESC") , array("events_program_id" , $crm_id)); 
       
       $data["catdisc"]  = $catname;
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       $data["selectedcat"] = $item; //??
       $data["category"] = $this->site_model->get("category" , false , false , array("category_lang_id" , $this->currunt_lang));
       
      $data["seo"] = $this->Seo->toString($data["programes"]);
       
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
     
       
       $data["siteconfig"] = $this->site_model->Getwebsite();
       $data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
       $data["smallfooter"] = $this->site_model->Getsmallfooter($this->currunt_lang);
       
       $data["name"] = "category";
       $data["bodyview"] = "master/evview";
       $this->load->view("master" , $data);
   
       
            }
    
    
}
