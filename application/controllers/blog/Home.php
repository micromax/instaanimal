<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of home
 *
 * @author sadeem-pc
 */

class Home extends MY_Central{
   
   public $langList;
   public $currunt_lang ;
   public $stsicwords;
   public $catlist;
   public $barndlist;
   public $slinks;
   public $KV;
  
   

   public function  __construct()
    {
        parent::__construct();

        $this->load->model("master_model");
        $this->load->model("site_model");

        $this->load->model("data_model");

        $this->load->model("Citys_model");

        $this->langList = $this->site_model->getLangList();
        $this->currunt_lang = $this->site_model->GetLangRUN();
        $this->stsicwords = $this->site_model->getStaticWords($this->currunt_lang);
        $this->catlist = $this->site_model->genreateCatList($this->currunt_lang);
        $this->barndlist = $this->site_model->genreateBrandlist($this->currunt_lang);
        $this->slinks = $this->site_model->get("social_links" , false , false , false) ;
        $this->KV =  $this->site_model->get("key_setting" , false , false , array("lang_id" , $this->currunt_lang) , FALSE , FALSE, FALSE,FALSE , array("key" , "val")) ;
        
        //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
    }
    
    
    public function index(){
        
        
      define('THEME','jobportal');
        
      foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }
       
       $data["lang"] = $this->langList;
       
       $data["lid"] = $this->currunt_lang;
       
       $data["catlist"] = $this->catlist;
       
       $data["slinks"] = $this->slinks;
       
       $data["sw"] = $this->stsicwords;
       
       $data["brandist"] = $this->barndlist;
       
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
       
       $data["langs"] = $this->master_model->get_langs();
       
       $data["siteconfig"] = $this->site_model->Getwebsite();
       
       $data["footerlinks"] = $this->site_model->GetFotterinJson($this->currunt_lang); 
        
        //$this->Templets
        					
		$this->template
                        ->set_partial("header" , "partials/header" , $data)
                         ->set_partial("menu" , "partials/menu" , $data)    
                        ->set_partial("welcome" , "partials/welcome" , $data)
                       
                        ->set_partial("footer" , "partials/footer" , $data)
			->set_layout('layouts/main')
			->enable_parser(FALSE)
			->title("CodeIgniter Mutiple Themes")			
			->build('homepage/index', $data);
              //  var_dump($this->template);
    }
}
