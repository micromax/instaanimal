<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pages
 *
 * @author sadeem-pc
 */
class Pages  extends CI_Controller{
    
    
      public $langList;
   public $currunt_lang ;
   public $stsicwords;
   public $catlist;
   public $barndlist;
   
   public $slinks;
public $KV;


  public function  __construct() {
        parent::__construct();
         $this->load->model("site_model");
        $this->langList = $this->site_model->getLangList();
        $this->currunt_lang = $this->site_model->GetLangRUN();
        $this->stsicwords = $this->site_model->getStaticWords($this->currunt_lang);
        $this->catlist = $this->site_model->genreateCatList($this->currunt_lang);
        $this->barndlist = $this->site_model->genreateBrandlist($this->currunt_lang);
         $this->slinks = $this->site_model->get("social_links" , false , false , false) ;
        //$this->wordofabout40 = $this->site_model->get("about" , $limit = 1 , false , array("lang_id" , $this->currunt_lang));
        //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
        $this->KV =  $this->site_model->get("key_setting" , false , false , array("lang_id" , $this->currunt_lang) , FALSE , FALSE, FALSE,FALSE , array("key" , "val")) ;
    }
    
    
    public function index(){
        
        redirect("About");
    }

    



    public function v()
    {

        $uuid = $this->input->get("pid");
        
         foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }
       $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["expage"] = $this->site_model->get("pages" , null , NULL , array("pages.pages_lang" , $this->currunt_lang));
       $joine = array("category" , "category_pcategory_id = pcategory_id" , "left");
       $dax = $this->site_model->get("pcategory" ,  FALSE , array("category_score", "DESC") , FALSE ,FALSE , FALSE ,  $joine) ;
       
       $joins = array("programes" , " events.events_program_id = programes.programes_crm_id  " , "LEFT");
       $gb = array("events.events_program_id" ,"events.events_start" );
       $data["category"] = $this->site_model->get("category");
        $data['citys'] = $this->site_model->geycity();
       
     //  $data["last5"] = $this->site_model->get("events" ,   4 , array("events_start" , "DESC" ) , array("programes.programes_lang_id" , $this->currunt_lang) , false ,  FALSE , $joins   , $gb , FALSE);
       $data["sbx"] = $this->site_model->GetSearchBox($this->currunt_lang);
       $dao = array();
       
             $data["docs"] = $this->site_model->get("decoments" , null , NULL , array("decoments.decoments_lang" , $this->currunt_lang));
       $data["citys_c"] = $this->site_model->getCount("citys");
       
       $data["programes_c"] = $this->site_model->getCount("events");
       
       $data["category_c"] = $this->site_model->getCount("category");
       
        $data["contacts"] = $this->site_model->get("contacts" , false , false , array("contacts_lang_id" , $this->currunt_lang));
        
        $data["ww"] = $this->site_model->get("welcomword" , 1 , false , array("ww_lang_id" , $this->currunt_lang));
       
        $data["lang"] = $this->langList;
       $data["lid"] = $this->currunt_lang;
       $data["catlist"] = $this->catlist;
       //get static
       $data["slinks"] = $this->slinks;
       $data["sw"] = $this->stsicwords;
       //$data["brandist"] = $this->barndlist;
       $data["arts"] = $this->site_model->get("paragraphs" , false , array( "paragraphs_id" ,"ASC") , array("paragraphs_page_id" , $uuid));
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
       
       $data["siteconfig"] = $this->site_model->Getwebsite();
       $data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
       $data["smallfooter"] = $this->site_model->Getsmallfooter($this->currunt_lang);
       
       
     //  $data["bodyview"] = "master/pagemain";
       $this->load->view("Masterexpage" , $data);

    }

    //put your code here
}
