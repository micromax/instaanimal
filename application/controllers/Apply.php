<?php


class Apply extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
    }
    
    public function index()
    {
        $this->load->view("master/applyform");
    }
    
    
    public function ajax(){
        $map = array();
        $map["fullname"] = "fullname";
        $map["phone"] = "phone";
        $map["email"] = "email";
        $map["address"] = "address";
        $map["gender"] = "gender";
        $map["age"] = "age";
        $map["marital"] = "marital";
        $map["college"] = "college";
        $map["degree"] = "degree";
        $map["job"] = "job";
        $map["salary"] = "salary";
        $map["skills"] = "skills";
        foreach ($map as $mk=>$mv)
        {
            if($this->input->post($mk) && $this->input->post($mk) != " " )
            {
                $map[$mk] = $this->input->post($mk);
            }
        }
        
        $this->db->insert("apply" , $map);
        echo "Thank you we will call you soon ";
        
        
    }
}
