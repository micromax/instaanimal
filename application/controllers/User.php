<?php

class User extends MY_Central {
  
    public function  __construct()
    {
        parent::__construct();

        $this->load->model("master_model");
        $this->load->model("site_model");

        $this->load->model("data_model");

        $this->load->model("Citys_model");

         //$this->load->model('logg_model');
        //$this->logg_model->inilmap();
          define('THEME','jobportal');
          if($this->islogin == TRUE)
          {
              
          }  else {
              redirect('Userlogin');
          }
    }
    
    
    
    public function index() 
    {
         
            $data["islogin"] = $this->islogin;
               $data["userkey"] = $this->userkey;
               $data["username"] = $this->username;
               $data["userFullname"] = $this->userFullname;
               
      foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }
          $data["sbx"] = $this->site_model->GetSearchBox($this->currunt_lang);
       $data["lang"] = $this->langList;
       
       $data["lid"] = $this->currunt_lang;
       
       $data["catlist"] = $this->catlist;
       
       $data["slinks"] = $this->slinks;
       
       $data["sw"] = $this->stsicwords;
       
       $data["brandist"] = $this->barndlist;
         $data["category"] = $this->site_model->get("category");
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
   
       $data["langs"] = $this->master_model->get_langs();
          $sel = NULL;
          $joins = array("programes" , " events.events_program_id = programes.programes_crm_id  " , "LEFT");
            $gb = array("events.events_program_id" ,"events.events_start" );
         
            $data["last5"] = $this->site_model->get("items" ,   12 , array("items_date_add" , "DESC" ) , array( "items_add_by" , $this->userkey) , false ,  FALSE , null   , null , null);
      //     $data["arts"] = $this->site_model->get("articls" , 4 , array( "articls_date" ,"DESC") , array("articls_lang_id" , $this->currunt_lang));
       $data["siteconfig"] = $this->site_model->Getwebsite();
       
       $data["footerlinks"] = $this->site_model->GetFotterinJson($this->currunt_lang); 
            $data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
        //$this->Templets
        					
			$this->template
                        ->set_partial("header" , "partials/header" , $data)
                         ->set_partial("menu" , "partials/menu" , $data)    
                      
                         ->set_partial("welcome" , "partials/searchbox" , $data)
                        ->set_partial("footer" , "partials/footer" , $data)
			->set_layout('layouts/explore')
			->enable_parser(FALSE)
			->title("CodeIgniter Mutiple Themes")			
			->build('homepage/explore', $data);
    }
    
    public function newpost() {
        
        
              $data["islogin"] = $this->islogin;
               $data["userkey"] = $this->userkey;
               $data["username"] = $this->username;
               $data["userFullname"] = $this->userFullname;
               
      foreach($this->KV as $value){
           $data["$value->key"] = $value->val ;
       }
          $data["sbx"] = $this->site_model->GetSearchBox($this->currunt_lang);
       $data["lang"] = $this->langList;
       
       $data["lid"] = $this->currunt_lang;
       
       $data["catlist"] = $this->catlist;
       
       $data["slinks"] = $this->slinks;
       
       $data["sw"] = $this->stsicwords;
       
       $data["brandist"] = $this->barndlist;
         $data["category"] = $this->site_model->get("category");
       $data["mlist"] = $this->site_model->getList($this->currunt_lang);
        
       $data["langs"] = $this->master_model->get_langs();
 
     //      $data["arts"] = $this->site_model->get("articls" , 4 , array( "articls_date" ,"DESC") , array("articls_lang_id" , $this->currunt_lang));
       $data["siteconfig"] = $this->site_model->Getwebsite();
       
       $data["footerlinks"] = $this->site_model->GetFotterinJson($this->currunt_lang); 
            $data["footer"] = $this->site_model->GenFooter($this->currunt_lang);
        //$this->Templets
        					
			$this->template
                        ->set_partial("header" , "partials/header" , $data)
                         ->set_partial("menu" , "partials/menu" , $data)    
                      
                         ->set_partial("welcome" , "partials/searchbox" , $data)
                        ->set_partial("footer" , "partials/footer" , $data)
			->set_layout('layouts/explore')
			->enable_parser(FALSE)
			->title("CodeIgniter Mutiple Themes")			
			->build('homepage/newpost', $data); 
        
        
        
    }
    
    
    
    public function addjob()
    {  $this->load->model("uploads");
        $this->load->helper('date');
        $now = date("Y-m-d");
      
       
         $thismonth = date("m");

                if ($thismonth != 12) {
                    (string) $m = date("m") + 1;
                    (string) $Y = date("Y");
                    (string) $d = date("d");
                } else {
                    (string) $m = 1;
                    (string) $Y = date("Y") + 1;
                    (string) $d = date("d");
                }
                $final =  date($Y."-".$m."-".$d);
                
                //$valic = human_to_unix($final);
                //$final = unix_to_human($valic);
                
        $data["items_name"] = $this->input->post("items_name");
        $data["items_cat_id"] = $this->input->post("items_cat_id");
        $data["items_description"] = $this->input->post("items_description");
        $data["items_date_add"] = $now;
        $data["items_add_by"] = $this->userkey;
      
        $data["items_img"]  = $this->uploads->upfilex();
        
        
 
               
        
      $this->db->insert("items" , $data);
      
      $id = $this->db->insert_id();
      $arry1  = $this->input->post("fn");
      $arry2  = $this->input->post("fv");
      
      
      if(is_array($arry1) && isset($arry1) && sizeof($arry1) > 0)
      {
          for($i = 0 ; $i < sizeof($arry1) ;$i++)
          {
                    $fet["features_item_id"] = $id;
                    $fet["features_name"] = $arry1[$i];
                    $fet["features_value"] = $arry2[$i];
                    
                    $this->db->insert("featuresmanager" , $fet); 
          }
          
      }
      
      
        redirect('user');
        
    }

    public function applications() 
    {
        
    }
    
    public function myposts() 
    {
        
    }
    
    
    
}
