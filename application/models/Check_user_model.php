<?php

class Check_user_model extends CI_Model {

    public $userdata;
    
    public function  __construct() {
        
        parent::__construct();
        
       $this->db->reconnect();
    }

    public function chekData()
    {
        $input['USER'] = $this->input->post("username");
        $input['PASS'] = $this->input->post("passpw");
        

        $this->userdata = $this->authrized->auth($input);
     
        if($this->userdata)
         {
            
            
            return $this->authrized->getDATA();

        }
        else
         {
            return  FALSE;
        }


    }

    public function chekUserViaSession()
    {
        $this->db->where('username',$this->session->Get_mySession("userName"));
        $this->db->where('userkey', $this->session->Get_mySession("userKey"));
        $this->db->where('token', $this->session->Get_mySession("userToken"));
        $this->db->where('userpassword', $this->session->Get_mySession("userpass"));
        $this->db->where('lastip', $_SERVER['REMOTE_ADDR']);
        $this->db->where('isonline',  1);
        $query = $this->db->get('user_login');

        if($query->num_rows() == 1)
            {
                return TRUE;
            }
            
    }
    
    
    public function checkQuserSession(){
        
        $this->db->where('username',$this->session->Get_mySession("user"));
        $this->db->where('token', $this->session->Get_mySession("token"));
        $this->db->where('userpassword', $this->session->Get_mySession("pw"));
        $this->db->where('lastip', $_SERVER['REMOTE_ADDR']);
        $this->db->where('isonline',  1);
        $query = $this->db->get('aac_login');

        if($query->num_rows() == 1)
            {
                return TRUE;
            }
            
    }

    public function chekUserViaCookie()
    {
        $this->db->where('username', $this->encrypt->decode(get_cookie("username")));
        $this->db->where('userkey',  $this->encrypt->decode(get_cookie("userkey")));
        $this->db->where('userpassword',  $this->encrypt->decode(get_cookie("useros")));
        $this->db->where('isonline',  1);


	$query = $this->db->get('user_login');

        if($query->num_rows() == 1)
               	{
             foreach ($query->result() as $row)
                   {
                         $userlvel = $row->userlevel;

                   }
                 $datauser = array(
                        "token"  => md5(rand(10000, 9999999)),
                        "lastip"  => $_SERVER['REMOTE_ADDR'],
                        "isonline"  =>  1,
                        "accesstime" => date("Y-m-d H:i:s")
                        );
		$data = array(
                    "userName"=> $this->encrypt->decode(get_cookie("username")),
                    'userKey' => $this->encrypt->decode(get_cookie("userkey")) ,
                    'userToken'=> $datauser["token"],
                    "userLevel" => $userlvel
                    );
                    $this->authrized->UPDATA_USER_RECORED($data["userName"] ,$data["userKey"] ,$datauser);
                    $this->session->Create_mySession($data);
                    return TRUE;
                }
    }


    public function chekUserToken()
    {
        $this->db->where('userkey', $this->session->Get_mySession("userKey"));
        $this->db->where('token', $this->session->Get_mySession("userToken"));
        $query = $this->db->get('user_login');

        if($query->num_rows() == 1)
            {
                return TRUE;
            }


    }

    public function chekUserLevel($DpartmentLevel)
    {
        $this->db->where('userkey', $this->session->Get_mySession("userKey"));
        
        $query = $this->db->get('user_login');

        if($query->num_rows() == 1)
            {
              foreach ($query->result() as $row)
                {
                 $userlvel = $row->userlevel;
                }

                if($userlvel == $DpartmentLevel)
                    {
                    
                    return TRUE;
                    }
                else
                    {
                        return FALSE;
                    }

                    
            }
            else
            {
                    return FALSE;
            }
    }

    
}
