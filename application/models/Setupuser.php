<?php


class Setupuser extends CI_Model{
    public $userdb;
    public $results;
    public $masterDB = "BES1";
    public $A_DB ;
    public $tablestree;
    public $B_DB;
    
    public function __construct() {
        parent::__construct();
            $this->config->load('database');
        
            
            $config['hostname'] = $this->config->item("hostname");
            $config['username'] = $this->config->item("username");
            $config['password'] = $this->config->item("password");
            $config['database'] = "$this->masterDB";
            $config['dbdriver'] = $this->config->item("dbdriver");
            $config['dbprefix'] = $this->config->item("dbprefix");
            $config['pconnect'] = $this->config->item("pconnect");
            $config['db_debug'] = $this->config->item("db_debug");
            $config['cache_on'] = $this->config->item("cache_on");
            $config['cachedir'] = $this->config->item("cachedir");
            $config['char_set'] = $this->config->item("char_set");
            $config['dbcollat'] = $this->config->item("dbcollat");
            
            $this->A_DB = $this->load->database( $config, TRUE);
            $this->getTables();
            //$this->A_DB->close();
    }
    
    
    
    public function setdabname($userkey ,$username){
        
        $this->results["DBNAME"] = "ok";
        $this->userdb = "bse_".$userkey."_".$username;
    }

    public function createdatabase()
    {
        $this->load->dbforge();
        if ($this->dbforge->create_database($this->userdb))
        {
           $this->results["CREATEDB"] = "ok"; 
        }  else {
           $this->results["CREATEDB"] = "erorr"; 
        }
    }
    
    public function copymastertables() 
    {
        //create table db2.t2 like db1.t1
        
        foreach ($this->tablestree as $key => $value) {
            $query = "create table ".$this->userdb.".".$value." like ".$this->masterDB.".".$value;
            $this->A_DB->query($query);
        }
        $this->A_DB->close();
    }
    
    public function createdefaultuser($quser , $qpass , $date2)
    {
            $config['hostname'] = $this->config->item("hostname");
            $config['username'] = $this->config->item("username");
            $config['password'] = $this->config->item("password");
            $config['database'] = "$this->userdb";
            $config['dbdriver'] = $this->config->item("dbdriver");
            $config['dbprefix'] = $this->config->item("dbprefix");
            $config['pconnect'] = $this->config->item("pconnect");
            $config['db_debug'] = $this->config->item("db_debug");
            $config['cache_on'] = $this->config->item("cache_on");
            $config['cachedir'] = $this->config->item("cachedir");
            $config['char_set'] = $this->config->item("char_set");
            $config['dbcollat'] = $this->config->item("dbcollat");
            
            $this->B_DB = $this->load->database( $config, TRUE);
            
            $dafta["username"] = $quser;
            $dafta["userpassword"] = md5($qpass);
            
            $this->B_DB->insert("user_login" ,$dafta );
            $this->B_DB->close();
            $this->newconnection($quser , $qpass , $date2);
        
    }
    
    
    public function newconnection($quser , $qpass , $date2){
           
            $this->db->where("username" , $quser );
            $this->db->where("userpassword" , md5($qpass) );
            $ofla["databasename"] = $this->userdb;
            
            $ofla["validUntile"] = $date2;
            $ofla["packedg_id"] = "5";
            
            
            $this->B_DB->update("qsystic.aac_login" ,$ofla );
            
    }

        public function R(){
        return $this->results;
    }
    
   public function getTables()
   {
          $this->tablestree = $this->A_DB->list_tables();
   }
      
     
    
    
 
}
