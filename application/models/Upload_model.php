<?php

class Upload_model  extends CI_Model {

    

    public function  __construct() {

        parent::__construct();

        
        }


        public function do_uploads($data)
        {
            $config['allowed_types'] = "jpg|gif|jpeg|png";
            $config['encrypt_name'] = true ;
            $config['max_size'] = 300 ;
            $config['upload_path'] = './uploads' ;
            $this->load->library('upload' ,$config );
            
            if($this->upload->do_upload($data))
             {
                
                $imagedata = $this->upload->data();

                
                //$configs['image_library'] = 'gd2';
                $configs['source_image']	= $imagedata['full_path'];
                //$configs['master_dim'] = "auto";
                $configs['maintain_ratio'] = TRUE;
                $configs['new_image'] = "./uploads/thumb";
                $configs['width']	 = 380;
                $configs['height']	= 240;

                $this->load->library('image_lib', $configs);
                $this->image_lib->resize();
                $imagedata['error'] = $this->image_lib->display_errors();
                
             return $imagedata;
             }
             else
                 {
                 return false;
             }
            
            
        }
        public  function  __destruct() {
            
        }
       
 
}

