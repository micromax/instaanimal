<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of design
 *
 * @author sadeem-pc
 */
class Design extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    
    
    
    public function __add_design($post_data){
        
        $this->db->insert('design', $post_data);
         $insert_id = $this->db->insert_id();

         return  $insert_id;
    }
    
    
    public  function __edit_design($post_data , $id){
         $this->db->where("design_id" , $id );
         $this->db->update("design" , $post_data);
      
    }
    
    
    public  function __getalls()
    {
        $this->db->where("design_html !=" ,null);
        $r = $this->db->get("design")->result_array();
        
        
        return json_encode($r);
    }
    
    public  function __getbyid($id)
    {
        $this->db->where("design_id" ,$id);
        $this->db->limit(1);
        $this->db->where("design_html !=" ,null);
        $r = $this->db->get("design")->result_array();
        
        
        return json_encode($r);
    }
}
