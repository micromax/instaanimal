<?php

class Logg_model extends CI_Model {

    private $userIP;
    
    public function __construct() {
        parent::__construct();
        $this->db->reconnect();
    }

    public function GetIP() {
        //lap 1 
        
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        

        return $ip;// sprintf(" GetIP Elapsed:  %f", $now-$then);
       
    }

    public function setuserIP($ip) {
       // $then = microtime();
        $this->userIP = ip2long($ip);
         //$now = microtime();

         return $this->userIP; 
    }

    public function GetAgentType() {
       /// $then = microtime();
        $this->load->library('user_agent');
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
         

        //echo sprintf(" GetAgentType Elapsed:  %f", $now-$then);
        return $agent;
    }
    
    public function getAinfo(){
   //     $then = microtime();
        $this->load->library('user_agent'); 
      $d["platform"] =  $this->agent->platform();
      $d["browser"] =  $this->agent->browser();
      $d["version"] =  $this->agent->version();
      $d["accept_lang"] =  $this->agent->accept_lang();
      $d["referrer"] = $this->agent->referrer();
      $d["agent_string"] =  $this->agent->agent_string();
     // $now = microtime();

    //    echo sprintf(" getAinfo Elapsed:  %f", $now-$then);
      return $d;
    }
    
    public function getCaC()
    {
       // $then = microtime();
        
        $data["city"] = "unknown";
        $data["country"] = "unknown";
        $this->db->where("ip_from <=" , $this->userIP);
        $this->db->where("ip_to >=" , $this->userIP);
        //$sql = "SELECT * FROM ip2location_db3 where ip_from <= ? AND ip_to >= ?  LIMIT 1";
        //$q = $this->db->query($sql , array($this->userIP ,$this->userIP ) );
        $q = $this->db->get("ip2location_db3", 1);
        if($q->num_rows() > 0)
         {
            
            foreach ($q->result() as  $value) 
            {
                $data["city"] = $value->city_name;
                $data["country"] = $value->country_code;
            }
         }
       //   $now = microtime();

      //  echo sprintf(" getCaC Elapsed:  %f", $now-$then);
         return $data;
        
    }
    
    
    
    public function inilAPI($uri , $refl)
            
    {
        //$then = microtime();
        flush ();
        ob_flush ();
        if (connection_aborted () == 1) {
        ob_flush();
            flush();
            exit();
        die ();
        }

        $ip = $this->GetIP();
        $info = $this->getAinfo();
        $this->setuserIP($ip);
        $CaC = $this->getCaC();
        $insert = array();
        $insert["statistic_url"]  =$uri ;
        $insert["statistic_time"] = date("Y-m-d H:i:s");
        $insert["statistic_unixtime"] =  human_to_unix($insert["statistic_time"]);
        $insert["statistic_source"] = $refl ;
        $insert["statistic_ip"] = $ip;
        $insert["statistic_country"] = $CaC["country"] ;
        $insert["statistic_city"]  = $CaC["city"] ;
        $insert["statistic_browser"] = $info["browser"] ;
        $insert["statistic_os"] = $info["platform"] ;
        $insert["statistic_atype"] = $this->GetAgentType() ;
        
        $insert["statistic_pagename"] = $uri;
        $insert["statistic_lang"] = $info["accept_lang"];
        $insert["statistic_agenttxt"] = $info["agent_string"] ;
        
        
        $this->db->insert( "statistic" , $insert);
         $this->db->close();
        
     //    $now = microtime();

      //  echo sprintf(" Elapsed inilAPI:  %f", $now-$then);
        
        
        
    }

        public function inilmap()
    {
      
            /*
             $ip = $this->GetIP();
             
        $info = $this->getAinfo();
        $this->setuserIP($ip);
        $CaC = $this->getCaC();
        $insert = array();
        
        $insert["statistic_url"]  =$_SERVER['REQUEST_URI'] ;
        $insert["statistic_time"] = date("Y-m-d H:i:s");
        $insert["statistic_unixtime"] =  human_to_unix($insert["statistic_time"]);
        $insert["statistic_source"] = $info["referrer"] ;
        $insert["statistic_ip"] = $ip;
        $insert["statistic_country"] = $CaC["country"] ;
        $insert["statistic_city"]  = $CaC["city"] ;
        $insert["statistic_browser"] = $info["browser"] ;
        $insert["statistic_os"] = $info["platform"] ;
        $insert["statistic_atype"] = $this->GetAgentType() ;
        
        $insert["statistic_pagename"] = current_url();
        $insert["statistic_lang"] = $info["accept_lang"];
        $insert["statistic_agenttxt"] = $info["agent_string"] ;
        
        
        $this->db->insert( "statistic" , $insert);
         $this->db->close();
             
             */
    }
    //current_url(); / / $_SERVER['REQUEST_URI']
    

}
