<?php

class Uploads extends CI_Model {


    public function upimg($dirs)
    {
       

       $dir = $dirs;
       $file = current($_FILES); // we handle the only file in time

	if($file['error'] == UPLOAD_ERR_OK) {
	if(@move_uploaded_file($file['tmp_name'], "{$dir}/{$file['name']}"))
			$file['error']	= ''; //no errors, 0 - is our error code for 'moving error'
	}

	$arr = array(
		'error' => $file['error'],
		'file' => "{$dir}/{$file['name']}",
		'tmpfile' => $file['tmp_name'],
		'size' => $file['size']
	);

	if(function_exists('json_encode'))
		return json_encode($arr);

	$result = array();

	foreach($arr as $key => $val) {
		$val = (is_bool($val)) ? ($val ? 'true' : 'false') : $val;
		$result[] = "'{$key}':'{$val}'";
	}

	return  '{' . implode(',', $result) . '}';
        
    }

    public function do_uploads()
        {
            $config['allowed_types'] = "jpg|gif|jpeg|png|csv|cvs|xml|doc|docx|ppt|pdf|xls|xlsx";
            $config['encrypt_name'] = true ;
            
            $config['upload_path'] = './uploads' ;
            $this->load->library('upload' ,$config );

            if($this->upload->do_upload())
             {

                $imagedata = $this->upload->data();

                
		
                $data["error"]      = $this->upload->display_errors();
                $data["file"]       =  base_url()."uploads/".$imagedata['file_name'];
                $data["file_size"]    = $imagedata['file_size'];

                $updata["file_id"]          = rand("10000000", "99999999");
                $updata["file_type"]        =  $imagedata['file_type'];
                $updata["file_real_name"]   =  $imagedata['orig_name'];
                $updata["file_server_name"] = $imagedata['file_name'];
                $updata["file_path"]        = $data["file"];

                

                
                $this->db->insert("pics" , $updata);


                json_encode($data);
                $result = array();

                $result["error"] =  $data["error"] ;
                
                $msg =  "" ;
                $msg .= " File Name: " . $updata["file_real_name"] . ", ";
		$msg .= " File Size: " . $updata["file_server_name"];
                $msg .= " Massage: " . "تم التحميل";

              foreach($data as $key => $val) 
                    {
                     $val = (is_bool($val)) ? ($val ? 'true' : 'false') : $val;
                     $result[] = "'{$key}':'{$val}'";
                    }
                 $error = $this->upload->display_errors();

                //return '{' . implode(',', $result) . '}';
                return    "{"."error: '" . $error . "',\n"."msg: '" . $msg . "'\n"."}";

             }
             else{
                 return  '{error : ' .$this->upload->display_errors() . '}' ;
             }
            


        }


  public function upfile()
        {
            $config['allowed_types'] = "jpg|gif|jpeg|png|tiff";
            $config['encrypt_name'] = true ;

            $config['upload_path'] = './uploads' ;
            $this->load->library('upload' ,$config );

            if($this->upload->do_upload())
             {

                $imagedata = $this->upload->data();



                $data["error"]      = $this->upload->display_errors();
                $data["file"]       =  base_url()."uploads/".$imagedata['file_name'];
                $data["file_size"]    = $imagedata['file_size'];

                $file = "uploads/".$imagedata['file_name'];
                $thim = "uploads/small/".$imagedata['file_name'];


              
              
              
                $updata["pic_server_abslout"]        = $imagedata['file_name'];

                list($in_w, $in_h) = getimagesize($file);
                   $extension = $imagedata['file_ext'];

                if($this->input->post("junk") != '' ){

                $winput = intval($this->input->post("junk"));
                    
              

                    $new_w = $winput;
                    $new_h = ($in_h * $new_w) / $in_w;
                       if($extension == ".jpg" || $extension == ".jpeg")
                        {
                            $in = imagecreatefromjpeg($file);

                            $out = imagecreatetruecolor($new_w, $new_h);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $new_w, $new_h, $in_w, $in_h);
                             imagejpeg($out, $file , 100);

                        }
                        if($extension == ".gif")
                        {
                            $in = imagecreatefromgif($file);
                            $out = imagecreatetruecolor($new_w, $new_h);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $new_w, $new_h, $in_w, $in_h);
                             imagegif($out,$file);

                        }
                        if($extension == ".png")
                        {
                            $in = imagecreatefrompng($file);

                            $out = imagecreatetruecolor($new_w, $new_h);
                            imagealphablending($out, false);
                            imagesavealpha($out,true);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $new_w, $new_h, $in_w, $in_h);
                             imagepng($out,$file);

                        }

                        imagedestroy($in);
                        imagedestroy($out);



                

                }
               

                 list($in_w, $in_h) = getimagesize($file);
                 $thim_w = 130;
                 $thim_h = ($in_h * $thim_w) / $in_w;
                 if($extension == ".jpg" || $extension == ".jpeg")
                        {
                            $in = imagecreatefromjpeg($file);
                            $out = imagecreatetruecolor($thim_w, $thim_h);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $thim_w, $thim_h, $in_w, $in_h);
                             imagejpeg($out, $thim , 100);

                        }
                        if($extension == ".gif")
                        {
                            $in = imagecreatefromgif($file);
                            $out = imagecreatetruecolor($thim_w, $thim_h);
                           
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $thim_w, $thim_h, $in_w, $in_h);
                             imagegif($out,$thim);

                        }
                        if($extension == ".png")
                        {
                            $in = imagecreatefrompng($file);
                            $out = imagecreatetruecolor($thim_w, $thim_h);
                             imagealphablending($out, false);
                            imagesavealpha($out,true);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $thim_w, $thim_h, $in_w, $in_h);
                             imagepng($out,$thim);

                        }

                        imagedestroy($in);
                        imagedestroy($out);

                
                $this->db->insert("files" , $updata);




                json_encode($data);
                $result = array();

                $result["error"] =  $data["error"] ;

                $msg =  "" ;
              //  $msg .= " File Name: " . $updata["file_real_name"] . ", ";
            //	$msg .= " File Size: " . $updata["file_server_name"];
                $msg .= " Massage: " . "file has been uploaded " ;

              foreach($data as $key => $val)
                    {
                     $val = (is_bool($val)) ? ($val ? 'true' : 'false') : $val;
                     $result[] = "'{$key}':'{$val}'";
                    }
                 $error = $this->upload->display_errors();

                //return '{' . implode(',', $result) . '}';
                return    "{"."error: '" . $error . "',\n"."msg: '" . $msg . "'\n"."}";

             }
             else{
                 return  '{error : ' .$this->upload->display_errors() . '}' ;
             }



        }
    
        
        
        
        public function upfilex()
        {
            $config['allowed_types'] = "jpg|gif|jpeg|png|tiff";
            $config['encrypt_name'] = true ;

            $config['upload_path'] = './uploads' ;
            $this->load->library('upload' ,$config );
            
            if($this->upload->do_upload())
             {

                $imagedata = $this->upload->data();
                //var_dump($imagedata);


                $data["error"]      = $this->upload->display_errors();
                $data["file"]       =  base_url()."uploads/".$imagedata['file_name'];
                $data["file_size"]    = $imagedata['file_size'];

                $file = "uploads/".$imagedata['file_name'];
                $thim = "uploads/small/".$imagedata['file_name'];


              
              
              
                $updata["pic_server_abslout"]        = $imagedata['file_name'];

                list($in_w, $in_h) = getimagesize($file);
                   $extension = $imagedata['file_ext'];

                if($this->input->post("junk") != '' ){

                $winput = intval($this->input->post("junk"));
                    
              

                    $new_w = $winput;
                    $new_h = ($in_h * $new_w) / $in_w;
                       if($extension == ".jpg" || $extension == ".jpeg")
                        {
                            $in = imagecreatefromjpeg($file);

                            $out = imagecreatetruecolor($new_w, $new_h);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $new_w, $new_h, $in_w, $in_h);
                             imagejpeg($out, $file , 100);

                        }
                        if($extension == ".gif")
                        {
                            $in = imagecreatefromgif($file);
                            $out = imagecreatetruecolor($new_w, $new_h);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $new_w, $new_h, $in_w, $in_h);
                             imagegif($out,$file);

                        }
                        if($extension == ".png")
                        {
                            $in = imagecreatefrompng($file);

                            $out = imagecreatetruecolor($new_w, $new_h);
                            imagealphablending($out, false);
                            imagesavealpha($out,true);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $new_w, $new_h, $in_w, $in_h);
                             imagepng($out,$file);

                        }

                        imagedestroy($in);
                        imagedestroy($out);



                

                }
               

                 list($in_w, $in_h) = getimagesize($file);
                 $thim_w = 130;
                 $thim_h = ($in_h * $thim_w) / $in_w;
                 if($extension == ".jpg" || $extension == ".jpeg")
                        {
                            $in = imagecreatefromjpeg($file);
                            $out = imagecreatetruecolor($thim_w, $thim_h);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $thim_w, $thim_h, $in_w, $in_h);
                             imagejpeg($out, $thim , 100);

                        }
                        if($extension == ".gif")
                        {
                            $in = imagecreatefromgif($file);
                            $out = imagecreatetruecolor($thim_w, $thim_h);
                           
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $thim_w, $thim_h, $in_w, $in_h);
                             imagegif($out,$thim);

                        }
                        if($extension == ".png")
                        {
                            $in = imagecreatefrompng($file);
                            $out = imagecreatetruecolor($thim_w, $thim_h);
                             imagealphablending($out, false);
                            imagesavealpha($out,true);
                            imagecopyresampled($out, $in, 0, 0, 0, 0, $thim_w, $thim_h, $in_w, $in_h);
                             imagepng($out,$thim);

                        }

                        imagedestroy($in);
                        imagedestroy($out);

                
                $this->db->insert("files" , $updata);




                json_encode($data);
                $result = array();

                $result["error"] =  $data["error"] ;

                $msg =  "" ;
              //  $msg .= " File Name: " . $updata["file_real_name"] . ", ";
            //	$msg .= " File Size: " . $updata["file_server_name"];
                $msg .= " Massage: " . "file has been uploaded " ;

              foreach($data as $key => $val)
                    {
                     $val = (is_bool($val)) ? ($val ? 'true' : 'false') : $val;
                     $result[] = "'{$key}':'{$val}'";
                    }
                 $error = $this->upload->display_errors();

                //return '{' . implode(',', $result) . '}';
              //  return    "{"."error: '" . $error . "',\n"."msg: '" . $msg . "'\n"."}";

             }
             else{
                 //return  '{error : ' .$this->upload->display_errors() . '}' ;
             }

               return $imagedata['file_name'];

        }
    
}
