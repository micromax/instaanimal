<?php

class Check_reseller_model extends CI_Model {
  public $userdata;
    
    public function  __construct() {
        
        parent::__construct();
$this->db->reconnect();
    }

    public function chekData()
    {
        $input['USER'] = $this->input->post("username");
        $input['PASS'] = $this->input->post("passpw");


        $this->userdata = $this->authrized->auth($input);
        if($this->userdata)
         {
            return $this->authrized->getDATA();

        }
        else
         {
            return  FALSE;
        }


    }

    public function chekUserViaSession()
    {
        $this->db->where('username',$this->session->Get_mySession("userName_v"));
        $this->db->where('userkey', $this->session->Get_mySession("userKey_v"));
        $this->db->where('token', $this->session->Get_mySession("userToken_v"));
        $this->db->where('userpassword', $this->session->Get_mySession("userpass_v"));
        $this->db->where('lastip', $_SERVER['REMOTE_ADDR']);
        $this->db->where('isonline',  1);
        $query = $this->db->get('rs_login');

        if($query->num_rows() == 1)
            {
                return TRUE;
            }
            
    }
    
    
    public function chekUserActive()
    {
        $this->db->where('username',$this->session->Get_mySession("userName_v"));
        $this->db->where('userkey', $this->session->Get_mySession("userKey_v"));
        $this->db->where('token', $this->session->Get_mySession("userToken_v"));
        $this->db->where('userpassword', $this->session->Get_mySession("userpass_v"));
        $this->db->where('lastip', $_SERVER['REMOTE_ADDR']);
        $this->db->where('isonline',  1);
        $query = $this->db->get('rs_login');

        if($query->num_rows() == 1)
            {
                foreach($query->result() as $row) {}
               
                    if($row->activesys == 1)
                        {
                        return TRUE;
                        }  else {
                            return FALSE;    
                        }
                
            }
            else {
                return FALSE;
            }
            
    }
    
    

    public function chekUserViaCookie()
    {
        $this->db->where('username', $this->encrypt->decode(get_cookie("username_v")));
        $this->db->where('userkey',  $this->encrypt->decode(get_cookie("userkey_v")));
        $this->db->where('userpassword',  $this->encrypt->decode(get_cookie("useros_v")));
        $this->db->where('isonline',  1);


	$query = $this->db->get('rs_login');

        if($query->num_rows() == 1)
               	{
             foreach ($query->result() as $row)
                   {
                         $userlvel = $row->userlevel;

                   }
                 $datauser = array(
                        "token"  => md5(rand(10000, 9999999)),
                        "lastip"  => $_SERVER['REMOTE_ADDR'],
                        "isonline"  =>  1,
                        "accesstime" => date("Y-m-d H:i:s")
                        );
		$data = array(
                    "userName_v"=> $this->encrypt->decode(get_cookie("username_v")),
                    'userKey_v' => $this->encrypt->decode(get_cookie("userkey_v")) ,
                    'userToken_v'=> $datauser["token"],
                    "userLevel_v" => $userlvel
                    );
                    $this->authrized->UPDATA_USER_RECORED($data["userName_v"] ,$data["userKey_v"] ,$datauser);
                    $this->session->Create_mySession($data);
                    return TRUE;
                }
    }


    public function chekUserToken()
    {
        $this->db->where('userkey', $this->session->Get_mySession("userKey_v"));
        $this->db->where('token', $this->session->Get_mySession("userToken_v"));
        $query = $this->db->get('rs_login');

        if($query->num_rows() == 1)
            {
                return TRUE;
            }


    }

    public function chekUserLevel($DpartmentLevel)
    {
        $this->db->where('userkey', $this->session->Get_mySession("userKey_v"));
        
        $query = $this->db->get('rs_login');

        if($query->num_rows() == 1)
            {
              foreach ($query->result() as $row)
                {
                 $userlvel = $row->userlevel;
                }

                if($userlvel == $DpartmentLevel)
                    {
                    
                    return TRUE;
                    }
                else
                    {
                        return FALSE;
                    }

                    
            }
            else
            {
                    return FALSE;
            }
    }

    
}
