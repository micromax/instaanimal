<?php
class Site_model extends CI_Model{
    
        public $site;
    
    public function  __construct() {
        parent::__construct();
        //@session_start();
        
        
        $this->setdefult() ;
        $this->db->reconnect();
    }
    
    
    
    
    public function getCount($tablename)
    {
        return $this->db->count_all($tablename);
    }

    

    public function setdefult() {
        $this->site["list"]["defaults"] = "home" ;
        $this->site["list"]["about"] = "about us" ;
        $this->site["list"]["contact"] = "contact us" ;
        $this->site["list"]["products"] = "Products" ;
        $this->site["list"]["jobs"] = "jobs" ;
        $this->site["list"]["brands"] = "brands" ;
        $this->site["list"]["articles"] = "Articles" ;
        $this->site["list"]["category"] = "Category" ;


        $this->site["static"]['1']["search"] = "search" ;
        $this->site["static"]['2']["search"] = "بحث" ;

        $this->site["static"]['1']["Statistics"] = "Statistics" ;
        $this->site["static"]['2']["Statistics"] = "احصائيات" ;

        $this->site["static"]['1']["JoinourNewsletter"] = "Join our Newsletter!" ;
        $this->site["static"]['2']["JoinourNewsletter"] = "اشترك في القائمه البريدية" ;

        $this->site["static"]['1']["firstname"] = "First name" ;
        $this->site["static"]['2']["firstname"] = "الاسم الاول" ;

        $this->site["static"]['1']["lastname"] = "last name" ;
        $this->site["static"]['2']["lastname"] = "الاسم الاخير" ;

        $this->site["static"]['1']["email"] = "Email" ;
        $this->site["static"]['2']["email"] = "بريدك الاليكتروني" ;

        $this->site["static"]['1']["subscribe"] = "subscribe" ;
        $this->site["static"]['2']["subscribe"] = "اشترك معنا" ;

        $this->site["static"]['1']["TWEETER"] = "TWEETER" ;
        $this->site["static"]['2']["TWEETER"] = "تويتر" ;

        $this->site["static"]['1']["News"] = "Call us" ;
        $this->site["static"]['2']["News"] = "الاخبار" ;

        $this->site["static"]['1']["contact"] = "Contact us" ;
        $this->site["static"]['2']["contact"] = "الاتصال بنا" ;

        $this->site["static"]['1']["message"] = "message" ;
        $this->site["static"]['2']["message"] = "الرساله" ;

        $this->site["static"]['1']["sendmessage"] = "send message" ;
        $this->site["static"]['2']["sendmessage"] = "ارسل رساله" ;

        $this->site["static"]['1']["readmore"] = "read more" ;
        $this->site["static"]['2']["readmore"] = "اقراء المزيد" ;

        $this->site["static"]['1']["title"] = "invest training center" ;
        $this->site["static"]['2']["title"] = "invest training center" ;

        $this->site["static"]['1']["lang"] = "language" ;
        $this->site["static"]['2']["lang"] = "اللغه" ;

        $this->site["static"]['1']["reg"] = "Register" ;
        $this->site["static"]['2']["reg"] = "تسجيل" ;
        
        $this->site["static"]['1']["inc"] = "Inquiry" ;
        $this->site["static"]['2']["inc"] = "استعلام" ;
        
        $this->site["static"]['1']["month"] = "Month" ;
        $this->site["static"]['2']["month"] = "شهر" ;
        
        $this->site["static"]['1']["views"] = "view option" ;
        $this->site["static"]['2']["views"] = "خيارات العرض" ;
        
        $this->site["static"]['1']["listview"] = "List view" ;
        $this->site["static"]['2']["listview"] = "عرض قائمه" ;
        
        
        $this->site["static"]['1']["gridview"] = "Grid view" ;
        $this->site["static"]['2']["gridview"] = "عرض شبكي" ;
      
        
        
        $this->site["static"]['1']["standview"] = "Standerd view" ;
        $this->site["static"]['2']["standview"] = "عرض قياسي " ;
        
        
        $this->site["static"]['1']["by"] = "Written by" ;
        $this->site["static"]['2']["by"] = "كتب بواسطه" ;
        
        
      
        
        
        $this->site["static"]['1']["cattext"] = " is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. " ;
        $this->site["static"]['2']["cattext"] = "هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام هنا يوجد محتوى نصي، هنا يوجد محتوى نصي فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص،  في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها." ;
        
        
        
        
        
    }


    public function loadDefualtLang(){}

    public function getStaticWords($id)
    {
        if(isset($this->site["static"][$id]))
                {
                $sataic = $this->site["static"][$id];
                }else
                    {
                    $sataic = $this->site["static"]['1'];
                }
        return $sataic;
    }

    public function l($w , $lid = '1')
    {
        $def = '1';
       if(isset($this->site[$w][$lid]))
         {
           return $this->site[$w][$lid];
         }else {
             return $this->site[$w][$def];
         }
    }

    public function GetLangRUN(){
        
        if (isset ($_SESSION["mh_lang"])){
            return $_SESSION["mh_lang"];
        }else
        {
            return '1';
        }
    }
    
    public function SetLang($langID){
        
        $_SESSION["mh_lang"] = $langID;
    }


    public function getLangList()
    {
           $query = $this->db->get("languge");
           if($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                     {
                        $data[] = $row;
                     }
                         return $data;
                }
            else
                {
                    return FALSE;
                }
           
    }
    public function getlangDiraction($_currunt_lang){
          $this->db->where("lang_id" , $_currunt_lang);
        $query = $this->db->get("languge");
        if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data = $row->lang_orintation;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
    }

    public function Getlangstyle()
    {
        $_currunt_lang = $this->GetLangRUN();

        $diraction = $this->getlangDiraction($_currunt_lang);
        if($diraction != False)
        {
            switch($diraction)
            {
                case "ltr":
                    $style = "main.css";
                    break;

                case "rtl":
                    $style = "mainR.css";
                  break;

                default:
                    $style = "main.css";
                  break;

            }
        }else {
            $style = "main.css";
        }

        return $style;
    }



    public function getList($lngId){
        
        $this->db->where("mlist_lang_id" , $lngId);
         $query = $this->db->get("mlist");
        if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[$row->mlist_link] = $row->mlist_text;
                             }
                                 return $data;
                        }
                    else
                        {
                          
                                    return $this->site['list'];
                                
                        }

    }
    
    public function GetStandAloneProducts($langID){}

 

    public function GetbrandsList($langID){}


    public function GetCatLIst($langID)
    {
        
        $this->db->where("cat_lang_id" ,$langID);
        $query = $this->db->get("cat");
           if($query->num_rows() > 0)
              {
                 foreach ($query->result() as $row)
                      {
                             $data[] = $row;
                       }
                 return $data;
             }
             else
             {
                return False;
             }

         
    }
    public function GetBrandLIst($langID)
    {

        $this->db->where("brands_lang_id" ,$langID);
        $query = $this->db->get("brands");
           if($query->num_rows() > 0)
              {
                 foreach ($query->result() as $row)
                      {
                             $data[] = $row;
                       }
                 return $data;
             }
             else
             {
                return False;
             }


    }


    public function GetProductList($langID ,  $by = false  , $hash = false , $solo = 0 )
    {
         $this->db->where("products_lang_key" ,$langID);
        $this->db->where("products_is_solo" ,$solo);
        switch($by)
        {
            case 'cat':
          $this->db->where("products_cat_hashkey" ,$hash);
                break;
            case 'brand':
           $this->db->where("products_brand_hashkey" ,$hash);
                break;
            case FALSE:
                break;
            default:
                break;
        }
        $query = $this->db->get("products");
        
         if($query->num_rows() > 0)
              {
                 foreach ($query->result() as $row)
                      {
                             $data[] = $row;
                       }
                 return $data;
             }
             else
             {
                return False;
             }
         
    }



    public function genreateCatList($langID)
    {
        $stract = '';
        $stract .="<ul>";
        //get Catlist
        $catlist = $this->GetCatLIst($langID);
       /* if($catlist != false)
            {
                
                foreach($catlist as $cat)
                    {
                    $stract .="<li>";
                    $stract .="<a href=\"#$cat->cat_hashkey\">$cat->cat_name</a>";

                        //nisted products
                        $np = $this->GetProductList($langID ,  'cat' , $cat->cat_hashkey , 0 );
                        if($np != FALSE)
                            {
                                 $stract .="<ul>";
                                 foreach ($np as $products)
                                        {
                                            $stract .="<li id='sup'><a href='".base_url().urlfix()."products/viewproduct/$products->products_hashkey'>$products->products_name</a></li>";
                                        }
                                 $stract .="</ul>";
                            }
                     $stract .="</li>";
                    }
                    
               
            }*/
           //get solo products

              $soloproduct = $this->GetProductList($langID ,  FALSE , false , 1 );
            if($soloproduct != false){
                foreach ($soloproduct as $product)
                     {
                            $encode = urlencode($product->products_name);
                         $stract .="<li > <a href='".base_url().urlfix()."products/viewproduct/$encode'>$product->products_name</a> </li>";
                     }
            }

            $stract .="</ul>";
            return $stract;

    }
    
    
    public function genreateBrandlist($langID)
    {
        $stract = '';
        $stract .="<ul>";
        //get Catlist
        $catlist = $this->GetBrandLIst($langID);
        if($catlist != false)
            {

                foreach($catlist as $cat)
                    {
                    $stract .="<li>";
                    $stract .="<a href=\"#$cat->brands_hashkey\">$cat->brands_name</a>";

                        //nisted products
                        $np = $this->GetProductList($langID ,  'brand' , $cat->brands_hashkey , 0 );
                        if($np != FALSE)
                            {
                                 $stract .="<ul>";
                                 foreach ($np as $products)
                                        {
                                            $stract .="<li id='sup'><a href='".base_url().urlfix()."products/viewproduct/$products->products_hashkey'>$products->products_name</a></li>";
                                        }
                                 $stract .="</ul>";
                            }
                     $stract .="</li>";
                    }


            }
           //get solo products

            

            $stract .="</ul>";
            return $stract;

    }

    
    
    
    public function getCountForCat($item) {
        
              $sql = "SELECT * FROM `programes` "
                . "JOIN `products_titles_cat` ON `products_titles_cat`.`ptc_title_id` = `programes`.`programes_crm_id` "
                . "JOIN `category` ON `category`.`category_crm_id` = `products_titles_cat`.`ptc_category_id` "
                . "LEFT JOIN `events` ON `events`.`events_program_id` = `programes`.`programes_crm_id`"
                . " WHERE `products_titles_cat`.`ptc_category_id` = ".$item
                ." GROUP BY `programes`.`programes_crm_id` " ;
              
              
                $query = $this->db->query($sql);
                
                    //foreach ($query->result() as $row){}
                             
                        return $query->num_rows();
                
        
    }
    
    
    
    public function getproductbycat( $item ,  $limit  = 25 , $page = 0 ,$order = false )
    {
        //
        $sql = "SELECT * , GROUP_CONCAT(events.events_start order by events_start DESC )  AS programes_start"
                . " , GROUP_CONCAT(events.events_end order by events_start DESC )  AS programes_end"
                . " , GROUP_CONCAT(DISTINCT (events.events_locaion) ) AS programes_location FROM `programes` "
                . "JOIN `products_titles_cat` ON `products_titles_cat`.`ptc_title_id` = `programes`.`programes_crm_id` "
                . "JOIN `category` ON `category`.`category_crm_id` = `products_titles_cat`.`ptc_category_id` "
                . "LEFT JOIN `events` ON `events`.`events_program_id` = `programes`.`programes_crm_id`"
                . " WHERE `products_titles_cat`.`ptc_category_id` = ".$item
                
                ." GROUP BY `programes`.`programes_crm_id` "
                ." order by events.events_start DESC"
                . " LIMIT ".$limit ." OFFSET  $page";
        
                        
        
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                                
                             }
                             
                                 return $data;
                        }
                    else
                        {

                                    return false;

                        }
        
    }

    
    public function get($table , $limit = false , $order = false , $where = false , $w2 = false ,  $extra = FALSE , $joins = FALSE , $GB = FALSE , $select = FALSE)
    {
        
        if($select != FALSE){
            $this->db->select($select);
        }
        if($joins != false AND is_array($joins)){
            
            $this->db->join($joins[0] , $joins[1] , $joins[2]);
            
        }
        
        if($w2 != false)
            {
                $this->db->where($w2[0] , $w2[1]);
            }
      if($where != false)
        {
            $this->db->where($where[0] , $where[1]);
        }
      if($limit != false)
        {
        $this->db->limit($limit);
        }
       if($GB != false)
        {
           foreach ($GB as  $gv) {
               $this->db->group_by($gv);
           }
        
        }
        
      if($order != false)
        {
        $this->db->order_by($order[0] , $order[1]);
        if(sizeof($order) > 2)
        {
            $this->db->order_by($order[2] , $order[3]);
        }
        }
        
        

        $query = $this->db->get($table);
        //var_dump($query);
         if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                                
                             }
                             
                                 return $data;
                        }
                    else
                        {

                                    return false;

                        }
        

    }

    
      public function geycity(){
       $this->db->select("events_locaion as citys_name");
       $this->db->distinct();
       $query =  $this->db->get("events");
       $data = array();
              if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                                
                             }
                             
                                 return $data;
                        }
                    else
                        {

                                    return false;

                        }
                        
       return $data;
       
   }

    public function add($table , $data)
   {
        $this->db->insert($table , $data);
            return true;
   }
   public function dgroup($item , $langId){
   $this->db->where("products_lang_key" , $langId );
   $this->db->where( "products_name" , urldecode($item) );
   $query = $this->db->get("products" ,1);
    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data = $row->products_details_groub_id;
                             }
                                 return $data;
                        }
                    else
                        {

                                    return false;

                        }
   }
   
   
   public function getplans($item , $langId){
       $this->db->where("PP_product_id" , $item);
       $this->db->where("PP_lang_key" , $langId);
       $query = $this->db->get("products_plans");
          if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {

                                    return false;

                        }
       
       
   }
   public function getsp($item , $langId)
   {
        // get first get ditails grobe
       $dg = $this->dgroup($item, $langId);
       if($dg != false ){
       $this->db->where("spcifaction_details_groub_id" , $dg);
       $this->db->order_by("spcifaction" , "ASC");
       $query = $this->db->get("spcifaction");
        if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {

                                    return false;

                        }
       }
       else{
           return FALSE;
       }
       
   }

   
   public function ser($item , $lang , $limit = 100){
        //$this->db->where("programes_lang_id" , $lang);
        return $this->MatchsProgramName(trim($item) , $lang , $limit);
                        
   }
   
   
   public function MatchsProgramName($item , $lang , $limit = 100) {
       
       
       $data = array();
       
       $programes = FALSE;
       
       $xplodedtxt = explode(" ", $item);
       $this->db->limit($limit);
       $this->db->select("programes_name AS wordlist_w");
       $this->db->select("programes_name AS disc");
       $qw = "";
       foreach ($xplodedtxt as  $value) {
           $qw = " ".$value;
           $this->db->or_like("programes_name" , $qw , "both");
           $this->db->or_like("programes_desc" , $qw , "both");
           
       }
       //programes
                $query = $this->db->get("programes");
       
                if($query->num_rows() > 0)
                    {
                    $programes = TRUE;
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 
                    }
                    
           $query->free_result();
           $query = null;
           
       $this->db->limit($limit);
       $this->db->select("category_name AS wordlist_w");
       $this->db->select("category_disc AS disc");
       foreach ($xplodedtxt as  $value) {
           $this->db->or_like("category_name" , $value , "both");
           $this->db->or_like("category_disc" , $value , "both");
           
       }
       
       $query = $this->db->get("category");
        if($query->num_rows() > 0)
                    {
                    
                    $programes = TRUE;
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 
                    }    
                    
       if($programes == TRUE){
           return $data;
       }  else {
           return FALSE;
       }
   }




  public function getPrograms($item , $lang , $page = 0 , $date = FALSE , $cat = FALSE)
  {
       $c = $this->input->get("c");
       $ca = $this->input->get("ca");
       $d = $this->input->get("d");
       
       $this->db->select("*");
       
            $xploed = explode(" ", $item);
    $xps = array();
      
      
  }
   
   
   
  
  
  public function SerachAlgorism($item = NULL , $lang , $page = 0 , $date = FALSE , $cat = FALSE)
  {
      
      
      //  $c = $this->escapeString($this->input->get("c")) ;
       
        $ca = $this->escapeString($this->input->get("ca"));

      //  $d = $this->escapeString($this->input->get("d"));

      //  $l = $this->escapeString($this->input->get("l"));
      //  $v = $this->escapeString($this->input->get("v"));

                    
       $items =  $this->escapeString($item);
       
       
      
       
       
       //SELECT `*` FROM `search` WHERE `search`.`events_start` >= '2019-01-01' AND `search`.`ptc_category_id` = '7' LIMIT 20 , 10
       
       
       $qui = " SELECT * FROM `items` ";
       $where  = "WHERE ";
       $useWhere = 0;

       
       if($ca != null && $ca != FALSE )
       {
           $useWhere = 1;
           $where .=" `items_cat_id` = '" . $ca ."' AND ";
       }
       
  
   
       if($items != NULL && $items != FALSE){
           $useWhere = 1;
           
           $where .=" `items_name` LIKE '%".$items."%' AND";
       }
       
       
       $where = substr($where, 0, -4);
       
       if($useWhere == 1)
       {
           $qui.= $where;
       }
       
    
       
       $qui .=" LIMIT  $page, 25 ";
       
       
      // var_dump($qui);
       
      $query =  $this->db->query($qui);
       
          if($query->num_rows() > 0)
                    {
        
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {

                                    return false;

                        } 
        
          
      
  }

  
  
  public function SerachAlgorismCounter($item = NULL , $lang , $page = 0 , $date = FALSE , $cat = FALSE)
  {
      
      
      //  $c = $this->escapeString($this->input->get("c")) ;
       
        $ca = $this->escapeString($this->input->get("ca"));

     //   $d = $this->escapeString($this->input->get("d"));

       // $l = $this->escapeString($this->input->get("l"));

                    
       $items =  $this->escapeString($item);
       
       
      
       
       
       //SELECT `*` FROM `search` WHERE `search`.`events_start` >= '2019-01-01' AND `search`.`ptc_category_id` = '7' LIMIT 20 , 10
       
       
       $qui = " SELECT count(*) as total FROM `items` ";
       $where  = "WHERE ";
       $useWhere = 0;
  
       
       if($ca != null && $ca != FALSE )
       {
           $useWhere = 1;
           $where .=" items_cat_id = '" . $ca ."' AND ";
       }
       
      
       
       
       if($items != NULL && $items != FALSE){
           $useWhere = 1;
           
           $where .=" items_name LIKE '%".$items."%' AND";
       }
       
       
       $where = substr($where, 0, -4);
       
       if($useWhere == 1)
       {
           $qui.= $where;
       }
       
       
       
       
      $query =  $this->db->query($qui);
       
          
    //  var_dump($query->result()[0]->total);
      
      return $query->result()[0]->total;
      
  }

  



  public function SearchC($item , $lang , $page = 0 , $date = FALSE , $cat = FALSE)
  {
       $c =  $this->input->get("c");
       
       $ca = $this->input->get("ca");
       
       $d = $this->input->get("d");
       
       $l = $this->input->get("l");
          
       $this->db->select("*");
       $this->db->select("events.events_start AS programes_start");
       $this->db->select("events.events_end AS programes_end");
       $this->db->select("events.events_locaion AS programes_location");
       //$this->db->from("programes ");
       
        $this->db->from("programes");
    $item = $this->escapeString($item);
            $xploed = explode(" ", $item);
    $xps = array();
    
    
      if($l != FALSE )
       {
          //$this->db->where("programes.programes_lang_id" , $l );
          $this->db->join("(SELECT * FROM programes WHERE programes_lang_id = $l    LIMIT  100 ) as X ", "X.programes_id = programes.programes_id" , "INNER" );
       }  else {
           $this->db->join("(SELECT * FROM programes  LIMIT  100 ) as X ", "X.programes_id = programes.programes_id" , "INNER" );
       }
    
    
    $this->db->join("products_titles_cat", "products_titles_cat.ptc_title_id = programes.programes_crm_id" , "LEFT" );
    $this->db->join("category","category.category_crm_id =products_titles_cat.ptc_category_id" , "INNER");
    $this->db->join("events", "events.events_program_id = programes.programes_crm_id" , "LEFT");
  
    $this->db->where("X.programes_id > ", $page );
    
    if($d != FALSE){
        $d = date("Y-m-d", strtotime($d));
        $this->db->where("events.events_start >=" , $d);
    } else {
            $this->db->where("events.events_start >=" , date("Y-m-d"));    
    }
    if($c != FALSE ){
        if(!is_array($c))
        {
            $this->db->where("events.events_locaion" , $c );
        }  else {
            $this->db->where_in("events.events_locaion" , $c );
        }
    }
    if($ca != FALSE){
        if(!is_array($ca)){
            $this->db->where("products_titles_cat.ptc_category_id" , $ca );
        }
        else {
            $this->db->where_in("products_titles_cat.ptc_category_id" , $ca );
        }
    }
    
    
    
       $qw = " ( ";
    $r = array("!" , "@" , "#" , "$" , "%" , "^" , "&" ,  "*" , "(" , ")" , "_" , "-" , "/" , "\\" , "|");
    foreach ($xploed as  $value) {
        $value = str_replace($r, "", $value);
         if(trim($value) != "" && $value != "null"){
             
                $qw .=" programes_name RLIKE '[".$value."]' OR ";
               
                
            }
          
    }
    
    //$valuex = str_replace($r, "", $item);
     //$qw ="( programes_name LIKE '%".$valuex."%' OR ";
    $qw = substr($qw, 0, -3);
    $qw .= " ) ";
    
    if($item != null && $item != "" && $item != "null"){
        $this->db->where($qw);
    }
    
    
      //$this->db->limit(100 ,$page );
    
  //  $this->db->group_by("programes_crm_id");
    $this->db->order_by("programes_start" , "ASC");
    
    
    
    $query = $this->db->get();
  
   // var_dump($query);
    
    if($query->num_rows() > 0)
                    {
        
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {

                                    return false;

                        }
                        
                        
                        
                        
    
   
    
   }

   




   private function escapeString($val) {
    //$db = get_instance()->db->conn_id;
   // $val = mysqli_real_escape_string($db, $val);
    return $val;
}

   public function search($item , $lang , $page = 0 , $date = FALSE , $cat = FALSE )
   {
       $c = $this->input->get("c");
       $ca = $this->input->get("ca");
       $d = $this->input->get("d");
        $this->db->select("*");
    
        
    $this->db->select("events.events_start AS programes_start");
    $this->db->select("events.events_end AS programes_end");
    
    $this->db->select("events.events_locaion AS programes_location");
     $this->db->from("( SELECT * from programes LIMIT $page , 100 ) as programes ");
    //$this->db->where("programes_lang_id" , $lang);
    $xploed = explode(" ", $item);
    $xps = array();
    $this->db->join("products_titles_cat", "products_titles_cat.ptc_title_id = programes.programes_crm_id" ,"INNER");
    $this->db->join("category","category.category_crm_id =products_titles_cat.ptc_category_id" ,"INNER");
    $this->db->join("events", "events.events_program_id = programes.programes_crm_id" ,"INNER" );
    
   //$this->db->group_by("events.events_start");
   $this->db->order_by("events.events_start" , "DESC");
    //$this->db->like("programes_name" , $xps);
    //$this->db->like_in("programes_desc" , $xps);
    if($d != FALSE){
        $d = date("Y-m-d", strtotime($d));
        $this->db->where("events.events_start >=" , $d);
    }
    if($c != FALSE ){
        if(!is_array($c))
        {
            $this->db->where("events.events_locaion" , $c );
        }  else {
            $this->db->where_in("events.events_locaion" , $c );
        }
    }
    if($ca != FALSE){
        if(!is_array($ca)){
            $this->db->where("products_titles_cat.ptc_category_id" , $ca );
        }
        else {
            $this->db->where_in("products_titles_cat.ptc_category_id" , $ca );
        }
    }
   
        if($l != FALSE ){
        
        
            $this->db->where("programes.programes_lang_id" , $l );
        
        
    }
    
      $qw = " ( ";
    $r = array("!" , "@" , "#" , "$" , "%" , "^" , "&" ,  "*" , "(" , ")" , "_" , "-" , "/" , "\\" , "|");
    foreach ($xploed as  $value) {
        $value = str_replace($r, "", $value);
         if(trim($value) != "" && $value != "null"){
             //$xps[] = $value;
                $qw .=" programes_name LIKE '%".$value."%' OR ";
                //$this->db->or_like("programes_name  " , trim($value) , "both");
                //$this->db->or_like("programes_desc  " , trim($value) , "both");
                
            }
          
    }
    $qw = substr($qw, 0, -3);
    $qw .= " ) ";
    
    if($item != null && $item != "" && $item != "null"){
        $this->db->where($qw);
    }
    
    
    //$this->db->limit(24 ,$page );
    //$this->db->group_by("programes_id");
    $this->db->order_by("programes_start" , "DESC");
    
    
    $query = $this->db->get();
    //print($query->queryString);
    //print($query->result_id->queryString);
    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {

                                    return false;

                        }
    
   }
   
   
   
   public function searchCount($item , $lang )
   {
       $c = $this->input->get("c");
       $ca = $this->input->get("ca");
       $d = $this->input->get("d");
   // $this->db->select("COUNT(programes_id) AS totalr");
    
    //$this->db->where("programes_lang_id" , $lang);
    $xploed = explode(" ", $item);
    $this->db->join("products_titles_cat", "products_titles_cat.ptc_title_id = programes.programes_crm_id" );
    $this->db->join("category","category.category_crm_id = products_titles_cat.ptc_category_id" );
    $this->db->join("events", "events.events_program_id = programes.programes_crm_id"  );
    
    if($d != FALSE){
        $d = date("Y-m-d", strtotime($d));
        $this->db->where("events.events_start >=" , $d);
    }
    else {
            $this->db->where("events.events_start >=" , date("Y-m-d"));    
    }
    
    if($c != FALSE ){
        if(!is_array($c))
        {
            $this->db->where("events.events_locaion" , $c );
        }  else {
            $this->db->where_in("events.events_locaion" , $c );
        }
    }
    if($ca != FALSE){
        if(!is_array($ca)){
            $this->db->where("products_titles_cat.ptc_category_id" , $ca );
        }
        else {
            $this->db->where_in("products_titles_cat.ptc_category_id" , $ca );
        }
    }
    
       if($l != FALSE ){
        
        
            $this->db->where("programes.programes_lang_id" , $l );
        
        
    }
   
      $qw = " ( ";
    $r = array("!" , "@" , "#" , "$" , "%" , "^" , "&" ,  "*" , "(" , ")" , "_" , "-" , "/" , "\\" , "|");
    foreach ($xploed as  $value) {
        $value = str_replace($r, "", $value);
         if(trim($value) != "" && $value != "null"){
             //$xps[] = $value;
                $qw .=" programes_name LIKE '%".$value."%' OR ";
                //$this->db->or_like("programes_name  " , trim($value) , "both");
                //$this->db->or_like("programes_desc  " , trim($value) , "both");
                
            }
          
    }
    $qw = substr($qw, 0, -3);
    $qw .= " ) ";
    
    if($item != null && $item != "" && $item != "null"){
        $this->db->where($qw);
    }
    
    
    
    //$this->db->group_by("programes_id");
    $query = $this->db->count_all_results("programes");
  //  var_dump($query);
        
        return $query;
                          
                              
                      
    
   }
   
   
   
   public function GetSearchBox($l){
       
       
       $this->db->where("searchbox_lang" , $l);
       $this->db->order_by("searchbox_id" , "DESC");
       $q = $this->db->get("searchbox");
       $data = array();
       if($q->num_rows() > 0)
       {
           foreach ($q->result() as $row) 
               {
                    $data[] = $row;
                }
                
                return $data;
       }  else {
           return FALSE;
       }
   }
           
   
   
   
   public function Getwebsite()
   {
       
       $this->db->limit(1);
       $q = $this->db->get("website");
       
       $data = array();
       if($q->num_rows() > 0)
       {
           foreach ($q->result() as $row) 
               {
                    $data[] = $row;
                }
                
                return $data;
       }  else {
           return FALSE;
       }
   }
   
   
   
   public function Getfooterlinks($secid)
   {
       $this->db->where("footerlinks_sec_id" , $secid );
       
       $q = $this->db->get("footerlinks");
         $data = array();
       if($q->num_rows() > 0)
       {
           foreach ($q->result() as $row) 
               {
                    $data[] = $row;
                }
                
                return $data;
       }  else {
           return FALSE;
       }
   }
   
   
   
   
   public function Getfootersection($lang) 
    {
       $this->db->where("footersection_langid" , $lang );
       $q = $this->db->get("footersection");
        $data = array();
       if($q->num_rows() > 0)
       {
           foreach ($q->result() as $row) 
               {
                    $data[] = $row;
                }
                
                return $data;
       }  else {
           return FALSE;
       }
       
   }
   
   
   public function Getsmallfooter($lang) 
    {
       $this->db->where("smallfooter_langid" , $lang );
       $q = $this->db->get("smallfooter");
        $data = array();
       if($q->num_rows() > 0)
       {
           foreach ($q->result() as $row) 
               {
                    $data[] = $row;
                }
                
                return $data;
       }  else {
           return FALSE;
       }
       
   }
   
   
 

           
          
        
   public function GenFooter($lang) {
       $sec = $this->Getfootersection($lang); 
       
       $result = "";
       foreach ($sec as $key => $vas)
        {
           //$this->load->view('newview/tiny/f', '', true)
            $result .= "<div class=\"col-md\">";
            $result .= " <div class=\"ftco-footer-widget mb-4\">";
          
            $result .="<h2 class='ftco-heading-2'> $vas->footersection_text </h2><ul class='list-unstyled'>";
           
          
           
           
           $links = $this->Getfooterlinks($vas->footersection_id);
           
           foreach ($links as $k => $v) 
           {
            
               $result .="   <li><a class=\"py-2 d-block\" href=\"$v->footerlinks_url\">$v->footerlinks_text</a></li>";
           }
           
     
           $result .="</ul></div></div>";
        }
        
        
        
       
        return $result;
       
   }
   
   public function GetFotterinJson($lang){
       
       
       
        $sec = $this->Getfootersection($lang); 
       
       $result = array();
       foreach ($sec as $key => $vas)
        {
           //$this->load->view('newview/tiny/f', '', true)
           
            
            
           
           $links = $this->Getfooterlinks($vas->footersection_id);
           
           foreach ($links as $k => $v) 
           {
            $result[$vas->footersection_text][]= array($v->footerlinks_url => $v->footerlinks_text);
               
           }
           
           
        }
        
        
        
       
        return $result;
       
       
   }

   
}