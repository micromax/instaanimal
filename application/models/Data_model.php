<?php
class Data_model extends CI_Model {

    public function  __construct()
        {
            parent::__construct();
           $this->db->reconnect();
        }

        
        public function getallimges(){
            $this->db->order_by("pic_id" , "desc");
            $q = $this->db->get("files");
            $data = array();
            if($q->num_rows() > 0)
             {
              foreach ($q->result() as $row)
                      {
                            $data[] = $row;
                           
                       }
                            
             }
             return $data;  
        }
        
        public function getonline($now){
            $currunt = $now - 50;
            $this->db->where("activtime >=" , $currunt);
              $query = $this->db->get("acc_login");
               if($query->num_rows() > 0)
                {
                   foreach ($query->result() as $row)
                      {
                            $data[] = $row;
                       }
                            return $data;
                }else
                {
                    return FALSE;
                }
            
        }
        
        
        public function getch2(){
            $this->db->select('*');
        $this->db->from('androidchannels');
        $this->db->join('categories', 'androidchannels.category_id = categories.id');
        $query = $this->db->get();
        if($query->num_rows() > 0)
                {
                foreach ($query->result() as $row)
                      {
                            $data[] = $row;
                       }
                            return $data;
                     }
                    else
                    {
                    return FALSE;
                    }
        }



        public function get_last($table , $filds)
        {
          
            $this->db->order_by( $filds , "desc");
            $this->db->limit(3);
            $query = $this->db->get($table);
        if($query->num_rows() > 0)
                {
                foreach ($query->result() as $row)
                      {
                            $data[] = $row;
                       }
                            return $data;
                     }
                    else
                    {
                    return FALSE;
                    }


        }
        public function getandroid($type)
                {
                    //androidchannels
          $this->db->where("type" ,$type );
          $query = $this->db->get("androidchannels");
        if($query->num_rows() > 0)
                {
                foreach ($query->result() as $row)
                      {
                            $data[] = $row;
                       }
                            return $data;
                     }
                    else
                    {
                    return FALSE;
                    }
                    
                }
        public function get_max($table , $filds)
        {
            
         $this->db->select_max($filds);
         $this->db->where("item_active" ,'1' );
         $query = $this->db->get($table);
          if($query->num_rows() > 0)
                    {
                    
                   foreach ($query->result() as $row) { }
                    
                    $this->db->where($filds , $row->item_price );
                    $queryx = $this->db->get($table);
                       foreach ($queryx->result() as $rows)
                        {
                            $data[] = $rows;
                        }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }



        }

         public function get_min($table , $filds)
        {

         $this->db->where("item_active" ,'1' );
         $this->db->select_min($filds);
         $query = $this->db->get($table);
          if($query->num_rows() > 0)
                    {

                   foreach ($query->result() as $row) { }

                    $this->db->where($filds , $row->item_price );
                   
                    $queryx = $this->db->get($table);
                       foreach ($queryx->result() as $rows)
                        {
                            $data[] = $rows;
                        }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }



        }

       

        public function get($table)
        {
            $query = $this->db->get($table);
            if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
            
        }
        
        
        
        public function getl($table , $where)
        {
            
            $this->db->where($where[0] , $where[1]);
            $query = $this->db->get($table);
            if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
            
        }
        
        

         public function getcol($table , $col)
        {
             $this->db->select($col);
            $query = $this->db->get($table);
            if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row->$col;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }

        }
        public function GetCondiction($table , $condetion = false)
        {
            // $condetion =  array ("col"=>"colomn name" , "con"=>"!="  , "spot"=>"value"  );
            if($condetion != FALSE ){
                $this->db->where($condetion[0]." ".$condetion[1],$condetion[2]);
            }
            $query = $this->db->get($table);
            if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }

        }


            public function get_row($table , $filds , $value)
                {
                    $this->db->where($filds , $value);
                    $this->db->limit(1);
                    $query = $this->db->get($table);
                    
                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }

                    
                }



     public function get_where($table , $filds , $value)
         {
                    $this->db->where($filds , $value);
                    
                    $query = $this->db->get($table);

                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }


        }
     public function get_like($table , $filds , $value)
         {
                    $this->db->like($filds , $value);
                    
                    $query = $this->db->get($table);

                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }


        }
        
        
     public function get_whereX($table , $filds , $value)
         {
                    $this->db->where($filds , $value);
                    $this->db->join('items', 'items.Item_ID = sales_quotation_items.Item_ID' , 'left');
                    $this->db->join('item_sales_prices', 'item_sales_prices.Item_ID = sales_quotation_items.Item_ID' , 'left');
                    $this->db->join('unitsofmeasure', 'items.Unit_Of_Measure_ID = unitsofmeasure.Units_of_Measure_ID' , 'left');
                    
                    $query = $this->db->get($table);

                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }


        }


        public function get_whereY($table , $filds , $value)
         {
                    $this->db->where($filds , $value);
                    $this->db->join('items', 'items.Item_ID = purchase_order_entry_item.Item_ID' , 'left');
                    $this->db->join('item_purches_prices', 'item_purches_prices.Item_ID = purchase_order_entry_item.Item_ID' , 'left');
                    $this->db->join('unitsofmeasure', 'items.Unit_Of_Measure_ID = unitsofmeasure.Units_of_Measure_ID' , 'left');

                    $query = $this->db->get($table);

                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }


        }


        

       public function get_imgs()
        {
                   $this->db->where("file_type" , "image/jpeg");
                   $this->db->or_where("file_type" , "image/jpg");
                   $this->db->or_where("file_type" , "image/png");
                   $this->db->or_where("file_type" , "image/gif");


                    $query = $this->db->get("file_manager");

                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
           
       }

       public function get_files()
       {
                   $this->db->where( 'file_type' , 'application/msword');
                   $this->db->or_where('file_type' , 'application/pdf');
                   


                    $query = $this->db->get("file_manager");

                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
       }

       public function get_last10_new()
       {

            $this->db->where( 'item_status' , 'n');
            $this->db->order_by("item_time" , "desc");
            $this->db->limit(10);


                    $query = $this->db->get("items");

                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }

       }

        public function get_last10_used()
       {
            $this->db->where( 'item_status' , 'o');
            $this->db->order_by("item_time" , "desc");
            $this->db->limit(10);


                    $query = $this->db->get("items");

                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
        }

        public function get_item($value)
        {
                    $this->db->where("item_id" , $value);
                   
                   
                    $this->db->join('details', 'details.Details_code = items.item_Details_code' , 'left');
                    $this->db->join('genral_details', 'genral_details.GDetails_code = items.item_Details_code' , 'left');
                    $this->db->join('printing_details', 'printing_details.PDetails_code = items.item_Details_code' , 'left');
                    $this->db->join('copy_details', 'copy_details.CDetails_code = items.item_Details_code' , 'left');
                    $this->db->join('scan_details', 'scan_details.SDetails_code = items.item_Details_code' , 'left');
                    $this->db->join('system_details', 'system_details.SYSDetails_code = items.item_Details_code' , 'left');

                    $this->db->join('brands', 'brands.brand_id = items.item_brand_id' , 'left');
                    $this->db->join('cat', 'cat.cat_ID = items.item_cat_ID' , 'left');



                    $this->db->limit(1);
                    $query = $this->db->get("items");

                    if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
            
        }

        public function get_details($v) {
           $this->db->where("Details_code" , $v);
           $this->db->join('genral_details', 'genral_details.GDetails_code = details.Details_code' , 'left');
           $this->db->join('printing_details', 'printing_details.PDetails_code = details.Details_code' , 'left');
           $this->db->join('scan_details', 'scan_details.SDetails_code = details.Details_code' , 'left');
           $this->db->join('copy_details', 'copy_details.CDetails_code = details.Details_code' , 'left');
           $this->db->join('system_details', 'system_details.SYSDetails_code = details.Details_code' , 'left');



              $this->db->limit(1);
            $query = $this->db->get("details");

               if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }

        }
       
        public function GeInventoryItems(){
            $this->db->select('*');
             $this->db->join('items', 'items.item_id = airbook_sorge.sorge_itemId');
             $query = $this->db->get("airbook_sorge");
              if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                         {
                                $data[] = $row;
                         }
                                 return $data;
                    }
                    else
                        {
                            return FALSE;
                        }
            
        }

        
        public function GetItemPrice($ItemID){
            
            $this->db->where("item_id" , $ItemID );
            $query = $this->db->get("items" , 1);
             if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row){}

                                $data = $row->item_price;
                         
                                 return $data;
                    }
                    else
                        {
                            return 0;
                        }
            
        }

        public function GetPromptinPercent($proID){

            $this->db->where("promotion_id" , $proID );
            $query = $this->db->get("airbook_promotion" , 1);
             if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row){}

                                $data = $row->promotion_discountPercent;

                                 return $data;
                    }
                    else
                        {
                            return 0;
                        }

        }

        public function GetPromptinVal($proID){

            $this->db->where("promotion_id" , $proID );
            $query = $this->db->get("airbook_promotion" , 1);
             if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row){}

                                $data = $row->promotion_discountMony;

                                 return $data;
                    }
                    else
                        {
                            return 0;
                        }

        }

        public function GetItemCountInstorg($ItemID)
        {
             $this->db->where("sorge_itemId" , $ItemID );
            $query = $this->db->get("airbook_sorge" , 1);
             if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row){}

                                $data = $row->sorge_unites;

                                 return $data;
                    }
                    else
                        {
                            return 0;
                        }
        }


        public function ADDtoSoreg($itemID , $data)
        {
            $this->db->where("sorge_itemId",$itemID);
            $query = $this->db->get("airbook_sorge");
            if($query->num_rows() == 0)
                {
                $this->db->insert("airbook_sorge" , $data);
                return true;
                }
            else
                {
                $this->db->where("sorge_itemId" , $itemID);
                $this->db->update("airbook_sorge" ,$data );
                return true;
                }

        }

        public function GetItemsNotInSorge()
        {
             
             $this->db->from("airbook_sorge");
             $this->db->join('items', 'items.item_id != airbook_sorge.sorge_itemId');
             $query = $this->db->get();
              if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                         {
                                $data[] = $row;
                         }
                                 return $data;
                    }
                    else
                        {
                            return FALSE;
                        }
        }

        public function get_branches()
        {
             $this->db->select('*');
             $this->db->join('branches_info', 'branches_info.INFO_branch_id = branches.branch_id');
             $query = $this->db->get('branches');
              if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                         {
                                $data[] = $row;
                         }
                                 return $data;
                    }
                    else
                        {
                            return FALSE;
                        }



        }
        public function get_branche($val)
        {
             $this->db->where('branch_id' , $val );
             $this->db->join('branches_info', 'branches_info.INFO_branch_id = branches.branch_id');
             $this->db->limit(1);
             $query = $this->db->get('branches');

              if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                         {
                                $data[] = $row;
                         }
                                 return $data;
                    }
                    else
                        {
                            return FALSE;
                        }


        }
        
     public function Get_ForSelect($table , $col1 , $col2 , $expected = false )
        {
             $this->db->select(" $col1 , $col2 " );
             if( $expected != false){
                $this->db->where($expected[0] , $expected[1]);
             }
             $query = $this->db->get($table);

              if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as  $xrow)
                         {
                                $xdata[$xrow->$col1] =  $xrow->$col2 ;
                         }
                         
                         
                             
                                 return $xdata;
                    }
                    else
                        {
                            return FALSE;
                        }


        }
       
        public function GetThis($table , $col , $item)
        {
            $this->db->where( $col , $item );
            $query = $this->db->get($table , 1);
            
             if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $cols=>$xrow)
                         {
                                $xdata[$cols] =  $xrow ;
                         }
                         
                         
                             
                                 return $xdata;
                    }
                    else
                        {
                            return FALSE;
                        }
            
            
        }




        public  function add($table , $data){
       $this->db->insert($table , $data);
            return true;
        

    }
 public  function addw( $data){
     $taple = "wordlist";
     
     foreach ($data as $key => $value) 
         {
            $datax["wordlist_hash"] = md5(trim($value));
            $datax["wordlist_w"] = trim($value);
            $q = $this->db->insert_string($taple , $datax);
            $query_string = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $q);
            
            $this->db->query($query_string);
        }
 }
    public  function edit($data , $table , $row , $value)
	{
        
       
        
         $this->db->where($row , $value);
	 $this->db->update($table ,$data );
         
    
        }

        

  public function delete($table , $row , $value)
     {

	$this->db->where($row , $value);
	$this->db->delete($table);
	return true;
     }

     




     public function is_UniqeVal($table , $col , $val)
     {
         $this->db->where($col , $val);
         $query = $this->db->get($table);
         if($query->num_rows() != 0)
            {
             return FALSE;

            }else
            {
                return TRUE;
            }
         
         
     }
     
  

    public function GetNodeValueForID($table , $ID , $NodeCol )
    {
        $this->db->where($ID[0] , $ID[1]);
        $query = $this->db->get($table);
        if($query->num_rows() == 1 )
        {
            foreach($query->result() as $row)
            {
                $data = $row->$NodeCol;
            }

            return $data;
        }
        else
            {
            return false;
        }
        
        
    }


    public function GetWhereNot($table , $col , $val)
    {
            $this->db->where("$col !=" , $val);
           $query = $this->db->get($table);
            if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
    }

    public function reFixDeleted($tabel , $col , $val , $data)
    {
         $this->db->where($col , $val);
	 $this->db->update($tabel ,$data );
	 return true;
    }

    public function GetSumOf($table , $col)
    {
        $this->db->select_sum($col);
        $query = $this->db->get($table);
         if($query->num_rows() > 0)
                 {
                    foreach($query->result() as $row){}
                 return $row->$col;
         }else
             {
             return 0;
         }
    }

    public function GetValOf($table , $col ,$spot , $where)
    {
        $this->db->where($col , $where);
        $query = $this->db->get($table , 1);
         if($query->num_rows() > 0)
                 {
                    foreach($query->result() as $row){}
                 return $row->$spot;
         }else
             {
             return 0;
         }
    }

    public function GetSumOfWhere($table1 , $table2 , $col , $relation1 , $relation2)
    {
        $this->db->select_sum($col);
        $this->db->join($table2 , "$table2.$relation2 = $table1.$relation1" );
        $query = $this->db->get($table1);
         if($query->num_rows() > 0)
                 {
                    foreach($query->result() as $row){}
                 return $row->$col;
         }else
             {
             return 0;
         }
    }
    
    public function GetSum($table , $col  , $where , $val)
    {
        
        $this->db->select_sum($col);
        $this->db->where($where , $val);
        
        $query = $this->db->get($table);
         if($query->num_rows() > 0)
                 {
                    foreach($query->result() as $row){}
                 return $row->$col;
         }else
             {
             return 0;
         }
    }
    
     public function GetSumALL($table , $col )
    {

        $this->db->select_sum($col);
        

        $query = $this->db->get($table);
         if($query->num_rows() > 0)
                 {
                    foreach($query->result() as $row){}
                 return $row->$col;
         }else
             {
             return 0;
         }
    }

    public function GetStorgItemSALEMony()
    {
       $result = 0;
        $this->db->join("item_sales_prices" , '  item_sales_prices.Item_ID = item_adjustments_note.Item_ID  ' );

        $query = $this->db->get("item_adjustments_note");
         if($query->num_rows() > 0)
                 {
                  foreach($query->result() as $row){
                      if($row->Quantity > 0)
                      {
                        $result +=  $row->Quantity * $row->UPrice;
                      }
                      else
                      {
                          $result = $result + 0;
                      }
                  }
                 return $result;
         }else
             {
             return 0;
         }
    }
    public function GetStorgItemGoldMony()
    {
            $result = 0;
        $this->db->join("item_purches_prices" , ' item_purches_prices.Item_ID = item_adjustments_note.Item_ID  ' );
       
        $query = $this->db->get("item_adjustments_note");
         if($query->num_rows() > 0)
                 {
                  foreach($query->result() as $row){
                      if($row->Quantity > 0)
                      {
                        $result +=  $row->Quantity * $row->Price;
                      }
                      else
                      {
                          $result = $result + 0;
                      }
                  }
                 return $result;
         }else
             {
             return 0;
         }

    }
    
    public function GetStorgItemNigativMony()
    {
            $result = 0;
        $this->db->join("items" , 'items.item_id = airbook_sorge.sorge_itemId' );
        $query = $this->db->get("airbook_sorge");
         if($query->num_rows() > 0)
                 {
                  foreach($query->result() as $row){
                      if($row->sorge_unites < 0)
                      {
                        $result +=  $row->sorge_unites * $row->item_price;
                      }
                      else
                      {
                          $result = $result + 0;
                      }
                  }
                 return $result;
         }else
             {
             return 0;
         }

    }


    public function RUNbills($id)
    {
        $this->db->where("SQ_Entry_ID" , $id );
        $this->db->join("customer" , 'customer.Customer_ID = sales_quotation_entry.Customer_ID' , "left" );
        $this->db->join("locations" , 'locations.locationsID = sales_quotation_entry.Deliver_from_Location_ID' , "left" ); 
        $this->db->join("shipping_company" , 'shipping_company.shipping_company_id = sales_quotation_entry.Shipping_Company_ID' , "left" ); 
        $query = $this->db->get("sales_quotation_entry");
        if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
        
    }
    
    public function GetBurchesInv($id)
    {
         $this->db->where("Purchase_Order_Entry_ID" , $id );
          
         $this->db->join("locations" , 'locations.locationsID = purchase_order_entry.Receive_Into_ID' , "left" );

         $this->db->join("supplier" , 'supplier.Supplier_ID = purchase_order_entry.Supplier_ID' , "left" );
         

         $query = $this->db->get("purchase_order_entry");
        if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                                $data[] = $row;
                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }

    }



    public function user_paide()
    {
        
        $query = $this->db->get("acc_login");


         if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                              $row->avi = $row->vaid_untile;
                               $row->avi = timeConvert($row->avi);
                              
                                $data[] = $row;

                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
    }
    
    
    public function user_paiderc($rcid)
    {
        $this->db->where("reseeler_id" ,$rcid);
        $query = $this->db->get("acc_login");


         if($query->num_rows() > 0)
                    {
                        foreach ($query->result() as $row)
                             {
                              $row->avi = $row->vaid_untile;
                               $row->avi = timeConvert($row->avi);
                              
                                $data[] = $row;

                             }
                                 return $data;
                        }
                    else
                        {
                            return FALSE;
                        }
    }
    
    

    public function get_mymsg($me){
         $this->db->where("msg_to" ,$me );
         $this->db->order_by("msg_date","desc");
         $query = $this->db->get("msg");
         if($query->num_rows() > 0 )
         {
             foreach ($query->result() as $row){
                    $data[] = $row;
             }
             return $data;
         }
         else
         {
             return FALSE;
         }
    }
    
    public function get_msg($me , $msgId){
        $msgId = intval($msgId);
         $this->db->where("msg_id" ,$msgId );
         $this->db->where("msg_to" ,$me );
         $query = $this->db->get("msg" , 1);
         if($query->num_rows() > 0 )
         {
             $this->db->where("msg_id" ,$msgId );
                $this->db->where("msg_to" ,$me );
             $x["msg_marked"] = 1;
             $this->db->update("msg" , $x);
             foreach ($query->result() as $row){
                    $data[] = $row;
             }
             return $data;
         }
         else
         {
             return FALSE;
         }
    }


    public function get_unreaded_msg($me){
        
        $this->db->where("msg_marked" ,0 );
        $this->db->where("msg_to" ,$me );
        $this->db->from("msg");
        return $this->db->count_all_results();
        
    }
    
    public function getUsersList($me){
        $this->db->where("reseeler_id" , $me);
        $this->db->where("isonline" , 1);
        $query = $this->db->get("acc_login");
        
        if($query->num_rows() > 0 )
        {
             foreach ($query->result() as $row){
                    $data[] = $row;
             }
             return $data;
            
        }
        else{
            return FALSE;
        }
        
    }
    
    
    public function getstat(){
        
        $data["allvistes"] = $this->db->count_all_results("statistic");
        
        $this->db->group_by("statistic_ip");
        $data["uniqvist"] = $this->db->count_all_results("statistic");
        
        //$this->db->group_by("statistic_ip");
        $data["todayuniq"] = $this->gettodayuniq();
        $data["todaytotal"] = $this->gettodaytotalpages();
        
        $data["monthuniq"] = $this->getmonthuniq();
        $data["monthtotalpages"] = $this->getmonthtotalpages();
        
        $this->getgraph() ;
        
        return $data;
        
    }
    
    
    public function getgraph() 
    {
        $qu = "SELECT COUNT(`statistic_id`) as vists, YEAR(FROM_UNIXTIME(`statistic_unixtime`)) as years , DAY(FROM_UNIXTIME(`statistic_unixtime`)) as days , MONTH(FROM_UNIXTIME(`statistic_unixtime`)) as months FROM `statistic` GROUP BY days , years ,months ORDER BY years , months , days";
        
        $q = $this->db->query($qu);
        $data =array();
        if($q->num_rows() > 0)
        {
            foreach ($q->result() as $row){
                $data[] = $row;
            }
            //var_dump($data);
            return $data;
        }else
        {
            return FALSE;
        }
        
        
    }
     public function getgraphagent() 
    {
        $qu = "SELECT COUNT(`statistic_id`) as vists , `statistic_browser` as agents   FROM `statistic` GROUP BY agents   ORDER BY agents  ";
        
        $q = $this->db->query($qu);
        $data =array();
        if($q->num_rows() > 0)
        {
            foreach ($q->result() as $row){
                $data[] = $row;
            }
            //var_dump($data);
            return $data;
        }else
        {
            return FALSE;
        }
        
        
    }
    public function getcountrylist()
    {
        $this->db->select("country_code");
        $this->db->group_by("country_code");
        $q = $this->db->get("ip2location_db3");
        $data = array();
        foreach ($q->result() as $row){
            $data[$row->country_code] = 0;
            
        }
        return $data;
    }

    public function getgraphcou() {
         $qu = "SELECT COUNT(`statistic_id`) as vists , `statistic_country` as country   FROM `statistic` GROUP BY country   ORDER BY vists DESC  ";
         $q = $this->db->query($qu);
        $data =array();
        if($q->num_rows() > 0)
        {
            foreach ($q->result() as $row){
                $data[] = $row;
            }
            //var_dump($data);
            return $data;
        }else
        {
            return FALSE;
        }
     }
    
    public function getgraphpage() 
    {
        $qu = "SELECT COUNT(`statistic_id`) as vists , `statistic_pagename` as urls   FROM `statistic` GROUP BY urls   ORDER BY vists DESC LIMIT 20 ";
        
        $q = $this->db->query($qu);
        $data =array();
        if($q->num_rows() > 0)
        {
            foreach ($q->result() as $row){
                $data[] = $row;
            }
            //var_dump($data);
            return $data;
        }else
        {
            return FALSE;
        }
        
        
    }
    
  
    public function gettodayuniq(){
        $today = human_to_unix(date("Y-m-d 00:00:00"));
        
        $this->db->where("statistic_unixtime >=", $today);
        $this->db->group_by("statistic_ip");
        return $this->db->count_all_results("statistic");
    }
    
     public function getmonthuniq(){
        $today = human_to_unix(date("Y-m-01 00:00:00"));
        
        $this->db->where("statistic_unixtime >=", $today);
        $this->db->group_by("statistic_ip");
        return $this->db->count_all_results("statistic");
    }
    public function gettodaytotalpages(){
        $today = human_to_unix(date("Y-m-d 00:00:00"));
        
        $this->db->where("statistic_unixtime >=", $today);
        
        return $this->db->count_all_results("statistic");
    }
    
    
    public function getmonthtotalpages(){
        $today = human_to_unix(date("Y-m-01 00:00:00"));
        
        $this->db->where("statistic_unixtime >=", $today);
        
        return $this->db->count_all_results("statistic");
    }

}
