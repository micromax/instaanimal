<?php


class permission_model extends CI_Model{

    
    
    private $userID;
    private $user_PG;
    private $pagese;
    
    public function __construct() {
        parent::__construct();
    }
    
    
    
    
    public function GetuserID()
    {
        return $this->userID;
    }
    
    public function SetuserID($input)
    {
        $this->userID = $input ;
    }
    
    
    public function Getuser_PG()
    {
        return  $this->user_PG; 
        
    }
    
    public function Setuser_PG($input)
    {
        $this->user_PG = $input  ; 
    }
    
    public function Getpagese(){
        return $this->pagese;
        
    }
    
    
    public function Setpagese($input){
        $this->pagese = $input ;
    }
    
    
    
    
    
    
    public function ChickeUserPermmition( $Action ) // ADD || EDIT || DELETE 
    {
        //$this->GetPGFromDB();
                
       $ar =  $this->Getuser_PG();
       
        if(key_exists($this->Getpagese(), $ar))
                {
          
        
        if($ar[$this->pagese][$Action])
            {
                return TRUE;
                
               
            }
            else {
                return FALSE;
            }
        
        
        }  else {
            
            return FALSE;
        }
        
    }
    
    
    // Active Recored Style
    public function GetPGFromDB($userGroupeId)
    {
        //SELECT * FROM PG WHER USER_ID = $user_id //
        //
        //SELECT * FROM USERPERMGROPE WHER USER_GROPE = USER_GRP_ID //
        
        $permition = array();
        
        $this->db->where("p_list_p_group_id" ,$userGroupeId );
        
        $q = $this->db->get("p_list");
        
        
        if($q->num_rows() > 0)
            {
            foreach ($q->result() as $row)
                {
                    $permition[$row->p_list_page_name]["add"] = $row->p_list_add;
                    $permition[$row->p_list_page_name]["edit"] = $row->p_list_edit;
                    $permition[$row->p_list_page_name]["delete"] = $row->p_list_delete;
                    $permition[$row->p_list_page_name]["view"] = $row->p_list_view;
                    
                }
            }
        
        
        
        
        $this->Setuser_PG($permition);
        return $permition;
    }
    
    
    
    // Active Recored Style
    public function GetPGFromDBWithNormalQuiry($userGroupeId)
    {
        //SELECT * FROM PG WHER USER_ID = $user_id //
        //
        //SELECT * FROM USERPERMGROPE WHER USER_GROPE = USER_GRP_ID //
        
        $permition = array();
        
        //$this->db->where("p_list_p_group_id" ,$userGroupeId ); =------> لاحظ ان الفرق ليس كبير في الاستخدام
        
        $q = $this->db->query("SELECT * FROM p_list WHERE p_list_p_group_id =  $userGroupeId");
        
        
        if($q->num_rows() > 0)
            {
            
            
            //سوف نقوم باسترجاع البيانات علي شكل مصفوفه عاديه هذه المره
            foreach ($q->result_array() as $row)
                {
                
                    //نخزن اسم الصفحه في متغير
                    $pagename = $row["p_list_page_name"];
                
                    //نبني المصفوفه
                    $permition[$pagename]["add"] = $row["p_list_add"];
                    $permition[$pagename]["edit"] = $row["p_list_edit"];
                    $permition[$pagename]["delete"] = $row["p_list_delete"];
                    $permition[$pagename]["view"] = $row["p_list_view"];
                    
                }
            }
        
        
        
        
        $this->Setuser_PG($permition);
        return $permition;
    }
    
    
    
}
