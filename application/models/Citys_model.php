<?php


class Citys_model  extends CI_Model{
    
    
    
    
    public function  __construct()
        {
            parent::__construct();
         
        }
    
    
        
        
        public function getCtiysWithProg($limit = 5)
        {
            
            
            
            
            $this->db->limit($limit);
            
            return $this->db->get("citys")->result();
            
            
            
        }
        
        
        
        public function getcitysprograms($c)
        {
            
            
          
            $this->db->join("products_titles_cat", "products_titles_cat.ptc_title_id = programes.programes_crm_id" );
            $this->db->join("category","category.category_crm_id =products_titles_cat.ptc_category_id" );
            $this->db->join("events", "events.events_program_id = programes.programes_crm_id" ,"left" );
            $this->db->order_by("events.events_start" , "DESC");
            $this->db->where("events.events_locaion" , $c );
            $this->db->limit(1);
            
        
            return $this->db->get("programes")->result();

            
        }
        
        
        
        public function getdefultcitys(){
            
            $result = array();
            
            $all  = $this->getCtiysWithProg();
                    
            foreach ($all as $k => $v)
            {
                    $result[$v->citys_id]["cityinfo"] = $v;
                    $result[$v->citys_id]["programfor"] = $this->getcitysprograms($v->citys_name);
                    
                    
            }
         
            
            return $result;
                    
        }
                
}