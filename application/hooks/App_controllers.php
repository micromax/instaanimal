<?php



function load_app_controllers()
{
  spl_autoload_register('CG_controllers');
}
function CG_controllers($class)
{
  if (strpos($class, 'CI_') !== 0)
  {
    if (is_readable(APPPATH . 'core/' . $class . '.php'))
    {
      require_once(APPPATH . 'core/' . $class . '.php');
    }
  }
}