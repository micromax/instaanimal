<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cloud Gen </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    <link href="<?php echo base_url(); ?>css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

    
    <link href="<?php echo base_url(); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>js/bootstrap3-wysihtml5.min.css" />
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>build/css/custom.min.css" rel="stylesheet">
   <style >
       .gbody {
           clear: both;
       }
      .gbody img {
          width: 150px;
         height: 150px;
         float: left;
         
      }
      
  </style>
  </head>

 
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                  &nbsp;
              </div>
              <div class="profile_info">
                <span>Welcome, <?php echo $username; ?></span>
                
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li>
                      <a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>qcms"><span>Dashboard</span></a></li>
          <li class ="icon" id="iconr" name="Contact us En"  rel="qcms/statics" alt="statics" note="Contact us English" ><a href="#">Widgets  in Home page </a></li>            
          
          <li class ="icon" id="iconr" name="Contact us AR"  rel="qcms/staticsar" alt="staticsar" note="Contact us English" ><a href="#">Widgets  in Home page  Arabic</a></li>            
          
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li class ="icon" id="iconr" name="About us En"  rel="qcms/aboutusen" alt="aboutusen" note="about us English" ><a href="#">About US</a></li>
                        <li class ="icon" id="iconr" name="Contact us En"  rel="qcms/contact" alt="contact" note="Contact us English" ><a href="#">Contact US </a></li>
                        
                        
                        
                    </ul>
                  </li>
                  
                  <li><a><i class="fa fa-table"></i> Blog <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li class ="icon" id="iconr" name="Articles En"  rel="qcms/articls" alt="articls" note="Articles English" ><a href="#">Blog </a></li>
                      
                    </ul>
                  </li>
                  
                  <li><a><i class="fa fa-clone"></i>Products <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li class ="icon" id="iconr" name="product En"  rel="qcms/product" alt="product" note="products English" ><a href="#">Products </a></li>
                        
                    </ul>
                  </li>
                  
                  
                  <li><a><i class="fa fa-clone"></i>category <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li class ="icon" id="iconr" name="category En"  rel="qcms/pcat" alt="pcat" note="category English" ><a href="#">Main category  </a></li>
                        <li class ="icon" id="iconr" name="category En"  rel="qcms/cat" alt="cat" note="category English" ><a href="#">category  </a></li>
                    </ul>
                  </li>
                  
                  
                  
                  
                  
                  
                 
                  
                  
                  
                  <li><a><i class="fa fa-clone"></i>tools <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                          <li class ="icon" id="iconr" name="general Settings"  rel="qcms/settings" alt="settings" note="settings" ><a href="#">General Settings</a></li>
                  <li class ="icon" id="iconr" name="language"  rel="qcms/language" alt="language" note="language" ><a href="#">Language</a></li>
                  <li class ="icon" id="iconr" name="social"  rel="qcms/sociallinks" alt="sociallinks" note="social links" ><a href="#">Social Links</a></li>
                  <li class ="icon" id="iconr" name="Home Slide"  rel="qcms/qcmsslide" alt="qcmsslide" note="Home Slide" ><a href="#">Home Slide</a></li>
                  <li class ="icon" id="iconr" name="Files"  rel="qcms/uploader" alt="uploader" note="uploader" ><a href="#">Files Uploader</a></li>
                  
                  <li class ="icon" id="iconr" name="supscription"  rel="qcms/supscription" alt="supscription" note="supscription" ><a href="#">Email list</a></li>
                  <li class ="icon" id="iconr" name="offers"  rel="qcms/offers" alt="offers" note="Offers" ><a href="#">Offers</a></li>
                  <li class ="icon" id="iconr" name="online orders"  rel="qcms/onlineorders" alt="onlineorders" note="online orders" ><a href="#">online orders</a></li>
                  <li class ="icon" id="iconr" name="main list"  rel="qcms/mlist" alt="mlist" note="main list" ><a href="#">main list</a></li>
                  <li class ="icon" id="iconr" name="main list"  rel="qcms/city" alt="city" note="main city" ><a href="#">city's</a></li>
                  
<li class ="icon" id="iconr" name=""  rel="qcms/kv" alt="kv" note="kv" ><a href="#">Key Settings </a></li>
                    </ul>
                  </li>
                  
                  
                  
                </ul>
              </div>
              

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    
                    
                    <li><a href="qcms/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col maindiv" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Pages Visit </span>
              <div class="count"><?php echo $static["allvistes"]; ?></div>
              
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> total's users </span>
              <div class="count"><?php echo $static["uniqvist"]; ?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i></i> </span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> today unique users</span>
              <div class="count green"><?php echo $static["todayuniq"]; ?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i><?php echo $static["todaytotal"]; ?> </i> total page viewed today</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> unique users  month</span>
              <div class="count"><?php echo $static["monthuniq"]; ?></div>
              
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total page viewed </span>
              <div class="count"><?php echo $static["monthtotalpages"]; ?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i></i> month</span>
            </div>
            
          </div>
          <!-- /top tiles -->

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Network Activities <small>Graph</small></h3>
                  </div>
                  
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                  <div style="width: 100%;">
                    <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height:270px;"></div>
                  </div>
                </div>
               
                <div class="clearfix"></div>
              </div>
            </div>

          </div>
          <br />

           <div class="row">
          


            <div class="col-md-12 col-sm-12 col-xs-12">



              <div class="row">

                   <div class="col-md-5 col-sm-5 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2>Device Usage</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="" style="width:100%">
                    <tr>
                      <th style="width:37%;">
                       
                      </th>
                      <th>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                          <p class="">Device</p>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                          <p class="">count</p>
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <canvas id="canvas1" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                      </td>
                      <td>
                        <table class="tile_info">
                            <?php if($agent!= FALSE)  { foreach ($agent as $row) { ?>
                          <tr>
                            
                            <td>
                              <p><i class="fa fa-square blue"></i><?php echo $row->agents; ?> </p>
                            </td>
                            <td><?php echo round(($row->vists*100)/$static["allvistes"] , 2);?>%</td>
                          </tr>
                            <?php }  }?>
                          
                          
                         
                          
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>


           
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>Visitors location <small>geo-presentation</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a>
                            </li>
                            <li><a href="#">Settings 2</a>
                            </li>
                          </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div class="dashboard-widget-content">
                        <div class="col-md-4 hidden-small">
                          <h2 class="line_30">125.7k Views from 60 countries</h2>

                          <table class="countries_list">
                            <tbody>
                                <?php if($country != FALSE) {                                      
                                        foreach ($country as $key => $value) { ?>
                                       
                                <tr>
                                <td><?php echo $value->country; ?></td>
                                <td class="fs15 fw700 text-right"><?php echo round(($value->vists*100)/$static["allvistes"] , 4);?>%</td>
                              </tr>
                                    <?php  } 
                                      
                                        } ?>
                              
                             
                             
                              
                              
                            </tbody>
                          </table>
                        </div>
                        <div id="world-map-gdp" class="col-md-8 col-sm-12 col-xs-12" style="height:230px;"></div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              
            </div>
          </div>
          <div class="row">


            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile ">
                <div class="x_title">
                  <h2>Page  statistic's</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <h4>URL's</h4>
                  
                  <?php 
                  foreach ($urlss as  $value) { ?>
                      
                  
                  <div class="widget_summary">
                    <div class="w_left w_25">
                      <span><?php echo $value->urls;?></span>
                    </div>
                    <div class="w_center w_55">
                      <div class="progress">
                        <div class="progress-bar bg-green" role="progressbar" aria-valuenow="<?php echo round(($value->vists*100)/$static["allvistes"] , 4);?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($value->vists*100)/$static["allvistes"] , 6);?>%;">
                          <span class="sr-only"><?php echo round(($value->vists*100)/$static["allvistes"] , 4);?>% Complete</span>
                        </div>
                      </div>
                    </div>
                    <div class="w_right w_20">
                      <span><?php echo round(($value->vists*100)/$static["allvistes"] , 4);?>%</span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <?php }?>
                
                </div>
              </div>
            </div>

           

          </div>


         
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>vendors/jquery/dist/jquery.min.js"></script>
    <script  src="<?php echo base_url(); ?>js/ajaxfileupload.js"></script>
         <script src="<?php echo base_url(); ?>js/vendor/jquery.ui.widget.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.iframe-transport.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.fileupload.js" ></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url(); ?>vendors/chart/dist/chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url(); ?>vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url(); ?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url(); ?>vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url(); ?>vendors/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>vendors/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url(); ?>vendors/flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url(); ?>vendors/flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url(); ?>vendors/flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url(); ?>js/flot/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url(); ?>js/flot/date.js"></script>
    <script src="<?php echo base_url(); ?>js/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url(); ?>js/flot/curvedLines.js"></script>
    <!-- jVectorMap -->
    <script src="<?php echo base_url(); ?>js/maps/jquery-jvectormap-2.0.3.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url(); ?>js/moment/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>/build/js/custom.min.js"></script>

     <script src="<?php echo base_url(); ?>vendors/datatables-net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables-net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap3-wysihtml5.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/pdfmake/build/vfs_fonts.js"></script>
    
      <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=s1r05k7j16iyya0uxs4khn1e5ew7lby3zd9lpmrfpqqyqlp5"></script>


<script></script>
 
    <!-- Flot -->
    <script>
      $(document).ready(function() {

        var data1 = [
           <?php if($graph != FALSE){ 
     foreach ($graph as $key => $value) {
         
     ?>
               
          [gd(<?php echo $value->years." ,".$value->months.",".$value->days; ?>), <?php echo $value->vists; ?>],
          
  
  <?php } } ?> 

        ];

        
        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
          data1
        ], {
          series: {
            lines: {
              show: false,
              fill: true
            },
            splines: {
              show: true,
              tension: 0.4,
              lineWidth: 1,
              fill: 0.4
            },
            points: {
              radius: 0,
              show: true
            },
            shadowSize: 2
          },
          grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
          },
          colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
          xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [1, "day"],
            //tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
          },
          yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
          },
          tooltip: false
        });

        function gd(year, month, day) {
          return new Date(year, month - 1, day).getTime();
        }
      });
    </script>
    <!-- /Flot -->

    <!-- jVectorMap -->
    <script src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
    <script src="js/maps/gdp-data.js"></script>
    
    
    <link rel="stylesheet" type="text/css" href="vendors/simditor/styles/simditor.css" />


<script type="text/javascript" src="vendors/simditor/site/assets/scripts/module.js"></script>
<script type="text/javascript" src="vendors/simditor/site/assets/scripts/hotkeys.js"></script>
<script type="text/javascript" src="vendors/simditor/site/assets/scripts//uploader.js"></script>
<script type="text/javascript" src="vendors/simditor/lib/simditor.js"></script>
    <script>
        
        var gdpData = {
            "AD":0,"AE":0,"AF":0,"AG":0,"AI":0,"AL":0,"AM":0,"AO":0,"AQ":0,"AR":0,"AS":0,"AT":0,"AU":0,"AW":0,"AX":0,"AZ":0,"BA":0,"BB":0,"BD":0,"BE":0,"BF":0,"BG":0,"BH":0,"BI":0,"BJ":0,"BL":0,"BM":0,"BN":0,"BO":0,"BQ":0,"BR":0,"BS":0,"BT":0,"BW":0,"BY":0,"BZ":0,"CA":0,"CD":0,"CF":0,"CG":0,"CH":0,"CI":0,"CK":0,"CL":0,"CM":0,"CN":0,"CO":0,"CR":0,"CU":0,"CV":0,"CW":0,"CY":0,"CZ":0,"DE":0,"DJ":0,"DK":0,"DM":0,"DO":0,"DZ":0,"EC":0,"EE":0,"EG":0,"ER":0,"ES":0,"ET":0,"FI":0,"FJ":0,"FK":0,"FM":0,"FO":0,"FR":0,"GA":0,"GB":0,"GD":0,"GE":0,"GF":0,"GG":0,"GH":0,"GI":0,"GL":0,"GM":0,"GN":0,"GP":0,"GQ":0,"GR":0,"GS":0,"GT":0,"GU":0,"GW":0,"GY":0,"HK":0,"HN":0,"HR":0,"HT":0,"HU":0,"ID":0,"IE":0,"IL":0,"IM":0,"IN":0,"IO":0,"IQ":0,"IR":0,"IS":0,"IT":0,"JE":0,"JM":0,"JO":0,"JP":0,"KE":0,"KG":0,"KH":0,"KI":0,"KM":0,"KN":0,"KP":0,"KR":0,"KW":0,"KY":0,"KZ":0,"LA":0,"LB":0,"LC":0,"LI":0,"LK":0,"LR":0,"LS":0,"LT":0,"LU":0,"LV":0,"LY":0,"MA":0,"MC":0,"MD":0,"ME":0,"MF":0,"MG":0,"MH":0,"MK":0,"ML":0,"MM":0,"MN":0,"MO":0,"MP":0,"MQ":0,"MR":0,"MS":0,"MT":0,"MU":0,"MV":0,"MW":0,"MX":0,"MY":0,"MZ":0,"NA":0,"NC":0,"NE":0,"NF":0,"NG":0,"NI":0,"NL":0,"NO":0,"NP":0,"NR":0,"NU":0,"NZ":0,"OM":0,"PA":0,"PE":0,"PF":0,"PG":0,"PH":0,"PK":0,"PL":0,"PM":0,"PR":0,"PS":0,"PT":0,"PW":0,"PY":0,"QA":0,"RE":0,"RO":0,"RS":0,"RU":0,"RW":0,"SA":0,"SB":0,"SC":0,"SD":0,"SE":0,"SG":0,"SI":0,"SK":0,"SL":0,"SM":0,"SN":0,"SO":0,"SR":0,"SS":0,"ST":0,"SV":0,"SX":0,"SY":0,"SZ":0,"TC":0,"TD":0,"TG":0,"TH":0,"TJ":0,"TK":0,"TL":0,"TM":0,"TN":0,"TO":0,"TR":0,"TT":0,"TV":0,"TW":0,"TZ":0,"UA":0,"UG":0,"UM":0,"US":0,"UY":0,"UZ":0,"VA":0,"VC":0,"VE":0,"VG":0,"VI":0,"VN":0,"VU":0,"WF":0,"WS":0,"YE":0,"YT":0,"ZA":0,"ZM":0,"ZW":0 ,
       <?php if($country != FALSE) {
           
            foreach ($country as $key => $value) {  echo "'".$value->country."'".":".$value->vists.",";  }
       
        }
     
        ?> 
                 
       };
      $(document).ready(function(){
        $('#world-map-gdp').vectorMap({
          map: 'world_mill_en',
          backgroundColor: 'transparent',
          zoomOnScroll: false,
          series: {
            regions: [{
              values: gdpData,
              scale: ['#E6F2F0', '#149B7E'],
              normalizeFunction: 'polynomial'
            }]
          },
          onRegionTipShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });
    </script>
    <!-- /jVectorMap -->

    <!-- Skycons -->
    <script>
      $(document).ready(function() {
        var icons = new Skycons({
            "color": "#73879C"
          }),
          list = [
            "clear-day", "clear-night", "partly-cloudy-day",
            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
            "fog"
          ],
          i;

        for (i = list.length; i--;)
          icons.set(list[i], list[i]);

        icons.play();
      });
    </script>
    <!-- /Skycons -->

    <!-- Doughnut Chart -->
    <script>
      $(document).ready(function(){
        var options = {
          legend: false,
          responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
          type: 'doughnut',
          tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          data: {
            labels: [
                <?php if($agent!= FALSE)  { foreach ($agent as $row) { echo "'".$row->agents."',"; ?>
              
                <?php } } ?>
            ],
            datasets: [{
              data: [<?php if($agent!= FALSE)  { foreach ($agent as $row) { echo $row->vists.","; ?>
              
                <?php } } ?>],
              backgroundColor: [
                "#BDC3C7",
                "#9B59B6",
                "#E74C3C",
                "#26B99A",
                "#3498DB"
              ],
              hoverBackgroundColor: [
                "#CFD4D8",
                "#B370CF",
                "#E95E4F",
                "#36CAAB",
                "#49A9EA"
              ]
            }]
          },
          options: options
        });
      });
    </script>
    <!-- /Doughnut Chart -->
    
    <!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {

        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

    <!-- gauge.js -->
    <script>
      var opts = {
          lines: 12,
          angle: 0,
          lineWidth: 0.4,
          pointer: {
              length: 0.75,
              strokeWidth: 0.042,
              color: '#1D212A'
          },
          limitMax: 'false',
          colorStart: '#1ABC9C',
          colorStop: '#1ABC9C',
          strokeColor: '#F0F3F3',
          generateGradient: true
      };
      var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 6000;
      gauge.animationSpeed = 32;
      gauge.set(3200);
      gauge.setTextField(document.getElementById("gauge-text"));
    </script>
    <!-- /gauge.js -->
    
    
    <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
            
            
            
            function aup(){
                
    $('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            $(".popups").hide();
            if(data._response.result.msg == "File uploaded"){

                console.log("%o",data._response.result.hn);
                if(data._response.result.hn !== undefined)
                {
                        imglist.push(data._response.result.hn);
                        var newli = $("<li rel="+ (imglist.length - 1) +">  <img src=cdn/"+data._response.result.hn+" /> </li>");
                        var hnr = $("<a id='rm' rel="+ (imglist.length - 1) +">X</a>");
                        $("#plist").append(newli);
                        newli.append(hnr);
                        $("#ttimg").html(imglist.length);


                        hnr.click(function(){

                            var r = $(this ).attr('rel');
                             //remove from view
                             newli.remove();
                            //remove from array
                            imglist.splice(r, 1);
                            $("#ttimg").html(imglist.length);
                        });
                }
            }
            else
            {

            }
            
        }
    });

  
            }
            
            
            
            function ajaxFileUpload()
            {
                $("#loading")
                        .ajaxStart(function() {
                    $(this).show();
                })
                        .ajaxComplete(function() {
                    $(this).hide();
                });

                $.ajaxFileUpload
                        (
                                {
                                    url: base_url + 'qcms/uploadfile',
                                    secureuri: false,
                                    fileElementId: 'userfile',
                                    dataType: 'json',
                                    success: function(data, status)
                                    {
                                        if (typeof(data.error) != 'undefined')
                                        {
                                            if (data.error != '')
                                            {
                                                alert(data.error);
                                            }
                                            else
                                            {
                                                alert(data.msg);
                                            }
                                        }
                                    }
                                    ,
                                    error: function(data, status, e)
                                    {
                                        alert(e);
                                    }
                                }
                        )

                return false;

            }
        </script>

        <script type="text/javascript" language="javascript">
            
            var base_url = '<?php echo base_url(); ?>';
            $(function()
            {
                

      $('#cssmenu > ul > li ul').each(function(index, e){
  var count = $(e).find('li').length;
  var content = '<span class=\"cnt\">' + count + '</span>';
  $(e).closest('li').children('a').append(content);
});
$('#cssmenu ul ul li:odd').addClass('odd');
$('#cssmenu ul ul li:even').addClass('even');
$('#cssmenu > ul > li > a').click(function() {
  $('#cssmenu li').removeClass('active');
  $(this).closest('li').addClass('active');	
  var checkElement = $(this).next();
  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
    $(this).closest('li').removeClass('active');
    checkElement.slideUp('normal');
  }
  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
    $('#cssmenu ul ul:visible').slideUp('normal');
    checkElement.slideDown('normal');
  }
  if($(this).closest('li').find('ul').children().length == 0) {
    return true;
  } else {
    return false;	
  }	
});

                var acticvG = false;
                var mitro = false;
                var ActivAlt = false;



               // $('.icons').tipsy({title: 'note', fade: true, gravity: 'nw'});
               // $('.icon').tipsy({title: 'note', fade: true});
                //$(".bwind").draggable();
                // $('#clock').draggable();

                //$(".innertry").hide();
                $(".gruob").css({opacity: 0.3});
                $(".gruob").hide();
                $("#ajax").hide();
                $('#ajax').ajaxStart(function() {
                    $(this).show();
                }).ajaxStop(function() {
                    $(this).hide();
                });


               // $(".icons").draggable();
                $(".icons").hover(function() {
                    $(this).addClass("light");
                    $(this).css({opacity: 0.5});
                }, function() {
                    $(this).removeClass("light");
                    $(this).css({opacity: 1.0});
                });





                $(".search").hover(function() {
                    $(this).css({"z-index": 100000000});
                }, function() {
                    $(this).css({"z-index": 100});
                });

                $(".gruob").hover(function() {
                    $(this).css({opacity: 1.0});
                }, function() {
                    $(this).css({opacity: 0.2});
                });
                $(".bwind").hover(function() {

                    ActivAlt = $(this).attr("alt");

                });
              









                $(".icons").on("dblclick", function(event) {


                    var vals = $(this).attr("name");
                    var rels = $(this).attr("rel");
                    var alts = $(this).attr("alt");
                    $("#" + acticvG).animate({top: -1000}, 1000, "linear");
                    $(this).fadeOut(1000);
                    $(this).fadeIn(1000);

                    //$("#"+acticvG).hide();
                    $("#" + rels).show(100);

                    $("#" + rels).animate({top: 200}, 900, "linear");



                    return acticvG = rels;


                });


                $(".icon").on("click", function(event) {
                    $("#" + acticvG).animate({top: -1000}, 1000, "linear");
                    var $source = $(event.target);
                    var val = $(this).attr("name");
                    var rel = $(this).attr("rel");
                    var alt = $(this).attr("alt");
                    //ActivAlt = alt;
                    //$($source).each(function(){
                    //   $(this).niceScroll();

                    //});

                  //  $(".innertry").fadeIn(200);
                  //  $(this).fadeOut(1000);
                  //  $(this).fadeIn(1000);
                    //$("."+alt).parents("div").hide();
                    //$("."+alt).parents("div").empty();
                    // $("."+alt).empty();

                    var cin = "<div rel=\"" + alt + "\" ><div class=\"wind_head\"></div><div class=\"wind " + alt + " \"></div></div>";
                    $(".maindiv").html(cin);

                    

                    $("." + alt).load(base_url + rel);
                    //$("."+alt).parents("div").draggable();
                    //$("."+alt).jScrollPane();



                    $(".innertry ul").append("<li alt='"+alt+"'>"+val+"</li>");

                  //  $(".innertry").fadeOut(300);
                  //  $(".innertry").fadeIn(200);
                    $(".innertry ul li[alt="+alt+"]").fadeOut(4000);
                    $("#try img").css("max-width", "10px");





                });

                 
                 
                  
                


                $(".wind_close").on("click", function() {

                    $(this).parents("div").hide();
                    $(this).parents("div").empty();



                });


   

                $("#trybg").css({opacity: 0.2});
                $("#ajaxbg").css({opacity: 0.5});

                $(".icon").css({opacity: 1.0, "z-index": 1000010});



                






            });

            function removeElement(elemID) {
                var elem = document.getElementById(elemID);
                elem.parentNode.removeChild(elem);
            }

        </script>
  </body>
</html>