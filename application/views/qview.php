
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery-ui-1.11.3/jquery-ui.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bs/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bs/bootstrap-responsive.min.css" />



        <script src="<?php echo base_url(); ?>js/jquery-1.11.3.min.js"></script>

        <script src="<?php echo base_url(); ?>vendors/jquery.knob.js"></script>
        <script src="<?php echo base_url(); ?>vendors/raphael-min.js"></script>
        <script src="<?php echo base_url(); ?>vendors/morris/morris.min.js"></script>

        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>


        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>css/jquery-ui-1.11.3/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.fileupload.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/panel2.css" />

        <link rel="stylesheet" href="<?php echo base_url(); ?>vendors/morris/morris.css">

        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>vendors/morris/morris.min.js"></script>
        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>js/ajaxfileupload.js"></script>

        <script src="<?php echo base_url(); ?>js/vendor/jquery.ui.widget.js"></script>

        <script src="<?php echo base_url(); ?>js/jquery.iframe-transport.js"></script>

        <script src="<?php echo base_url(); ?>js/jquery.fileupload.js"></script>



        <title>Clouds  Gen</title>
    </head>
    <body>

        <div id="ajax" >
            <div id="ajaxbg"></div>
            <img src="<?php echo base_url(); ?>images/admin.thim/AirBookLogo.png" width="100" />

            <img src="<?php echo base_url(); ?>images/ajax-loader.gif" />
        </div>


        <div class="navbar bar">
            <div class="container">
                <div class="navbar-inner">
                    <a class="brand" href="#">   <img src="<?php echo base_url(); ?>images/admin.thim/AirBookLogo.png" width="180" /></a>
                    <div class="right_item">
                        <ul>
                            <li>Welcome , <?php echo $username; ?></li>
                            <li><a href="<?php echo base_url(); ?>qcms/logout">  Logout </a></li>
                        </ul>


                    </div>

                </div>
            </div>

        </div>

        <div class="row" style="height: 100%;">




            <div class="col-md-2  wells ">



                <div id="cssmenu">
                    <ul>
                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.home.png" width="20px" />

                            <a href="<?php echo base_url(); ?>qcms"><span>Dashboard</span></a>
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.people.multiple.png" width="20px" />


                            <a href="#"><span>About us </span></a>
                            <ul>
                                <li class ="icon" id="iconr" name="About us En"  rel="qcms/aboutusen" alt="aboutusen" note="about us English" ><a href="#">About us original</a></li>
                                <li class ="icon" id="iconr" name="About us Translation"  rel="qcms/aboutustr" alt="aboutustr" note="about us Translation" ><a href="#">About us Translation </a></li>
                            </ul>
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.tv.png" width="20px" />

                            <a href="#"><span>Branches</span></a>
                            <ul>
                                <li class ="icon" id="iconr" name="Branches En"  rel="qcms/branches" alt="branches" note="branches English" ><a href="#">Branches original </a></li>
                                <li class ="icon" id="iconr" name="Branches Translation"  rel="qcms/branchestr" alt="branchestr" note="branches Translation" ><a href="#">Branches Translation </a></li>
                            </ul>
                        </li>

                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.list.star.png" width="20px" />

                            <a href="#"><span>Articles</span></a>
                            <ul>

                                <li class ="icon" id="iconr" name="Articles En"  rel="qcms/articls" alt="articls" note="Articles English" ><a href="#">Articles original </a></li>
                                <li class ="icon" id="iconr" name="Articles Translation"  rel="qcms/articlstr" alt="articlstr" note="Articles Translation" ><a href="#">Articles Translation </a></li>

                            </ul>
                        </li>

                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.message.smiley.png" width="20px" />

                            <a href="#"><span>Contact us</span></a>
                            <ul>
                                <li class ="icon" id="iconr" name="Contact us En"  rel="qcms/contact" alt="contact" note="Contact us English" ><a href="#">Contact original </a></li>
                                <li class ="icon" id="iconr" name="Contact us Translation"  rel="qcms/contacttr" alt="contacttr" note="Contact us Translation" ><a href="#">Contact Translation </a></li>

                            </ul>
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.currency.euro.png" width="20px" />
                            <a href="#"><span>Products</span></a>
                            <ul>
                                <li class ="icon" id="iconr" name="product En"  rel="qcms/product" alt="product" note="products English" ><a href="#">Products original </a></li>
                                <li class ="icon" id="iconr" name="product Plans En"  rel="qcms/productplans" alt="productplans" note="products Plans English" ><a href="#">Products Plans </a></li>

                                <li class ="icon" id="iconr" name="product Translation"  rel="qcms/producttr" alt="producttr" note="products Translation" ><a href="#">Products Translation </a></li>

                            </ul>
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.currency.euro.png" width="20px" />
                            <a href="#"><span>category</span></a>
                            <ul>
                               <li class ="icon" id="iconr" name="category En"  rel="qcms/pcat" alt="pcat" note="category English" ><a href="#">Main category  </a></li>
                        <li class ="icon" id="iconr" name="category En"  rel="qcms/cat" alt="cat" note="category English" ><a href="#">category  </a></li>
                            </ul>
                        </li>

                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.currency.euro.png" width="20px" />
                            <a href="#"><span>brands</span></a>
                            <ul>
                                <li class ="icon" id="iconr" name="brands En"  rel="qcms/brands" alt="brands" note="brands English" ><a href="#">brands original </a></li>
                                <li class ="icon" id="iconr" name="brands Translation"  rel="qcms/brandstr" alt="brandstr" note="brands Translation" ><a href="#">brands Translation </a></li>

                            </ul>
                        </li>

                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.currency.euro.png" width="20px" />
                            <a href="#"><span>jobs</span></a>
                            <ul>
                                <li  class ="icon" id="iconr" name=" jobs EN "  rel="qcms/jobs" alt="jobs" note="jobs En" ><a href="#">jobs original  </a></li>
                                <li class ="icon" id="iconr" name="jobs Translation"  rel="qcms/jobstr" alt="jobstr" note="jobs Translation" ><a href="#">jobs Translation  </a></li>

                            </ul>
                        </li>

                        <li>
                            <img src="<?php echo base_url(); ?>images/admin.thim/appbar.currency.euro.png" width="20px" />
                            <a href="#"><span>tools</span></a>
                            <ul>
                                <li class ="icon" id="iconr" name="general Settings"  rel="qcms/settings" alt="settings" note="settings" ><a href="#">General Settings</a></li><br/>
                                <li class ="icon" id="iconr" name="language"  rel="qcms/language" alt="language" note="language" ><a href="#">Language</a></li><br/>
                                <li class ="icon" id="iconr" name="social"  rel="qcms/sociallinks" alt="sociallinks" note="social links" ><a href="#">Social Links</a></li><br/>
                                <li class ="icon" id="iconr" name="Home Slide"  rel="qcms/qcmsslide" alt="homeslide" note="Home Slide" ><a href="#">Home Slide</a></li><br/>
                                <li class ="icon" id="iconr" name="Files"  rel="qcms/uploader" alt="uploader" note="uploader" ><a href="#">Files Uploader</a></li><br/>
                                <li class ="icon" id="iconr" name="details group"  rel="qcms/detailsgroup" alt="detailsgroup" note="details group" ><a href="#">details group</a></li><br/>
                                <li class ="icon" id="iconr" name="specifications"  rel="qcms/specifications" alt="specifications" note="specifications" ><a href="#">specifications</a></li><br/>
                                <li class ="icon" id="iconr" name="supscription"  rel="qcms/supscription" alt="supscription" note="supscription" ><a href="#">Email list</a></li><br/>
                                <li class ="icon" id="iconr" name="offers"  rel="qcms/offers" alt="offers" note="Offers" ><a href="#">Offers</a></li><br/>
                                <li class ="icon" id="iconr" name="online orders"  rel="qcms/onlineorders" alt="onlineorders" note="online orders" ><a href="#">online orders</a></li><br/>
                                <li class ="icon" id="iconr" name="main list"  rel="qcms/mlist" alt="mlist" note="main list" ><a href="#">main list</a></li><br/>
                                
                                
                                <li class ="icon" id="iconr" name="main list Translation"  rel="qcms/mlisttr" alt="mlisttr" note="main list Translation" ><a href="#">main list Translation</a></li><br/>

                            </ul>
                        </li>




                    </ul>
                </div>
















            </div>





            <div class=" col-md-10  maindiv">


                <div class="titboard">

                    Dashboard    
                </div>

                <div class="row-fluid ststicdv">
                    <div  id="bar-ex"></div>    
                </div>
                <div class="row-fluid ststicdv">
                    <div  id="bar-ex3"></div>    
                </div>
                <div class="row-fluid ststicdv marged">
                    <div class="titboard">


                    </div>

                    <div  id="bar-ex2"></div>    
                </div>


                <div class="row-fluid marged">
                    <div class="titboard">

                        users counters    
                    </div>


                    <div class="col-md-5 ststicdv widget">


                    </div>

                    <div class="col-md-5 ststicdv widget">


                    </div>

                    <div class="col-md-5 ststicdv widget">


                    </div>



                    <div class="col-md-3 ststicdv widget">


                    </div>

                    <div class="col-md-2 ststicdv widget">


                    </div>

                </div>

                <div class="row-fluid ">



                    <div class="col-md-2 ststicdv widget">


                    </div>
                    <div class="col-md-2 ststicdv widget">


                    </div>
                    <div class="col-md-2 ststicdv widget">


                    </div>

                    <div class="col-md-2 ststicdv widget">


                    </div>


                </div>

                <div class="row-fluid ">
                    <div class="titboard">

                        channels counters    
                    </div>


                    <div class="col-md-5 ststicdv widget">








                    </div>

                    <div class="col-md-5 ststicdv widget">


                    </div>



                </div>





            </div>






        </div>































        <div class="innertry">
            <ul>

            </ul>
        </div>





    </div>


    <script>









    </script>


    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
        function ajaxFileUpload()
        {
            $("#loading")
                    .ajaxStart(function () {
                        $(this).show();
                    })
                    .ajaxComplete(function () {
                        $(this).hide();
                    });

            $.ajaxFileUpload
                    (
                            {
                                url: base_url + 'qcms/uploadfile',
                                secureuri: false,
                                fileElementId: 'userfile',
                                dataType: 'json',
                                success: function (data, status)
                                {
                                    if (typeof (data.error) != 'undefined')
                                    {
                                        if (data.error != '')
                                        {
                                            alert(data.error);
                                        }
                                        else
                                        {
                                            alert(data.msg);
                                        }
                                    }
                                }
                                ,
                                error: function (data, status, e)
                                {
                                    alert(e);
                                }
                            }
                    )

            return false;

        }
    </script>

    <script type="text/javascript" language="javascript">

        var base_url = '<?php echo base_url(); ?>';
        $(function ()
        {


            $('#cssmenu > ul > li ul').each(function (index, e) {
                var count = $(e).find('li').length;
                var content = '<span class=\"cnt\">' + count + '</span>';
                $(e).closest('li').children('a').append(content);
            });
            $('#cssmenu ul ul li:odd').addClass('odd');
            $('#cssmenu ul ul li:even').addClass('even');
            $('#cssmenu > ul > li > a').click(function () {
                $('#cssmenu li').removeClass('active');
                $(this).closest('li').addClass('active');
                var checkElement = $(this).next();
                if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                    $(this).closest('li').removeClass('active');
                    checkElement.slideUp('normal');
                }
                if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                    $('#cssmenu ul ul:visible').slideUp('normal');
                    checkElement.slideDown('normal');
                }
                if ($(this).closest('li').find('ul').children().length == 0) {
                    return true;
                } else {
                    return false;
                }
            });

            var acticvG = false;
            var mitro = false;
            var ActivAlt = false;



            // $('.icons').tipsy({title: 'note', fade: true, gravity: 'nw'});
            // $('.icon').tipsy({title: 'note', fade: true});
            //$(".bwind").draggable();
            // $('#clock').draggable();

            //$(".innertry").hide();
            $(".gruob").css({opacity: 0.3});
            $(".gruob").hide();
            $("#ajax").hide();
            $('#ajax').ajaxStart(function () {
                $(this).show();
            }).ajaxStop(function () {
                $(this).hide();
            });


            $(".icons").draggable();
            $(".icons").hover(function () {
                $(this).addClass("light");
                $(this).css({opacity: 0.5});
            }, function () {
                $(this).removeClass("light");
                $(this).css({opacity: 1.0});
            });





            $(".search").hover(function () {
                $(this).css({"z-index": 100000000});
            }, function () {
                $(this).css({"z-index": 100});
            });

            $(".gruob").hover(function () {
                $(this).css({opacity: 1.0});
            }, function () {
                $(this).css({opacity: 0.2});
            });
            $(".bwind").hover(function () {

                ActivAlt = $(this).attr("alt");

            });










            $(".icons").on("dblclick", function (event) {


                var vals = $(this).attr("name");
                var rels = $(this).attr("rel");
                var alts = $(this).attr("alt");
                $("#" + acticvG).animate({top: -1000}, 1000, "linear");
                $(this).fadeOut(1000);
                $(this).fadeIn(1000);

                //$("#"+acticvG).hide();
                $("#" + rels).show(100);

                $("#" + rels).animate({top: 200}, 900, "linear");



                return acticvG = rels;


            });


            $(".icon").on("click", function (event) {
                $("#" + acticvG).animate({top: -1000}, 1000, "linear");
                var $source = $(event.target);
                var val = $(this).attr("name");
                var rel = $(this).attr("rel");
                var alt = $(this).attr("alt");
                //ActivAlt = alt;
                //$($source).each(function(){
                //   $(this).niceScroll();

                //});

                //  $(".innertry").fadeIn(200);
                //  $(this).fadeOut(1000);
                //  $(this).fadeIn(1000);
                //$("."+alt).parents("div").hide();
                //$("."+alt).parents("div").empty();
                // $("."+alt).empty();

                var cin = "<div rel=\"" + alt + "\" ><div class=\"wind_head\"></div><div class=\"wind " + alt + " \"></div></div>";
                $(".maindiv").html(cin);



                $("." + alt).load(base_url + rel);
                //$("."+alt).parents("div").draggable();
                //$("."+alt).jScrollPane();



                $(".innertry ul").append("<li alt='" + alt + "'>" + val + "</li>");

                //  $(".innertry").fadeOut(300);
                //  $(".innertry").fadeIn(200);
                $(".innertry ul li[alt=" + alt + "]").fadeOut(4000);
                $("#try img").css("max-width", "10px");





            });







            $(".wind_close").on("click", function () {

                $(this).parents("div").hide();
                $(this).parents("div").empty();



            });




            $("#trybg").css({opacity: 0.2});
            $("#ajaxbg").css({opacity: 0.5});

            $(".icon").css({opacity: 1.0, "z-index": 1000010});








        });

        function removeElement(elemID) {
            var elem = document.getElementById(elemID);
            elem.parentNode.removeChild(elem);
        }

    </script>
</body>


</html>
