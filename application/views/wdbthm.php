<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cloud Gen </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    <link href="<?php echo base_url(); ?>css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

    
    <link href="<?php echo base_url(); ?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>js/bootstrap3-wysihtml5.min.css" />
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>build/css/custom.min.css" rel="stylesheet">
   <style >
       .gbody {
           clear: both;
       }
      .gbody img {
          width: 150px;
         height: 150px;
         float: left;
         
      }
      
  </style>
  </head>

 
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                  &nbsp;
              </div>
              <div class="profile_info">
                <span>Welcome, <?php echo $username; ?></span>
                
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li>
                      <a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>qcms"><span>Dashboard</span></a></li>
                      
                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li class ="icon" id="iconr" name="About us En"  rel="qcms/aboutusen" alt="aboutusen" note="about us English" ><a href="#">About US</a></li>
                        <li class ="icon" id="iconr" name="Contact us En"  rel="qcms/contact" alt="contact" note="Contact us English" ><a href="#">Contact US </a></li>
                        
                        
                        <li  ><a href="<?php echo base_url(); ?>newpage">Pages  </a></li>
                    </ul>
                  </li>
                  
                  <li><a><i class="fa fa-table"></i> Blog <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li class ="icon" id="iconr" name="Articles En"  rel="qcms/articls" alt="articls" note="Articles English" ><a href="#">Blog </a></li>
                      
                    </ul>
                  </li>
                  
                  <li><a><i class="fa fa-clone"></i>Products <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li class ="icon" id="iconr" name="product En"  rel="qcms/product" alt="product" note="products English" ><a href="#">Products </a></li>
                       
                    </ul>
                  </li>
                  
                  
                  <li><a><i class="fa fa-clone"></i>category <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li class ="icon" id="iconr" name="category En"  rel="qcms/pcat" alt="pcat" note="category English" ><a href="#">Main category  </a></li>
                        <li class ="icon" id="iconr" name="category En"  rel="qcms/cat" alt="cat" note="category English" ><a href="#">category  </a></li>
                        
                    </ul>
                  </li>
                  
                  
                  
                  
                  
                  
                 
                  
                  
                  
                  <li><a><i class="fa fa-clone"></i>tools <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                          <li class ="icon" id="iconr" name="general Settings"  rel="qcms/settings" alt="settings" note="settings" ><a href="#">General Settings</a></li>
                  <li class ="icon" id="iconr" name="language"  rel="qcms/language" alt="language" note="language" ><a href="#">Language</a></li>
                  <li class ="icon" id="iconr" name="social"  rel="qcms/sociallinks" alt="sociallinks" note="social links" ><a href="#">Social Links</a></li>
                  <li class ="icon" id="iconr" name="Home Slide"  rel="qcms/qcmsslide" alt="qcmsslide" note="Home Slide" ><a href="#">Home Slide</a></li>
                  <li class ="icon" id="iconr" name="Files"  rel="qcms/uploader" alt="uploader" note="uploader" ><a href="#">Files Uploader</a></li>
                  <li class ="icon" id="iconr" name="supscription"  rel="qcms/supscription" alt="supscription" note="supscription" ><a href="#">Email list</a></li>
                  <li class ="icon" id="iconr" name="offers"  rel="qcms/offers" alt="offers" note="Offers" ><a href="#">Offers</a></li>
                  <li class ="icon" id="iconr" name="online orders"  rel="qcms/onlineorders" alt="onlineorders" note="online orders" ><a href="#">online orders</a></li>
                  <li class ="icon" id="iconr" name="main list"  rel="qcms/mlist" alt="mlist" note="main list" ><a href="#">main list</a></li>
                  <li class ="icon" id="iconr" name="main list Translation"  rel="home/mlisttr" alt="mlisttr" note="main list Translation" ><a href="#">main list Translation</a></li>

                    </ul>
                  </li>
                  
                  
                  
                </ul>
              </div>
              

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    
                    
                    
                    <li><a href="qcms/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <?php $this->load->view("xui/".$xui); ?>
        
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>vendors/jquery/dist/jquery.min.js"></script>
    <script  src="<?php echo base_url(); ?>js/ajaxfileupload.js"></script>
         <script src="<?php echo base_url(); ?>js/vendor/jquery.ui.widget.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.iframe-transport.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.fileupload.js" ></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url(); ?>vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url(); ?>vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url(); ?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url(); ?>vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url(); ?>vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url(); ?>js/flot/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url(); ?>js/flot/date.js"></script>
    <script src="<?php echo base_url(); ?>js/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url(); ?>js/flot/curvedLines.js"></script>
    <!-- jVectorMap -->
    <script src="<?php echo base_url(); ?>js/maps/jquery-jvectormap-2.0.3.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url(); ?>js/moment/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>/build/js/custom.min.js"></script>

     <script src="<?php echo base_url(); ?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>vendors/pdfmake/build/vfs_fonts.js"></script>


 
    <!-- Flot -->
  
    <!-- /Flot -->

    <!-- jVectorMap -->
    
    
    <!-- /jVectorMap -->

    <!-- Skycons -->
    
    
    <!-- /Skycons -->

    <!-- Doughnut Chart -->
    
    <!-- /Doughnut Chart -->
    
    <!-- bootstrap-daterangepicker -->
    
    
    <!-- /bootstrap-daterangepicker -->

    <!-- gauge.js -->
   
    <!-- /gauge.js -->
    
    
    <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
            
            
            
            function aup(){
                
    $('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            $(".popups").hide();
            if(data._response.result.msg == "File uploaded"){

                console.log("%o",data._response.result.hn);
                if(data._response.result.hn !== undefined)
                {
                        imglist.push(data._response.result.hn);
                        var newli = $("<li rel="+ (imglist.length - 1) +">  <img src=cdn/"+data._response.result.hn+" /> </li>");
                        var hnr = $("<a id='rm' rel="+ (imglist.length - 1) +">X</a>");
                        $("#plist").append(newli);
                        newli.append(hnr);
                        $("#ttimg").html(imglist.length);


                        hnr.click(function(){

                            var r = $(this ).attr('rel');
                             //remove from view
                             newli.remove();
                            //remove from array
                            imglist.splice(r, 1);
                            $("#ttimg").html(imglist.length);
                        });
                }
            }
            else
            {

            }
            
        }
    });

  
            }
            
            
            
            function ajaxFileUpload()
            {
                $("#loading")
                        .ajaxStart(function() {
                    $(this).show();
                })
                        .ajaxComplete(function() {
                    $(this).hide();
                });

                $.ajaxFileUpload
                        (
                                {
                                    url: base_url + 'qcms/uploadfile',
                                    secureuri: false,
                                    fileElementId: 'userfile',
                                    dataType: 'json',
                                    success: function(data, status)
                                    {
                                        if (typeof(data.error) != 'undefined')
                                        {
                                            if (data.error != '')
                                            {
                                                alert(data.error);
                                            }
                                            else
                                            {
                                                alert(data.msg);
                                            }
                                        }
                                    }
                                    ,
                                    error: function(data, status, e)
                                    {
                                        alert(e);
                                    }
                                }
                        )

                return false;

            }
        </script>

        <script type="text/javascript" language="javascript">
            
            var base_url = '<?php echo base_url(); ?>';
            $(function()
            {
                

      $('#cssmenu > ul > li ul').each(function(index, e){
  var count = $(e).find('li').length;
  var content = '<span class=\"cnt\">' + count + '</span>';
  $(e).closest('li').children('a').append(content);
});
$('#cssmenu ul ul li:odd').addClass('odd');
$('#cssmenu ul ul li:even').addClass('even');
$('#cssmenu > ul > li > a').click(function() {
  $('#cssmenu li').removeClass('active');
  $(this).closest('li').addClass('active');	
  var checkElement = $(this).next();
  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
    $(this).closest('li').removeClass('active');
    checkElement.slideUp('normal');
  }
  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
    $('#cssmenu ul ul:visible').slideUp('normal');
    checkElement.slideDown('normal');
  }
  if($(this).closest('li').find('ul').children().length == 0) {
    return true;
  } else {
    return false;	
  }		
});

                var acticvG = false;
                var mitro = false;
                var ActivAlt = false;



               // $('.icons').tipsy({title: 'note', fade: true, gravity: 'nw'});
               // $('.icon').tipsy({title: 'note', fade: true});
                //$(".bwind").draggable();
                // $('#clock').draggable();

                //$(".innertry").hide();
                $(".gruob").css({opacity: 0.3});
                $(".gruob").hide();
                $("#ajax").hide();
                $('#ajax').ajaxStart(function() {
                    $(this).show();
                }).ajaxStop(function() {
                    $(this).hide();
                });


               // $(".icons").draggable();
                $(".icons").hover(function() {
                    $(this).addClass("light");
                    $(this).css({opacity: 0.5});
                }, function() {
                    $(this).removeClass("light");
                    $(this).css({opacity: 1.0});
                });





                $(".search").hover(function() {
                    $(this).css({"z-index": 100000000});
                }, function() {
                    $(this).css({"z-index": 100});
                });

                $(".gruob").hover(function() {
                    $(this).css({opacity: 1.0});
                }, function() {
                    $(this).css({opacity: 0.2});
                });
                $(".bwind").hover(function() {

                    ActivAlt = $(this).attr("alt");

                });
              









                $(".icons").on("dblclick", function(event) {


                    var vals = $(this).attr("name");
                    var rels = $(this).attr("rel");
                    var alts = $(this).attr("alt");
                    $("#" + acticvG).animate({top: -1000}, 1000, "linear");
                    $(this).fadeOut(1000);
                    $(this).fadeIn(1000);

                    //$("#"+acticvG).hide();
                    $("#" + rels).show(100);

                    $("#" + rels).animate({top: 200}, 900, "linear");



                    return acticvG = rels;


                });


                $(".icon").on("click", function(event) {
                    $("#" + acticvG).animate({top: -1000}, 1000, "linear");
                    var $source = $(event.target);
                    var val = $(this).attr("name");
                    var rel = $(this).attr("rel");
                    var alt = $(this).attr("alt");
                    //ActivAlt = alt;
                    //$($source).each(function(){
                    //   $(this).niceScroll();

                    //});

                  //  $(".innertry").fadeIn(200);
                  //  $(this).fadeOut(1000);
                  //  $(this).fadeIn(1000);
                    //$("."+alt).parents("div").hide();
                    //$("."+alt).parents("div").empty();
                    // $("."+alt).empty();

                    var cin = "<div rel=\"" + alt + "\" ><div class=\"wind_head\"></div><div class=\"wind " + alt + " \"></div></div>";
                    $(".maindiv").html(cin);

                    

                    $("." + alt).load(base_url + rel);
                    //$("."+alt).parents("div").draggable();
                    //$("."+alt).jScrollPane();



                    $(".innertry ul").append("<li alt='"+alt+"'>"+val+"</li>");

                  //  $(".innertry").fadeOut(300);
                  //  $(".innertry").fadeIn(200);
                    $(".innertry ul li[alt="+alt+"]").fadeOut(4000);
                    $("#try img").css("max-width", "10px");





                });

                 
                 
                  
                


                $(".wind_close").on("click", function() {

                    $(this).parents("div").hide();
                    $(this).parents("div").empty();



                });


   

                $("#trybg").css({opacity: 0.2});
                $("#ajaxbg").css({opacity: 0.5});

                $(".icon").css({opacity: 1.0, "z-index": 1000010});








            });

            function removeElement(elemID) {
                var elem = document.getElementById(elemID);
                elem.parentNode.removeChild(elem);
            }

                

        </script>
        
        <script type="text/javascript">
  $(function()
            {
                 editable("#spi1" , "img");
                 editable("#spt1");
                 
                 editable("#spi2" , "img");
                 editable("#spt2");
                 
                 editable("#spi3" , "img");
                 editable("#spt3");
                 
                 editable("#st");
                    function editable(elemn , ty)
                    {

                        $(elemn).click( function(){
                            switch(ty){
                                case "img":
                                    ev = $(elemn).attr("src");
                                    break;
                                default:
                                    var divHtml = $(elemn).html();
                                    var editableText = $("<textarea />");
                                    editableText.val(divHtml);
                                    $(elemn).replaceWith(editableText);
                                    ev = $(elemn).text();
                                    break;
                            }
                            

                           // alert(ev);



                        });
                    }
            });   



 
                 
                 
                 
                 
 

</script>        
  </body>
</html>