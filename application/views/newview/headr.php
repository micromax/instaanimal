<?php foreach ($siteconfig as  $sitev) {} ?>
<!DOCTYPE HTML>
<html>

    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8" />

        <?php 
        
            $ks =  $sitev->website_keywords;
            if (isset($kw) && $kw != null) {
                $ks .=$kw;
            } 
            
            if (isset($seo) && $seo != null) {
                $ks .=$seo;
            }
            
         ?>
        
        
            <meta name="keywords" content="<?php print($ks); ?>" />

            
        <?php 
        $d = $sitev->website_description." ";
        if(isset($disks))
        {
            $d .=$disks." ";
        }
        if (isset($dic) and $dic != null) {
            $d .=" ".$dic;
        }
            ?>
            <meta name="description" content="<?php print($d); ?>" />
            
        
        <meta property="twitter:image" content=" <?php echo base_url(); ?>uploads/<?= $sitev->website_defulatimg; ?>">
        <meta property="og:type" content="website">
      
        
        
        
        
        <meta property="og:title" content="<?php 
         
          if (isset($name) && $name != null) {
                echo  $name ." ";
            }  else {
                 echo $sw["title"];
            } ?> ">
            <meta property="og:description" content="O<?= word_limiter($d , 10); ?>">
            <meta property="og:image" content="<?php echo base_url(); ?>uploads/<?= $sitev->website_defulatimg; ?>">
            <meta property="og:url" content="<?=  current_url()."?uuid=".$this->input->get("uuid"); ?>">
      
        

        <title><?php
            echo $sw["title"];
            if (isset($name) && $name != null) {
                echo " " . $name;
            }
            ?></title>
        
        
     
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="<?php echo base_url() ?>/css/style.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>/css/superfish.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>/css/event_template.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>/css/responsive.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>/css/modstyle.css" rel="stylesheet" />
        <link href="<?php echo base_url() ?>/css/ion.calendar.css" rel="stylesheet" />


        <link rel="stylesheet" href="<?php echo base_url() ?>/css/css.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>/css/css2.css">
 
        <link rel="stylesheet" href="<?php echo base_url() ?>/css/bootstrap.css">
        
        
        <link rel="stylesheet" href="<?php echo base_url("assets") ?>/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo base_url("assets") ?>/owl.theme.default.min.css">
 
        
        <link rel="stylesheet" href="<?php echo base_url() ?>/css/theme.css?h=<?= md5(date("d")); ?>">
        <?php if ($lid == 1) { ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>/css/custom.css">
        <?php }  else { ?>
                <link rel="stylesheet" href="<?php echo base_url() ?>/css/custom-ar.css">
          <?php  } ?>
        


                
                <style>
                    
                    
                    .transbox {
                    margin: 30px;
                    background-color: #000000;
                    
                    opacity: 0.6;
                    filter: alpha(opacity=60); /* For IE8 and earlier */
                  }

                  .transbox * {
                    margin: 2%;
                    font-weight: bold;
                    color: #ffffff;
                  }
                </style>
                
    </head>

    
    
    
    <body id="tm-container" class="tm-sidebar-a-left tm-sidebars-1 tm-isblog ">
        <div class="tm-header-bg"></div>

        <div class="tm-inner-container uk-container uk-container-center">


            <div class="uk-sticky-placeholder uk-hidden-small uk-hidden-touch">
                <div data-uk-smooth-scroll data-uk-sticky="{top:-500}"><a class="tm-totop-scroller uk-animation-slide-bottom" href="#" ></a></div>
            </div>

            <div id="tm-toolbar" class="tm-toolbar">
                <div class="uk-container uk-container-center uk-clearfix">

                    <div class="uk-float-left"><div class="uk-panel">

                            <?php foreach ($slinks as $key => $value) { ?>
                           
                            
                            
                             <a href="<?= $value->social_link; ?>" class="uk-icon-button <?= $value->social_imgepath; ?>" target="_blank"></a>
                            
                            
                            <?php            }  ?>
                            
                            <a href="tel:<?php echo $PHONE; ?>" class="uk-icon-button uk-icon-whatsapp" target="_blank" ></a>
                        
                        </div></div>

                    <div class="uk-float-right uk-hidden-small">

                        <form class="search-form" id="search-227" class="uk-search search-form" action="<?php echo base_url(); ?>Courses" method="GET" role="search">
                            <div class="form-group waves-effect">
                                <input name="sewo" type="text" <?php if($this->input->get("sewo") != NULL) {   ?> value="<?= $this->input->get("sewo") ?>"  <?php } ?> class="uk-search-field" placeholder="<?php if (isset($SEARCH)) {
                echo $SEARCH;
            } ?>">
                            </div>
                        </form>           


                    </div>

                    <div class="uk-float-right"><div class="uk-panel">
                            <ul class="uk-subnav uk-subnav-line">
                                <li></li>
                            </ul>
                        </div>
                        <div class="uk-panel">

                            <div class="uk-button-dropdown" data-uk-dropdown="">
                                <a class="uk-button-link uk-button" href="#" target="_self"><?php echo $sw["lang"]; ?> <i class="uk-icon-angle-down"></i></a>
                                <div class="uk-dropdown uk-dropdown-small uk-color" style="">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <?php if ($lang != null) {
                                            foreach ($lang as $l) { ?>
                                                <li> <a class="waves-effect" href="<?php echo base_url() . urlfix() ?>comman/setlang/<?php echo $l->lang_id . "/" . $this->uri->uri_string(); ?>" ><?php echo $l->lang_name; ?></a></li>    

    <?php }
} ?>
                                    </ul>
                                </div>
                            </div></div></div>

                </div>
            </div>

            <div class="tm-header-container" data-uk-sticky="{showup: true, animation: 'uk-animation-slide-top'}">
                <div class="tm-header-call">
                    <div class="tm-header uk-flex uk-flex-middle uk-flex-space-between">

                        <a class="tm-logo uk-hidden-small" href="<?php echo base_url(); ?>">

                            
                            
                            <img class="" style="max-width: 150px;" src="<?php echo base_url()."uploads/".$sitev->website_logo; ?>" alt="education" width="150" height="40"></a>

                        <a class="tm-logo-small uk-visible-small" href="<?php echo base_url(); ?>">

                            <img class=""  style="max-width: 150px;"  src="<?php echo base_url()."uploads/".$sitev->website_logo; ?>" alt="education" width="150" height="40"></a>


                        <div class="uk-flex uk-flex-middle uk-flex-space-between">

                            <div class="uk-hidden-small">
                                <nav class="tm-navbar uk-navbar">
                                    <ul class="uk-navbar-nav">
                                        <li class="uk-parent" data-uk-dropdown="{'preventflip':'y'}" aria-haspopup="true" aria-expanded="false">
                                            <a href="<?php echo base_url() . urlfix() ?>defaults"><?php if (isset($HOME)) {
    echo $HOME;
} ?></a>

                                        </li>
                                        <li class="uk-parent" data-uk-dropdown="{'preventflip':'y'}" aria-haspopup="true" aria-expanded="false">
                                            <a href="<?php echo base_url() . urlfix() ?>about"><?php if (isset($ABOUTUS)) {
    echo $ABOUTUS;
} ?></a>

                                        </li>
                                        <li class="uk-parent" data-uk-dropdown="{'preventflip':'y'}" aria-haspopup="true" aria-expanded="false">
                                            <a href="<?php echo base_url() . urlfix() ?>articles "><?php if (isset($articles)) {
    echo $articles;
} ?></a>


                                        </li>
                                        <li data-menu-fullwidth="1" class="uk-parent" data-uk-dropdown="{'preventflip':'n'}" aria-haspopup="true" aria-expanded="false"><a href="#">
                                            <?php if (isset($Category)) {
    echo $Category;
} ?> </a>
                                            <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-5">
                                                <div class="uk-grid uk-dropdown-grid">




<?php if ($category != FALSE) {
    $j = 0; ?>
    <?php foreach ($category as $key => $value) {
        $j = $j + 1; ?>

                                                            <div class="uk-width-1-3">
                                                                <ul class="uk-nav uk-nav-navbar">
                                                                    <li>
                                                                        <a  href="<?php echo base_url() . "Courses?ca=" . $value->category_crm_id; ?>"><?php
                                                                    if($lid == 1)
                                                                    {
                                                                    echo $value->category_name; 
                                                                    } else {
                                                                        echo $value->category_name_ar;
                                                                    }
                                                                    
                                                                    ?></a> </li>
                                                                </ul>
                                                            </div>

                                                                <?php }
                                                            } ?>





                                                </div>
                                            </div>
                                        </li>
                                        
 
                                        
                                        
                                        
                            <li data-menu-fullwidth="1" class="uk-parent" data-uk-dropdown="{'preventflip':'n'}" aria-haspopup="true" aria-expanded="false"><a href="#">
                                            <?php if (isset($CityLocation)) {
    echo $CityLocation;
} ?> </a>
                                            <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-5">
                                                <div class="uk-grid uk-dropdown-grid">




<?php if ($category != FALSE) {
    $j = 0; ?>
    <?php foreach ($citys as $key => $value) {
        $j = $j + 1; ?>

                                                            <div class="uk-width-1-3">
                                                                <ul class="uk-nav uk-nav-navbar">
                                                                    <li>
                                                                        <a  href="<?php echo base_url() . "Courses?c=" . $value->citys_name; ?>"><?php
                                                                    if($lid == 1)
                                                                    {
                                                                    echo $value->citys_name; 
                                                                    } else {
                                                                        echo $value->citys_name;
                                                                    }
                                                                    
                                                                    ?></a> </li>
                                                                </ul>
                                                            </div>

                                                                <?php }
                                                            } ?>





                                                </div>
                                            </div>
                                        </li>
                                        
                                        
                                        
                                        
                                   
                                                        <li><a href="<?= base_url() ?>/Courses"><?php echo $EVENT; ?></a></li>
                                                        
                                                        
                                        
                                                        
                                                        
                                                        
                                                              <li  class="uk-parent" data-uk-dropdown="{'preventflip':'n'}" aria-haspopup="false" aria-expanded="false"><a href="#"><?php if (isset($Pages)) {
                                                                echo $Pages;
                                                            } ?></a>
                                                                  
                                                                  
                                                                  
                                                                  <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-1 uk-dropdown-bottom" style=" margin-top: 15px;">
                                                <div class="uk-grid uk-dropdown-grid">




<?php if ($Pages != FALSE) {
    $j = 0; ?>
    <?php foreach ($expage as $key => $value) {
        $j = $j + 1; ?>

                                                            <div class="uk-width-1-1">
                                                                <ul class="uk-nav uk-nav-navbar">
                                                                    <li>
                                                                            <a href="<?php echo base_url() . "Pages/v/". urlencode($value->pages_title)."?pid=" . $value->pages_id; ?>"><?php echo $value->pages_title; ?></a>
 </li>
                                                                </ul>
                                                            </div>

                                                                <?php }
                                                            } ?>





                                                </div>
                                            </div>
                                                                  
                                                                  
                                                                  
                                                                  
                                         
                                        
                                        </li>
                                                        
                                                        
                                                        

                                    </ul>                        



                                </nav>



                            </div>


                            <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>

                            <div class="tm-call-action uk-hidden-small uk-flex uk-flex-middle">
                                <div class="">

                                    <h4><?php if (isset($Contactus)) { echo $Contactus; } ?></h4>
                                    <a href="tel:<?php echo $PHONE; ?>"><?php echo $PHONE; ?></a>
                                </div>        </div>

                        </div>
                    </div>
                </div>

                <div class="tm-minibar">
                    <div class="">

                        <p>
                            
                        </p></div>  </div>
            </div>    
        </div>



