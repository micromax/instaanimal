 <div class="uk-width-1-1 uk-width-medium-1-6">
                        <div class="uk-panel uk-panel-box">
                            <h3 class="uk-h4 uk-module-title uk-margin-bottom "><?= $value->footersection_text; ?></h3>

                            
                            <ul class="uk-list ">
                                
                                
                                <li><a href="#">Community</a></li>
                                <li><a href="#">Administration</a></li>
                                <li><a href="#">News Network</a></li>
                                <li><a href="#">History &amp; Mission</a></li>
                                <li><a href="#">Around the World</a></li>
                                <li><a href="#">Visitor Information</a></li>
                                <li><a href="#">Facts &amp; Statistics</a></li>
                                <li><a href="#">Social Media</a></li>
                                <li><a href="#">Careers</a></li>
                            </ul></div>
                    </div>