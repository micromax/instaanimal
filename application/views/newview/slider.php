
<section id="tm-fullscreen" class="tm-fullscreen uk-width-1-1 " data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
            <div class="">

                <div class="tm-slideshow-gravity uk-slidenav-position tm-fullscreen-slideshow" data-uk-slideshow="{autoplay:true, animation: 'puzzle', pauseOnHover: true, duration: 750, autoplayInterval: 10000, kenburns: false, kenburnsanimations: 'uk-animation-top-center', slices: 20}">
                    <ul class="uk-slideshow tm-slideshow-fullscreen uk-overlay-active">

                      
                        <?php foreach ($sbx as $key => $value) { ?>
                            
                             <li>
                                 <img src="<?= base_url(); ?>/uploads/<?php echo $value->searchbox_bg ; ?>" width="1900" height="600" alt="image">
                            <div class="uk-overlay-panel uk-flex uk-flex-middle uk-overlay-slide-left">
                                <div class="transbox">
                                    <h4><?php echo $value->searchbox_title ; ?></h4>
                                    <div class=""><?php echo $value->searchbox_p1_t ; ?></div>
                                   
                                </div>
                            </div>
                        </li>
                         
                            
                            
                            
                        <?php } ?>
                        

                      
                        

                    </ul>
                    <div class="uk-margin">
                        <ul class="uk-dotnav uk-flex-right uk-hidden-touch">
                              <?php
                              $j = 0;
                              foreach ($sbx as $key => $value) { $j++?>
                            <li data-uk-slideshow-item="<?= $j ?>"><a href="#"></a></li>
                              <?php } ?>
                            
                           
                        </ul></div>

                    <div class="tm-slidenav uk-position-absolute">
                        <div class="tm-slidenav-container uk-flex">
                            <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous uk-hidden-touch" data-uk-slideshow-item="previous"></a>
                            <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next uk-hidden-touch" data-uk-slideshow-item="next"></a>
                        </div>
                    </div>
                </div></div> 
        </section>