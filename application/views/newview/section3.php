
<?php foreach ($pdata as $key => $value) {
    
        }  ?>
<section id="tm-bottom-c" class="tm-bottom-c uk-grid uk-grid-match" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                    <div class="uk-width-1-1"><div class="uk-panel uk-contrast tm-overlay-secondary">
                            <div class="tm-background-cover uk-cover-background" style="background-image: url(mages/background/bg-image-5.jpg)" data-uk-parallax="{bg: '-250'}">
                                <div class="uk-position-relative uk-container tm-inner-container">

                                    <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-4" data-uk-grid-margin> 

                                        <div class="uk-panel uk-panel-space">
                                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:50}">
                                                <h3 class="uk-margin-top tm-text-large"><?= $value->lp_t2 ?>  </h3>
                                            </div>
                                        </div>

                                        <div class="uk-panel uk-panel-space">
                                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:150}">
                                                <div class="tm-block-icon-container">
                                                    <div class="tm-block-icon uk-icon-grav-vector tm-block-icon-large"></div>
                                                    <h2 class="tm-thin-font uk-margin-small-top"><?= $value->lp_st2 ?></h2>
                                                    <div class="tm-block-content">    <?= $value->lp_p3 ?></div>
                                                </div>
                                                
                                            </div>
                                        </div>

                                        <div class="uk-panel uk-panel-space">
                                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:250}">
                                                <div class="tm-block-icon-container">
                                                    <div class="tm-block-icon uk-icon-grav-display2 tm-block-icon-large"></div>
                                                    <h2 class="tm-thin-font uk-margin-small-top"><?= $value->lp_st3 ?></h2>
                                                    <div class="tm-block-content">    <?= $value->lp_p4 ?></div>
                                                </div>
                                              
                                            </div>
                                        </div>

                                        <div class="uk-panel uk-panel-space">
                                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:350}">
                                                <div class="tm-block-icon-container">
                                                    <div class="tm-block-icon uk-icon-grav-medal tm-block-icon-large"></div>
                                                    <h2 class="tm-thin-font uk-margin-small-top"><?= $value->lp_st4 ?></h2>
                                                    <div class="tm-block-content">
                                                            <?= $value->lp_p5 ?>        
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>

                                    </div>	</div>
                            </div>
                        </div></div>



                </section>
