<?php foreach ($siteconfig as  $sitev) {} ?>
                <section id="tm-bottom-e" class="tm-bottom-e uk-grid uk-grid-match" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                    <div class="uk-width-1-1"><div class="uk-panel uk-panel-box uk-panel-box-primary">

                            <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-medium-1-2 uk-grid-width-large-1-4" data-uk-grid-margin>

                                <div class="uk-flex uk-flex-middle">
                                    
                                    <img class="" src="<?= base_url()."/uploads/".$sitev->website_icon; ?>" alt="school and education" width="140" height="40">
                                </div>

                                 <?php
                                 if($smallfooter != null && sizeof($smallfooter) != 0){
                                 foreach ($smallfooter as $sfoot) { ?>
                                     
                                <div class="tm-block-icon-link uk-flex uk-flex-middle uk-flex-left">
                                    <i class=" tm-block-icon-large uk-margin-right"> <img  src="<?= base_url()."/uploads/".$sfoot->smallfooter_icon; ?>" width="20" height="20"> </i>
                                    <h3 class="tm-thin-font uk-margin-remove"><a class="uk-link-reset" href="<?= $sfoot->smallfooter_url; ?>"> <?= $sfoot->smallfooter_text; ?> </a></h3>
                                </div>     
                                     
                                 <?php  } } ?>   
                                
                                
                            </div></div></div>



                </section>
