
                        <main id="tm-content" class="tm-content">
                            <div id="system-message-container">
                            </div>
                                   
                        
                            
                            <?php if($arts != false) {
    $i = 0;
    foreach($arts as $art){
        $i++;
    ?>
                        
                        
                            
                            <article class="uk-article tm-article" data-permalink="<?php echo base_url().urlfix(); ?>articles/viewart/<?php echo $art->articls_hashkey; ?>">

                                        <div class="tm-article-wrapper">

                                            <div class="tm-article-featured-image">
                                                <a class="" href="<?php echo base_url().urlfix(); ?>articles/viewart/<?php echo $art->articls_hashkey; ?>" title="">
                                                    <img class="uk-overlay-scale"  src="<?php echo base_url(); ?>/uploads/<?php echo $art->articls_image; ?>" alt="blog" style="width:100% !important;">
                                                    <div class="uk-overlay-panel uk-overlay-background uk-overlay-icon uk-overlay-fade"></div>
                                                </a>
                                            </div>


                                            <h2 class="uk-article-title">
                                                <a href="<?php echo base_url().urlfix(); ?>articles/viewart/<?php echo $art->articls_hashkey; ?>" title="<?php echo $art->articls_title; ?>"><?php echo $art->articls_title; ?></a>
                                            </h2>



                                            <p class="uk-article-meta">

                                             


                                            <div class="tm-article-content uk-margin-top-remove">

                                                <div class="tm-article">
                                                    <p>
                                                    <?php echo word_limiter($art->articls_description, 30);    ?> 
                                                    </p>
                                                </div>

                                                <p><a class="uk-button uk-button-primary uk-button-large uk-margin-top" href="<?php echo base_url().urlfix(); ?>articles/viewart/<?php echo $art->articls_hashkey; ?>"><?= $sw["readmore"];?> </a></p>
                                            </div>

                                        </div>


                                        

                    

                                    </article>
                  
                            
                            <?php } } ?>
                            
                        
                        </main>

