     <?php $this->load->view("newview/searchsection")  ?>

    <div id="tm-middle" class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>

                    <div class="tm-main uk-width-medium-3-4 uk-push-1-4">

                       

                       
                        <?php $this->load->view("newview/mainsec")  ?>






                        
                        <section id="tm-main-bottom" class="tm-main-bottom uk-grid uk-grid-match" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                            <div class="uk-width-1-1"><div class="uk-panel uk-panel-box uk-padding-remove">

                                    <div class="tm-event-tabs uk-grid uk-grid-collapse uk-grid-match" data-uk-grid-match="{target:'> div > ul'}">

                                        <div class="uk-width-medium-2-10">
                                            <ul class="uk-tab uk-tab-left" data-uk-tab="{connect:'#event_tabs', animation: 'fade'}">


                                                
                                                <?php
                                                
                                                foreach ($city_ps as $key => $va) { 
                                                        //foreach ($va["cityinfo"] as $value ) {
                                                            
                                                    ?>

 
                                                <li>
                                                    <a href=#">
                                                        <span class="uk-flex uk-flex-column uk-text-center">
                                                            <span class="tm-tab-day"><?= $va["cityinfo"]->citys_name; ?></span>
                                                           
                                                        </span>
                                                    </a>
                                                </li>

                                                <?php     
                                                } 
                                                ?>


                                            
                                 
                                             
                                                

                                            </ul>
                                        </div>

                                        <div class="uk-width-medium-8-10">
                                            <ul id="event_tabs" data-uk-check-display class="uk-switcher tm-event-tab-content">
                                                
                                                <?php foreach ($city_ps as $key => $value) { ?>
                                                <li>
                                                      
                                                    <div class="uk-grid uk-grid-collapse uk-grid-match" data-uk-grid-match="{target:'> div > a > figure'}">
                                                        <div class="uk-width-large-3-5">
                                                            <a>
                                                                <figure class="uk-width-1-1 uk-overlay uk-overlay-hover">
                                                                    <img class="uk-width-1-1 uk-overlay-scale" src="<?= base_url("uploads/".$value["cityinfo"]->img_path); ?>" width="800" height="768" alt="Summer Camp">
                                                                    <figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-ignore"><?= $value["cityinfo"]->citys_name; ?></figcaption>
                                                                </figure>
                                                            </a>
                                                        </div>

                                                        <div class="uk-width-large-2-5 uk-panel uk-panel-space uk-overflow-hidden tm-slider-panel">
                                                            <h2 class="tm-slider-title uk-margin-top-remove"></h2>
                                                            <div class="uk-margin">
                                                                <p class="uk-article-lead">
                                                                    
                                                                    <a href="<?= base_url("Courses/v/").  urlencode($value["programfor"][0]->programes_name)."?uuid=".$value["programfor"][0]->programes_id;  ?>" >
                                                                    <?php //var_dump($value["programfor"][0]->programes_name); ?>
                                                                    <?= $value["programfor"][0]->programes_name; ?>
                                                                    </a>
                                                                    </p>

                                                                    <p><?= word_limiter($value["programfor"][0]->programes_desc  , 30); ?></p>
                                                            
                                                                    
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php     
                                                    } 
                                                ?>
                                             

                                            </ul>
                                        </div>

                                    </div></div></div>


                        </section>

                    </div>

                    <?php $this->load->view("newview/aside"); ?>

                </div>
