                    <aside class="tm-sidebar-a uk-width-medium-1-4 uk-pull-3-4">

                        <?php if(isset($ww) ){ 
                            
                                foreach($ww as $wws) {}
                            ?>
                        <div class="uk-panel uk-panel-box uk-padding-remove">

                            <div class="uk-panel uk-panel-box tm-panel-teaser">
                            <div class="uk-panel-teaser">
                                <img src="<?= base_url("uploads/".$wws->ww_img); ?>" alt="Head's welcome" width="450" height="300">
                            </div>
                            <div class="tm-teaser-content">
                            <h3 class="uk-module-title"><?= $wws->ww_title; ?></h3>

                            <?= word_limiter($wws->ww_text , 40); ?>
                            

                            <br>
                            
                            </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="uk-panel uk-panel-box uk-padding-remove">





                            <div class="uk-width-1-1 uk-width-medium-4-4">
                                <div class="uk-panel uk-panel-box uk-panel-box-secondary">

                                    <br>
                                    <div class="tm-counter uk-flex" data-uk-scrollspy="{cls:'uk-animation-fade', delay:100}">
                                        <div class="tm-counter-icon uk-flex-item-none"><i class="uk-icon-grav-users"></i></div>
                                        <div class="tm-counter-content uk-flex-item-1">
                                            <h2 class="tm-counter-number" id="counter1" data-end="<?= $programes_c ;?>" data-duration="1000"><?= $programes_c ;?></h2>
                                            <h3 class="tm-counter-title"><?php echo $EVENT; ?></h3>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="tm-counter uk-flex" data-uk-scrollspy="{cls:'uk-animation-fade', delay:100}">
                                        <div class="tm-counter-icon uk-flex-item-none"><i class="uk-icon-grav-license"></i></div>
                                        <div class="tm-counter-content uk-flex-item-1">
                                            <h2 class="tm-counter-number" id="counter2" data-end="<?= $category_c ;?>" data-duration="1200"> <?= $category_c ;?> </h2>
                                            <h3 class="tm-counter-title"><?php echo $Category; ?></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>


                        <div class="uk-panel uk-panel-box uk-padding-remove">

                            <div class="uk-width-1-1 uk-width-medium-4-4">
                                <div class="uk-panel uk-panel-box uk-panel-box-primary">
                                    <h3 class="uk-panel-title "> <?= $Documents ;?></h3>

                                    <ul class="uk-list list-icons">
                                        <?php if($docs != NULL) { 
                                                            foreach ($docs as $dk => $dv) {
                                            ?>
                                        <li><i class="uk-icon-file-pdf-o"></i><a href="<?= $dv->decoments_url ?>"> <?= $dv->decoments_name ?></a></li>
                                        <?php }  } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="uk-panel uk-panel-box uk-padding-remove">
                            <div style="margin:0px;padding:0px;border-width:0px;"><span id='testspan437' style='display:none'></span>

                                <div id="calendar-1">

                                </div>



                            </div>

                        </div>
                    </aside>
