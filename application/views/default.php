<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/960/960.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>css/960/text.css"  />

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/ui-lightness/jquery-ui-1.8.5.custom.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css3/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css3/bootstrap-theme.min.css" />
        
        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.7.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/main.css" />
        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.7.2.min.js"></script>
        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-1.8.4.custom.min.js"></script>
        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        
      			
        <title><?php echo $sw["title"]; ?></title>
    </head>
    <body>
        
          <!-- head start -->
        <div class="container-full">
            <div class="container ">
                <div class="row " id="heads">
                    <div class="col-lg-4"> <img src="<?php echo base_url(); ?>images/zxfiles/logo.png" /> </div>
                    <div class="col-lg-8" align="right">
                        <div class="col-lg-12" >
                            <?php foreach($lang as $l){ ?>
                            <a href="<?php echo base_url().urlfix() ?>comman/setlang/<?php echo $l->lang_id."/".$this->uri->uri_string(); ?>" type="button" class="btn btn-primary"><?php echo $l->lang_name; ?></a>
                            
                            <?php } ?>
                        </div>
                        <div class="col-lg-12 padder">
                            <form class="form-inline" role="form">
                                <input  type="text" class="form-control search_f" id="search" placeholder="<?php echo $sw["search"]; ?>">
                                <button  type="button" class="btn btn-info"><?php echo $sw["search"]; ?></button >
                            </form>
                            
                        </div>
                    </div>
                </div>
               
            </div>
            <!-- head end -->
          <!-- nav barstart -->
              <div class="row " id="navs">
                    <div class="navcontiner col-lg-12">
                            <div class="container ">
                               <div class="row " id="slider">
                                    <div class="col-lg-12">
                                        <nav class="col-lg-12">
                                                <ul>
                                                    <li><a href="<?php echo base_url().urlfix() ?>defaults"><?php echo $mlist["defaults"]; ?></a></li>
                                                        <li><a href="#"><?php echo $mlist["products"]; ?></a>
                                                                <?php echo $catlist ; ?>
                                                        </li>
                                                        <li><a href="#"><?php echo $mlist["brands"]; ?></a>
                                                               <?php echo $brandist; ?>
                                                        </li>
                                                        <li><a href="<?php echo base_url().urlfix() ?>about"><?php echo $mlist["about"]; ?></a></li>
                                                        <li><a href="<?php echo base_url().urlfix() ?>articles"><?php echo $mlist["articles"]; ?></a></li>
                                                        <li><a href="<?php echo base_url().urlfix() ?>contact"><?php echo $mlist["contact"]; ?></a></li>
                                                        <li><a href="<?php echo base_url().urlfix() ?>jobs"><?php echo $mlist["jobs"]; ?></a></li>
                                                </ul>
                                        </nav>

                                    </div>
                               </div>
                            </div>
                    </div>
               </div>
  <!-- nav END -->
  <!-- slider start -->
            <div class="row">

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
        <img src="<?php echo base_url(); ?>images/zxfiles/slide1.jpg" alt="...">
      <div class="carousel-caption">
          <h3> title </h3>
          <p>
              test<br/>
                 <a type="button" class="btn btn-warning">Order Now</a>
          </p>
      </div>
    </div>
    <div class="item">
      <img src="<?php echo base_url(); ?>images/zxfiles/slide1.jpg" alt="...">
      <div class="carousel-caption">
          <h3> title 2</h3>
          <p>test 2</p>
      </div>
    </div>
    
    <div class="item">
      <img src="<?php echo base_url(); ?>images/zxfiles/slide1.jpg" alt="...">
      <div class="carousel-caption" style="background-image: url('images/zxfiles/divbg.png')">
          <h3 > title 3</h3>
          <p style="color: #e0e0e0;" >test </p>
      </div>
    </div>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>

            </div>
<!-- slider End -->
<!-- news letter strip -->

<div class="row newsletterbg">

    <div class="container">
        <div class="row newslitter">
            <div class="col-lg-4">
                <h1>
                    <?php echo $sw["JoinourNewsletter"]; ?>
                    
                </h1>
            </div>
            
            <div class="col-lg-8">
                
                
                <form class="form-inline" role="form">
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail2"><?php echo $sw["firstname"]; ?></label>
    <input type="text" class="form-control in" id="exampleInputEmail2" placeholder="<?php echo $sw["firstname"]; ?>">
  </div>
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword2"><?php echo $sw["lastname"]; ?></label>
    <input type="text" class="form-control in" id="exampleInputPassword2" placeholder="<?php echo $sw["lastname"]; ?>">
  </div>
  
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword2"><?php echo $sw["email"]; ?></label>
    <input type="email" class="form-control in" id="exampleInputPassword2" placeholder="<?php echo $sw["email"]; ?>">
  </div>

  <button type="submit" class="btn btn-primary"><?php echo $sw["subscribe"]; ?></button>
                </form>


            </div>
            
        </div>

    </div>


</div>
<!-- news letter strip END-->

<!-- feed foot strip start-->

<div class="row feedfoot">
     <div class="container">
        <div class="row padder">
            <div class="col-lg-4">
                <h4>

                    <?php echo $sw["TWEETER"]; ?>
                </h4>
                <hr/>
                <div class="row dtedbtm">
                    <img class="col-lg-2" src="<?php echo base_url() ?>images/zxfiles/ct.png" id="ctwit" />
                    <h5 class="col-lg-5"> @ company name </h5>
                    <h5 class="col-lg-4"> dates </h5>
                    <div class="row col-lg-9">
                        <p>
                        product slide tesxt body ef, Lorem Ipsum is not simply
                        random text. It has roots in a piece of classical Latin
                        literature from 45 BC, making it over 2000 years old.
                    </p>
                    </div>
                      
                </div>

                <div class="row dtedbtm">
                    <img class="col-lg-2" src="<?php echo base_url() ?>images/zxfiles/ct.png" id="ctwit" />
                    <h5 class="col-lg-5"> @ company name </h5>
                    <h5 class="col-lg-4"> dates </h5>
                    <div class="row col-lg-9">
                        <p>
                        product slide tesxt body ef, Lorem Ipsum is not simply
                        random text. It has roots in a piece of classical Latin
                        literature from 45 BC, making it over 2000 years old.
                    </p>
                    </div>

                </div>


            </div>

            <div class="col-lg-4">
                <h4>

                    <?php echo $sw["News"]; ?>
                </h4>
                <hr/>


                 <div class="row dtedbtm">
                    <img class="col-lg-2" src="<?php echo base_url() ?>images/zxfiles/ct.png" id="ctwit" />
                    <h5 class="col-lg-5"> Title of news </h5>
                    <h5 class="col-lg-4"> dates </h5>
                    <div class="row col-lg-9">
                        <p>
                        product slide tesxt body ef, Lorem Ipsum is not simply
                        random text. Itng it over 2000 years old.
                    </p>
                    </div>
                    <div class="row col-lg-12" align="right">
                        <a href="#"> <img src="<?php echo base_url() ?>images/zxfiles/s_1.png"/> <?php echo $sw["readmore"]; ?> </a>
                    </div>
                </div>
                 <div class="row dtedbtm">
                    <img class="col-lg-2" src="<?php echo base_url() ?>images/zxfiles/ct.png" id="ctwit" />
                    <h5 class="col-lg-5"> Title of news </h5>
                    <h5 class="col-lg-4"> dates </h5>
                    <div class="row col-lg-9">
                        <p>
                        product slide tesxt body ef, Lorem Ipsum is not simply
                        random text. Itng it over 2000 years old.
                    </p>
                    </div>
                    <div class="row col-lg-12" align="right">
                        <a href="#"> <img src="<?php echo base_url() ?>images/zxfiles/s_1.png"/> 
                          <?php echo $sw["readmore"]; ?>
                        </a>
                    </div>
                </div>


            </div>

            <div class="col-lg-4">
                <h4>
                    
                    <?php echo $sw["contact"]; ?>
                </h4>
                <hr/>

                 <div class="row padders">
                    <input  type="text" class="form-control search_f" id="search" placeholder="<?php echo $sw["firstname"]; ?>" />
                </div>
                 <div class="row padders">
                    <input  type="text" class="form-control search_f" id="search" placeholder="<?php echo $sw["email"]; ?>" />
                </div>

                 <div class="row padders">
                     <textarea  type="text" class="form-control search_f" id="search" placeholder="<?php echo $sw["message"]; ?>"></textarea>
                </div>

                <div class="row padders" align="right">
                     <button type="button" class="btn btn-primary"> <?php echo $sw["sendmessage"]; ?> <img src="<?php echo base_url() ?>images/zxfiles/s_2.png"/> </button>
                </div>


            </div>

        </div>

    </div>
</div>
<!-- feed foot strip END-->
<!-- Socail links foot strip start-->
<div class="row socialfoot">
    <div class="container">
        <div class="row padder">
            <div class="col-lg-8">
                <p>
                    ef, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur,
                </p>
            </div>

            <div class="col-lg-4">
                <img src="<?php echo base_url() ?>images/zxfiles/s_v.png"/>
                <img src="<?php echo base_url() ?>images/zxfiles/s_t.png"/>
                <img src="<?php echo base_url() ?>images/zxfiles/s_g.png"/>
                <img src="<?php echo base_url() ?>images/zxfiles/s_i.png"/>
            </div>
            

        </div>

    </div>
</div>
<!-- Socail links foot strip END-->
<div class="row" >
    <div class="container" align="center">
        <a href="#">links </a>
        <a href="#">links </a>
        <a href="#">links </a>
        <a href="#">links </a>
        <a href="#">links </a>
        <a href="#">links </a>
        <a href="#">links </a>
    </div>

    <div class="container" align="center">
        developed by <a href="http://orangeteam-eg.com" target="_new">orangeteam-eg.com</a>
    </div>

</div>

            
        </div>
    </body>
</html>
