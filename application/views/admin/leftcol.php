<?php $cn = $this->router->fetch_class();
$cn = strtolower($cn);
?> 
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url("v2"); ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $username; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php if( $cn == "dashboard"){ ?> active <?php } ?> treeview">
            <a href="<?= base_url("admin"); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="<?php if(strtolower($cn) == "dashboard"){ ?> active <?php } ?>"><a href="<?= base_url("admin"); ?>"><i class="fa fa-circle-o"></i> Dashboard </a></li>
            
          </ul>
        </li>
       
        
         <li class="<?php if($cn == "statics" || $cn == "staticsar" ||  $cn == "qcmsslide" ){ ?> active <?php } ?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Landing page </span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">2</span>
            </span>
          </a>
          <ul class="treeview-menu">
      
            <li class="<?php if($cn == "qcmsslide" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Qcmsslide"); ?>"><i class="fa fa-circle-o"></i> New Slide [slider widgets]</a></li>
            <li class="<?php if($cn == "qcmsslide" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Qcmsslide/manage"); ?>"><i class="fa fa-circle-o"></i> Manage Slide [slider widgets]</a></li>
           
            
          </ul>
        </li>
        
        
        
        <li class="<?php if($cn == "cat"  ){ ?> active <?php } ?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Category's </span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">2</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($cn == "cat" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Cat"); ?>"><i class="fa fa-circle-o"></i> New Category</a></li>
            <li class="<?php if($cn == "cat" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Cat/manage"); ?>"><i class="fa fa-circle-o"></i> Manage Category</a></li>
            
          </ul>
        </li>
        
         <li class="<?php if($cn == "product"  ){ ?> active <?php } ?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Items </span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">2</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li  class="<?php if($cn == "product" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Product"); ?>"><i class="fa fa-circle-o"></i> New Product</a></li>
            <li  class="<?php if($cn == "product" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Product/manage"); ?>"><i class="fa fa-circle-o"></i> Manage Product</a></li>
            
          </ul>
        </li>
        
        
        <li class="<?php if($cn == "pages"  ){ ?> active <?php } ?> treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Pages </span>
                <span class="pull-right-container">
                  <span class="label label-primary pull-right">2</span>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?php if($cn == "pages" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Pages"); ?>"><i class="fa fa-circle-o"></i> New Page</a></li>
                <li class="<?php if($cn == "pages" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Pages/manage"); ?>"><i class="fa fa-circle-o"></i> Manage Pages</a></li>

              </ul>
            </li>
        
        
        
            <li class="<?php if($cn == "aboutusen"  ){ ?> active <?php } ?> treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>About </span>
                <span class="pull-right-container">
                  <span class="label label-primary pull-right">2</span>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?php if($cn == "aboutusen" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Aboutusen"); ?>"><i class="fa fa-circle-o"></i> New About</a></li>
                <li class="<?php if($cn == "aboutusen" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Aboutusen/manage"); ?>"><i class="fa fa-circle-o"></i> Manage About</a></li>

              </ul>
            </li>
        
        <li class="<?php if($cn == "contact"  ){ ?> active <?php } ?>  treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Contact Us </span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">2</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($cn == "contact" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Contact"); ?>"><i class="fa fa-circle-o"></i> New Contacts</a></li>
            <li class="<?php if($cn == "contact" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Contact/manage"); ?>"><i class="fa fa-circle-o"></i> Manage Contact us</a></li>
            
          </ul>
        </li>
        
       
        
         <li class="<?php if($cn == "articls"  ){ ?> active <?php } ?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Article & Blog </span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">2</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($cn == "articls" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Articls"); ?>"><i class="fa fa-circle-o"></i> New Article</a></li>
            <li class="<?php if($cn == "articls" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Articls/manage"); ?>"><i class="fa fa-circle-o"></i> Manage Article</a></li>
            
          </ul>
        </li>
        
        
        
        
        
        
        
        
        
        

        
        
        
        
 
        
 
        
  
        
        
        
        
        <li class=" <?php if($cn == "language" || $cn == "sociallinks" ||  $cn == "uploader" || $cn == "supscription"  
               
                || $cn == "onlineorders" 
                || $cn == "mlist"
                || $cn == "city"
                || $cn == "kv"
                 || $cn == "website"
                 || $cn == "decoments"
                ){ ?> active <?php } ?> treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Utility's </span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">16</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($cn == "language" ){ ?> active <?php } ?>" ><a   href="<?= base_url("admin/Language"); ?>"><i class="fa fa-circle-o"></i> New Language</a></li>
            <li class="<?php if($cn == "language" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Language/manage"); ?>"><i class="fa fa-circle-o"></i> Manage Language</a></li>
            
            
            <li class="<?php if($cn == "sociallinks" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Sociallinks"); ?>"><i class="fa fa-circle-o"></i> New Social Links</a></li>
            <li class="<?php if($cn == "sociallinks" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Sociallinks/manage"); ?>"><i class="fa fa-circle-o"></i> Manage Social Links</a></li>
            
            <li class="<?php if($cn == "uploader" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Uploader"); ?>"><i class="fa fa-circle-o"></i> New File</a></li>
            <li class="<?php if($cn == "uploader" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Uploader/manage"); ?>"><i class="fa fa-circle-o"></i> File Manager</a></li>
            
            
            
            <li class="<?php if($cn == "supscription" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Supscription"); ?>"><i class="fa fa-circle-o"></i> Subscription</a></li>
            
            <li class="<?php if($cn == "onlineorders" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Onlineorders"); ?>"><i class="fa fa-circle-o"></i> Online Orders</a></li>
            
            <li class="<?php if($cn == "mlist" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Mlist"); ?>"><i class="fa fa-circle-o"></i> new Main item </a></li>
            <li class="<?php if($cn == "mlist" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Mlist/manage"); ?>"><i class="fa fa-circle-o"></i> Manage Main item </a></li>
            
            <li class="<?php if($cn == "city" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/City"); ?>"><i class="fa fa-circle-o"></i> New City</a></li>
            <li class="<?php if($cn == "city" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/City/manage"); ?>"><i class="fa fa-circle-o"></i> manage City</a></li>
            
            
            
            <li class="<?php if($cn == "kv" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Kv"); ?>"><i class="fa fa-circle-o"></i> New Key Settings</a></li>
            <li class="<?php if($cn == "kv" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Kv/manage"); ?>"><i class="fa fa-circle-o"></i> manage Key Settings</a></li>
            
            
            <li class="<?php if($cn == "website" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Website"); ?>"><i class="fa fa-circle-o"></i> New Website Settings</a></li>
            <li class="<?php if($cn == "website" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/Website/manage"); ?>"><i class="fa fa-circle-o"></i> manage Websterite Settings</a></li>
            
            
            <li class="<?php if($cn == "decoments" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/decoments"); ?>"><i class="fa fa-circle-o"></i> New document </a></li>
            <li class="<?php if($cn == "decoments" ){ ?> active <?php } ?>" ><a href="<?= base_url("admin/decoments/manage"); ?>"><i class="fa fa-circle-o"></i> manage document Settings</a></li>
            
            
            
          </ul>
        </li>
        
        
        
        
        
        
       
  
   
      </ul>
    </section>
   
  </aside>