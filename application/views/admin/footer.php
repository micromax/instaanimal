<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2015-2019 <a target="new" href="http://clouds-gen.com">clouds-gen @ sadeem-egypt.com</a>.</strong> All rights
    reserved.
  </footer>