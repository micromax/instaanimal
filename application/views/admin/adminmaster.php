<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cloud-Gen | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/dist/css/skins/_all-skins.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?= base_url("v2"); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  
  <!-- jQuery 3 -->
<script src="<?= base_url("v2"); ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url("v2"); ?>/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="<?= base_url("v2"); ?>/bower_components/ckeditor/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url("v2"); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<link rel="stylesheet" href="<?= base_url("js/wb"); ?>/css/grapes.min.css">
<link href="<?= base_url("js/wb"); ?>/grapesjs-preset-webpage.min.css" rel="stylesheet"/>

<script src="<?= base_url("js/wb"); ?>/grapes.js"></script>
<script src="<?= base_url("js/wb"); ?>/grapesjs-preset-webpage.min.js"></script>

<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url("v2"); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php $this->load->view("admin/head"); ?>
  <!-- Left side column. contains the logo and sidebar -->
 
<?php $this->load->view("admin/leftcol"); ?>
  <!-- Content Wrapper. Contains page content -->
  
  <?php 
  
  if(isset($cont) && $cont != NULL)
  {
      $this->load->view("admin/".$cont);
  } else {
      $this->load->view("admin/cont");
  }
  ?>
  
  <?php  ?>
  
  
  <!-- /.content-wrapper -->
  

  <?php $this->load->view("admin/footer"); ?>
  
  
  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->



<!-- Morris.js charts -->
<script src="<?= base_url("v2"); ?>/bower_components/raphael/raphael.min.js"></script>
<script src="<?= base_url("v2"); ?>/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url("v2"); ?>/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= base_url("v2"); ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url("v2"); ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url("v2"); ?>/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?= base_url("v2"); ?>/bower_components/moment/min/moment.min.js"></script>
<script src="<?= base_url("v2"); ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?= base_url("v2"); ?>/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url("v2"); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>



<!-- Slimscroll -->
<script src="<?= base_url("v2"); ?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>




<!-- FastClick -->
<script src="<?= base_url("v2"); ?>/bower_components/fastclick/lib/fastclick.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url("v2"); ?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url("v2"); ?>/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<script src="<?= base_url("v2"); ?>/dist/js/adminlte.min.js"></script>
<script src="<?= base_url("v2"); ?>/dist/js/html2canvas.min.js"></script>


<?php if(isset($cont) == FALSE && $cont != "ui/grid") {?>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url("v2"); ?>/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url("v2"); ?>/dist/js/demo.js"></script>

<?php } 

if(isset($cont) == FALSE && $cont != "ui/grid") {?>
<script >

$(function(){
      var visitorsData = {
          
            <?php if($graph != FALSE){ 
     foreach ($country as $key => $value) {
          echo "'".$value->country."'".":".$value->vists.",";
     ?>
    
      <?php } } ?> 
  };
  // World map by jvectormap
  $('#world-map').vectorMap({
    map              : 'world_mill_en',
    backgroundColor  : 'transparent',
    regionStyle      : {
      initial: {
        fill            : '#e4e4e4',
        'fill-opacity'  : 1,
        stroke          : 'none',
        'stroke-width'  : 0,
        'stroke-opacity': 1
      }
    },
    series           : {
      regions: [
        {
          values           : visitorsData,
          scale            : ['#92c1dc', '#ebf4f9'],
          normalizeFunction: 'polynomial'
        }
      ]
    },
    onRegionLabelShow: function (e, el, code) {
      if (typeof visitorsData[code] != 'undefined')
        el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
    }
  });
  
  
  
  
  
  
});

</script>
<?php } ?>

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    
      $('#datatable').DataTable();
      
     <?php if( $cont == "ui/form" ||  $cont == "ui/paragraph"  )  {?>
       $('textarea').wysihtml5();
       //CKEDITOR.replace('textarea');
     <?php } ?>
  
  
     
     
     
     
    <?php  if( isset($graph) ==TRUE && $graph != null && $agent != NULL) { ?>
     
     /* Morris.js Charts */
  // Sales chart
  var area = new Morris.Area({
    element   : 'revenue-chart',
    resize    : true,
    data      : [
      <?php foreach ($graph as $kk => $vv) { ?>
      { d: '<?= $kk ?>', item1: <?= $vv; ?>  },             
      <?php }?>            
      
      
    ],
    xkey      : 'd',
    ykeys     : ['item1'],
    labels    : [' Visits'],
    lineColors: ['#a0d0e0'],
    hideHover : 'auto'
  });


  // Donut Chart
  var donut = new Morris.Donut({
    element  : 'sales-chart',
    resize   : true,
    colors   : ['#3c8dbc', '#f56954', '#00a65a'],
    data     : [
      <?php foreach ($agent as $ks => $vs) { ?>
      { label: '<?= $vs->agents ?>', value: <?= $vs->vists ?> },    
      <?php } ?>    
      
    ],
    hideHover: 'auto'
  });

  // Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
    donut.redraw();
    line.redraw();
  });
  
  
  
    <?php } ?>
  
  
  
  
  })
  
  
  
</script>
</body>
</html>
