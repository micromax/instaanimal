<style>
 .uk-panel, .uk-panel {
    text-decoration: none;
}
.uk-grid > * > :last-child, .tt_event_column_left > :last-child, .tt_event_column_right > :last-child, .tt_event_page_left > :last-child, .tt_event_page_right > :last-child {
    margin-bottom: 0;
}
.uk-panel-box-primary {
    color: #ffffff;
    background-color: #ec8a3b;
    background: linear-gradient(180deg, #f76211 0%, #ec8a3b 100%);
}
.uk-panel-box {
    padding: 30px;
    background: #ffffff;
    color: #6b6b6b;
    box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.05);
    border-radius: 4px;
}
.uk-panel {
    display: block;
    position: relative;
}
.uk-grid-match > * > * {
    -ms-flex: none;
    -webkit-flex: none;
    flex: none;
    box-sizing: border-box;
    width: 100%;
}
user agent stylesheet
div {
    display: block;
}
.uk-grid, .tt_event_columns, div.tt_event_theme_page {
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -ms-flex-wrap: wrap;
    -webkit-flex-wrap: wrap;
    flex-wrap: wrap;
    margin: 0;
    padding: 0;
    list-style: none;
}

.uk-grid:before, .uk-grid:after {
    content: "";
    display: block;
    overflow: hidden;
}
.uk-grid:after {
    clear: both;
}
.uk-grid:before, .uk-grid:after {
    content: "";
    display: block;
    overflow: hidden;
}
.uk-grid:before, .uk-grid:after {
    content: "";
    display: block;
    overflow: hidden;
}
.uk-grid:after {
    clear: both;
}
.uk-grid:before, .uk-grid:after {
    content: "";
    display: block;
    overflow: hidden;
}
.uk-panel:before, .uk-panel:after {
    content: "";
    display: table;
}
.uk-panel:after {
    clear: both;
}
.uk-panel:before, .uk-panel:after {
    content: "";
    display: table;
}
::selection {
    background: rgba(236, 72, 59, 0.75);
    color: #ffffff;
    text-shadow: none;
}
.uk-panel:before, .uk-panel:after {
    content: "";
    display: table;
}
.uk-panel:after {
    clear: both;
}
.uk-panel:before, .uk-panel:after {
    content: "";
    display: table;
}
   
.uk-sub-title, .uk-sub-title-small {
    text-transform: uppercase;
    letter-spacing: 3px;
    margin: 0 0 15px 0;
    font-family: 'Varela';
    font-size: 16px;
    font-weight: 400;
    color: #ec8a3b;
}
.uk-margin-large-bottom {
    margin-bottom: 50px !important;
}
h1, h2, h3, h4, h5, h6 {
    letter-spacing: 0;
}
h4, .uk-h4 {
    font-size: 18px;
    line-height: 32px;
}
* + h1, * + h2, * + h3, * + h4, * + h5, * + h6 {
    margin-top: 30px;
}
h1, h2, h3, h4, h5, h6 {
    margin: 0 0 20px 0;
    font-family: 'Dosis';
    font-weight: 600;
    color: #282e3f;
    text-transform: none;
}
h1, h2, h3, h4, h5, h6 {
    text-rendering: auto;
}
h4 {
    font-size: 20px;
}
h1, h2, h3, h4, h5, h6 {
    margin: 15.5px 0;
    font-family: inherit;
    font-weight: normal;
    line-height: 31px;
    color: #282e3f;
    text-rendering: optimizelegibility;
}
h4 {
    display: block;
    margin-block-start: 1.33em;
    margin-block-end: 1.33em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    font-weight: bold;
}
.uk-grid, .tt_event_columns, div.tt_event_theme_page {
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -ms-flex-wrap: wrap;
    -webkit-flex-wrap: wrap;
    flex-wrap: wrap;
    margin: 0;
    padding: 0;
    list-style: none;
}
.uk-panel-box {
    padding: 30px;
    background: #ffffff;
    color: #6b6b6b;
    box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.05);
    border-radius: 4px;
}
.uk-grid, .tt_event_columns, div.tt_event_theme_page {
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -ms-flex-wrap: wrap;
    -webkit-flex-wrap: wrap;
    flex-wrap: wrap;
    margin: 0;
    padding: 0;
    list-style: none;
}    

.tm-thin-font {
    font-weight: 300;
}
h2, .uk-h2 {
    font-size: 32px;
    line-height: 36px;
}
h1, h2, h3, h4, h5, h6 {
    letter-spacing: 0;
}
h3, .uk-h3 {
    font-size: 24px;
    line-height: 32px;
}
h1, h2, h3, h4, h5, h6 {
    margin: 0 0 20px 0;
    font-family: 'Dosis';
    font-weight: 600;
    color: #282e3f;
    text-transform: none;
}
h1, h2, h3, h4, h5, h6 {
    text-rendering: auto;
}
h3 {
    font-size: 28px;
}
h1, h2, h3 {
    line-height: 62px;
}
h1, h2, h3, h4, h5, h6 {
    margin: 15.5px 0;
    font-family: inherit;
    font-weight: normal;
    line-height: 31px;
    color: #282e3f;
    text-rendering: optimizelegibility;
}
.orgsec {
    margin-left: 5%;
    width: 90%;
    background: #ec8a3b;
    color: #ffffff;
}
h3 {
    display: block;
    font-size: 1.17em;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    font-weight: bold;
}
.dark{
        background-color: rgba(40, 46, 63, 1.75);
        color: #ffffff;
        padding-top: 100px;
        padding-bottom: 100px;
        padding-left: 30px;
        padding-righ: 30px;
}
.dark h1 ,
.dark h2 ,
.dark h3 ,
.dark h4 ,
.dark h5 {
    color: #ffffff;
}
.dark button{
    
    background: #ec8a3b;
    padding: 5px;
    color: #000;
}
</style>
    
    <?php foreach ($pdata as $key => $value) {
    
        }  ?>





 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Preview View
        <small>Realtime View</small>
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
      
       

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
           

    
    
    <div class="row col-lg-12" >
        <h3 class="tm-thin-font title"><?= $value->lp_t1 ?>
            
            
        </h3><button data-src="title" data-toggle="modal" data-target="#exampleModalCenter" class="fa fa-pencil-square-o btn btn-app"> Edit</button>
        <h4 class="uk-sub-title uk-sub-title-small" ><?= $value->lp_st1 ?>
            
             </h4>
        <button data-src="uk-sub-title-small" data-toggle="modal" data-target="#exampleModalCenter"  class="fa fa-pencil-square-o btn btn-app"> Edit</button> 
    </div>    
    
   
    
    
    <div class="col-lg-4" >
        <p class="p1">
        <?= $value->lp_p1 ?>  
        </p>
       
       
       <button data-src="p1" class="fa fa-pencil-square-o btn btn-app" data-toggle="modal" data-target="#exampleModalCenter" > Edit</button>
    </div>
    
    
   
    
    
    <div class="col-lg-4">
        <p class="p2">
           <?= $value->lp_p2 ?>
        </p>
        
    <button data-src="p2" class="fa fa-pencil-square-o btn btn-app" data-toggle="modal" data-target="#exampleModalCenter" > Edit</button>    
        
    </div>
    
    
    
    
    <div class="col-lg-4">
        <p class="p3">
            <img class="uk-margin-top p3img" src="<?= $value->lp_im1 ?>" alt="School Learning" width="300" height="225">
        </p>
        
        <button data-src="p3" class="fa fa-pencil-square-o imgc btn btn-app"  id ='img'  data-toggle="modal" data-target="#exampleModalCenter1" > Edit</button>
        
    </div>
    
    
    
    
    
    





   <section id="tm-bottom-c" class="tm-bottom-c uk-grid uk-grid-match dark" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                    <div class="uk-width-1-1"><div class="uk-panel uk-contrast tm-overlay-secondary">
                            <div class="tm-background-cover uk-cover-background" style="background-image: url(mages/background/bg-image-5.jpg)" data-uk-parallax="{bg: '-250'}">
                                <div class="uk-position-relative uk-container tm-inner-container">

                                    <div class="row " data-uk-grid-margin> 
    
                                        <div class="uk-panel uk-panel-space col-lg-1">
                                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:50}">
                                                <h3 class="uk-margin-top tm-text-large title2"><?= $value->lp_t2 ?></h3>
                                            </div>
                                            <button data-src="title2" data-toggle="modal" data-target="#exampleModalCenter"  class="fa fa-pencil-square-o btn btn-app"> Edit</button> 
                                        </div>

                                        <div class="uk-panel uk-panel-space col-lg-3">
                                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:150}">
                                                <div class="tm-block-icon-container">
                                                    <div class="tm-block-icon uk-icon-grav-vector tm-block-icon-large"></div>
                                                    <h2 class="tm-thin-font uk-margin-small-top st2"><?= $value->lp_st2 ?></h2>
                                                    <button data-src="st2" data-toggle="modal" data-target="#exampleModalCenter"  class="fa fa-pencil-square-o btn btn-app"> Edit</button> 
                                                    <div class="tm-block-content p5">
                                                        
                                                    <?= $value->lp_p3 ?>
                                                    </div>
                                                </div>
                                                <button data-src="p5" data-toggle="modal" data-target="#exampleModalCenter"  class="fa fa-pencil-square-o btn btn-app"> Edit</button>
                                            </div>
                                        </div>

                                        <div class="uk-panel uk-panel-space col-lg-3">
                                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:250}">
                                                <div class="tm-block-icon-container">
                                                    <div class="tm-block-icon uk-icon-grav-display2 tm-block-icon-large"></div>
                                                    <h2 class="tm-thin-font uk-margin-small-top st3"><?= $value->lp_st3 ?></h2>
                                                    <button data-src="st3" data-toggle="modal" data-target="#exampleModalCenter"  class="fa fa-pencil-square-o btn btn-app "> Edit</button>
                                                    <div class="tm-block-content p6">
                                                        
                                                    <?= $value->lp_p4 ?>
                                                    </div>
                                                </div>
                                               <button data-src="p6" data-toggle="modal" data-target="#exampleModalCenter"  class="fa fa-pencil-square-o btn btn-app"> Edit</button>
                                            </div>
                                        </div>

                                        <div class="uk-panel uk-panel-space col-lg-3">
                                            <div data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:350}">
                                                <div class="tm-block-icon-container">
                                                    <div class="tm-block-icon uk-icon-grav-medal tm-block-icon-large"></div>
                                                    <h2 class="tm-thin-font uk-margin-small-top st4"><?= $value->lp_st4 ?></h2>
                                                    <button data-src="st4" data-toggle="modal" data-target="#exampleModalCenter"  class="fa fa-pencil-square-o btn btn-app"> Edit</button>
                                                    <div class="tm-block-content p7">
                                                    <?= $value->lp_p5 ?>    
                                                    </div>
                                                    
                                                </div>
                                                <button data-src="p7" data-toggle="modal" data-target="#exampleModalCenter"  class="fa fa-pencil-square-o btn btn-app"> Edit</button>
                                            </div>
                                        </div>

                                    </div>	</div>
                            </div>
                        </div></div>



                </section>

 
                
            
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    
    
    
    
    
    
    <!-- /.content -->
  </div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Mode</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <textarea class="modal-body-f" data-src="" style=" width: 100%; height: 100%; border: 0 none; ">
              

          </textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary sun" data-dismiss="modal" >Save changes</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="exampleModalCenter1" tabindex="-2" role="dialog" aria-labelledby="exampleModalCenterTitle1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Mode</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body imgsb">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary sun2" data-dismiss="modal" >Save changes</button>
      </div>
    </div>
  </div>
</div>



<script>
    
    //edit
    
  
         
         
         
      $(document).ready(function() {
          //var src = null;
       
       
       $.get( "<?php echo base_url(); ?>Qcms/galaryview2", function( data ) {
        $(".imgsb").html(data);
        });
       
       
       
       
       
       
       
       
       
         $(".fa-pencil-square-o").click(function() {
             
             
             src = $(this).attr("data-src");

              vs = $("."+src).html();
          //   alert( $("."+src).html() );
             $(".modal-body-f").removeAttr("data-src");
             $(".modal-body-f").html("");
             $(".modal-body-f").attr( "data-src", src );
             $(".modal-body-f").val(vs);
         });
                 
         
           $(".sun").click(function() {
                          tos =  $(".modal-body-f").attr( "data-src");
                          datavalue =  $(".modal-body-f").val();
                         
                         
                        $.post( "<?= base_url() ?>Qcms/landpageedit",  { tochange: tos, greay: datavalue } ,function( data ) {
                                $( ".result" ).html( data );
                               // if (data === "Done")
                                 //   {
                                        $("."+tos).html(datavalue);
                                  //  }
                                    
                                    //$("#exampleModalCenter").modal("dismiss");
                             
                                });
        
                                   $(".modal-body-f").removeAttr("data-src");
                                   $(".modal-body-f").html("");       
                               $("#exampleModalCenter").modal("hide");          
                    
                        }
                 );
         
         
         
          $(".sun2").click(function() {
                          tos =  $(".p3img").attr("src");
                          
                         
                         
                        $.post( "<?= base_url() ?>Qcms/landpageedit",  { tochange: "p3", greay: tos } ,function( data ) {
                                $( ".result" ).html( data );
                                //if (data === "Done")
                                    
                                        $("."+tos).html(datavalue);
                                    
                                    
                                    //$("#exampleModalCenter").modal("dismiss");
                             
                                });
        
                                   $(".modal-body-f").removeAttr("data-src");
                                   $(".modal-body-f").html("");       
                               $("#exampleModalCenter").modal("hide");          
                    
                        }
                 );
         
         
         
         
      });
    
    
    
</script>    
    