
<style>
    /* Let's highlight canvas boundaries */
#gjs {
  border: 3px solid #444;
  
}

/* Reset some default styling */
.gjs-cv-canvas {
  top: 0;
  width: 100%;
  height: 100%;
}

.gjs-block {
  width: auto;
  height: auto;
  min-height: auto;
}
.panel__top {
  padding: 0;
  width: 100%;
  display: flex;
  position: initial;
  justify-content: center;
  justify-content: space-between;
}
.panel__basic-actions {
  position: initial;
}
.editor-row {
  display: flex;
  justify-content: flex-start;
  align-items: stretch;
  flex-wrap: nowrap;
  height: 300px;
}

.editor-canvas {
  flex-grow: 1;
}

.panel__right {
  flex-basis: 230px;
  position: relative;
  overflow-y: auto;
}
.panel__switcher {
  position: initial;
}

.recent
{
    
    border: 2px solid #ccc;
    background: #fff;   
}


.recent ul ,
.recent ul li{
    
    list-style: none;
    
}
.recent ul li{
    display: inline;
    margin: 5px;
   

   
}
.recent ul li span{
display: inline-block;
}
.recent ul li span{
    padding: 10px;
   
}
.trils{
    border: 1px solid #b5b5b5;
    background: #f5f5f5;    
}
</style>

 <style>
        .panel {
          width: 90%;
          max-width: 700px;
          border-radius: 3px;
          padding: 30px 20px;
          margin: 150px auto 0px;
          background-color: #d983a6;
          box-shadow: 0px 3px 10px 0px rgba(0,0,0,0.25);
          color:rgba(255,255,255,0.75);
          font: caption;
          font-weight: 100;
        }
        .welcome {
          text-align: center;
          font-weight: 100;
          margin: 0px;
        }
        .logo {
          width: 70px;
          height: 70px;
          vertical-align: middle;
        }
        .logo path {
          pointer-events: none;
          fill: none;
          stroke-linecap: round;
          stroke-width: 7;
          stroke: #fff
        }
        .big-title {
          text-align: center;
          font-size: 3.5rem;
          margin: 15px 0;
        }
        .description {
          text-align: justify;
          font-size: 1rem;
          line-height: 1.5rem;
        }
      </style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Designer studio</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
         
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Files <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#" onclick="opens()" >Open</a></li>
                  <li><a href="#" onclick="save()" >Save</a></li>
                  <li><a href="#"  >Save As</a></li>
                 <li><a href="#"  " > Load From</a></li>
                  <li role="separator" class="divider"></li>
                  <li class="dropdown-header">Create</li>
                 <li><a href="#" onclick="newblanck()" >New blank</a></li>
                 <li><a href="#"  >New From Template</a></li>
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
          
                <input type="hidden"  name="docid" />
                <li style=" padding: 10px; ">Document : <input type="text" style="border: 0px #fff;" value="Untiteld-100" name="docname" class=" " /></li>
              
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    
    
    <div id="recent"  class="row">
        <div class="row recent" >
            <ul id="reviews"  >
           
            </ul>
        </div>
        
        
    </div>

    <section  class="row"  style="position: absolute; margin-top: -1000000px;" >
        
        <ul id="reviews_hide"  >
           
        </ul>
        
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
     
        <!-- Left col -->
       
           
                
    <div id="gjs" style="height:0px; overflow:hidden ; margin-top: -35px !important; margin-left: -13px; margin-right: -25px; "></div>
            
          
            
            
       
        <!-- /.Left col -->
        
           
        
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
          
        <!-- right col -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
      
      
      <script type="text/javascript">
  var editor = grapesjs.init({
      container : '#gjs',
     
     
     
     
      plugins: ['gjs-preset-webpage'],
      pluginsOpts: {
        'gjs-preset-webpage': {
          // options
        }
      }
      
      
      
  });
  
  
  function newblanck(){
      editor.setComponents("");
      editor.setStyle("");
      var d = new Date();
      var postfix =  d.getFullYear()+"-"+ d.getMonth()+"-"+ d.getDate()+"-"+ d.getHours()+"-"+ d.getMinutes()+"-"+ d.getSeconds()+"-"+ d.getMilliseconds();
      $("input[name=docname]").val("untitled-"+postfix );
      
      var docname = $("input[name=docname]").val();
      $.post( "<?= base_url("admin/Pbuilder/add") ?>", { docname : docname } , function( data ) {
            $( "input[name=docid]" ).val( data );
        });
      
      
  }
  function save(){
  var html = editor.getHtml();
  var css = editor.getCss();
  var curruntDocId =$( "input[name=docid]" ).val();
   var docname = $("input[name=docname]").val();      



    $.post( "<?= base_url("admin/Pbuilder/save") ?>", { docid : curruntDocId , html : html ,  css : css , docname : docname } , function( data ) {
           // $( "input[name=docid]" ).val( data );
        });
      


            //editor.setComponents(myvar);
            //editor.setStyle(myvar2);
    
    }
    
    
function opens()
    {
        //show saved temp dialog
        
         $("#reviews_hide").html(" "); 
         $("#reviews").html(" ");
        $("#reviews").show();
        $.post( "<?= base_url("admin/Pbuilder/getall") ?>" , function( data ) {
           // $( "input[name=docid]" ).val( data );
           
                obj = JSON.parse(data);
                
                
                for(var k in obj) {
                         id = "";
                        html = "";
                        name = "";
                 
                       
                           html = obj[k].design_compiled;
                           id = obj[k].design_id;
                           name = obj[k].design_name;
                        
                         appendtmp(id , html ,  name);
                        
                     }
                     
                       for(var k in obj) {
                         id = "";
                        html = "";
                        name = "";
                 
                       
                           html = obj[k].design_compiled;
                           id = obj[k].design_id;
                           name = obj[k].design_name;
                        
                          genreatcanvs(id);
           
                        
                     }
                     
                     
                    
             });
        
        //genreatcanvs();
        
    }
    
    
    function opt(idso){
    
     $("#reviews").hide();
   
     $.post( "<?= base_url("admin/Pbuilder/getthis") ?>", { pid : idso } , function( data ) {
           
           obj = JSON.parse(data);
           
           html = obj[0].design_html;
           css = obj[0].design_css;
           name = obj[0].design_name;
           idd = obj[0].design_id;
           
           
          $( "input[name=docid]" ).val(idd);
         $("input[name=docname]").val(name);     
           
             editor.setComponents(html);
             editor.setStyle(css);
           
        });
    
    
    }
    
    
    function appendtmp(ids , html , name)
    {
          $("#reviews_hide").append("<li id="+ids+" >"+html+"</li>");  
          $("#reviews").append("<li onclick='opt("+ids+")' class='col-lg-2 trils' id='s"+ids+"' >  <span class='row' id='s"+ids+"'>  </span>    <span class='row'>"+name+"</span>  </li>");
    }
    
    
    function genreatcanvs(htmls){
        
        
        
        
       html2canvas(document.getElementById(htmls)).then(function(canvas) {
          
            
                $("span#s"+htmls).append(canvas);
           
              $("#reviews canvas").css({ width: 200 , height: 100 });  
        });
    }
    
</script>

            
    
  