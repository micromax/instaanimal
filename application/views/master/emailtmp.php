<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>qsystic  </title>
<style>
/* -------------------------------------
    GLOBAL
------------------------------------- */
* {
  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
  font-size: 100%;
  line-height: 1.6em;
  margin: 0;
  padding: 0;
}
img {
  max-width: 600px;
  width: auto;
}
body {
  -webkit-font-smoothing: antialiased;
  height: 100%;
  -webkit-text-size-adjust: none;
  width: 100% !important;
}
/* -------------------------------------
    ELEMENTS
------------------------------------- */
a {
  color: #348eda;
}
.btn-primary {
  Margin-bottom: 10px;
  width: auto !important;
}
.btn-primary td {
  background-color: #348eda; 
  border-radius: 25px;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
  font-size: 14px; 
  text-align: center;
  vertical-align: top; 
}
.btn-primary td a {
  background-color: #348eda;
  border: solid 1px #348eda;
  border-radius: 25px;
  border-width: 10px 20px;
  display: inline-block;
  color: #ffffff;
  cursor: pointer;
  font-weight: bold;
  line-height: 2;
  text-decoration: none;
}
.last {
  margin-bottom: 0;
}
.first {
  margin-top: 0;
}
.padding {
  padding: 10px 0;
}
/* -------------------------------------
    BODY
------------------------------------- */
table.body-wrap {
  padding: 20px;
  width: 100%;
}
table.body-wrap .container {
  border: 1px solid #f0f0f0;
}
/* -------------------------------------
    FOOTER
------------------------------------- */
table.footer-wrap {
  clear: both !important;
  width: 100%;  
}
.footer-wrap .container p {
  color: #666666;
  font-size: 12px;
  
}
table.footer-wrap a {
  color: #999999;
}
/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, 
h2, 
h3 {
  color: #111111;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: 200;
  line-height: 1.2em;
  margin: 40px 0 10px;
}
h1 {
  font-size: 36px;
}
h2 {
  font-size: 28px;
}
h3 {
  font-size: 22px;
}
p, 
ul, 
ol {
  font-size: 14px;
  font-weight: normal;
  margin-bottom: 10px;
}
ul li, 
ol li {
  margin-left: 5px;
  list-style-position: inside;
}
/* ---------------------------------------------------
    RESPONSIVENESS
------------------------------------------------------ */
/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
  clear: both !important;
  display: block !important;
  Margin: 0 auto !important;
  max-width: 600px !important;
}
/* Set the padding on the td rather than the div for Outlook compatibility */
.body-wrap .container {
  padding: 20px;
}
/* This should also be a block element, so that it will fill 100% of the .container */
.content {
  display: block;
  margin: 0 auto;
  max-width: 600px;
}
/* Let's make sure tables in the content area are 100% wide */
.content table {
  width: 100%;
}
</style>
</head>

<body bgcolor="#f6f6f6">

<!-- body -->
<table class="body-wrap" bgcolor="#f6f6f6">
  <tr>
    <td></td>
    <td class="container" bgcolor="#FFFFFF">
  <center>
      <img width="180px" src="http://www.qsystic.com/images/bakrassts/logo.png">
  </center>

      <!-- content -->
      <div class="content">
      <table>
        <tr>
          <td>
            <p>Hi there,</p>
            <p>did wonder how to build Chat app.</p>
            <h1>it's good APP idea to build chat APP bout how?</h1>
            <p>chat app is a composition of many component , </p>
            <ul>
                <li><b> the APP that run on device  </b> </li>
                <li><b> Chat balance server   </b> </li>
                <li><b> Chat server nodes    </b> </li>
                
            </ul>
            
            <h2>we provide all of this and more .. we build and support </h2>
            <p>All the information you need is on qsystic.com.</p>
            <!-- button -->
            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
                  <a href="http://www.qsystic.com/products/viewproduct/Android+DEVELOPMENT">Android DEVELOPMENT</a>
                </td>
              </tr>
            </table>
            <!-- /button -->
          
          </td>
        </tr>
        
        <tr>
          <td>
            
            <p>did wonder how to build web site .</p>
            <h1>its important for you startup to have web site talk about your idea's</h1>
            <p>web site is a composition of many component , </p>
            <ul>
                <li><b> domain name like yourname.com  </b> </li>
                <li><b> hosting that host your web application   </b> </li>
                <li><b> the application  it self  (design , CMS , database ,etc )  </b> </li>
                <li><b> email server for you with unlimited email's </b> </li>
                <li><b> DDOS protection for your web server  </b> </li>
                <li><b> RESTfull services built for your needs   </b> </li>
                
                
                <li><b> price starting from 280$ yes it's the lowest price in the world include VPS for hosting </b> </li>
                
            </ul>
            <p>host information(included with our web development plan ) * </p>
            
             <ul>
                <li><b>space 250 GiB </b> </li>
                <li><b>traffic 2000 GiB </b> </li>
                <li><b>1 vCore </b> </li>
                <li><b>Ram 750 mb </b> </li>
                <li><b>Full ssh root access </b> </li>
                <li><b>OS : any linux distribution you need (recommendation is Ubuntu LTS, CentOs 7) </b> </li>
                
            </ul>
            
            <p> * if you need more space more core or Ram's we could provide as you need </p>
            <h2>we provide all of this and more .. we build and support </h2>
            <p>All the information you need is on http://www.qsystic.com/products/viewproduct/WEB+DESIGN.</p>
            <!-- button -->
            <table class="btn-primary" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
                  <a href="http://www.qsystic.com/products/viewproduct/WEB+DESIGN">WEB</a>
                </td>
              </tr>
            </table>
            <!-- /button -->
            <p>Feel free to contact us.</p>
            <p>Thanks, have a lovely day.</p>
            <p><a href="http://twitter.com/qsystic">Follow @qsystic on Twitter</a></p>
          </td>
        </tr>
        
      </table>
      </div>
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /body -->

<!-- footer -->
<table class="footer-wrap">
  <tr>
    <td></td>
    <td class="container">
      
      <!-- content -->
      <div class="content">
        <table>
          <tr>
            <td align="center">
              <p>Don't like these annoying emails? <a href="http://www.qsystic.com/"><unsubscribe>Unsubscribe</unsubscribe></a>.
              </p>
            </td>
          </tr>
        </table>
      </div>
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /footer -->

</body>
</html>