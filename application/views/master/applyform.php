<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Qsystic Startup </title>
<style>

* {
  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
  font-size: 100%;
  line-height: 1.6em;
  margin: 0;
  padding: 0;
}
img {
  max-width: 600px;
  width: auto;
}
body {
  -webkit-font-smoothing: antialiased;
  height: 100%;
  -webkit-text-size-adjust: none;
  width: 100% !important;
}
/* -------------------------------------
    ELEMENTS
------------------------------------- */
a {
  color: #348eda;
}
.btn-primary {
  Margin-bottom: 10px;
  width: auto !important;
}
.btn-primary td {
  background-color: #348eda; 
  border-radius: 25px;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
  font-size: 14px; 
  text-align: center;
  vertical-align: top; 
}
.btn-primary td a {
  background-color: #348eda;
  border: solid 1px #348eda;
  border-radius: 25px;
  border-width: 10px 20px;
  display: inline-block;
  color: #ffffff;
  cursor: pointer;
  font-weight: bold;
  line-height: 2;
  text-decoration: none;
}

.btn-primarys {
  background-color: #348eda;
  border: solid 1px #348eda;
  border-radius: 25px;
  border-width: 10px 20px;
  display: inline-block;
  color: #ffffff;
  cursor: pointer;
  font-weight: bold;
  line-height: 2;
  text-decoration: none;
}

.redboard{
    border: 1px solid red;
}
.last {
  margin-bottom: 0;
}
.first {
  margin-top: 0;
}
.padding {
  padding: 10px 0;
}
/* -------------------------------------
    BODY
------------------------------------- */
table.body-wrap {
  padding: 20px;
  width: 100%;
}
table.body-wrap .container {
  border: 1px solid #f0f0f0;
}
/* -------------------------------------
    FOOTER
------------------------------------- */
table.footer-wrap {
  clear: both !important;
  width: 100%;  
}
.footer-wrap .container p {
  color: #666666;
  font-size: 12px;
  
}
table.footer-wrap a {
  color: #999999;
}
/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, 
h2, 
h3 {
  color: #111111;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: 200;
  line-height: 1.2em;
  margin: 40px 0 10px;
}
h1 {
  font-size: 36px;
}
h2 {
  font-size: 28px;
}
h3 {
  font-size: 22px;
}
p, 
ul, 
ol {
  font-size: 14px;
  font-weight: normal;
  margin-bottom: 10px;
}
ul li, 
ol li {
  margin-left: 5px;
  list-style-position: inside;
}
/* ---------------------------------------------------
    RESPONSIVENESS
------------------------------------------------------ */
/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
  clear: both !important;
  display: block !important;
  Margin: 0 auto !important;
  max-width: 600px !important;
}
/* Set the padding on the td rather than the div for Outlook compatibility */
.body-wrap .container {
  padding: 20px;
}
/* This should also be a block element, so that it will fill 100% of the .container */
.content {
  display: block;
  margin: 0 auto;
  max-width: 600px;
}
/* Let's make sure tables in the content area are 100% wide */
.content table {
  width: 100%;
}
.inputs{
    width: 70%;
    max-width: 70%;
    padding: 5px;
    color: #006666;
    margin-bottom: 20px;
}
.inputs2{
        width: 25%;
        padding: 5px;
        color: #006666;
        margin-bottom: 20px;
}
ul li{
    list-style: none;
}
</style>
</head>

<body bgcolor="#f6f6f6">

<!-- body -->
<table class="body-wrap" bgcolor="#f6f6f6">
  <tr>
    <td></td>
    <td class="container" bgcolor="#FFFFFF">
  <center>
      <img width="180px" src="http://www.qsystic.com/images/bakrassts/logo.png">
  </center>

      <!-- content -->
      <div class="content">
      <table>
        <tr>
          <td>
            <p>Hi there,</p>
            <h1>Qsystic is startup software company if you would like to join our training program apply this application  </h1>
            <h3>Personal Information</h3>
            <hr/>
            <br>
            <form name="apply" id="apply" method="post" >
            <ul>
                <li><b> Full Name  </b> </li>
                <li><input class="inputs" type="text" name="fullname" />  </li>
                <li><b> Email  </b> </li>
                <li><input class="inputs" type="email" name="email" />  </li>
                <li><b> Cell phone  </b> </li>
                <li><input class="inputs" type="text" name="phone" />  </li>
                <li><b> Full Address  </b> </li>
                <li><input class="inputs" type="text" name="address" />  </li>
                <li></li>
                <li>
                    <b> Gender  </b> 
                    <select name="gender" class="inputs2">
                        <option value="male">   Male </option>
                        <option value="female"> female </option>
                    </select>
                    
                    <b> Age  </b> 
                    <select name="age" class="inputs2">
                        <option value="18"> 18 </option>
                        <option value="19"> 19 </option>
                        <option value="20"> 20 </option>
                        <option value="21"> 21 </option>
                        <option value="22"> 22 </option>
                        <option value="23"> 23 </option>
                        <option value="24"> 24 </option>
                        <option value="25"> 25 </option>
                        <option value="26"> 26 </option>
                        <option value="27"> 27 </option>
                        <option value="28"> 28 </option>
                        <option value="29"> 29 </option>
                        <option value="30"> 30 </option>
                        <option value="31"> 31 </option>
                        <option value="32"> 32 </option>
                        <option value="33"> 33 </option>
                        <option value="34"> 34 </option>
                        <option value="35"> 35 </option>
                        
                    </select>
                </li>
                <li>
                    <b> Marital Status  </b> 
                    <select name="marital" class="inputs2">
                        <option value="single">Single </option>
                        <option value="married">Married </option>
                    </select>
                    <b>Military Status </b>
                    <select name="military" class="inputs2" >
                        <option value="Exemption">Exemption</option>
                        <option value="Complete">Complete</option>
                        <option value="Postponed">Postponed</option>
                        <option value="Does not apply ">Does not apply</option>
                    </select>
                    
                </li>
                
               
            </ul>
            <hr/>
            <h3>Educational & work information</h3>
            <br>
            <hr/>
           
            
            <ul>
                 <li><b>Graduate college</b> </li>
                <li><input class="inputs" type="text" name="college" />  </li>
                
                 <li><b>Graduate degree</b> </li>
                <li><input class="inputs" type="text" name="degree" />  </li>
                
                <li><b>current job title </b> </li>
                <li><input class="inputs" type="text" name="job" />  </li>
                
                <li><b>current salary </b> </li>
                <li><input class="inputs" type="text" name="salary" />  </li>
                
                 <li><b>your skills  </b> </li>
                 <li><textarea class="inputs"  name="skills" ></textarea>  </li>
            </ul>
            
            <!-- button -->
            <table  cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
                    <input class="btn-primarys" type="button" value="APPLY" id="feedback"> 
                </td>
              </tr>
            </table>
            <!-- /button -->
            </form>
          </td>
        </tr>
        
       
        
      </table>
      </div>
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /body -->

<!-- footer -->
<table class="footer-wrap">
  <tr>
    <td></td>
    <td class="container">
      
      <!-- content -->
      <div class="content">
        <table>
          <tr>
            <td align="center">
              <p>you are welcome  <a href="http://www.qsystic.com/"><unsubscribe>main site</unsubscribe></a>.
              </p>
            </td>
          </tr>
        </table>
      </div>
      <!-- /content -->
      
    </td>
    <td></td>
  </tr>
</table>
<!-- /footer -->

</body>

<script  type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.11.3.min.js" ></script>


<script type="text/javascript">
   var base_url = '<?php echo base_url().urlfix(); ?>';
   
   $("#feedback").on("click" ,feedback );
   function feedback(){
       
            var fo = $("#apply").serializeArray();
            var bl = 0;
            
          $.each(fo, function(i, field){
                     //alert(field.name + ":" +  + " ");
                     $("*[name="+ field.name+"]").removeClass("redboard");
                     if(field.value == "" || field.value == false || field.value == " ")
                     {
                      bl = 1;
                      $("*[name="+ field.name+"]").addClass("redboard");
                     }
                });
            
       if(bl === 0){
              $.post(base_url+'apply/ajax', $("#apply").serialize() , function(data)
              {
                 $(".content:first").html("<h3>"+data+"</h3>");
              });
            }else{
                
               
            }
           

         }
</script>
</html>