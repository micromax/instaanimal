<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "">
<html>
    <head>
        <link rel="icon" type="image/png" href="<?php echo base_url();?>images/zxfiles/favicon.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="chrome=1 , IE=10" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js"></script>
           <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/960/960.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>css/960/text.css"  />

        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/ui-lightness/jquery-ui-1.8.5.custom.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css3/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css3/bootstrap-theme.min.css" />

      
        <?php if($lid == 2){ ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/main2.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/main12.css" />
        <?php }else{ ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/main1.css" />
        <?php } ?>
        

        <title><?php echo $sw["title"]; ?></title>
    </head>
    <body>
          <!-- head start -->
        <div class="container-full">
          
            <!-- head end -->
          <!-- nav barstart -->
              <div class="row" id="navs">
                    <div class="navcontiner col-md-12">
                            <div class="container ">
                               <div class="row" id="slider">
                                    <div class="col-md-12">
                                      <nav class="">
                                       <a href="<?php echo base_url().urlfix() ?>"> <img width="120px" src="<?php echo base_url(); ?>images/zxfiles/logow.png" />   </a>  
                                          <ul>
                                                    
                                                    <li><a href="<?php echo base_url().urlfix() ?>defaults"><?php echo $mlist["defaults"]; ?></a></li>
                                                        <li><a href="#"><?php echo $mlist["products"]; ?></a>
                                                                <?php echo $catlist ; ?>
                                                        </li>
                                                       
                                                        <li><a href="<?php echo base_url().urlfix() ?>about"><?php echo $mlist["about"]; ?></a></li>
                                                        <li><a href="<?php echo base_url().urlfix() ?>articles"><?php echo $mlist["articles"]; ?></a></li>
                                                        <li><a href="<?php echo base_url().urlfix() ?>contact"><?php echo $mlist["contact"]; ?></a></li>
                                                        
                                                </ul>
                                      
                                                <?php foreach($lang as $l){ ?>
                                                <a href="<?php echo base_url().urlfix() ?>comman/setlang/<?php echo $l->lang_id."/".$this->uri->uri_string(); ?>" type="button" class="btn btn-primary lang"><?php echo $l->lang_name; ?></a>

                                                <?php } ?>
                                      
                                        </nav>

                                    </div>
                               </div>
                            </div>
                    </div>
               </div>
  <!-- nav END -->