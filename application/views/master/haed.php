<!DOCTYPE html">
<html lang="en">
    <head>
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>images/bakrassts/favicon.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/slick/slick.css"/>
    <script  type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.7.2.min.js"></script>



        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/slick/slick-theme.css"/>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/QSDK.js"></script>
        <link href="<?php echo base_url(); ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css3/bootstrap-theme.min.css" />
          <script type="text/javascript" src="<?php echo base_url(); ?>js/classie.js"></script>
    
        <script>
        
        function init() {
        window.addEventListener('scroll', function(e){
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 100,
            header = document.querySelector("header");
            //"smallx"
            
        if (distanceY > shrinkOn) {
            classie.add(header,"smaller");
            
        } else {
            if (classie.has(header,"smaller")) {
                classie.remove(header,"smaller");
                
                    }
                }
            });
        }
        window.onload = init();
        </script>
        <link href="<?php echo base_url(); ?>sb/css/mdb.css" rel="stylesheet">


        <?php if (isset($kw) and $kw != null) {
            ?>
            <meta name="keywords" content="<?php print($kw); ?>" />
            <?php
        } else {
            ?>
            <meta name="keywords" content=" " />
        <?php } ?>
        <?php if (isset($dic) and $dic != null) {
            ?>
            <meta name="description" content="<?php print($dic); ?>" />
            <?php
        } else {
            ?>
            <meta name="description" content=" " />
        <?php } ?>
        <meta property="og:image" content="<?php echo base_url(); ?>images/bakrassts/logo.png" />
        <meta name="twitter:image" content="<?php echo base_url(); ?>images/bakrassts/logo.png" />


<?php if ($lid == 2) { ?>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main2.css" />
        <?php } else { ?>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main.css" />
        <?php } ?>

        <title><?php
        echo $sw["title"];
        if (isset($name) && $name != null) {
            echo " " . $name;
        }
        ?></title>
    </head>
    
    <body class="main">
        <script type="text/javascript" >
            
            var QS = new Qsystic("ssix" , "POST" , "<?php echo base_url(); ?>ssapi/x");
            QS.inil();
            
        </script>
        <!-- head start -->
        <div class="container-full">

            <!-- head end -->
            <!-- nav barstart -->
            <div class="row " >
                
                <div class="navcontiner navtop">
                    
                    <div class="row" id="slider">
                        
                        <header class="row header col-md-12 col-sm-12 col-xs-12">
                        <div class="row strip "></div>     
                            
                            
                            <div class="logo col-md-2 col-sm-2 col-xs-2">
                            <a class="" href="<?php echo base_url() . urlfix() ?>"> <img src="<?php echo base_url(); ?>images/bakrassts/logo.png" />   </a>  
                        </div> 


                        <div class="col-md-10 col-sm-10 col-xs-10">

                            <a class="mp" >
                                <img src="<?php echo base_url() ?>images/mn.png" />
                            </a>
                            



                        </div>
                        
                        </header>
                        



                        <!-- SideNav slide-out button -->
                        <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
                        <!--/. SideNav slide-out button -->

                        <!-- Sidebar navigation -->
                        <ul id="slide-out" class="side-nav fixed default-side-nav light-side-nav">

                            <!-- Logo -->
                            <div class="logo-wrapper waves-light">
                                <a href="#"><img src="<?php echo base_url(); ?>images/bakrassts/logo.png" class="img-fluid flex-center"></a>
                            </div>
                            <!--/. Logo -->

                            <!--Search Form-->
                            <form class="search-form" action="<?php echo base_url(); ?>search" method="GET" role="search">
                                <div class="form-group waves-effect">
                                    <input name="sewo" type="text" class="form-control" placeholder="<?php if(isset($SEARCH )) { echo $SEARCH  ; }  ?>">
                                </div>
                            </form>
                            <!--/.Search Form-->

                            <!-- Side navigation links -->
                            <ul class="collapsible collapsible-accordion">
                                
                                <li><a class="waves-effect" href="<?php echo base_url() . urlfix() ?>defaults"><?php if(isset($HOME )) { echo $HOME  ; }  ?></a></li>
                               
                                
                               <li><a class="waves-effect" href="<?php echo base_url() . urlfix() ?>about"><?php if(isset($ABOUTUS )) { echo $ABOUTUS  ; }  ?></a></li>
                                <li><a class="waves-effect" href="<?php echo base_url() . urlfix() ?>articles"><?php if(isset($articles )) { echo $articles  ; }  ?></a></li>
                                
                                
                               <li><a class="collapsible-header waves-effect"><?php echo $sw["lang"]; ?></a>
                                    <div class="collapsible-body">
                                        
                                        <ul>                    <?php
                                            if ($lang != null) {
                                                foreach ($lang as $l) {
                                                        ?>
                                        <ul>
                                             <li> <a class="waves-effect" href="<?php echo base_url() . urlfix() ?>comman/setlang/<?php echo $l->lang_id . "/" . $this->uri->uri_string(); ?>" ><?php echo $l->lang_name; ?></a></li>
                                        </ul>
                                            <?php } } ?>
                                    </div>
                                </li>
                                
                                
                            </ul>
                            <!--/. Side navigation links -->
                        </ul>
                        <!--/. Sidebar navigation -->




                    </div>

                </div>
            </div>


            <!-- nav END -->