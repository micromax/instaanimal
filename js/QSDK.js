function Qsystic(ssix , mths , urls){
    this.ssid = ssix;
    this.local = window.location;
    this.ref = document.referrer;
    this.mth = mths;
    this.purl = urls;
    
    this.XMLHttpFactories = [
    function () {return new XMLHttpRequest()},
    function () {return new ActiveXObject("Msxml2.XMLHTTP")},
    function () {return new ActiveXObject("Msxml3.XMLHTTP")},
    function () {return new ActiveXObject("Microsoft.XMLHTTP")}
    ];
    
    
   this.createXMLHTTPObject = function () {
    var xmlhttp = false;
    for (var i=0;i<this.XMLHttpFactories.length;i++) {
        try {
                xmlhttp = this.XMLHttpFactories[i]();
            }
            catch (e) {
                continue;
            }
            break;
        }
        return xmlhttp;
    }
   
    
   this.sendRequest =  function (url,callback,postData) {
    
      var req = this.createXMLHTTPObject();
    if (!req) return;
    var method = 'POST';
    req.open(method,url,true);
    //req.setRequestHeader('User-Agent','XMLHTTP/1.0');
    if (postData)
        req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    
        req.onreadystatechange = function () {
        if (req.readyState != 4) return;
        if (req.status != 200 && req.status != 304) {
         
            return;
        }
            //callback(req);
        }
        if (req.readyState == 4) return;
        
        req.send(postData);
        console.log(req.response);
        console.log(req.responseText);
       
    }
    
    this.inil = function(){
        
         this.sendRequest(this.purl , null ,"local="+this.local+"&"+"ref="+this.ref );
        
        
    };
    
    this.leaving  = function(){
        //req.abort();
        alert("leving");
    }
    
}