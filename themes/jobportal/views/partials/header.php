<?php foreach ($siteconfig as  $sitev) {} ?>    
<!DOCTYPE html>
<html lang="en">
  <head>
 
        <title><?php
            echo $sitename;
            if (isset($name) && $name != null) {
                echo " " . $name;
            }
            ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    

        <?php $ks =  $sitev->website_keywords; if (isset($kw) && $kw != null) { $ks .=$kw; } if (isset($seo) && $seo != null) { $ks .=$seo; } ?>
        <meta name="keywords" content="<?php print($ks); ?>" />

            
        <?php  $d = $sitev->website_description." ";
        if(isset($disks))
        {
            $d .=$disks." ";
        }
        if (isset($dic) and $dic != null) {
            $d .=" ".$dic;
        }
            ?>
       <meta name="description" content="<?php print($d); ?>" />
            
        
        <meta property="twitter:image" content=" <?php echo base_url(); ?>uploads/<?= $sitev->website_defulatimg; ?>">
        <meta property="og:type" content="website">
      
        
        
        
        
        <meta property="og:title" content="<?php 
         
          if (isset($name) && $name != null) {
                echo  $name ." ";
            }  else {
                 echo $sw["title"];
            } ?> ">
            <meta property="og:description" content="<?= word_limiter($d , 10); ?>">
            <meta property="og:image" content="<?php echo base_url(); ?>uploads/<?= $sitev->website_defulatimg; ?>">
            <meta property="og:url" content="<?=  current_url()."?uuid=".$this->input->get("uuid"); ?>">
      
        

        <title><?php
            echo $sw["title"];
            if (isset($name) && $name != null) {
                echo " " . $name;
            }
            ?></title>
        
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet">


    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/open-iconic-bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/animate.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/owl.carousel.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/owl.theme.default.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/magnific-popup.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/aos.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/ionicons.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/bootstrap-datepicker.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/jquery.timepicker.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/flaticon.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/icomoon.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('/themes/'.THEME.'/css/style.css');?>">
    
  </head>
  <body>