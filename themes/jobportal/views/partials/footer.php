  <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
        	<div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">About</h2>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
       
                                       <?php foreach ($slinks as $key => $value) { ?>
                           
                            
                            
                <li>  <a href="<?= $value->social_link; ?>" class="ftco-animate " target="_blank"> <span class="<?= $value->social_imgepath; ?>"></span</a> </li>
                            
                            
                            <?php            }  ?>
              </ul>
            </div>
          </div>
            
            
            

   <?= $footer; ?>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Have a Questions?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p>
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved  >
</p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<?php echo base_url('/themes/'.THEME.'/js/jquery.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/jquery-migrate-3.0.1.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/popper.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/bootstrap.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/jquery.easing.1.3.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/jquery.waypoints.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/jquery.stellar.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/owl.carousel.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/jquery.magnific-popup.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/aos.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/jquery.animateNumber.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/bootstrap-datepicker.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/jquery.timepicker.min.js');?>"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/scrollax.min.js');?>"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>js/QSDK.js"></script>
  <script src="<?php echo base_url('/themes/'.THEME.'/js/main.js');?>"></script>
  
  <script>
      
                  $(document).ready(function () {
                
                         var QS = new Qsystic("ssix", "POST", "<?php echo base_url(); ?>ssapi/x");
                                    QS.inil();

                           
                                });
      </script>
  
  
  
  </body>
</html>