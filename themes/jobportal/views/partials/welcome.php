
<?php foreach ($sbx as $key => $value) { } ?>
<div class="hero-wrap js-fullheight" style="background-image: url('<?= base_url(); ?>/uploads/<?php echo $value->searchbox_bg ; ?>');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-xl-10 ftco-animate mb-5 pb-5" data-scrollax=" properties: { translateY: '70%' }">
          	<p class="mb-4 mt-5 pt-5" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">We have <span class="number" data-number="850000">0</span> great pet!</p>
            <h1 class="mb-5" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><?php echo $value->searchbox_title ; ?><br><span><?php echo $value->searchbox_p1_t ; ?></span></h1>

						<div class="ftco-search">
							<div class="row">
		            <div class="col-md-12 nav-link-wrap">
			            <div class="nav nav-pills text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
			              <a class="nav-link active mr-md-1" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">Find your pet</a>

			          

			            </div>
			          </div>
			          <div class="col-md-12 tab-wrap">
			            
			            <div class="tab-content p-4" id="v-pills-tabContent">

			              <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-nextgen-tab">
			                                      <form action="explore" method="GET" class="search-job">
			              		<div class="row">
			              			<div class="col-md">
			              				<div class="form-group">
				              				<div class="form-field">
				              					<div class="icon"><span class="icon-briefcase"></span></div>
                                                                                <input type="text" name="sewo" class="form-control"  <?php if($this->input->get("sewo") != NULL) {   ?> value="<?= $this->input->get("sewo") ?>"  <?php } ?> placeholder="eg. Dog. Cat">
								              </div>
							              </div>
			              			</div>
			              			<div class="col-md">
			              				<div class="form-group">
			              					<div class="form-field">
				              					<div class="select-wrap">
						                      <div class="icon"><span class="ion-ios-arrow-down"></span></div>
						                      <select name="ca" id="" class="form-control">
	                                           <option value=""><?= $Category ?></option>
<?php
if ($category != FALSE) {
    foreach ($category as $key => $value) {
        
        ?>
                                                               <?php if ($this->input->get("ca") != NULL && $this->input->get("ca") == $value->category_id) { 
                                                                   
                                                                   ?>

                                                                           <option value="<?php echo $value->category_id;  ?>" selected><?php 
                                                                           if($lid == 2)
                                                                           {
                                                                               echo $value->category_name;
                                                                           }  else {
                                                                               echo $value->category_name;
                                                                           }
                                                                            ?>
                                                                           
                                                                           </option>

<?php                                                               }else { ?>
                                                            
                                                                    <option value="<?php echo $value->category_id; ?>"><?php  if($lid == 2)
                                                                           {
                                                                               echo $value->category_name;
                                                                           }  else {
                                                                               echo $value->category_name;
                                                                           } ?></option>

                                                               <?php } ?>
                                                            
                                                                <?php } 
                                                            } ?>
						                      </select>
						                    </div>
								              </div>
							              </div>
			              			</div>
	
			              			<div class="col-md">
			              				<div class="form-group">
			              					<div class="form-field">
								                <input type="submit" value="Search" class="form-control btn btn-primary">
								              </div>
							              </div>
			              			</div>
			              		</div>
			              	</form>
			              </div>

			              <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-performance-tab">
			              	<form action="#" class="search-job">
			              		<div class="row">
			              			<div class="col-md">
			              				<div class="form-group">
				              				<div class="form-field">
				              					<div class="icon"><span class="icon-user"></span></div>
								                <input type="text" class="form-control" placeholder="eg. Adam Scott">
								              </div>
							              </div>
			              			</div>
			              			<div class="col-md">
			              				<div class="form-group">
			              					<div class="form-field">
				              					<div class="select-wrap">
						                      <div class="icon"><span class="ion-ios-arrow-down"></span></div>
						                      <select name="" id="" class="form-control">
						                      	<option value="">Category</option>
						                      	<option value="">Full Time</option>
						                        <option value="">Part Time</option>
						                        <option value="">Freelance</option>
						                        <option value="">Internship</option>
						                        <option value="">Temporary</option>
						                      </select>
						                    </div>
								              </div>
							              </div>
			              			</div>
			              			<div class="col-md">
			              				<div class="form-group">
			              					<div class="form-field">
				              					<div class="icon"><span class="icon-map-marker"></span></div>
								                <input type="text" class="form-control" placeholder="Location">
								              </div>
							              </div>
			              			</div>
			              			<div class="col-md">
			              				<div class="form-group">
			              					<div class="form-field">
								                <input type="submit" value="Search" class="form-control btn btn-primary">
								              </div>
							              </div>
			              			</div>
			              		</div>
			              	</form>
			              </div>
			            </div>
			          </div>
			        </div>
		        </div>
          </div>
        </div>
      </div>
    </div>